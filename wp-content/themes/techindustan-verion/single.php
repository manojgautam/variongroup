<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 
			$theevent_theme_option = get_option('theevent_admin_option', array());
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			if( empty($theevent_post_option['sidebar']) || $theevent_post_option['sidebar'] == 'default-sidebar' ){
				$theevent_sidebar = array(
					'type'=>$theevent_theme_option['post-sidebar-template'],
					'left-sidebar'=>$theevent_theme_option['post-sidebar-left'], 
					'right-sidebar'=>$theevent_theme_option['post-sidebar-right']
				); 
			}else{
				$theevent_sidebar = array(
					'type'=>$theevent_post_option['sidebar'],
					'left-sidebar'=>$theevent_post_option['left-sidebar'], 
					'right-sidebar'=>$theevent_post_option['right-sidebar']
				); 				
			}
			
			
			$theevent_theme_option['single-post-author'] = 'enable';
			$thumbnail_size = (empty($theevent_post_settings['thumbnail-size']))? $theevent_theme_option['theevent-post-thumbnail-size']: $theevent_post_settings['thumbnail-size'];
			$theevent_sidebar = theevent_get_sidebar_class($theevent_sidebar);			
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="kode-left-sidebar-area <?php echo esc_attr($theevent_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="kode-content-area <?php echo esc_attr($theevent_sidebar['center'])?>">
				<div class="kode-item kode-blog-full kode-single-detail">
				<?php while ( have_posts() ){ the_post();global $post; ?>
					<div class="kf_blog_detail">
                        <figure>
                            <?php 
								$theevent_get_image = theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
								if(!empty($theevent_get_image)){
									//Enable /Disbale Feature Image
									if($theevent_theme_option['single-post-feature-image'] == 'enable'){
										get_template_part('single/thumbnail', get_post_format());
									}?>	
									<figcaption class="blog_img_year">
										<a href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_date('d M')?></a>
									</figcaption>
							<?php }?>
                        </figure>
                        <div class="kf_blog_detail_des">
                        	<h5><?php echo esc_attr(get_the_title());?></h5>
                            <ul class="kode-list-style">
                            	<?php echo theevent_get_blog_info(array('comment'), false, '','li');?>
								<?php echo theevent_get_blog_info(array('category'), false, '','li');?>
								<?php echo theevent_get_blog_info(array('tags'), false, '','li');?>
                            </ul>
                           <?php the_content();?>
                        </div>
                    </div>
					<?php
					//Post Author Enable / Disable
					if($theevent_theme_option['single-post-author'] == 'enable'){ ?>
					<div class="kode-admin-post">
						<figure><?php echo get_avatar(get_the_author_meta('ID'), 90); ?></figure>
						<div class="admin-info">							
							<h2><?php the_author_posts_link(); ?></h2>
							<?php if(get_the_author_meta('description') <> ''){ ?>
								<p><?php echo esc_attr(get_the_author_meta('description')); ?></p>
							<?php }?>
						</div>
					</div>
					<?php } 
					//Recommended Posts Start
					if(isset($theevent_theme_option['single-recommended-post']) && $theevent_theme_option['single-recommended-post'] == 'enable'){ ?>
						<!--Recommended For You Wrap Start-->
						<?php theevent_related_posts($post->ID);?>
						<!--Recommended For You Wrap End-->
					<?php
					} //Recommended Posts Start Condition Ends
					//--Next and Previous Wrap Start-->
					if($theevent_theme_option['single-next-pre'] == 'enable'){ ?>
					<div class="kf_pagination single-next-pre-kode">
						<ul class="pagination">
							<li class="pull-left">
								<?php previous_post_link('<div class="kode-next thcolorhover next-nav inner-post">%link</div>', ' <i class="fa fa-long-arrow-left"></i> Previous <h6>%title</h6>', true); ?>
							</li>
							<li class="pull-right">
								<?php next_post_link('<div class="kode-next thcolorhover next-nav inner-post">%link</div>', 'Next <i class="fa fa-long-arrow-right"></i> <h6>%title</h6>', true); ?>
							</li>
						</ul>
					</div>
					<?php } ?>
					<!--Next and Previous Wrap End-->					
					
					<!-- Blog Detail -->
					<?php comments_template( '', true ); ?>
				<?php } ?>
				</div>
				<div class="clear clearfix"></div>
			</div>
			<?php			
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'right-sidebar' && $theevent_sidebar['right'] != ''){ ?>
				<div class="kode-right-sidebar-area <?php echo esc_attr($theevent_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } 			
			?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>