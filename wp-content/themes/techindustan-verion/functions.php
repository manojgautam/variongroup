<?php
	/*	
	*	Kodeforest Function File
	*	---------------------------------------------------------------------
	*	This file include all of important function and features of the theme
	*	---------------------------------------------------------------------
	*/
	
	define('THEEVENT_AJAX_URL', admin_url('admin-ajax.php'));
	define('THEEVENT_PATH', get_template_directory_uri());
	define('THEEVENT_LOCAL_PATH', get_template_directory());
	
	//WP Customizer
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/wp_customizer.php');
	
	//Responsive Menu
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kf_responsive_menu.php');
	
	// Framework
	include_once(THEEVENT_LOCAL_PATH . '/framework/kf_framework.php' );
	include_once(THEEVENT_LOCAL_PATH . '/framework/script-handler.php' );
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kode_header.php' );
	include_once(THEEVENT_LOCAL_PATH . '/framework/external/import_export/kodeforest-importer.php' );
	
	//Custom Widgets
	//include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/recent-comment.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/recent-post-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/contact-us-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/flickr-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/contact-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/recent-team-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/recent-testi-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/recent-event-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/connect-widget.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/custom_widgets/question-widget.php');
	
	
	// plugin support	
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/tgm_library/kode-activation.php');

	$theevent_theme_option = get_option('theevent_admin_option', array());
	//Load Fonts
	if( empty($theevent_theme_option['upload-font']) ){ $theevent_theme_option['upload-font'] = ''; }
	$theevent_font_controller = new theevent_font_loader( json_decode($theevent_theme_option['upload-font'], true) );	
	
	add_filter( 'body_class', 'theevent_class_names' );
	function theevent_class_names( $classes ) {
		
		$theevent_theme_option = get_option('theevent_admin_option', array());
		$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
		if( !empty($theevent_post_option) ){
			$theevent_post_option = json_decode( $theevent_post_option, true );					
		}
		
		//Mega Menu Class
		$theevent_mega_menu = get_option('mega_main_menu_options');
		$theevent_menu_class = '';		
		if(is_array($theevent_mega_menu)){
			if(in_array('main_menu',$theevent_mega_menu['mega_menu_locations'])){
				$theevent_menu_class = 'theevent_mega_menu';
			}
		}
		//Single Page Class Generated For Home
		
		$theevent_onepage = '';
		if(is_front_page()){
			if(isset($theevent_theme_option['enable-one-page-header-navi'])){
				if($theevent_theme_option['enable-one-page-header-navi'] == 'enable'){
					$theevent_onepage = 'nav_one_page';
				}
			}
		}
		//Header Sticky Class
		$theevent_header_class = '';
		$theevent_header_sticky = '';
		if(isset($theevent_theme_option['enable-sticky-menu'])){
			if($theevent_theme_option['enable-sticky-menu'] == 'enable'){
				$theevent_header_sticky = 'header-sticky';
			}
		}
		
		$theevent_sidebar_sticky = '';
		if(isset($theevent_theme_option['enable-sticky-sidebar'])){
			if($theevent_theme_option['enable-sticky-sidebar'] == 'enable'){
				$theevent_sidebar_sticky = 'sticky-sidebar-enable';
			}
		}
		
		$theevent_header_class = $theevent_header_sticky.' '.$theevent_sidebar_sticky.' '.$theevent_menu_class .' '.$theevent_onepage;
		
		// add 'class-name' to the $classes array
		$classes[] = $theevent_header_class;
		// return the $classes array
		return $classes;
	}
	
	//Title Hook
	function theevent_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $title;
		}

		// Add the site name.
		$title .= get_bloginfo( 'name' );

		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title = "$title $sep $site_description";
		}

		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 ) {
			$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'the-event' ), max( $paged, $page ) );
		}

		return $title;
	}
	//add_filter( 'wp_title', 'theevent_wp_title', 10, 2 );
	
	// a comment callback function to create comment list
	if ( !function_exists('theevent_comment_list') ){
		function theevent_comment_list( $comment, $args, $depth ){
			$GLOBALS['comment'] = $comment;
			switch ( $comment->comment_type ){
				case 'pingback' :
				case 'trackback' :
				?>	
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					<p><?php esc_html_e( 'Pingback :', 'the-event' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'the-event' ), '<span class="edit-link">', '</span>' ); ?></p>
				<?php break; ?>

				<?php default : global $post; ?>
				<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
					<div class="thumblist">
						<figure><?php echo get_avatar( $comment, 60 ); ?></figure>
						<div class="text">
							<div class="kode-title-comment-c">
								<?php echo get_comment_author_link(); ?>
								<time datetime="<?php echo esc_attr(get_comment_time('c')); ?>"><?php echo esc_attr(get_comment_date()) . ' ' . esc_html__('at', 'the-event') . ' ' . esc_attr(get_comment_time()); ?></time>
							</div>							
							<?php comment_text(); ?>
							<div class="clear clearfix"></div>
							<div class="kode-edit-reply">
								<?php edit_comment_link( esc_html__( 'Edit', 'the-event' ), '<p class="edit-link">', '</p>' ); ?>
								<?php if( '0' == $comment->comment_approved ){ ?>
									<p class="comment-awaiting-moderation"><?php echo esc_html__( 'Your comment is awaiting moderation.', 'the-event' ); ?></p>
								<?php } ?>
								<?php comment_reply_link( array_merge($args, array('before' => ' ', 'reply_text' => esc_html__('Reply', 'the-event'), 'depth' => $depth, 'max_depth' => $args['max_depth'])) ); ?>
							</div>
						</div>
					</div>
				<?php
				break;
			}
		}
	}	
	