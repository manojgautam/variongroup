<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 
			$theevent_theme_option = get_option('theevent_admin_option', array());
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			if( empty($theevent_post_option['sidebar']) || $theevent_post_option['sidebar'] == 'default-sidebar' ){
				$theevent_sidebar = array(
					'type'=>$theevent_theme_option['archive-sidebar-template'],
					'left-sidebar'=>$theevent_theme_option['archive-sidebar-left'], 
					'right-sidebar'=>$theevent_theme_option['archive-sidebar-right']
				); 
			}else{
				$theevent_sidebar = array(
					'type'=>$theevent_post_option['sidebar'],
					'left-sidebar'=>$theevent_post_option['left-sidebar'], 
					'right-sidebar'=>$theevent_post_option['right-sidebar']
				); 				
			}
			
			$theevent_sidebar = theevent_get_sidebar_class($theevent_sidebar);
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="<?php echo esc_attr($theevent_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="<?php echo esc_attr($theevent_sidebar['center'])?> kode-main-content">
					<?php
						if( !is_tax('work_category') && !is_tax('work_tag') ){		
							// set the excerpt length
							if( !empty($theevent_theme_option['archive-num-excerpt']) ){
								$theevent_excerpt_length = $theevent_theme_option['archive-num-excerpt'];
								add_filter('excerpt_length', 'theevent_set_excerpt_length');
							} 

							
							$theevent_lightbox_id++;
							$theevent_post_settings['title-num-fetch'] = 300;
							$theevent_post_settings['excerpt'] = intval($theevent_theme_option['archive-num-excerpt']);
							$theevent_post_settings['thumbnail-size'] = 'full';							
							$theevent_post_settings['blog-style'] = $theevent_theme_option['archive-blog-style'];					
							echo '<div class="kode-blog-list kode-fullwidth-blog row">';
							if($theevent_theme_option['archive-blog-style'] == 'blog-full'){
								echo theevent_get_blog_full($wp_query);
							}else if($theevent_theme_option['archive-blog-style'] == 'blog-medium'){
								echo '<div class="kode-blog-list kode-mediium-blog margin-bottom">';
								echo theevent_get_blog_medium($wp_query);			
								echo '</div>';
							}else{
								$blog_size = 3;
								echo '<div class="kode-blog-list kode-blog-grid margin-bottom-30">';
								echo theevent_get_blog_grid($wp_query, $blog_size, 'fitRows');
								echo '</div>';	
							}
							echo '<div class="clear"></div>';
							echo '</div>';
							remove_filter('excerpt_length', 'theevent_set_excerpt_length');
							
							$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
							echo theevent_get_pagination($wp_query->max_num_pages, $paged);													
						
						}else{
							// set the excerpt length
							if( !empty($theevent_theme_option['archive-num-excerpt']) ){
								$theevent_excerpt_length = $theevent_theme_option['archive-num-excerpt'];
								add_filter('excerpt_length', 'theevent_set_excerpt_length');
							} 
							
							$theevent_lightbox_id++;
							$settings['num-fetch'] = -1;
							$settings['element-item-class'] = '';
							$settings['tag'] = strtolower(single_cat_title('', false));
							
							$settings['work-padding'] = 'with-padding';
							$settings['element-item-id'] = '';
							$settings['margin-bottom'] = 30;
							$settings['work-filterable'] = 'disable';
							$settings['thumbnail-size'] = 'full';
							$settings['work-size'] = 4;
							
							//$theevent_post_settings['blog-style'] = $theevent_theme_option['archive-blog-style'];	

							echo '<div class="kode-work-column-archive">';
							$settings['work-style'] = 'style-2';
							$settings['pagination'] = 'enable';
							echo theevent_get_work_item($settings);
							echo '<div class="clear"></div>';
							echo '</div>';
															
						
						
						}
					?>
				</div>
			<?php
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'right-sidebar' && $theevent_sidebar['right'] != ''){ ?>
				<div class="<?php echo esc_attr($theevent_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>