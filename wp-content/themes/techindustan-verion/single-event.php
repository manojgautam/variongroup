<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 			
			$theevent_theme_option = get_option('theevent_admin_option', array());
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			if( empty($theevent_post_option['sidebar']) || $theevent_post_option['sidebar'] == 'default-sidebar' ){
				$theevent_sidebar = array(
					'type'=>$theevent_theme_option['post-sidebar-template'],
					'left-sidebar'=>$theevent_theme_option['post-sidebar-left'], 
					'right-sidebar'=>$theevent_theme_option['post-sidebar-right']
				); 
			}else{
				$theevent_sidebar = array(
					'type'=>$theevent_post_option['sidebar'],
					'left-sidebar'=>$theevent_post_option['left-sidebar'], 
					'right-sidebar'=>$theevent_post_option['right-sidebar']
				); 				
			}
			$theevent_sidebar = theevent_get_sidebar_class($theevent_sidebar);
			if(isset($theevent_sidebar['left']) && $theevent_sidebar['left'] <> ''){				
				if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'left-sidebar'){ ?>
					<div class="kode-left-sidebar-area <?php echo esc_attr($theevent_sidebar['left'])?>">
						<?php get_sidebar('left'); ?>
					</div>	
				<?php }
			}
			?>
			<div class="kode-content-area kode-item  <?php echo esc_attr($theevent_sidebar['center'])?>">
			<?php while ( have_posts() ){ the_post(); 
				global $EM_Event,$post;
				$event_year = esc_attr(date('Y',$EM_Event->start));
				$event_month = esc_attr(date('m',$EM_Event->start));
				$event_day = esc_attr(date('d',$EM_Event->start));
				$event_start_time = esc_attr(date("H:i:s", strtotime($EM_Event->start_time)));			
				$location = '';
				if(isset($EM_Event->get_location()->address)){
					$location = esc_attr($EM_Event->get_location()->address);
				}else{
					$location = '';				
				}
				
				$location_town = '';
				if(isset($EM_Event->get_location()->address)){
					$location_town = $EM_Event->get_location()->town;
				}else{
					$location_town = '';				
				}
				$theevent_theme_option['single-event-feature-size'];
				$event_year = date('Y',$EM_Event->start);
				$event_month = date('m',$EM_Event->start);
				$event_day = date('d',$EM_Event->start);
				$event_start_time = date("G,i,s", strtotime($EM_Event->start_time));
				?>
				<div class="kode_event_detail_timer">
				<!--KODE-EVENT-CONTER-SECTION START-->
					<?php if(isset($theevent_theme_option['single-event-feature-image']) && $theevent_theme_option['single-event-feature-image'] == 'enable'){ ?>
					<div class="kode_event_counter_section">
						<figure>
							<?php echo get_the_post_thumbnail( $EM_Event->post_id, $theevent_theme_option['single-event-feature-size']);?>
							<ul data-year="<?php echo esc_attr($event_year)?>" data-month="<?php echo esc_attr($event_month)?>" data-day="<?php echo esc_attr($event_day)?>" data-time="<?php echo esc_attr($event_start_time)?>" class="event_countdown">
								<li>
								<?php if(isset($theevent_theme_option['event-week']) && $theevent_theme_option['event-week'] <> ''){ ?>
									<span class="days"><?php esc_attr_e('00','the-event');?></span>
									<p class="days_ref"><?php esc_attr_e('Week','the-event');?></p>
								</li>
								<?php } ?>
								<li>
								<?php if(isset($theevent_theme_option['event-days']) && $theevent_theme_option['event-days'] <> ''){ ?>
									<span class="hours"><?php esc_attr_e('00','the-event');?></span>
									<p class="hours_ref"><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['event-days'])?></p>
								</li>
								<?php } ?>
								<li>
								<?php if(isset($theevent_theme_option['event-hours']) && $theevent_theme_option['event-hours'] <> ''){ ?>
									<span class="minutes"><?php esc_attr_e('00','the-event');?></span>
									<p class="minutes_ref"><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['event-hours'])?></p>
								</li>
								<?php } ?>
								<li>
								<?php if(isset($theevent_theme_option['event-sec']) && $theevent_theme_option['event-sec'] <> ''){ ?>
									<span class="seconds last"><?php esc_attr_e('00','the-event');?></span>
									<p class="seconds_ref"><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['event-sec'])?></p>
								</li>
								<?php } ?>
							</ul>
						</figure>
						
					</div>
					<!--KODE-EVENT-CONTER-SECTION END-->
					<?php }?>
					<!--KODE_EVENT_CONTER_CAPSTION START-->
					<div class="kode_event_conter_capstion">
						<h2><?php the_title();?></h2>
						<div class="counter-meta">
							<ul class="info-event">
								<?php echo theevent_get_event_info($EM_Event->post_id, array( 'author','category','comment','date') ,false,'' ,'li');?>
							</ul>
						</div>
					</div>
					
					<!--KODE_EVENT_CONTER_CAPSTION END-->
					<div class="other-events">
						<div class="row ">
							<?php if(isset($theevent_theme_option['single-related-events']) && $theevent_theme_option['single-related-events'] == 'enable'){ ?>
								<?php echo theevent_related_events($EM_Event->post_id);?>
							<?php }?>	
							
							<div class="col-md-12">
								<div class="kode-event-place-holder-capstion">
								   <?php $content = str_replace(']]>', ']]&gt;',$EM_Event->post_content); ?>
									<?php echo theevent_content_filter($content); ?>
								</div>
								<div class="booking_form1">
								<?php //if(isset($theevent_theme_option['event-booking-text']) && $theevent_theme_option['event-booking-text'] <> ''){ ?>
									<h3><?php //echo sprintf(__('%s','kf_event'),$theevent_theme_option['event-booking-text'])?></h3>
								<?php //} ?>	
									<?php //theevent_booking_form_event_manager();?>
									<?php 
									$EM_Tickets = $EM_Event->get_tickets();
									?>
								</div>
								<div class="row">
									<div class="kode-slider-speaker1">
										<div class="col-md-12">
										<?php //if(isset($theevent_theme_option['event-speakers-of-event']) && $theevent_theme_option['event-speakers-of-event'] <> ''){ ?>
											<h3><?php //echo sprintf(__('%s','kf_event'),$theevent_theme_option['event-speakers-of-event'])?></h3>
										<?php //} ?>	
										</div>
										<div class="owl-carousel owl-theme" data-slide="3">
											<?php
											$team_speaker_data = get_post_meta($EM_Event->post_id,'_team_speaker_data',true);
											$team_counter = 0;
											foreach($team_speaker_data as $data){
												$team_speaker = explode(',',$data);
												if($team_speaker[1] <> ''){
													if($team_speaker[0] <> 'lunch-break'){
														$theevent_post_option_team = theevent_decode_stopbackslashes(get_post_meta($team_speaker[0], 'post-option', true ));
														if( !empty($theevent_post_option_team) ){
															$theevent_post_option_team = json_decode( $theevent_post_option_team, true );					
														}
														$facebook = $theevent_post_option_team['facebook'];
														$twitter = $theevent_post_option_team['twitter'];
														$youtube = $theevent_post_option_team['youtube'];
														$pinterest = $theevent_post_option_team['pinterest'];
													echo '
													<div class="col-md-12">
														<div class="thumb">
															<figure>
																'.get_the_post_thumbnail($team_speaker[0], $theevent_theme_option['speaker-thumbnail-size']).'
															</figure>
															<div class="speaker-event-content">
																<h3><a href="'.esc_url(get_permalink($team_speaker[0])).'">'.esc_attr($team_speaker[1]).'</a></h3>
																<span>'.esc_attr($theevent_post_option_team['designation']).'</span>
																<strong class="bottom-border"></strong>
																<ul>';
																if(isset($facebook) && $facebook <> ''){
																	echo '<li><a href="'.esc_url($facebook).'" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>';
																}
																if(isset($facebook) && $twitter <> ''){		
																	echo '<li><a href="'.esc_url($twitter).'" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>';
																}
																if(isset($facebook) && $pinterest <> ''){
																	echo '<li><a href="'.esc_url($pinterest).'" data-toggle="tooltip" title="Pintrest"><i class="fa fa-pinterest-p"></i></a></li>';
																}	
																echo '</ul>
															</div>
														</div>
													</div>';
													}
												}	
												$team_counter++;
											}?>
										</div>
									</div>
								</div>
							</div>				
						</div>
					</div>
					
				</div>	
				<?php if(isset($theevent_theme_option['single-event-comments']) && $theevent_theme_option['single-event-comments'] == 'enable'){ ?>
					<!-- Blog Detail -->
					<?php comments_template( '', true ); ?>
				<?php } ?>
			<?php }?>		
			</div>
			<?php			
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'right-sidebar' && $theevent_sidebar['right'] != ''){ ?>
				<div class="kode-right-sidebar-area <?php echo esc_attr($theevent_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>