<?php
/**
 * A template for calling the left sidebar on everypage
 */
if( !function_exists('theevent_sidebar_left') ){
	function theevent_sidebar_left(){ 
	global $theevent_sidebar;
	?>
	<?php if( $theevent_sidebar['type'] == 'left-sidebar' || $theevent_sidebar['type'] == 'both-sidebar' ){ ?>
	<div class="kode-sidebar kode-left-sidebar columns">
		<?php dynamic_sidebar($theevent_sidebar['left-sidebar']); ?>
	</div>
	<?php }
	}
}
theevent_sidebar_left();
?>