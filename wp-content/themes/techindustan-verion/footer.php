	<?php 
	$theevent_theme_option = get_option('theevent_admin_option', array());?>
	</div>
	<?php 	
	if(isset($theevent_theme_option['kode-footer-style']) && $theevent_theme_option['kode-footer-style'] == 'footer-style-2'){ ?>
		<div class="clear" ></div>
		<footer id="footer">
			<div class="event-map">
				<?php 
				if(isset($theevent_theme_option['footer-text-map']) &&  $theevent_theme_option['footer-text-map'] <> ''){
					echo do_shortcode('[map address="'.esc_attr($theevent_theme_option['footer-text-map']).'" type="roadmap"  zoom="14" scrollwheel="no" scale="no" zoom_pancontrol="no"][/map]');
				}else{
					echo do_shortcode('[map address="London, UK" type="roadmap" zoom="14" scrollwheel="no" scale="no" zoom_pancontrol="no"][/map]');
				}
				?>
			</div>
			<div class="kode-footer-text">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 footer-cols">
							<?php dynamic_sidebar('Footer'); ?>
						</div>
					</div>
				</div>
			</div>
			<!--kode-footer-text End-->
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php if($theevent_theme_option['show-copyright'] == 'enable'){ ?>
							<p>©2016 Verion Media Group | All Right Reserved</p>
						<?php }?>
						</div>
					</div>
				</div>
				<div class="back-to-top">
					<?php if($theevent_theme_option['kode-back-top'] == 'enable'){ ?>
					<a href="#" style="opacity: 1;"><i class="fa fa-angle-up"></i></a>
					<?php }?>
				</div>
			</div>
			<!--copyright End-->
		</footer>
		<?php }else if(isset($theevent_theme_option['kode-footer-style']) && $theevent_theme_option['kode-footer-style'] == 'footer-style-3'){ ?>
			<div class="clear" ></div>
			<footer id="footer">				
				<div class="kode-footer-text-bg">
					<div class="container">
						<div class="row">
							<?php dynamic_sidebar('Footer'); ?>							
						</div>
					</div>
				</div>
				<!--kode-footer-text End-->
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php if($theevent_theme_option['show-copyright'] == 'enable'){ ?>
								<p><?php echo wp_kses($theevent_theme_option['kode-copyright-text'],array('a'=>array(),'strong'=>array()));?></p>
							<?php }?>
							</div>
						</div>
					</div>
					<div class="back-to-top">
						<?php if($theevent_theme_option['kode-back-top'] == 'enable'){ ?>
						<a href="#" style="opacity: 1;"><i class="fa fa-angle-up"></i></a>
						<?php }?>
					</div>
				</div>
				<!--copyright End-->
			</footer>
		<?php }else{  ?>
			<footer class="footer_2">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar('Footer'); ?>	
					</div>
				</div>
			</footer>
			<div class="kf_event_copyright">
				<div class="container">
					<div class="kf_event_copyright_text">
						<?php if($theevent_theme_option['show-copyright'] == 'enable'){ ?>
							<p><?php echo wp_kses($theevent_theme_option['kode-copyright-text'],array('a'=>array(),'strong'=>array()));?></p>
						<?php }?>
					</div>
					<div class="kf_event_footer_nav">
						<?php wp_nav_menu( array('theme_location'=>'footer_menu') );?>						
					</div>
				</div>
			</div>
		<?php }?>	
</div> <!-- body-wrapper -->
<?php wp_footer(); ?>
</body>
</html>