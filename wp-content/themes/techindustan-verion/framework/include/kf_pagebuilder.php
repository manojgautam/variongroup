<?php
	/*	
	*	Kodeforest Pagebuilder File
	*	---------------------------------------------------------------------
	*	This file contains the page builder settings
	*	---------------------------------------------------------------------
	*/	
	
	// create the page option
	add_action('init', 'theevent_create_page_options');
	if( !function_exists('theevent_create_page_options') ){
	
		function theevent_create_page_options(){
			global $theevent_theme_option;
			if(!isset($theevent_theme_option['sidebar-element'])){$theevent_theme_option['sidebar-element'] = array('blog','contact');}
			new theevent_page_options( 
					  
				// page option settings
				array(
					'page-layout' => array(
						'title' => esc_html__('Seo Page Option', 'the-event'),
						'options' => array(
							'post-seo-meta-title' => array(
								'title' => 'SEO Page Title',
								'type' => 'text',	
								'wrapper-class' => 'page-seo-meta-button-wrapper enable-wrapper',
								'description' => 'Please add page meta title.'
							),
							'post-seo-meta-description' => array(
								'title' => 'SEO Page Description',
								'type' => 'textarea',
								'wrapper-class' => 'page-seo-meta-button-wrapper enable-wrapper',
								'description' => 'Please add page meta descriotion here.'
							),
							'post-seo-meta-keyword' => array(
								'title' => 'SEO Page Keyword',
								'type' => 'textarea',
								'wrapper-class' => 'page-seo-meta-button-wrapper enable-wrapper',
								'description' => 'Please add page keyword quoma seperated for example: food, charity, news'
							),	
						)
					),
					
					'page-option' => array(
						'title' => esc_html__('Page Option', 'the-event'),
						'options' => array(
							'show-sub' => array(
								'title' => esc_html__('Show Sub Header' , 'the-event'),
								'type' => 'checkbox',
								'default' => 'enable',
								'wrapper-class' => 'four columns kode-btn-icons',
								'description'=> esc_html__('Click here to Turn On / Off the Sub Header from here.', 'the-event')
							),	
							'page-caption' => array(
								'title' => esc_html__('Page Caption' , 'the-event'),
								'type' => 'textarea',
								'wrapper-class' => 'four columns kode-txt-area',
								'description'=> esc_html__('Enter the page Caption here that you want to show.', 'the-event')
							),								
							'header-background' => array(
								'title' => esc_html__('Header Background Image' , 'the-event'),
								'button' => esc_html__('Upload', 'the-event'),
								'type' => 'upload',
								'wrapper-class' => 'four columns kode-bg-sec',
								'description'=> esc_html__('Please Upload the Header Background Image from here.', 'the-event')
							),
							// 'enable-header-top' => array(
								// 'title' => esc_html__('Enable/Disable Header On Page', 'the-event'),
								// 'type' => 'checkbox',
								// 'default'=>'enable',
								// 'wrapper-class' => 'three-fifth columns kode-bg-sec',
								// 'description'=> esc_html__('Disabling header will disable the top header by default so that it can be controled by pagebuilder "Header Element".', 'the-event')											
							// ),
							// 'kode-header-style' => array(
								// 'title' => esc_html__('Header Style', 'the-event'),
								// 'type' => 'combobox',
								// 'options' => array(
									// 'header-style-1' => esc_html__('Header Style 1', 'the-event'),
									// 'header-style-2' => esc_html__('Header Style 2', 'the-event'),
									// 'header-style-3' => esc_html__('Header Style 3', 'the-event'),
									// 'header-style-4' => esc_html__('Header Style 4', 'the-event'),
									// 'header-style-5' => esc_html__('Header Style 5', 'the-event'),
									// 'header-style-6' => esc_html__('Header Style 6', 'the-event'),
								// ),
								// 'wrapper-class' => 'two-fifth columns kode-bg-sec',
								// 'default'=>'header-style-1'
							// ),								
						)
					),

				),
				// page option attribute
				array(
					'post_type' => array('page'),
					'meta_title' => esc_html__('Page Option', 'the-event'),
					'meta_slug' => 'kodeforest-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);
			
		}
	}
	
	// create the page builder
	if( is_admin() ){
		add_action('init', 'theevent_create_page_builder_option');
	}
	if( !function_exists('theevent_create_page_builder_option') ){	
		function theevent_create_page_builder_option(){
			global $theevent_theme_option;
			new theevent_page_builder( 
				
				// page builder option setting
				apply_filters('theevent_page_builder_option',
					array(
						'content-item' => array(
							'title' => esc_html__('Content & Post Options', 'the-event'),
							'blank_option' => esc_html__('- Select Content Element -', 'the-event'),
							'options' => array(
								
								'full-size-wrapper' => array(
									'title'=> esc_html__('Section', 'the-event'), 
									'type'=>'wrapper',
									'icon'=>'fa-circle-o-notch',
									'options'=>array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'type' => array(
											'title' => esc_html__('Type', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'image'=> esc_html__('Background Image', 'the-event'),
												'pattern'=> esc_html__('Predefined Pattern', 'the-event'),
												'video'=> esc_html__('Video Background', 'the-event'),
												'color'=> esc_html__('Color Background', 'the-event'),
												'map'=> esc_html__('Google Map', 'the-event'),
											),
											'default'=>'color'
										),	
										
										'container' => array(
											'title' => esc_html__('Container', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'full-width'=> esc_html__('Full Width', 'the-event'),
												'container-width'=> esc_html__('Container (inside 1170px)', 'the-event'),
											),
											'description' => esc_html__('Select container width, container full width section will be full width.', 'the-event'),
											'default'=>'container-width'
										),
										'map_shortcode' => array(
											'title' => esc_html__('Google Map Shortcode', 'the-event'),
											'type' => 'text',
											'default' => '',
											'wrapper-class'=>'type-wrapper map-wrapper',
											'description' => esc_html__('Add google map shortcode to add background.', 'the-event')
										),	
										'video_url' => array(
											'title' => esc_html__('Video URL', 'the-event'),
											'type' => 'text',
											'default' => '',
											'wrapper-class'=>'type-wrapper video-wrapper',
											'description' => esc_html__('add video url for the parallax video background for mp4', 'the-event')
										),	
										'video_type' => array(
											'title' => esc_html__('Video Type', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'mp4'=> esc_html__('Video Type Mp4 ', 'the-event'),
												'ogg'=> esc_html__('Video Type Ogg ', 'the-event'),
												'webm'=> esc_html__('Video Type Webm ', 'the-event'),
											),
											'wrapper-class'=>'type-wrapper video-wrapper',
											'description' => esc_html__('Select video background type.', 'the-event'),
											'default'=>'mp4'
										),	
										'background-hook'=> array(
											'title'=> esc_html__('Background Image/Video on ::Before' ,'the-event'),
											'type'=> 'checkbox',
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper'
										),
										'background-width'=> array(
											'title'=> esc_html__('Background Area Width' ,'the-event'),
											'type' => 'text',
											'wrapper-class' => 'type-wrapper background-hook-wrapper background-hook-enable'
										),
										'background-image-align' => array(
											'title' => esc_html__('Background Image Alignment', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'left'=> esc_html__('Left', 'the-event'),
												'right'=> esc_html__('Right', 'the-event'),
											),
											'wrapper-class' => 'type-wrapper background-hook-wrapper background-hook-enable',
											'description' => esc_html__('Background Image alignment.', 'the-event'),
											'default'=>'left'
										),
										'background-image-attachment' => array(
											'title' => esc_html__('Background Image Attachment', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'fixed'=> esc_html__('Fixed', 'the-event'),
												'scroll'=> esc_html__('Scroll', 'the-event'),
											),
											'wrapper-class' => 'type-wrapper background-hook-wrapper background-hook-enable',
											'description' => esc_html__('Background Image alignment.', 'the-event'),
											'default'=>'left'
										),
										'background' => array(
											'title' => esc_html__('Background Image', 'the-event'),
											'button' => esc_html__('Upload', 'the-event'),
											'type' => 'upload',
											'wrapper-class' => 'type-wrapper image-wrapper'
										),	
										'trans-background'=> array(
											'title'=> esc_html__('Transparent Background' ,'the-event'),
											'type'=> 'checkbox',
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper'
										),
										'horizontal-background'=> array(
											'title'=> esc_html__('Horizontal Moving Background' ,'the-event'),
											'type'=> 'checkbox',
											'wrapper-class' => 'type-wrapper image-wrapper'
										),
										'background-color' => array(
											'title' => esc_html__('Overlay Color', 'the-event'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',
											'wrapper-class'=>'type-wrapper image-wrapper color-wrapper video-wrapper'
										),												
										'opacity' => array(
											'title' => esc_html__('Opacity', 'the-event'),
											'type' => 'text',
											'default' => '0.03',
											'wrapper-class'=>'type-wrapper image-wrapper video-wrapper',
											'description' => esc_html__('add opacity to the background image. for example from .01 to 1', 'the-event')
										),	
										'background-speed' => array(
											'title' => esc_html__('Background Speed', 'the-event'),
											'type' => 'text',
											'default' => '0',
											'wrapper-class' => 'type-wrapper image-wrapper',
											'description' => esc_html__('Fill 0 if you don\'t want the background to scroll and 1 when you want the background to have the same speed as the scroll bar', 'the-event') .
												'<br><br><strong>' . esc_html__('*** only allow the number between -1 to 1', 'the-event') . '</strong>'
										),
										'padding-top' => array(
											'title' => esc_html__('Padding Top', 'the-event'),
											'type' => 'text',
											'description' => esc_html__('Spaces before starting any content in this section', 'the-event')
										),	
										'padding-bottom' => array(
											'title' => esc_html__('Padding Bottom', 'the-event'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of the content in this section', 'the-event')
										),
									)
								),
								'column1-1' => array(
									'title'=> esc_html__('Column', 'the-event'),
									'type'=>'wrapper',
									'icon'=>'fa-columns',
									'size'=>'1/1',
									'options'=>array(	
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),
									)
								),
								'simple-column' => array(
									'title'=> esc_html__('Simple Column', 'the-event'), 
									'icon'=>'fa-sticky-note-o',
									'type'=>'item',
									'options'=>array(	
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),										
										'content'=> array(
											'title'=> esc_html__('Content Text' ,'the-event'),
											'type'=> 'textarea',						
										),
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),	 
									)
								),
								'column-service' => array(
									'title'=> esc_html__('Services', 'the-event'), 
									'icon'=>'fa-gg',
									'type'=>'item',
									'options'=>array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
											
										'title'=> array(
											'title'=> esc_html__('Title' ,'the-event'),
											'type'=> 'text',						
										),											
										'style'=> array(
											'title'=> esc_html__('Item Style' ,'the-event'),
											'type'=> 'styles',
											'wrapper-class'=> 'kode-custom-styles',
											'options'=> array(
												'type-1'=>THEEVENT_PATH . '/framework/include/backend_assets/images/services/services-1.jpg',
												'type-2'=>THEEVENT_PATH . '/framework/include/backend_assets/images/services/services-2.jpg',
												'type-3'=>THEEVENT_PATH . '/framework/include/backend_assets/images/services/services-3.jpg',
												'type-4'=>THEEVENT_PATH . '/framework/include/backend_assets/images/services/services-4.jpg',
												// 'type-5'=>THEEVENT_PATH . '/framework/include/backend_assets/images/services/services-5.jpg',
											)
										),	
										'thumbnail-size' => array(
											'title' => esc_html__('Thumbnail Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'wrapper-class' => 'style-wrapper type-1-wrapper type-4-wrapper',
											'default'=> 'theevent-post-thumbnail-size'
										),
										'icon_type'=> array(
											'title'=> esc_html__('Icon Class' ,'the-event'),
											'type'=> 'text',						
											'wrapper-class' => 'style-wrapper type-1-wrapper type-5-wrapper type-2-wrapper type-3-wrapper type-4-wrapper',
										),	
										'service-image-box' => array(
											'title' => esc_html__('Services Image' , 'the-event'),
											'button' => esc_html__('Upload', 'the-event'),
											'type' => 'upload',
											
										),										
										'content'=> array(
											'title'=> esc_html__('Content Text' ,'the-event'),
											'type'=> 'textarea',
											//'type'=> 'tinymce',						
										),
										'link' => array(
											'title' => esc_html__('Link', 'the-event'),
											'type' => 'text',
											'description' => esc_html__('Please add link here for services.', 'the-event')
										),	
										'link-text' => array(
											'title' => esc_html__('Text Link', 'the-event'),
											'type' => 'text',
											'wrapper-class' => 'style-wrapper type-1-wrapper type-4-wrapper type-5-wrapper',
											'description' => esc_html__('Please add text here for services link.', 'the-event')
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),	 
									)
								),
								'headings' => array(
									'title'=> esc_html__('Fancy Heading', 'the-event'), 
									'icon'=>'fa-header',
									'type'=>'item',
									'options'=>  array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'element-style'=> array(
											'title'=> esc_html__('Heading Style' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'style-1'=>esc_html__('Style 1', 'the-event'), 
												'style-2'=>esc_html__('Style 2', 'the-event'),
												'style-3'=>esc_html__('Style 3', 'the-event'),
												'style-4'=>esc_html__('Style 4', 'the-event'),
											)
										),	
										'title'=> array(
											'title'=> esc_html__('Title' ,'the-event'),
											'type'=> 'text',	
											'default'=> '',
											'description'=> esc_html__('Add heading title here.', 'the-event')
										),	
										'title-color' => array(
											'title' => esc_html__('Title Color', 'the-event'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),	
										'caption'=> array(
											'title'=> esc_html__('Caption' ,'the-event'),
											'type'=> 'text',	
											'default'=> '',
										),
										'caption-color' => array(
											'title' => esc_html__('Caption Color', 'the-event'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),	
										'line-color' => array(
											'title' => esc_html__('Line Color', 'the-event'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),										
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),										
									)
								),
								'sidebar' => array(
									'title'=> esc_html__('Sidebar', 'the-event'), 
									'icon'=>'fa-th',
									'type'=>'item',
									'options'=> 
									array(
										'widget'=> array(
											'title'=> esc_html__('Select Widget Area' ,'the-event'),
											'type'=> 'combobox_sidebar',
											'options'=> $theevent_theme_option['sidebar-element'],
											'description'=> esc_html__('You can select Widget Area of your choice.', 'the-event')
										),	
									)
								),
								'post-slider' => array(
									'title'=> esc_html__('Post Slider', 'the-event'), 
									'icon'=>'fa-codepen',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'category'=> array(
											'title'=> esc_html__('News Category' ,'the-event'),
											'type'=> 'multi-combobox',
											'options'=> theevent_get_term_list('category'),
											'wrapper-class' => 'four columns kode-txt-area kode-news-headline-wrapper kode-news-headline-enable',
											'description'=> esc_html__('Select categories to fetch its posts.', 'the-event')
										),
										'thumbnail-size' => array(
											'title' => esc_html__('Thumbnail Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> 'theevent-post-thumbnail-size'
										),
										'num-excerpt'=> array(
											'title'=> esc_html__('Num Excerpt (Word)' ,'the-event'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post description.', 'the-event')
										),	
										'num-fetch'=> array(
											'title'=> esc_html__('Num Fetch' ,'the-event'),
											'type'=> 'text',	
											'default'=> '8',
											'description'=> esc_html__('Specify the number of posts you want to pull out.', 'the-event')
										),										
										'orderby'=> array(
											'title'=> esc_html__('Order By' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'date' => esc_html__('Publish Date', 'the-event'), 
												'title' => esc_html__('Title', 'the-event'), 
												'rand' => esc_html__('Random', 'the-event'), 
											)
										),
										'order'=> array(
											'title'=> esc_html__('Order' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'desc'=>esc_html__('Descending Order', 'the-event'), 
												'asc'=> esc_html__('Ascending Order', 'the-event'), 
											)
										),											
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),	
									)
								),	
								
								
								'blog' => array(
									'title'=> esc_html__('Blog', 'the-event'), 
									'icon'=>'fa-cube',
									'type'=>'item',
									'options'=>  array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),									
										'category'=> array(
											'title'=> esc_html__('Category' ,'the-event'),
											'type'=> 'multi-combobox',
											'options'=> theevent_get_term_list('category'),
											'description'=> esc_html__('Select categories to fetch its posts.', 'the-event')
										),	
										'tag'=> array(
											'title'=> esc_html__('Tag' ,'the-event'),
											'type'=> 'multi-combobox',
											'options'=> theevent_get_term_list('post_tag'),
											'description'=> esc_html__('Select tags to fetch its posts.', 'the-event')
										),	
										'kode-blog-thumbnail-size' => array(
											'title' => esc_html__('Single Post Thumbnail Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> 'theevent-post-thumbnail-size'
										),
										'blog-style'=> array(
											'title'=> esc_html__('Blog Style' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'blog-grid' => esc_html__('Blog Grid', 'the-event'),
												'blog-medium' => esc_html__('Blog Medium', 'the-event'),
												'blog-modern-grid' => esc_html__('Blog Modern Grid', 'the-event'),
												'blog-full' => esc_html__('Blog Full', 'the-event'),
											),
											'default'=>'blog-full'
										),	
										'blog-size'=> array(
											'title'=> esc_html__('Blog Size' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'2' => esc_html__('2 Column', 'the-event'),
												'3' => esc_html__('3 Column', 'the-event'),
												'4' => esc_html__('4 Column', 'the-event'),
											),
											'wrapper-class' => 'blog-grid-wrapper blog-modern-grid-wrapper blog-medium-wrapper blog-style-wrapper',
											'default'=>'blog-full'
										),	
										'title-num-fetch'=> array(
											'title'=> esc_html__('Num Title (Character)' ,'the-event'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post title.', 'the-event')
										),	
										'num-excerpt'=> array(
											'title'=> esc_html__('Num Excerpt (Word)' ,'the-event'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post description.', 'the-event')
										),	
										'num-fetch'=> array(
											'title'=> esc_html__('Num Fetch' ,'the-event'),
											'type'=> 'text',	
											'default'=> '8',
											'description'=> esc_html__('Specify the number of posts you want to pull out.', 'the-event')
										),										
										'orderby'=> array(
											'title'=> esc_html__('Order By' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'date' => esc_html__('Publish Date', 'the-event'), 
												'title' => esc_html__('Title', 'the-event'), 
												'rand' => esc_html__('Random', 'the-event'), 
											)
										),
										'order'=> array(
											'title'=> esc_html__('Order' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'desc'=>esc_html__('Descending Order', 'the-event'), 
												'asc'=> esc_html__('Ascending Order', 'the-event'), 
											)
										),	
										'pagination'=> array(
											'title'=> esc_html__('Enable Pagination' ,'the-event'),
											'type'=> 'checkbox'
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),										
									)
								),
								'blog-slider' => array(
									'title'=> esc_html__('Blog Slider', 'the-event'), 
									'icon'=>'fa-rocket',
									'type'=>'item',
									'options'=>  array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'element-style'=> array(
											'title'=> esc_html__('Element Style' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'style-1'=>esc_html__('Style 1', 'the-event'), 
												'style-2'=>esc_html__('Style 2', 'the-event'), 
												'style-3'=>esc_html__('Style 3', 'the-event'), 
											)
										),	
										'category'=> array(
											'title'=> esc_html__('Category' ,'the-event'),
											'type'=> 'multi-combobox',
											'options'=> theevent_get_term_list('category'),
											'description'=> esc_html__('Select categories to fetch its posts.', 'the-event')
										),	
										'element-type'=> array(
											'title'=> esc_html__('Element Type' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'slider'=>esc_html__('Slider', 'the-event'), 
												'simple-post'=> esc_html__('Simple Post', 'the-event'), 
											)
										),	
										'thumbnail-size' => array(
											'title' => esc_html__('Thumbnail Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> 'thumbnail-size'
										),
										'title-num-fetch'=> array(
											'title'=> esc_html__('Num Title (Character)' ,'the-event'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post title.', 'the-event')
										),	
										'num-excerpt'=> array(
											'title'=> esc_html__('Num Excerpt (Word)' ,'the-event'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post description.', 'the-event')
										),	
										'num-fetch'=> array(
											'title'=> esc_html__('Num Fetch' ,'the-event'),
											'type'=> 'text',	
											'default'=> '8',
											'description'=> esc_html__('Specify the number of posts you want to pull out.', 'the-event')
										),										
										'orderby'=> array(
											'title'=> esc_html__('Order By' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'date' => esc_html__('Publish Date', 'the-event'), 
												'title' => esc_html__('Title', 'the-event'), 
												'rand' => esc_html__('Random', 'the-event'), 
											)
										),
										'order'=> array(
											'title'=> esc_html__('Order' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'desc'=>esc_html__('Descending Order', 'the-event'), 
												'asc'=> esc_html__('Ascending Order', 'the-event'), 
											)
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),										
									)
								),
								'accordion' => array(
									'title'=> esc_html__('Accordion', 'the-event'), 
									'icon'=>'fa-server',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'accordion'=> array(
											'type'=> 'tab',
											'default-title'=> esc_html__('Accordion' ,'the-event')											
										),
										'initial-state'=> array(
											'title'=> esc_html__('On Load Open', 'the-event'),
											'type'=> 'text',
											'default'=> 1,
											'description'=> esc_html__('0 will close all tab as an initial state, 1 will open the first tab and so on.', 'the-event')						
										),												
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),
									)
								),
								'booking-form' => array(
									'title'=> esc_html__('Booking Form', 'the-event'), 
									'icon'=>'fa-envelope-o',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'booking-title' => array(
											'title' => esc_html__('Booking Title' , 'the-event'),
											'type' => 'text',
											'default' => 'Book Event',
											'description'=> esc_html__('Please enter the Booking Form Title here.', 'the-event')
										),	
										'booking-price' => array(
											'title' => esc_html__('Booking Price' , 'the-event'),
											'type' => 'text',
											'default' => '59',
											'description'=> esc_html__('Please enter the Booking Form Price.', 'the-event')
										),
										'booking-name' => array(
											'title' => esc_html__('Booking Name' , 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('You can enable / disable the booking person first name fields from here.', 'the-event')
										),	
										'booking-last-name'=> array(
											'title'=> esc_html__('Booking last Name' ,'the-event'),
											'type'=> 'checkbox',
											'description'=> esc_html__('You can enable / disable the booking person last name fields from here.', 'the-event')	
										),
										'booking-email' => array(
											'title' => esc_html__('Booking Email', 'the-event'),
											'type' => 'checkbox',
											'description' => esc_html__('You can enable / disable the booking email adress fields from here.', 'the-event')
										),	
										'booking-phone' => array(
											'title' => esc_html__('Booking Phone', 'the-event'),
											'type' => 'checkbox',
											'description' => esc_html__('You can enable / disable the booking phone number fields from here.', 'the-event')
										),
										'booking-address' => array(
											'title' => esc_html__('Booking Address', 'the-event'),
											'type' => 'checkbox',
											'description' => esc_html__('You can enable / disable the booking address fields from here.', 'the-event')
										),	
										'booking-note' => array(
											'title' => esc_html__('Booking Note', 'the-event'),
											'type' => 'checkbox',
											'description' => esc_html__('You can enable / disable the booking additional note fields from here.', 'the-event')
										),	
										'booking-by-email' => array(
											'title' => esc_html__('Booking By Email', 'the-event'),
											'type' => 'text',
											'default' => 'Book By Email We will contact you back.',
											'description' => esc_html__('Please enter the text you want to have.', 'the-event')
										),	
										'booking-by-email-btn' => array(
											'title' => esc_html__('Booking By Email', 'the-event'),
											'type' => 'checkbox',
											'description' => esc_html__('You can switch On / Off The booking via email from here.', 'the-event')
										),
										'booking-by-paypal' => array(
											'title' => esc_html__('Checkout via Paypal', 'the-event'),
											'type' => 'text',
											'default' => 'Checkout via Paypal',
											'description' => esc_html__('Please enter the text you want to have.', 'the-event')
										),
									)
								),	
								
								
								// 'contact-detail' => array(
									// 'title'=> esc_html__('Contact Detail', 'the-event'), 
									// 'icon'=>'fa-envelope-o',
									// 'type'=>'item',
									// 'options'=>array(
										// 'element-item-id' => array(
											// 'title' => esc_html__('Page Item ID', 'the-event'),
											// 'type' => 'text',
											// 'default' => '',
											// 'description' => esc_html__('please add the page item id.', 'the-event')
										// ),
										// 'element-item-class' => array(
											// 'title' => esc_html__('Page Item Class', 'the-event'),
											// 'type' => 'text',
											// 'default' => '',
											// 'description' => esc_html__('please add the page item class.', 'the-event')
										// ),	
										// 'office-1'=> array(
											// 'title'=> esc_html__('Office 1' ,'the-event'),
											// 'type'=> 'text',						
										// ),		
										// 'icon-1'=> array(
											// 'title'=> esc_html__('Icon' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'title-1'=> array(
											// 'title'=> esc_html__('Title' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'address-1'=> array(
											// 'title'=> esc_html__('Address' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'icon-2'=> array(
											// 'title'=> esc_html__('Icon' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'title-2'=> array(
											// 'title'=> esc_html__('Title' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'address-2'=> array(
											// 'title'=> esc_html__('Address' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'icon-3'=> array(
											// 'title'=> esc_html__('Icon' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'title-3'=> array(
											// 'title'=> esc_html__('Title' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'address-3'=> array(
											// 'title'=> esc_html__('Address' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'office-2'=> array(
											// 'title'=> esc_html__('Office 2' ,'the-event'),
											// 'type'=> 'text',						
										// ),		
										// 'icon-4'=> array(
											// 'title'=> esc_html__('Icon' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'title-4'=> array(
											// 'title'=> esc_html__('Title' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'address-4'=> array(
											// 'title'=> esc_html__('Address' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'icon-5'=> array(
											// 'title'=> esc_html__('Icon' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'title-5'=> array(
											// 'title'=> esc_html__('Title' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'address-5'=> array(
											// 'title'=> esc_html__('Address' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'icon-6'=> array(
											// 'title'=> esc_html__('Icon' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'title-6'=> array(
											// 'title'=> esc_html__('Title' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'address-6'=> array(
											// 'title'=> esc_html__('Address' ,'the-event'),
											// 'type'=> 'textarea',						
										// ),
										// 'margin-bottom' => array(
											// 'title' => esc_html__('Margin Bottom', 'the-event'),
											// 'type' => 'text',
											// 'description' => esc_html__('Spaces after ending of this item', 'the-event')
										// ),	 
									// )
								// ),
											
								'content' => array(
									'title'=> esc_html__('Content', 'the-event'), 
									'icon'=>'fa-contao',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'show-title' => array(
											'title' => esc_html__('Show Title' , 'the-event'),
											'type' => 'checkbox',
											'default' => 'enable',
										),						
										'page-caption' => array(
											'title' => esc_html__('Page Caption' , 'the-event'),
											'type' => 'textarea'
										),		
										'show-content' => array(
											'title' => esc_html__('Show Content (From Default Editor)' , 'the-event'),
											'type' => 'checkbox',
											'default' => 'enable',
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),														
									)
								), 	

								'divider' => array(
									'title'=> esc_html__('Divider', 'the-event'), 
									'icon'=>'fa-minus',
									'type'=>'item',
									'options'=>array(									
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),										
									)
								),
								
								// 'portfolio' => array(),
								
								'gallery' => array(
									'title'=> esc_html__('Gallery', 'the-event'), 
									'icon'=>'fa-houzz',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'slider'=> array(	
											'overlay'=> 'false',
											'caption'=> 'false',
											'type'=> 'gallery',
										),				
										'thumbnail-size'=> array(
											'title'=> esc_html__('Thumbnail Size' ,'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list()
										),
										'style'=> array(
											'title'=> esc_html__('Gallery Style' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'gallery-slider'=>esc_html__('Gallery Slider', 'the-event'),
												'simple-gallery'=>esc_html__('Simple Gallery', 'the-event'),
												'normal-gallery'=>esc_html__('Normal Gallery', 'the-event'),
											)
										),
										'layout'=> array(
											'title'=> esc_html__('Gallery Layout' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'with-space'=>esc_html__('With Padding', 'the-event'),
												'without-space'=>esc_html__('Without Padding', 'the-event'),
											)
										),
										'gallery-columns'=> array(
											'title'=> esc_html__('Gallery Image Columns' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array('2'=>'2', '3'=>'3', '4'=>'4'),
											'default'=> '4'
										),	
										'num-fetch'=> array(
											'title'=> esc_html__('Num Fetch (Per Page)' ,'the-event'),
											'type'=> 'text',
											'description'=> esc_html__('Leave this field blank to fetch all image without pagination.', 'the-event'),
											'wrapper-class'=>'gallery-style-wrapper grid-wrapper'
										),
										'show-caption'=> array(
											'title'=> esc_html__('Show Caption' ,'the-event'),
											'type'=> 'combobox',
											'options'=> array('yes'=>'Yes', 'no'=>'No')
										),			
										'pagination'=> array(
											'title'=> esc_html__('Enable Pagination' ,'the-event'),
											'type'=> 'checkbox'
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),	
									)	
								),		
								// 'post-slider' => array(
									// 'title'=> esc_html__('Post Slider', 'the-event'), 
									// 'icon'=>'fa-file-o',
									// 'type'=>'item',
									// 'options'=>array(
										// 'element-item-id' => array(
											// 'title' => esc_html__('Page Item ID', 'the-event'),
											// 'type' => 'text',
											// 'default' => '',
											// 'description' => esc_html__('please add the page item id.', 'the-event')
										// ),
										// 'element-item-class' => array(
											// 'title' => esc_html__('Page Item Class', 'the-event'),
											// 'type' => 'text',
											// 'default' => '',
											// 'description' => esc_html__('please add the page item class.', 'the-event')
										// ),	
										// 'category'=> array(
											// 'title'=> esc_html__('Category' ,'the-event'),
											// 'type'=> 'combobox',
											// 'options'=> theevent_get_term_list('category'),
											// 'description'=> esc_html__('Select categories to fetch its posts.', 'the-event')
										// ),	
										// 'num-excerpt'=> array(
											// 'title'=> esc_html__('Num Excerpt (Word)' ,'the-event'),
											// 'type'=> 'text',	
											// 'default'=> '25',
											// 'description'=> esc_html__('This is a number of word (decided by spaces) that you want to show on the post excerpt. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'the-event')
										// ),	
										// 'num-fetch'=> array(
											// 'title'=> esc_html__('Num Fetch' ,'the-event'),
											// 'type'=> 'text',	
											// 'default'=> '8',
											// 'description'=> esc_html__('Specify the number of posts you want to pull out.', 'the-event')
										// ),										
										// 'thumbnail-size'=> array(
											// 'title'=> esc_html__('Thumbnail Size' ,'the-event'),
											// 'type'=> 'combobox',
											// 'options'=> theevent_get_thumbnail_list()
										// ),	
										// 'style'=> array(
											// 'title'=> esc_html__('Style' ,'the-event'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'no-excerpt'=>esc_html__('No Excerpt', 'the-event'),
												// 'with-excerpt'=>esc_html__('With Excerpt', 'the-event'),
											// )
										// ),
										// 'caption-style'=> array(
											// 'title'=> esc_html__('Caption Style' ,'the-event'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'post-bottom post-slider'=>esc_html__('Bottom Caption', 'the-event'),
												// 'post-right post-slider'=>esc_html__('Right Caption', 'the-event'),
												// 'post-left post-slider'=>esc_html__('Left Caption', 'the-event')
											// ),
											// 'wrapper-class' => 'style-wrapper with-excerpt-wrapper'
										// ),											
										// 'orderby'=> array(
											// 'title'=> esc_html__('Order By' ,'the-event'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'date' => esc_html__('Publish Date', 'the-event'), 
												// 'title' => esc_html__('Title', 'the-event'), 
												// 'rand' => esc_html__('Random', 'the-event'), 
											// )
										// ),
										// 'order'=> array(
											// 'title'=> esc_html__('Order' ,'the-event'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'desc'=>esc_html__('Descending Order', 'the-event'), 
												// 'asc'=> esc_html__('Ascending Order', 'the-event'), 
											// )
										// ),			
										// 'margin-bottom' => array(
											// 'title' => esc_html__('Margin Bottom', 'the-event'),
											// 'type' => 'text',
											// 'description' => esc_html__('Spaces after ending of this item', 'the-event')
										// ),											
									// )
								// ),
								
								'slider' => array(
									'title'=> esc_html__('Slider', 'the-event'), 
									'icon'=>'fa-sliders',
									'type'=>'item',
									'options'=>array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'the-event')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'the-event'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'the-event')
										),	
										'slider'=> array(	
											'overlay'=> 'false',
											'caption'=> 'true',
											'type'=> 'slider'						
										),	
										'slider-type'=> array(
											'title'=> esc_html__('Slider Type', 'the-event'),
											'type'=> 'combobox',
											'options'=> array(
												'flexslider' => esc_html__('Flex slider', 'the-event'),
												'bxslider' => esc_html__('BX Slider', 'the-event'),
												'nivoslider' => esc_html__('Nivo Slider', 'the-event')
											)
										),		
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'the-event'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'the-event')
										),											
									)
								),	
								 					
							)
						),
					)
				),
				// page builder option attribute
				array(
					'post_type' => array('page'),
					'title' => 'Page Options',
					'meta_title' => esc_html__('Page Builder Options', 'the-event'),
				)
			);
			
		}
		
	}
	
	
	// show the pagebuilder item
	if( !function_exists('theevent_show_page_builder') ){
		function theevent_show_page_builder($content, $full_width = true){
			global $theevent_counter;
			$section = array(0, false); // (size, independent)
			foreach( $content as $item ){			
				// determine the current item size
				$current_size = 1;
				if( !empty($item['size']) ){
					$current_size = theevent_item_size_to_num($item['size']);
				}
				
				// print each section
				if( $item['type'] == 'color-wrapper' || $item['type'] == 'parallax-bg-wrapper' ||
					$item['type'] == 'full-size-wrapper' ){
					$section = theevent_show_section($section, $current_size, true);	
				}else{
					$section = theevent_show_section($section, $current_size, false);	
				}
				
				// start printing item
				if( $item['item-type'] == 'wrapper' ){
					if( $item['type'] == 'color-wrapper' ){
						theevent_show_color_wrapper( $item );
					}else if(  $item['type'] == 'parallax-bg-wrapper'){
						theevent_show_parallax_wrapper( $item );
					}else if(  $item['type'] == 'full-size-wrapper'){
						theevent_show_full_size_wrapper( $item );
					}else{
						theevent_show_column_wrapper( $item );
					}
				}else{
					theevent_show_element( $item );
				}
				$theevent_counter++;
			}
			
			echo '<div class="clear"></div>';
			
			if( !$section[1] ){
				echo '</div>';
				echo '</div>'; 
			} // close container of dependent section
			echo '</div>'; // close the last opened section
			
		}
	}
	
	// print each section
	if( !function_exists('theevent_show_section') ){
		function theevent_show_section( $section, $size, $independent = false ){
			global $theevent_section_id;
			if( empty($theevent_section_id) ){ $theevent_section_id = 1; }

			if( $section[0] == 0 ){ // starting section
				echo '<div id="content-section-' . $theevent_section_id . '" >';
				if( !$independent ){ echo '<div class="section-container container"><div class="row">'; } // open container
				
				$section = array($size, $independent);
				$theevent_section_id ++;
			}else{

				if( $independent || $section[1] ){ // current or previous section is independent
				
					echo '<div class="clear"></div>';
					if( !$section[1] ){ echo '</div></div>'; } // close container of dependent section
					echo '</div>';
					
					echo '<div id="content-section-' . $theevent_section_id . '" >';		
					if( !$independent ){ echo '<div class="section-container container"><div class="row">'; } // open container
					
					$section[0] = ceil($section[0]) + $size; $section[1] = $independent;
					$theevent_section_id ++;
				}else{

					if( abs((float)$section[0] - floor($section[0])) < 0.01 || 	// is integer or
						(floor($section[0]) < floor($section[0] + $size - 0.01)) ){ 	// exceeding current line
						echo '<div class="clear"></div>';
					}
					if( $size == 1 ){
						echo '<div class="clear"></div>';
						$section[0] = ceil($section[0]) + $size; $section[1] = $independent;
					}else{
						$section[0] += $size; $section[1] = $independent;
					}
				}
			}
			
			return $section;
		}
	}	

	
	
	// print color wrapper
	if( !function_exists('theevent_show_color_wrapper') ){
		function theevent_show_color_wrapper( $content ){
			$item_id = empty($content['option']['element-item-id'])? '': ' id="' . esc_attr($content['option']['element-item-id']) . '" ';
			
			global $theevent_spaces;
			$padding  = (!empty($content['option']['padding-top']) && 
				($theevent_spaces['top-wrapper'] != $content['option']['padding-top']))? 
				'padding-top: ' . esc_attr($content['option']['padding-top']) . 'px; ': '';
			$padding .= (!empty($content['option']['padding-bottom']) && 
				($theevent_spaces['bottom-wrapper'] != $content['option']['padding-bottom']))? 
				'padding-bottom: ' . esc_attr($content['option']['padding-bottom']) . 'px; ': '';
				
			$border = '';
			if( !empty($content['option']['border']) && $content['option']['border'] != 'none' ){
				if($content['option']['border'] == 'top' || $content['option']['border'] == 'both'){
					$border .= ' border-top: 4px solid '. esc_attr($content['option']['border-top-color']) . '; ';
				}
				if($content['option']['border'] == 'bottom' || $content['option']['border'] == 'both'){
					$border .= ' border-bottom: 4px solid '. esc_attr($content['option']['border-bottom-color']) . '; ';
				}
			}

			$content['option']['show-section'] = !empty($content['option']['show-section'])? $content['option']['show-section']: '';
			echo '<div class="kode-color-wrapper ' . ' ' . esc_attr($content['option']['show-section']) . '" ' . $item_id;
			if( !empty($content['option']['background']) || !empty($padding) ){
				echo 'style="';
				if( empty($content['option']['background-type']) || $content['option']['background-type'] == 'color' ){
					echo !empty($content['option']['background'])? 'background-color: ' . esc_attr($content['option']['background']) . '; ': '';
				}
				echo esc_attr($border);
				echo esc_attr($padding);
				echo '" ';
			}
			echo '>';
			echo '<div class="container"><div class="row">';
		
			foreach( $content['items'] as $item ){	
				if( $item['item-type'] == 'wrapper' ){
					theevent_show_column_wrapper( $item );
				}else{
					theevent_show_element( $item );
				}
			}	
			
			echo '<div class="clear"></div>';
			echo '</div></div>'; // close container
			echo '</div>'; // close wrapper
		}
	}	
	
	// show parallax wrapper
	if( !function_exists('theevent_show_parallax_wrapper') ){
		function theevent_show_parallax_wrapper( $content ){
			global $theevent_parallax_wrapper_id;
			$theevent_parallax_wrapper_id = empty($theevent_parallax_wrapper_id)? 1: $theevent_parallax_wrapper_id;
			if( empty($content['option']['element-item-id']) ){
				$content['option']['element-item-id'] = 'kode-parallax-wrapper-' . $theevent_parallax_wrapper_id;
				$theevent_parallax_wrapper_id++;
			}
			$item_id = ' id="' . esc_attr($content['option']['element-item-id']) . '" ';

			global $theevent_spaces;
			$padding  = (!empty($content['option']['padding-top']) && 
				($theevent_spaces['top-wrapper'] != $content['option']['padding-top']))? 
				'padding-top: ' . esc_attr($content['option']['padding-top']) . 'px; ': '';
			$padding .= (!empty($content['option']['padding-bottom']) && 
				($theevent_spaces['bottom-wrapper'] != $content['option']['padding-bottom']))? 
				'padding-bottom: ' . esc_attr($content['option']['padding-bottom']) . 'px; ': '';

			$border = '';

			echo '<div class="kode-parallax-wrapper kode-background-' . esc_attr($content['option']['type']) . '" ' . $item_id;
			
			// background parallax
			if( !empty($content['option']['background']) && $content['option']['type'] == 'image' ){
				if( !empty($content['option']['background-speed']) ){
					echo 'data-bgspeed="' . esc_attr($content['option']['background-speed']) . '" ';
				}else{
					echo 'data-bgspeed="0" ';
				}				
			
				if( is_numeric($content['option']['background']) ){
					$background = wp_get_attachment_image_src($content['option']['background'], 'full');
					$background = esc_url($background[0]);
				}else{
					$background = esc_url($content['option']['background']);
				}
				if(empty($content['option']['opacity']) || $content['option']['opacity'] == ''){ $content['option']['opacity'] = '0.03';}
				if(empty($content['option']['background-color']) || $content['option']['background-color'] == ''){ $content['option']['background-color'] = '#000';}
				echo 'style="background-color:'.esc_attr($content['option']['background-color']).';background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';			
				echo '<style scoped type="text/css">';
				echo '#' . esc_attr($content['option']['element-item-id']) . '{';
				echo ' position:relative;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
				echo ' position:relative;z-index:99999;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
				echo 'opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:100%;';
				echo '}';
				echo '</style>';
				if( !empty($content['option']['background-mobile']) ){
					if( is_numeric($content['option']['background-mobile']) ){
						$background = wp_get_attachment_image_src($content['option']['background-mobile'], 'full');
						$background = esc_url($background[0]);
					}else{
						$background = esc_url($content['option']['background-mobile']);
					}				
				
					echo '<style type="text/css">@media only screen and (max-width: 767px){ ';
					echo '#' . esc_attr($content['option']['element-item-id']) . '{';
					echo ' background-image: url(\'' . esc_url($background) . '\') !important;';
					echo '}';
					echo '}</style>';
				}
				
			// background pattern 
			}else if($content['option']['type'] == 'pattern'){
				$background = THEEVENT_PATH . '/images/pattern/pattern-' . esc_attr($content['option']['pattern']) . '.png';
				echo 'style="background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';
			
			// background video
			}else if( $content['option']['type'] == 'video' ){
				echo 'style="' . $padding . $border . '" >';
				
				global $theevent_gallery_id; $theevent_gallery_id++;
				$overlay_opacity = (empty($content['option']['video-overlay']))? 0: floatval($content['option']['video-overlay']);
				
				echo '<div id="kode-player-' . esc_attr($theevent_gallery_id) . '" class="kode-bg-player" data-property="';
				echo '{videoURL:\'' . esc_attr($content['option']['video']) . '\',containment:\'#kode-player-' . esc_attr($theevent_gallery_id) . '\',';
				echo 'startAt:0,mute:true,autoPlay:true,loop:true,printUrl:false,realfullscreen:false,quality:\'hd720\'';
				echo (!empty($content['option']['video-player']) && $content['option']['video-player'] == 'disable')? ',showControls:false':'';
				echo '}"><div class="kode-player-overlay" ';
				echo 'style="opacity: ' . esc_attr($overlay_opacity) . '; filter: alpha(opacity=' . esc_attr($overlay_opacity) * 100 . ');" ';
				echo '></div></div>';

			// background video / none
			}else if($content['option']['type'] == 'map'){
				echo '><span class="footertransparent-bg"></span>';
				echo do_shortcode($content['option']['map_shortcode']);
				echo '';
			}else if(!empty($padding) || !empty($border) ){
				echo 'style="' . $padding . $border . '" >';
			}

			echo '<div class="container">';
		
			foreach( $content['items'] as $item ){
				if( $item['item-type'] == 'wrapper' ){
					theevent_show_column_wrapper( $item );
				}else{
					theevent_show_element( $item );
				}
			}	
			
			echo '<div class="clear"></div>';
			echo '</div>'; // close container
			echo '</div>'; // close wrapper
		}
	}
	
	// print full size wrapper
	if( !function_exists('theevent_show_full_size_wrapper') ){
		function theevent_show_full_size_wrapper( $content ){
			global $theevent_wrapper_id;
			$theevent_wrapper_id = empty($theevent_wrapper_id)? 1: $theevent_wrapper_id;
			if( empty($content['option']['element-item-id']) ){
				$content['option']['element-item-id'] = 'kode-parallax-wrapper-' . $theevent_wrapper_id;
				$theevent_wrapper_id++;
			}
			
			$theevent_trans_class = '';
			if( !empty($content['option']['trans-background']) ){
				$theevent_trans_class = $content['option']['trans-background'];
			}
			$theevent_wrapper_class = '';
			if( !empty($content['option']['element-item-class']) ){
				$theevent_wrapper_class = $content['option']['element-item-class'];
			}
			$item_id = ' id="' . esc_attr($content['option']['element-item-id']) . '" ';

			global $theevent_spaces;
			$padding  = (!empty($content['option']['padding-top']) && 
				($theevent_spaces['top-wrapper'] != $content['option']['padding-top']))? 
				'padding-top: ' . esc_attr($content['option']['padding-top']) . 'px; ': '';
			$padding .= (!empty($content['option']['padding-bottom']) && 
				($theevent_spaces['bottom-wrapper'] != $content['option']['padding-bottom']))? 
				'padding-bottom: ' . esc_attr($content['option']['padding-bottom']) . 'px; ': '';

			$border = '';
			$content['option']['type'] = (empty($content['option']['type']))? ' ': $content['option']['type'];
			$theevent_trans_bg = '';
			$theevent_solid_bg = '';
			if($theevent_trans_class == 'enable'){
				$theevent_trans_bg = "background-color:".esc_attr($content['option']['background-color'])."";
			}else{
				$theevent_solid_bg = "background-color:".esc_attr($content['option']['background-color'])."";
			}
			if( !empty($content['option']['horizontal-background']) && $content['option']['horizontal-background'] == 'enable'){
				$theevent_wrapper_class .= ' overlay movingbg';
			}
			echo '<div class="'.esc_attr($theevent_wrapper_class).' kode-parallax-wrapper kode-background-' . esc_attr($content['option']['type']) . '" ' . $item_id;
			
			// background parallax
			if( !empty($content['option']['background']) && $content['option']['type'] == 'image' ){
				
				
				if( !empty($content['option']['horizontal-background']) && $content['option']['horizontal-background'] == 'enable'){
					echo 'data-id="customizer" data-title="Theme Customizer" data-direction="horizontal" ';
				}
				if( !empty($content['option']['background-speed']) ){
					echo 'data-bgspeed="' . esc_attr($content['option']['background-speed']) . '" ';
				}else{
					echo 'data-bgspeed="0" ';
				}				
			
				if( is_numeric($content['option']['background']) ){
					$background = wp_get_attachment_image_src($content['option']['background'], 'full');
					$background = esc_url($background[0]);
				}else{
					$background = esc_url($content['option']['background']);
				}
				if(empty($content['option']['opacity']) || $content['option']['opacity'] == ''){
					$content['option']['opacity'] = '0.03';
				}
				
				if(empty($content['option']['background-color']) || $content['option']['background-color'] == ''){
					$content['option']['background-color'] = '#000';
				}
				
				$bg_width = '50%';
				if(isset($content['option']['background-width']) && $content['option']['background-width'] <> ''){
					$bg_width = $content['option']['background-width'];
				}
				if(isset($content['option']['background-hook']) && $content['option']['background-hook'] == 'enable'){
					echo 'style="'.$theevent_solid_bg.';'.$padding . $border . '" ><style scoped>';
					echo '#' . esc_attr($content['option']['element-item-id']) . ' .row > .columns{ ';
					echo 'padding:0px 70px;';
					echo ' }';
					echo '#' . esc_attr($content['option']['element-item-id']) . ':after{';
					
					
					if(isset($content['option']['background-image-align']) && $content['option']['background-image-align'] == 'left'){
						echo ''.$theevent_solid_bg.';background-image: url(\'' . esc_url($background) . '\');background-position: center center;';
							if(isset($content['option']['background-image-attachment']) && $content['option']['background-image-attachment'] == 'fixed'){
								echo 'background-attachment: fixed;	';
							}
							echo 'background-repeat: no-repeat;
							background-size: cover;
							content: "";
							height: 100%;
							left: 0;
							position: absolute;
							top: 0;
							width: '.$bg_width.';';
						echo '}';
						echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
						echo ''.$theevent_solid_bg.';z-index:1;opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:'.$bg_width.';';
						echo '}';
					}else{
							echo ''.$theevent_solid_bg.';background-image: url(\'' . esc_url($background) . '\');background-position: center center;';
							if(isset($content['option']['background-image-attachment']) && $content['option']['background-image-attachment'] == 'fixed'){
								echo 'background-attachment: fixed;	';
							}
							echo '
							background-repeat: no-repeat;
							background-size: cover;
							content: "";
							height: 100%;
							right: 0;
							position: absolute;
							top: 0;
							width: '.$bg_width.';';
						echo '}';
						echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
						echo ''.$theevent_solid_bg.';z-index:1;opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;right:0px;top:0px;height:100%;width:'.$bg_width.';';
						echo '}';
					}
					echo '#' . esc_attr($content['option']['element-item-id']) . '{';
					echo ' position:relative;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ' > .container-fluid{';
					echo ' position:relative;z-index:99999;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
					echo ' position:relative;z-index:99999;';
					echo '}';
					
					echo '</style>';
				}else{
					if($background <> ''){
						echo 'style="'.$theevent_trans_bg.';background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';			
					}else{
						echo 'style="'.$theevent_trans_bg.';' . $padding . $border . '" >';			
					}
					echo '<style scoped>';
					echo '#' . esc_attr($content['option']['element-item-id']) . '{';
					echo ' position:relative;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
					echo ' position:relative;z-index:99999;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
					echo ''.$theevent_solid_bg.';opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:100%;';
					echo '}';
					echo '</style>';
				}
				
				
			// background pattern 
			}else if($content['option']['type'] == 'pattern'){
				$background = THEEVENT_PATH . '/images/pattern/pattern-' . esc_attr($content['option']['pattern']) . '.png';
				echo 'style="background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';
			
			// background MAP
			}else if($content['option']['type'] == 'map'){
				echo '><span class="footertransparent-bg"></span>';
				echo do_shortcode($content['option']['map_shortcode']);
				echo '';
			}else if($content['option']['type'] == 'color'){
				$content['option']['background-color'] = (empty($content['option']['background-color']))? ' ': $content['option']['background-color'];
				echo ' style="' . $padding . $border . ';background:'.esc_attr($content['option']['background-color']).'">';
				echo '';
			}else if($content['option']['type'] == 'video'){
				echo ' style="' . $padding . $border . ';">';
				echo '<style scoped>';
				echo '#' . esc_attr($content['option']['element-item-id']) . '{';
				echo ' position:relative;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
				echo ' position:relative;z-index:99999;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
				echo ''.$theevent_solid_bg.';opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:100%;z-index:10;';
				echo '}';
				echo '</style>';
				
				$content['option']['video_url'] = (empty($content['option']['video_url']))? THEEVENT_PATH.'/images/ocean.ogv': $content['option']['video_url'];
				$content['option']['video_type'] = (empty($content['option']['video_type']))? 'ogg': $content['option']['video_type'];
				echo '
				    <script>
						jQuery(document).ready(function($) {
							var BV = new $.BigVideo({
								useFlashForFirefox:false,
								container: $("#inner-' . esc_attr($content['option']['element-item-id']) . '"),
								forceAutoplay:true,
								controls:false,
								doLoop:false,			
								shrinkable:true
							});
							BV.init();
							BV.show([
								{ type: "video/'.esc_attr($content['option']['video_type']).'",  src: "'.esc_url($content['option']['video_url']).'" },
								
							],{doLoop:true});
						});
					</script>
					<div class="kode-video-bg" id="inner-' . esc_attr($content['option']['element-item-id']) . '"></div>';
				
			}
			else if(!empty($padding) || !empty($border) ){
				echo 'style="' . $padding . $border . '" >';
			}
			$content['option']['container'] = (empty($content['option']['container']))? ' ': $content['option']['container'];
			if($content['option']['container'] == 'container-width'){
				echo '<div class="container">';
			}else{
				echo '<div class="container-fluid">';
				echo '<div class="row">';
			}
		
			foreach( $content['items'] as $item ){
				if( $item['item-type'] == 'wrapper' ){
					theevent_show_column_wrapper( $item );
				}else{
					theevent_show_element( $item );
				}
			}	
			
			echo '<div class="clear"></div>';
			if($content['option']['container'] == 'container-width'){
				echo '</div>'; // close container or Container
			}else{
				echo '</div>'; // close container or Row
				echo '</div>'; // close container or Container-fluid
			}			
			echo '</div>'; // close wrapper
		}
	}	
	
	// Column Sizes Bootstrap 3+
	if( !function_exists('theevent_get_column_class') ){
		function theevent_get_column_class( $size ){
			switch( $size ){
				case '1/6': return 'col-md-1 columns'; break;
				case '1/5': return 'col-md-2 column'; break;
				case '1/4': return 'col-md-3 columns'; break;
				case '2/5': return 'col-md-5 columns'; break;
				case '1/3': return 'col-md-4 columns'; break;
				case '1/2': return 'col-md-6 columns'; break;
				case '3/5': return 'col-md-7 columns'; break;
				case '2/3': return 'col-md-8 columns'; break;
				case '3/4': return 'col-md-9 columns'; break;
				case '4/5': return 'col-md-10 columns'; break;
				case '1/1': return 'col-md-12 columns'; break;
				default : return 'col-md-12 columns'; break;
			}
		}
	}
	
	// show column wrapper
	if( !function_exists('theevent_show_column_wrapper') ){
		function theevent_show_column_wrapper( $content ){
			global $theevent_counter;			
			$content['option']['element-item-class'] = (empty($content['option']['element-item-class']))? ' ': $content['option']['element-item-class'];
			echo '<div class="'.esc_attr($content['option']['element-item-class']).' ' . esc_attr(theevent_get_column_class( $content['size'] )) . '" >';
			foreach( $content['items'] as $item ){
				theevent_show_element( $item );
				$theevent_counter++;
			}			
			echo '</div>'; // end of column section
		}
	}	
	
	// show the item
	if( !function_exists('theevent_show_element') ){
		function theevent_show_element( $content ){
			global $theevent_counter;
			switch ($content['type']){
				case 'accordion': echo theevent_get_accordion_item($content['option']); break;
				case 'breaking-news': echo theevent_get_breaking_news($content['option']); break;
				case 'news': echo theevent_get_news_item($content['option']); break;				
				case 'blog': echo theevent_get_blog_item($content['option']); break;
				case 'booking-form': echo thevent_paypal_form($content['option']); break;
				case 'team-table': 
					if(function_exists('theevent_get_team_points_table') ){
						echo theevent_get_team_points_table($content['option']); 
					}
				break;
				case 'events': 
					if(class_exists('EM_Events')){
						echo theevent_get_events_item($content['option']); 
					}
				break;
				case 'upcoming-event': 
					if(class_exists('EM_Events')){
						echo theevent_get_upnext_event($content['option']); 
					}
				break;
				case 'woo': 
					if(class_exists('WooCommerce')){
						echo theevent_get_woo_item($content['option']); 
					}
				break;
				case 'team-slider': 
					if(function_exists('theevent_get_team_item_slider') ){
						echo theevent_get_team_item_slider($content['option']); 
					}
				break;
				case 'woo-slider': 
					if(function_exists('theevent_get_woo_item_slider') ){
						echo theevent_get_woo_item_slider($content['option']); 
					}
				break;
				case 'team': 
					if(function_exists('theevent_get_team_item') ){
						echo theevent_get_team_item($content['option']); 
					}
				break;
				case 'work': 
					if(function_exists('theevent_get_work_item') ){
						echo theevent_get_work_item($content['option']); 
					}
				break;
				case 'work': 
					if(function_exists('theevent_get_work_item') ){
						echo theevent_get_work_item($content['option']); 
					}
				break;
				case 'portfolio-slider': 
					if(function_exists('theevent_get_work_slider') ){
						echo theevent_get_work_slider($content['option']); 
					}	
				break;
				case 'timeline': 
					if(function_exists('theevent_get_timeline') ){
						echo theevent_get_timeline($content['option']); 
					}
				break;
				case 'volunteer': 
					if(function_exists('theevent_get_volunteer') ){
						echo theevent_get_volunteer($content['option']); 
					}
				break;
				case 'ignitiondeck': 
					if(class_exists('Deck')){
						echo theevent_get_crowdfunding_item($content['option']);
					}
				break;				
				case 'header-element': 
					global $theevent_theme_option;
					$theevent_post_option = $content['option'];
					$theevent_theme_option['enable-header-top'] = 'enable';
					echo '<div class="kode-header-pagebuilder">';
					theevent_get_selected_header($theevent_post_option,$theevent_theme_option); 
					echo '</div>';
				break;				
				case 'blog-slider': echo theevent_get_blog_slider_item($content['option']); break;
				case 'demo': 
				if(function_exists('theevent_get_demo_item') ){
					echo theevent_get_demo_item($content['option']);
				}
				break;
				case 'column-service': echo theevent_get_column_service_item($content['option']); break;
				case 'donate-service': echo theevent_get_column_donate_item($content['option']); break;
				case 'headings': echo theevent_get_headings_item($content['option']); break;
				case 'speaker': echo theevent_get_speaker_item($content['option']); break;
				case 'conference': echo theevent_get_events_conference($content['option']); break;
				case 'simple-column': echo theevent_get_simple_column_item($content['option']); break;				
				case 'next-event': echo theevent_next_event_starts($content['option']); break;				
				// case 'price-table': echo theevent_price_table_starts($content['option']); break;				
				case 'content': theevent_get_default_content_item($content['option']); break;
				case 'divider': echo theevent_get_divider_item($content['option']); break;
				case 'gallery': echo theevent_get_gallery_item($content['option']); break;				
				case 'post-slider': echo theevent_get_wp_post_slider($content['option']); break;				
				case 'sidebar': echo '<div class="widget kode-widget kode-sidebar-element">';theevent_get_sidebar_item($content['option']);echo '</div>'; break;
				case 'slider': echo theevent_get_slider_item($content['option']); break;				
				case 'testimonial': 
				if(function_exists('theevent_get_testimonial_item') ){
					echo theevent_get_testimonial_item($content['option']); 
				}
				break;				
				default: $default['show-title'] = 'enable'; $default['show-content'] = 'enable'; echo theevent_get_content_item($default); break;
			}
		}	
	}
	
	
	
?>