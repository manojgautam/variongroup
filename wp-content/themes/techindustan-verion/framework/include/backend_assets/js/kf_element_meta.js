(function($){	
	
	
	function theevent_sortable_item(item){
		item.find('.kode-sortable').sortable({ 
			connectWith				: '.kode-sortable',
			cursor					: 'move',
			forcePlaceholderSize	: true, 
			placeholder				: 'kode-placeholder',
			items					: '.kode-multi-fields',
			opacity					: 0.9,			
		});
		return item;
	}
	
	function theevent_sortable_filter_wrap(item){
		item.find('.kode-sortable').sortable({ 
			connectWith				: '.kode-sortable',
			cursor					: 'move',
			forcePlaceholderSize	: true, 
			placeholder				: 'kode-placeholder',
			items					: '.kode-multi-fields',
			opacity					: 0.9,			
		});
		return item;
	}
	
	function theevent_remove_val_textfields(item){
		item.find('.kode-option-input').find('input').val( '');
		
		return item;
	}
	function theevent_remove_val_chosen(item){
		item.find('.kode-combobox-wrapper').children('select').chosen();
		
		return item;
	}
	
	function theevent_remove_val_textarea(item){
		item.find('.kode-option-input').find('textarea').val( '');
		
		return item;
	}
	
	function theevent_remove_add_remove(item){
		item.children().append('<div class="kode-delete-item-clone"></div>');
		
		return item;
	}
	
	
	
	$(document).ready(function(){
		
		// $('.kode-page-option-input-wrapper .kode-multi-item > .kode-option').sortable({
			// revert: 100,
			// opacity: 0.8,
			// tolerance: "pointer",
			// placeholder: 'kode-placeholder',
			// connectWith: ".kode-element-wrapper-item",
		// });
		$('.up-arrow').click(function(){
			var current = $(this).parent();
			current.prev().before(current);
		});
		$('.down-arrow').click(function(){
			var current = $(this).parent();
			current.next().after(current);
		});
		// bind the clone item button
		$('#add-more-team').click(function(){
			var element = $(this).parent('.kode-option').parent('.kode-option-wrapper').siblings('.kode-multi-fields');
			var element_new = $(this).parent('.kode-option').parent('.kode-option-wrapper').siblings('.kode-multi-item');			
			
			// sortable destroy, clone & init for cloned element
			var column = element.parent().find('.kode-multi-fields');
			if(column.length){
				
				var clone = element.clone(true);
				var clone = clone.attr('class','kode-option-wrapper kode-multi-item');
				theevent_remove_val_textfields(clone);
				theevent_remove_add_remove(clone);				
				theevent_remove_val_textarea(clone);
				theevent_remove_val_chosen(clone);
				$('.kode-combobox-wrapper select').show();
				$('.chosen-container').remove();
				
			}else{
				
				var clone = element.clone(true);
				var clone = clone.attr('class','kode-option-wrapper kode-multi-item');
				theevent_remove_val_textfields(clone);
				theevent_remove_val_textarea(clone);
				theevent_remove_val_chosen(clone);
				$('.kode-combobox-wrapper select').show();
				$('.chosen-container').remove();
			}		
			
			clone.hide();
			element.after(clone);
			clone.show('slow');
			// $('.kode-page-option-input-wrapper').sortable();
			$('.kode-combobox-wrapper select').show();
			$('.chosen-container').remove();
			$('.kode-delete-item-clone').click(function(){
				var removethis = $(this).parent().parent();
				removethis.slideUp("normal", function() { $(this).remove(); } );				
				$(this).click(function(){
					$('body').theevent_confirm({
						success: function(){
							$(this).parent().parent().slideUp(function(){
								$(this).remove();
							});				
						}
					});

				});	
			});
					
						
		});
		
		$('.kode-delete-item').click(function(){			
			var removethis = $(this).parent('.kode-element-wrapper-item');
			removethis.slideUp("normal", function() { $(this).remove(); } );
			$(this).click(function(){
				$('body').theevent_confirm({
					success: function(){
						$(this).parent('.kode-element-wrapper-item').slideUp(function(){
							$(this).remove();
						});				
					}
				});

			});	
		});
		
		
		
		
		
		// Wp Color Picker
		$('.kode-option-input .wp-color-picker').wpColorPicker();		
		
		// animate font family section
		var theevent_custom_font_list = [];
		$('select.kode-font-combobox').change(function(){
			var font_family = $(this).val();
			var sample_font = $(this).parent().siblings('.kode-sample-font');
			var selected_option = $(this).children('option:selected');
			
			if( selected_option.attr('data-type') == 'web-safe-font' ){
				sample_font.css('font-family', font_family);
			}else if( selected_option.attr('data-type') == 'google-font' ){
				$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', selected_option.attr('data-url')) );
				sample_font.css('font-family', font_family + ', BlankSerif');
			}else if( selected_option.attr('data-type') == 'custom-font' ){
				if( theevent_custom_font_list.indexOf(font_family) <= 0 ){
					var new_font = '@font-face {';
					new_font    += 'font-family: "' + font_family + '";'
					new_font    += 'src: url("' + selected_option.attr('data-eot') + '");';
					new_font    += 'src: url("' + selected_option.attr('data-eot') + '?#iefix") format("embedded-opentype"),';
					new_font    += 'url("' + selected_option.attr('data-ttf') + '") format("truetype");';
					new_font    += '}';
					
					$('head').append($('<style type="text/css"></style>').append(new_font));
					theevent_custom_font_list.push(font_family);
				}
				sample_font.css('font-family', font_family + ', BlankSerif');
			}
		
		});
		$('select.kode-font-combobox').trigger('change');
		
		
		// kodeforest combobox
		$('.kode-option-input select').not('multiple').change(function(){
			var wrapper = $(this).attr('data-slug') + '-wrapper';
			var wrapper = wrapper.replace('[]', ' ');
			var selected_wrapper = $(this).val() + '-wrapper';
			$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
				if($(this).hasClass(selected_wrapper)){
					$(this).slideDown(300);
				}else{
					$(this).slideUp(300);
				}
			});
		});
		
		// kodeforest multiple Select
		$('.kode-option-input select').not('multiple').each(function(){
			var wrapper = $(this).attr('data-slug') + '-wrapper';
			var wrapper = wrapper.replace('[]', ' ');
			var selected_wrapper = $(this).val() + '-wrapper';

			$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
				if($(this).hasClass(selected_wrapper)){
					$(this).css('display', 'block');
				}else{
					$(this).css('display', 'none');
				}
			});
		});		
		
		// animate checkbox
		$('.kode-option-input input[type="checkbox"]').click(function(){	
			if( $(this).siblings('.checkbox-appearance').hasClass('enable') ){
				var wrapper = $(this).attr('data-slug') + '-wrapper';		
				var wrapper = wrapper.replace('[]', ' ');
				var selected_wrapper = $(this).val() + '-wrapper';
				$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
					$(this).slideUp(300);			
				});
			}else{
				var wrapper = $(this).attr('data-slug') + '-wrapper';	
				var wrapper = wrapper.replace('[]', ' ');				
				var selected_wrapper = $(this).val() + '-wrapper';
				$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
						
					$(this).slideDown(300);		
				});
			}
		});
		
		// kodeforest multiple Select
		$('.kode-option-input input[type="checkbox"]').each(function(){
			if( $(this).siblings('.checkbox-appearance').hasClass('enable') ){
				var wrapper = $(this).attr('data-slug') + '-wrapper';		
				var wrapper = wrapper.replace('[]', ' ');
				var selected_wrapper = $(this).val() + '-wrapper';
				$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
					$(this).css('display', 'block');
				});
			}else{
				var wrapper = $(this).attr('data-slug') + '-wrapper';		
				var wrapper = wrapper.replace('[]', ' ');
				var selected_wrapper = $(this).val() + '-wrapper';
				$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
					$(this).css('display', 'none');
				});
			}
		});	
				
		// KodeForest radio image 
		$('.kode-option-input input[type="radio"]').change(function(){
			$(this).parent().siblings('label').children('input').attr('checked', false); 
			$(this).parent().addClass('active').siblings('label').removeClass('active');
			
			// animate the related section
			var wrapper = $(this).attr('data-slug') + '-wrapper';
			var wrapper = wrapper.replace('[]', ' ');
			var selected_wrapper = $(this).val() + '-wrapper';
			$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
				if($(this).hasClass(selected_wrapper)){
					$(this).slideDown(300);
				}else{
					$(this).slideUp(300);
				}
			});
		});
		
		$('.kode-option-input input[type="radio"]:checked').each(function(){	
			// trigger the default value
			var wrapper = $(this).attr('data-slug') + '-wrapper';
			var wrapper = wrapper.replace('[]', ' ');
			var selected_wrapper = $(this).val() + '-wrapper';

			$(this).parents('.kode-option-wrapper').siblings('.' + wrapper).each(function(){
				if($(this).hasClass(selected_wrapper)){
					$(this).css('display', 'block');
				}else{
					$(this).css('display', 'none');
				}
			});
		});	
		
		var checked = $('.player-row .player_selected:checked');
		if(checked.length){
			checked.addClass('enable');
			checked.parent().parent().find('input[type="text"]').removeAttr('disabled');
			checked.parent().parent().find('input[type="radio"]').removeAttr('disabled');
			checked.parent().parent().find('.select-pos').removeAttr('disabled');
		}
		
		$('.player-row input[type="checkbox"]').click(function(){
			if( $(this).hasClass('enable') ){
				$(this).removeClass('enable');
				$(this).parent().parent().find('input[type="text"]').prop('disabled', true);
				$(this).parent().parent().find('input[type="radio"]').prop('disabled', true);
				$(this).parent().parent().find('.select-pos').prop('disabled', 'disabled');
			}else{
				$(this).addClass('enable');
				$(this).parent().parent().find('.select-pos').removeAttr('disabled');
				$(this).parent().parent().find('input[type="text"]').removeAttr('disabled');
				$(this).parent().parent().find('input[type="radio"]').removeAttr('disabled');
			}
		});	
		
		//
		
		// load page builder meta
		$('select[name="select_team_first"]').change(function(){
			var post_id = $(this).parent().attr('data-id');
			var theevent_ajax_url = $(this).parent().attr('data-ajax');
			var action = $(this).parent().attr('data-action');
			var settings = $(this).parent().attr('data-settings');
			var team_id = $(this).val();			
			$.ajax({
				type: 'POST',
				url: theevent_ajax_url,
				data: {'action': action,'team_id': team_id,'post_id':post_id,'settings':settings},
				// dataType: 'json',
				error: function(a, b, c){
					console.log(a, b, c);
					$('body').theevent_alert({
						text: '<span class="head">Loading Error</span> Please refresh the page and try this again.', 
						status: 'failed'
					});
				},
				success: function(data){
					$('.player_line_up_class_first').children('.kode-option').children().remove();
					$('.player_line_up_class_first').children('.kode-option').html(data);
				}
			});				
		});
		
		// load page builder meta
		$('select[name="select_team_second"]').change(function(){
			var post_id = $(this).parent().attr('data-id');
			var theevent_ajax_url = $(this).parent().attr('data-ajax');
			var action = $(this).parent().attr('data-action');
			var settings = $(this).parent().attr('data-settings');
			var team_id = $(this).val();			
			$.ajax({
				type: 'POST',
				url: theevent_ajax_url,
				data: {'action': action,'team_id': team_id,'post_id':post_id,'settings':settings},
				// dataType: 'json',
				error: function(a, b, c){
					console.log(a, b, c);
					$('body').theevent_alert({
						text: '<span class="head">Loading Error</span> Please refresh the page and try this again.', 
						status: 'failed'
					});
				},
				success: function(data){
					$('.player_line_up_class_second').children('.kode-option').children().remove();
					$('.player_line_up_class_second').children('.kode-option').html(data);
				}
			});				
		});
		
		
		// animate checkbox
		$('.kode-option-input input[type="checkbox"]').click(function(){	
			if( $(this).siblings('.checkbox-appearance').hasClass('enable') ){
				$(this).siblings('.checkbox-appearance').removeClass('enable');
				$(this).siblings('.checkbox-appearance').addClass('disable');
			}else{
				$(this).siblings('.checkbox-appearance').addClass('enable');
				$(this).siblings('.checkbox-appearance').removeClass('disable');
			}
		});
		
		
		
		// animate upload button
		$('.kode-option-input .kode-upload-box-input').change(function(){		
			$(this).siblings('.kode-upload-box-hidden').val($(this).val());
			if( $(this).val() == '' ){ 
				$(this).siblings('.kode-upload-img-sample').addClass('blank'); 
			}else{
				$(this).siblings('.kode-upload-img-sample').attr('src', $(this).val()).removeClass('blank');
			}
		});
		
		$('.kode-option-input .kode-upload-box-button').click(function(){
			var upload_button = $(this);
			var data_type = upload_button.attr('data-type');
			if( data_type == 'all' ){ data_type = ''; }
			
			var custom_uploader = wp.media({
				title: upload_button.attr('data-title'),
				button: { text: upload_button.attr('data-button') },
				library : { type : data_type },
				multiple: false
			}).on('select', function() {
				var attachment = custom_uploader.state().get('selection').first().toJSON();
				
				if( data_type == 'image' ){
					upload_button.siblings('.kode-upload-img-sample').attr('src', attachment.url).removeClass('blank');
				}
				upload_button.siblings('.kode-upload-img-sample').attr('src', attachment.url).removeClass('blank');
				upload_button.siblings('.kode-upload-box-input').val(attachment.url);
				upload_button.siblings('.kode-upload-box-hidden').val(attachment.id);
			}).open();			
		});
		
		// kodeforest sliderbar item
		$('.kode-option-input .kode-sliderbar').each(function(){
			$(this).slider({ min:10, max:72, value: $(this).attr('data-value'),
				slide: function(event, ui){
					$(this).siblings('.kode-sliderbar-text-hidden').val(ui.value);
					$(this).siblings('.kode-sliderbar-text').html(ui.value + ' px');
				}
			});
		});		
		
		// font family section
		var theevent_custom_font_list = [];
		$('select.kode-font-combobox').change(function(){
			var font_family = $(this).val();
			var sample_font = $(this).parent().siblings('.kode-sample-font');
			var selected_option = $(this).children('option:selected');
			
			if( selected_option.attr('data-type') == 'web-safe-font' ){
				sample_font.css('font-family', font_family);
			}else if( selected_option.attr('data-type') == 'google-font' ){
				$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', selected_option.attr('data-url')) );
				sample_font.css('font-family', font_family + ', BlankSerif');
			}else if( selected_option.attr('data-type') == 'custom-font' ){
				if( theevent_custom_font_list.indexOf(font_family) <= 0 ){
					var new_font = '@font-face {';
					new_font    += 'font-family: "' + font_family + '";'
					new_font    += 'src: url("' + selected_option.attr('data-eot') + '");';
					new_font    += 'src: url("' + selected_option.attr('data-eot') + '?#iefix") format("embedded-opentype"),';
					new_font    += 'url("' + selected_option.attr('data-ttf') + '") format("truetype");';
					new_font    += '}';
					
					$('head').append($('<style type="text/css"></style>').append(new_font));
					theevent_custom_font_list.push(font_family);
				}
				sample_font.css('font-family', font_family + ', BlankSerif');
			}
		
		});
		$('select.kode-font-combobox').trigger('change');
		
		// initiate slider selector		
		$('textarea.kode-slider-selection').each(function(){
			$(this).kodeCreateSliderSelection();	
		});
		
		// initiate slider selector		
		$('textarea.kode-gallery-selection').each(function(){
			$(this).kodeCreateGallerySelection();
		});
	});	
})(jQuery);