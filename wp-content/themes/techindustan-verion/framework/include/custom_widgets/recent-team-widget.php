<?php
/**
 * Plugin Name: Kodeforest Recent Post
 * Plugin URI: http://kodeforest.com/
 * Description: A widget that show recent posts( Specified by category ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'theevent_speaker_post_widget' );
if( !function_exists('theevent_speaker_post_widget') ){
	function theevent_speaker_post_widget() {
		register_widget( 'Kodeforest_Speaker_Post' );
	}
}

if( !class_exists('Kodeforest_Speaker_Post') ){
	class Kodeforest_Speaker_Post extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'theevent_speaker_post_widget', 
				esc_html__('Kodeforest Speaker Post Widget','the-event'), 
				array('description' => esc_html__('A widget that show Speaker', 'the-event')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
			global $theevent_theme_option;	
				
			$title = apply_filters( 'widget_title', $instance['title'] );
			$category = $instance['category'];
			
			
			// Opening of widget
			echo $args['before_widget'];
			
			// Open of title tag
			if( !empty($title) ){ 
				echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
			}

			// Widget Content
			// $current_post = array(get_the_ID());		
			// $query_args = array('post_type' => 'post', 'suppress_filters' => false);
			// $query_args['posts_per_page'] = $num_fetch;
			// $query_args['orderby'] = 'post_date';
			// $query_args['order'] = 'desc';
			// $query_args['paged'] = 1;
			// $query_args['category_name'] = $category;
			// $query_args['ignore_sticky_posts'] = 1;
			// $query_args['post__not_in'] = array(get_the_ID());
			// $query = new WP_Query( $query_args );
			
			// if($query->have_posts()){
					$post_single = get_post($category);
					
					$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta($post_single->ID, 'post-option', true ));
					if( !empty($theevent_post_option) ){
						$theevent_post_option = json_decode( $theevent_post_option, true );					
					}
					$designation = $theevent_post_option['designation'];
					$phone = $theevent_post_option['phone'];
					$website = $theevent_post_option['website'];
					$email = $theevent_post_option['email'];
					$facebook = $theevent_post_option['facebook'];
					$twitter = $theevent_post_option['twitter'];
					$youtube = $theevent_post_option['youtube'];
					$pinterest = $theevent_post_option['pinterest'];
					$item_class = '';
					if(!empty($post_single)){
						$attend_events = count(theevent_speaker_attended_event($post_single->ID));
						echo '
						<div class="aside_speaker">
							<figure>
								'.get_the_post_thumbnail($post_single->ID, array(80,80)).'
							</figure>
							
							<div class="aside_spekr_des">
								<h6>' . esc_attr($post_single->post_title) . '</h6>
								<span>'.esc_attr($designation).'</span>
							</div>
							
							<ul class="kf_conference">
								<li>
									<span>'.theevent_get_post_views($post_single->ID).'</span>
									'.esc_attr__('Views','the-event').'
								</li>
								
								<li>
									<span>'.esc_attr($attend_events).'</span>
									'.esc_attr__('Attended','the-event').'
								</li>
								
								<li>
									<span>'.esc_attr($attend_events).'</span>
									'.esc_attr__('Speaker At','the-event').'
								</li>
							</ul>
							
							<div class="kf_comment_meta">
								<span>
									<a href="' . esc_url(get_permalink($post_single->ID)) . '">View Comment</a>
								</span>
								
								<span><a href="mailto::'.esc_attr($email).'"><i class="fa fa-envelope"></i></a></span>
								
								<ul class="kf_social_icon">
									<li><a href="'.esc_attr($facebook).'"><i class="fa fa-facebook"></i></a></li>
									<li><a href="'.esc_attr($twitter).'"><i class="fa fa-twitter"></i></a></li>
									<li><a href="'.esc_attr($youtube).'"><i class="fa fa-youtube"></i></a></li>
									<li><a href="'.esc_attr($pinterest).'"><i class="fa fa-pinterest"></i></a></li>
								</ul>
								
							</div>
							
						</div>';					
					}
			// }
			// wp_reset_postdata();
					
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$category = isset($instance['category'])? $instance['category']: '';
			
			
			?>

			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'the-event'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>		

			<!-- Post Category -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Speaker :', 'the-event'); ?></label>		
				<select class="widefat" name="<?php echo esc_attr($this->get_field_name('category')); ?>" id="<?php echo esc_attr($this->get_field_id('category')); ?>">
				<option value="" <?php if(empty($category)) echo ' selected '; ?>><?php esc_html_e('All', 'the-event') ?></option>
				<?php 	
				$category_list = theevent_get_post_list_id('team'); 
				foreach($category_list as $cat_slug => $cat_name){ ?>
					<option value="<?php echo esc_attr($cat_slug); ?>" <?php if ($category == $cat_slug) echo ' selected '; ?>><?php echo esc_attr($cat_name); ?></option>				
				<?php } ?>	
				</select> 
			</p>
				
			

		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['category'] = (empty($new_instance['category']))? '': strip_tags($new_instance['category']);

			return $instance;
		}	
	}
}
?>