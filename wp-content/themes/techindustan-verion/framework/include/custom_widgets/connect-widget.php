<?php
/**
 * Plugin Name: Kodeforest Connect Widget
 * Plugin URI: http://kodeforest.com/
 * Description: A widget that show Connect With Us.
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'theevent_connect_widget' );
if( !function_exists('theevent_connect_widget') ){
	function theevent_connect_widget() {
		register_widget( 'Kodeforest_Connect_Widget' );
	}
}

if( !class_exists('Kodeforest_Connect_Widget') ){
	class Kodeforest_Connect_Widget extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'theevent_connect_widget', 
				esc_html__('Kodeforest Connect With Us Widget (KodeForest)','the-event'), 
				array('description' => esc_html__('A widget that show Connect With Us social networks', 'the-event')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
			global $theevent_theme_option;	
				
			$title = apply_filters( 'widget_title', $instance['title'] );
			$widget_facebook = $instance['widget_facebook'];
			$widget_twitter = $instance['widget_twitter'];
			$widget_youtube = $instance['widget_youtube'];
			$widget_dribble = $instance['widget_dribble'];
			$widget_behance = $instance['widget_behance'];
			$widget_plus = $instance['widget_plus'];
			

			// Opening of widget
			echo $args['before_widget'];
			
			
			?>
			
			<div class="kf_connect aside_widget_hdg">
				
				<?php
				// Open of title tag
				if( !empty($title) ){ 
					echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
				}
				?>
				<ul>
					<li class="facebook"><a title="" data-toggle="tooltip" href="<?php echo esc_url($widget_facebook);?>" data-original-title="Facebook"> <i class="fa fa-facebook"></i></a></li>
					<li class="twitter"><a title="" data-toggle="tooltip" href="<?php echo esc_url($widget_twitter);?>" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
					<li class="youtube"><a title="" data-toggle="tooltip" href="<?php echo esc_url($widget_youtube);?>" data-original-title="YouTube"><i class="fa fa-youtube"></i></a></li>
					<li class="dribble"><a title="" data-toggle="tooltip" href="<?php echo esc_url($widget_dribble);?>" data-original-title="LifeRing"><i class="fa fa-life-ring"></i></a></li>
					<li class="behance"><a title="" data-toggle="tooltip" href="<?php echo esc_url($widget_behance);?>" data-original-title="Behance"><i class="fa fa-behance"></i></a></li>
					<li class="gplus"><a title="" data-toggle="tooltip" href="<?php echo esc_url($widget_plus);?>" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
			
			<?php
					
			// Closing of widget
			echo $args['after_widget'];	
		}
					
		
		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$widget_facebook = isset($instance['widget_facebook'])? $instance['widget_facebook']: '';
			$widget_twitter = isset($instance['widget_twitter'])? $instance['widget_twitter']: '';
			$widget_youtube = isset($instance['widget_youtube'])? $instance['widget_youtube']: '';
			$widget_dribble = isset($instance['widget_dribble'])? $instance['widget_dribble']: '';
			$widget_behance = isset($instance['widget_behance'])? $instance['widget_behance']: '';
			$widget_plus = isset($instance['widget_plus'])? $instance['widget_plus']: '';
			
			?>

			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'the-event'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>	
			
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_facebook')); ?>"><?php esc_html_e('Facebook :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_facebook')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_facebook')); ?>" type="text" value="<?php echo esc_attr($widget_facebook); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_twitter')); ?>"><?php esc_html_e('Twitter :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_twitter')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_twitter')); ?>" type="text" value="<?php echo esc_attr($widget_twitter); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_youtube')); ?>"><?php esc_html_e('YouTube :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_youtube')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_youtube')); ?>" type="text" value="<?php echo esc_attr($widget_youtube); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_dribble')); ?>"><?php esc_html_e('Dribble :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_dribble')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_dribble')); ?>" type="text" value="<?php echo esc_attr($widget_dribble); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_behance')); ?>"><?php esc_html_e('Behance :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_behance')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_behance')); ?>" type="text" value="<?php echo esc_attr($widget_behance); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_plus')); ?>"><?php esc_html_e('Google Plus :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_plus')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_plus')); ?>" type="text" value="<?php echo esc_attr($widget_plus); ?>" />
			</p>
				


		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['widget_facebook'] = (empty($new_instance['widget_facebook']))? '': strip_tags($new_instance['widget_facebook']);	
			$instance['widget_twitter'] = (empty($new_instance['widget_twitter']))? '': strip_tags($new_instance['widget_twitter']);
			$instance['widget_youtube'] = (empty($new_instance['widget_youtube']))? '': strip_tags($new_instance['widget_youtube']);
			$instance['widget_dribble'] = (empty($new_instance['widget_dribble']))? '': strip_tags($new_instance['widget_dribble']);
			$instance['widget_behance'] = (empty($new_instance['widget_behance']))? '': strip_tags($new_instance['widget_behance']);
			$instance['widget_plus'] = (empty($new_instance['widget_plus']))? '': strip_tags($new_instance['widget_plus']);
			return $instance;
		}	
	}
}
?>