<?php
/**
 * Plugin Name: Kodeforest Recent Post
 * Plugin URI: http://kodeforest.com/
 * Description: A widget that show recent posts( Specified by category ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'theevent_contact_us_widget' );
if( !function_exists('theevent_contact_us_widget') ){
	function theevent_contact_us_widget() {
		register_widget( 'Kodeforest_Contact_Us' );
	}
}

if( !class_exists('Kodeforest_Contact_Us') ){
	class Kodeforest_Contact_Us extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'theevent_contact_us_widget', 
				esc_html__('Kodeforest Contact Us Widget','the-event'), 
				array('description' => esc_html__('A widget that show contact us information.', 'the-event')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
			global $theevent_theme_option;	
				
			$title = apply_filters( 'widget_title', $instance['title'] );
			$widget_desc = $instance['widget_desc'];
			$widget_address = $instance['widget_address'];
			$widget_phone = $instance['widget_phone'];
			$widget_email = $instance['widget_email'];
			

			// Opening of widget
			echo $args['before_widget'];
			
			
			?>
			<div class="kf_event_footer">
				<figure>
					<?php
					// Open of title tag
					if( !empty($title) ){ 
						echo '<img alt="footer-logo" src="'.esc_attr($title).'">'; 
					}else{
						echo '<img alt="footer-logo" src="'.THEEVENT_PATH.'/images/logo.png">'; 
					}
					?>					
				</figure>
				<p><?php echo esc_attr($widget_desc);?></p>
				<ul class="kf_event_social">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
					<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
				</ul>
				<ul class="kf_event_contact">
					<li><i class="fa fa-envelope-o"></i><a href="#"><?php echo esc_attr($widget_email);?></a></li>
					<li><i class="fa fa-phone"></i> <?php echo esc_attr($widget_phone);?></li>
					<li><i class="fa fa-map-marker"></i> <?php echo esc_attr($widget_address);?></li>
				</ul>
			</div>
			<!--// TextWidget //-->
			
			<?php
					
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$widget_desc = isset($instance['widget_desc'])? $instance['widget_desc']: '';
			$widget_address = isset($instance['widget_address'])? $instance['widget_address']: '';
			$widget_phone = isset($instance['widget_phone'])? $instance['widget_phone']: '';
			$widget_email = isset($instance['widget_email'])? $instance['widget_email']: '';
			
			?>

			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Image URL :', 'the-event'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>			
			<!-- Widget Icon --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>"><?php esc_html_e('Widget Description :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_desc')); ?>" type="text" value="<?php echo esc_attr($widget_desc); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_address')); ?>"><?php esc_html_e('Address :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_address')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_address')); ?>" type="text" value="<?php echo esc_attr($widget_address); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>"><?php esc_html_e('Phone :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_phone')); ?>" type="text" value="<?php echo esc_attr($widget_phone); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_email')); ?>"><?php esc_html_e('Email :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_email')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_email')); ?>" type="text" value="<?php echo esc_attr($widget_email); ?>" />
			</p>

		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['widget_desc'] = (empty($new_instance['widget_desc']))? '': strip_tags($new_instance['widget_desc']);	
			$instance['widget_address'] = (empty($new_instance['widget_address']))? '': strip_tags($new_instance['widget_address']);
			$instance['widget_phone'] = (empty($new_instance['widget_phone']))? '': strip_tags($new_instance['widget_phone']);
			$instance['widget_email'] = (empty($new_instance['widget_email']))? '': strip_tags($new_instance['widget_email']);
					

			return $instance;
		}	
	}
}
?>