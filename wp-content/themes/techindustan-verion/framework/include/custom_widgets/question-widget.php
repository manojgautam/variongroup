<?php
/**
 * Plugin Name: Kodeforest Question Wiget
 * Plugin URI: http://kodeforest.com/
 * Description: A widget that show Have a Question( Specified by category ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'theevent_question_widget' );
if( !function_exists('theevent_question_widget') ){
	function theevent_question_widget() {
		register_widget( 'Kodeforest_Question' );
	}
}

if( !class_exists('Kodeforest_Question') ){
	class Kodeforest_Question extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'theevent_question_widget', 
				esc_html__('Kodeforest Question Widget','the-event'), 
				array('description' => esc_html__('A widget that show Have A Question information.', 'the-event')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
			global $theevent_theme_option;	
				
			$title = apply_filters( 'widget_title', $instance['title'] );
			$widget_desc = $instance['widget_desc'];
			$widget_url = $instance['widget_url'];
			$widget_question = $instance['widget_question'];

			// Opening of widget
			echo $args['before_widget'];
			
			
			?>
			<!--// TextWidget //-->
			
			<div class="kode-ask-question">
				<h3><?php echo esc_attr($title)?></h3>
				<p><?php echo esc_attr($widget_desc);?></p>
				<a href="<?php echo esc_attr($widget_url);?>"><?php echo esc_attr($widget_question);?></a>
			</div>
			
			<?php
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$widget_desc = isset($instance['widget_desc'])? $instance['widget_desc']: '';
			$widget_url = isset($instance['widget_url'])? $instance['widget_url']: '';
			$widget_question = isset($instance['widget_question'])? $instance['widget_question']: '';
			
			?>

			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'the-event'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>			
			<!-- Widget Icon --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>"><?php esc_html_e('Widget Description :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_desc')); ?>" type="text" value="<?php echo esc_attr($widget_desc); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_url')); ?>"><?php esc_html_e('URL of Button :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_url')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_url')); ?>" type="text" value="<?php echo esc_attr($widget_url); ?>" />
			</p>
			<!-- Widget Link --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_question')); ?>"><?php esc_html_e('Button Text :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_question')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_question')); ?>" type="text" value="<?php echo esc_attr($widget_question); ?>" />
			</p>

		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['widget_desc'] = (empty($new_instance['widget_desc']))? '': strip_tags($new_instance['widget_desc']);	
			$instance['widget_url'] = (empty($new_instance['widget_url']))? '': strip_tags($new_instance['widget_url']);
			$instance['widget_question'] = (empty($new_instance['widget_question']))? '': strip_tags($new_instance['widget_question']);
			
			return $instance;
		}	
	}
}
?>