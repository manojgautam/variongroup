<?php
	/*	
	*	Kodeforest Load Style
	*	---------------------------------------------------------------------
	*	This file create the custom style
	*	For Color Scheme, and Typography Options
	*	---------------------------------------------------------------------
	*/	
		
		
		// load the style using this file
		if(!is_admin()){
			//for Frontend only
			// add_action('wp_footer', 'theevent_generate_style_custom');
		}
		
		$font_options_slug = 'font_option_slug';
		add_action('thedemocracy_save_' + $font_options_slug, 'theevent_save_font_options');
		if( !function_exists('theevent_save_font_options') ){
			function theevent_save_font_options($options){
				
				
				// for multisite
				$file_url = get_template_directory() . '/css/style-custom.css';
				if( is_multisite() && get_current_blog_id() > 1 ){
					$file_url = get_template_directory() . '/css/style-custom' . get_current_blog_id() . '.css';
				}
				
				// write file content
				$theevent_theme_option = get_option('theevent_admin_option', array());
				
				// for updating google font list to use on front end
				global $theevent_font_controller; $google_font_list = array(); 
				
				foreach( $options as $menu_key => $menu ){
					foreach( $menu['options'] as $submenu_key => $submenu ){
						if( !empty($submenu['options']) ){
							foreach( $submenu['options'] as $option_slug => $option ){
								if( !empty($option['selector']) ){
									// prevents warning message
									$option['data-type'] = (empty($option['data-type']))? 'color': $option['data-type'];
									
									// if( !empty($theevent_theme_option[$option_slug]) ){
										// $value = theevent_check_option_data_type($theevent_theme_option[$option_slug], $option['data-type']);
									// }else{
										// $value = '';
									// }
									// if($value){
										// $theevent_font = str_replace('#kode#', $value, $option['selector']) . "\r\n";
									// }
									
									// updating google font list
									if( $menu_key == 'font-settings' && $submenu_key == 'font-family' ){
										if( !empty($theevent_font_controller->google_font_list[$theevent_theme_option[$option_slug]]) ){
											$google_font_list[$theevent_theme_option[$option_slug]] = $theevent_font_controller->google_font_list[$theevent_theme_option[$option_slug]];
										}
									}
								}
							}
						}
					}
				}
				
				// update google font list
				update_option('theevent_google_font_list', $google_font_list);	
				$render_style = theevent_generate_style_custom($theevent_theme_option,$page_options='no');
				
				$current_page =  wp_nonce_url(admin_url('admin.php?import=kodeforest-importer'),'the-event');
				if( !theevent_write_filesystem($current_page, $file_url, $render_style) ){
					$ret = array(
						'status'=>'failed', 
						'message'=> '<span class="head">' . esc_html__('Cannot write style-custom.css file', 'the-event') . '</span> ' .
							esc_html__('Please contact the administrator.' ,'the-event')
					);	
					
					die(json_encode($ret));		
				}
				
			}
		}
		
		$theevent_allowed_html_array = array(
			'a' => array(
				'href' => array(),
				'title' => array()
			),
			'ul' => array(),
			'li' => array(),
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'&gt;' => array(),
			'style' => array(
				'scoped'=>array(),
				'id'=>array(),
			),
		);	
		
		
		if( !function_exists('theevent_generate_style_custom') ){
			function theevent_generate_style_custom( $theevent_theme_options,$page_options= "" ){				
				// write file content
				if($page_options == 'Yes'){
					$theevent_theme_option = $theevent_theme_options;
					$k_option = 'custom_style';
					if(isset($_GET['layout']) && $_GET['layout'] == 'boxed'){
						$theevent_theme_option['enable-boxed-style'] = 'boxed-style';
					}else{
						$theevent_theme_option['enable-boxed-style'] = 'full-style';
					}
				}else{
					$theevent_theme_option = get_option('theevent_admin_option', array());
					$k_option = 'default_stylesheet';
				}
				
				// $style = '<style id="'.$k_option.'" scoped>';
				$style = '';
				
				if(!empty($theevent_theme_option['logo-width'])){
					if($theevent_theme_option['logo-width'] <> ''){
						if($theevent_theme_option['logo-width'] <> 0){
							$style .= '.logo img{';
							$style .= 'width:'.esc_attr($theevent_theme_option['logo-width']) .'px';
							$style .= '}' . "\r\n";
						}
						
					}
				}
				
				if(!empty($theevent_theme_option['logo-height'])){
					if($theevent_theme_option['logo-height'] <> ''){
						if($theevent_theme_option['logo-height'] <> 0){
							$style .= '.logo img{';
							$style .= 'height:'.esc_attr($theevent_theme_option['logo-height']) .'px';
							$style .= '}' . "\r\n";
						}
					}
				}
				
				if(!empty($theevent_theme_option['navi-font-family'])){
					if($theevent_theme_option['navi-font-family'] <> ''){
						$style .= '.kode-navigation-wrapper, .kode-navigation-wrapper ul li a, .kode-navigation-wrapper ul li{';
						$style .= 'font-family:'.esc_attr($theevent_theme_option['navi-font-family']) .'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['heading-font-family'])){
					if($theevent_theme_option['heading-font-family'] <> ''){
						$style .= '.kode-caption-title, .kode-caption-text, h1, h2, h3, h4, h5, h6{';
						$style .= 'font-family:'.esc_attr($theevent_theme_option['heading-font-family']) .'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['body-font-family'])){
					if($theevent_theme_option['body-font-family'] <> ''){
						$style .= 'body{';
						$style .= 'font-family:'.esc_attr($theevent_theme_option['body-font-family']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['content-font-size'])){
					if($theevent_theme_option['content-font-size'] <> ''){
						$style .= 'body{';
						$style .= 'font-size:'.esc_attr($theevent_theme_option['content-font-size']) .'px';
						$style .= '}' . "\r\n";
					}
				}
				
				if(!empty($theevent_theme_option['content-font-weight'])){
					if($theevent_theme_option['content-font-weight'] <> ''){
						$style .= 'body{';
						$style .= 'font-weight:'.esc_attr($theevent_theme_option['content-font-weight']) .'';
						$style .= '}' . "\r\n";
					}
				}
				
				if(!empty($theevent_theme_option['h1-font-size'])){
					if($theevent_theme_option['h1-font-size'] <> ''){
						$style .= 'h1{';
						$style .= 'font-size:'.esc_attr($theevent_theme_option['h1-font-size']).'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['h1-font-weight'])){
					if($theevent_theme_option['h1-font-weight'] <> ''){
						$style .= 'h1{';
						$style .= 'font-weight:'.esc_attr($theevent_theme_option['h1-font-weight']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				
				
				
				if(!empty($theevent_theme_option['h2-font-size'])){
					if($theevent_theme_option['h2-font-size'] <> ''){
						$style .= 'h2{';
						$style .= 'font-size:'.esc_attr($theevent_theme_option['h2-font-size']).'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['h2-font-weight'])){
					if($theevent_theme_option['h2-font-weight'] <> ''){
						$style .= 'h2{';
						$style .= 'font-weight:'.esc_attr($theevent_theme_option['h2-font-weight']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				
				if(!empty($theevent_theme_option['h3-font-size'])){
					if($theevent_theme_option['h3-font-size'] <> ''){
						$style .= 'h3{';
						$style .= 'font-size:'.esc_attr($theevent_theme_option['h3-font-size']).'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['h3-font-weight'])){
					if($theevent_theme_option['h3-font-weight'] <> ''){
						$style .= 'h3{';
						$style .= 'font-weight:'.esc_attr($theevent_theme_option['h3-font-weight']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				
				if(!empty($theevent_theme_option['h4-font-size'])){
					if($theevent_theme_option['h4-font-size'] <> ''){
						$style .= 'h4{';
						$style .= 'font-size:'.esc_attr($theevent_theme_option['h4-font-size']).'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['h4-font-weight'])){
					if($theevent_theme_option['h4-font-weight'] <> ''){
						$style .= 'h4{';
						$style .= 'font-weight:'.esc_attr($theevent_theme_option['h4-font-weight']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				
				if(!empty($theevent_theme_option['h5-font-size'])){
					if($theevent_theme_option['h5-font-size'] <> ''){
						$style .= 'h5{';
						$style .= 'font-size:'.esc_attr($theevent_theme_option['h5-font-size']).'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['h5-font-weight'])){
					if($theevent_theme_option['h5-font-weight'] <> ''){
						$style .= 'h5{';
						$style .= 'font-weight:'.esc_attr($theevent_theme_option['h5-font-weight']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				
				if(!empty($theevent_theme_option['h6-font-size'])){
					if($theevent_theme_option['h6-font-size'] <> ''){
						$style .= 'h6{';
						$style .= 'font-size:'.esc_attr($theevent_theme_option['h6-font-size']).'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['h6-font-weight'])){
					if($theevent_theme_option['h6-font-weight'] <> ''){
						$style .= 'h6{';
						$style .= 'font-weight:'.esc_attr($theevent_theme_option['h6-font-weight']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				
				
				
				
				
				if(isset($theevent_theme_option['caption-background-color-switch']) && $theevent_theme_option['caption-background-color-switch'] == 'enable'){
					if(isset($theevent_theme_option['caption-background-color'])){
						$style .= '.kode-caption .kode-caption-title:before,.kode-caption .kode-caption-title::after{';
						$style .= 'border-bottom-color:'.esc_attr($theevent_theme_option['caption-background-color']).' !important';
						$style .= '}' . "\r\n";				
						
						$style .= '.kode-caption .kode-caption-title{';
						$style .= 'background-color:'.esc_attr($theevent_theme_option['caption-background-color']).' !important';
						$style .= '}' . "\r\n";				
					}
				}
					
				if(isset($theevent_theme_option['caption-btn-color-border'])){
					$style .= '.kode-linksection, .kode-modren-btn{';
					$style .= 'border-color:'.esc_attr($theevent_theme_option['caption-btn-color-border']).' !important';
					$style .= '}' . "\r\n";		
				}
				
				if(isset($theevent_theme_option['caption-btn-color-bg'])){
					$style .= '.kode-linksection, .kode-modren-btn{';
					$style .= 'background:'.esc_attr($theevent_theme_option['caption-btn-color-bg']).' !important';
					$style .= '}' . "\r\n";		
				}
				
				
				if(!empty($theevent_theme_option['navi-color'])){
					if($theevent_theme_option['navi-color'] <> ''){
						$style .= '.navigation ul > li > a, .navbar-nav > li > a{';
						$style .= 'color:'.esc_attr($theevent_theme_option['navi-color']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['navi-hover-bg'])){
					if($theevent_theme_option['navi-hover-bg'] <> ''){
						$style .= '.navigation ul > li:hover > a{';
						$style .= 'background-color:'.esc_attr($theevent_theme_option['navi-hover-bg']).'';
						$style .= '}' . "\r\n";
					}
				}
				
				if(!empty($theevent_theme_option['navi-dropdown-hover-bg'])){
					if($theevent_theme_option['navi-dropdown-hover-bg'] <> ''){
						$style .= '.navigation ul.sub-menu > li:hover > a{';
						$style .= 'background-color:'.esc_attr($theevent_theme_option['navi-dropdown-hover-bg']).' !important';
						$style .= '}' . "\r\n";
					}
				}
				
				if(!empty($theevent_theme_option['top-bar-background-color'])){
					if($theevent_theme_option['top-bar-background-color'] <> ''){
						$style .= ' .kode_top_strip2{';
						$style .= 'background:'.esc_attr($theevent_theme_option['top-bar-background-color']).' !important';
						$style .= '}' . "\r\n";
					}
				}
				
				
				
				
				
				if(!empty($theevent_theme_option['navi-hover-color'])){
					if($theevent_theme_option['navi-hover-color'] <> ''){
						$style .= '.navigation ul > li:hover > a, .navbar-nav > li:hover{';
						$style .= 'color:'.esc_attr($theevent_theme_option['navi-hover-color']).' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['navi-dropdown-bg'])){
					if($theevent_theme_option['navi-dropdown-bg'] <> ''){
						$style .= '.navigation .sub-menu, .navigation .children, .navbar-nav .children{';
						$style .= 'background:'.esc_attr($theevent_theme_option['navi-dropdown-bg']).' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				if(!empty($theevent_theme_option['navi-dropdown-hover'])){
					if($theevent_theme_option['navi-dropdown-hover'] <> ''){
						$style .= '.navigation .menu .sub-menu li:hover a:before, .navigation .sub-menu li:hover a:before, .navbar-nav .children li:hover a:before{ ';
						$style .= 'color:'.esc_attr($theevent_theme_option['navi-dropdown-hover']).' !important';
						$style .= ' }' . "\r\n";
						
					}
				}

				if(!empty($theevent_theme_option['navi-dropdown-hover'])){
					if($theevent_theme_option['navi-dropdown-hover'] <> ''){
						$style .= '.navigation ul li ul > li:hover > a, .navbar-nav li ul > li:hover > a{ ';
						$style .= 'color:'.esc_attr($theevent_theme_option['navi-dropdown-hover']).' !important';
						$style .= ' }' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['navi-dropdown-color'])){
					if($theevent_theme_option['navi-dropdown-color'] <> ''){
						$style .= '.navigation ul > li ul li > a, .navbar-nav > li ul li > a .navigation .sub-menu a, .navigation .children , .navbar-nav .children a{ ';
						$style .= 'color:'.esc_attr($theevent_theme_option['navi-dropdown-color']).'';
						$style .= ' }' . "\r\n";
						
					}
				}			
				
				if(!empty($theevent_theme_option['top-bar-background-color'])){
					if($theevent_theme_option['top-bar-background-color'] <> ''){
						$style .= '#header-style-3 .kode_top_strip:before, .kode_header_7 .kode_top_eng:before{';
						$style .= 'background:'.esc_attr($theevent_theme_option['top-bar-background-color']).' !important';
						$style .= '}' . "\r\n";
					}
				}
				
				if(!empty($theevent_theme_option['top-bar-text-color'])){
					if($theevent_theme_option['top-bar-text-color'] <> ''){
						$style .= '.kode_header_5 ul.kode_social_icon li a i{';
						$style .= 'color:'.esc_attr($theevent_theme_option['top-bar-text-color']).' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['header-background-opacity'])){
					if($theevent_theme_option['header-background-opacity'] <> ''){
						$style .= '#header-style-3 .kode_top_strip:before, .kode_header_7 .kode_top_eng:before{';
						$style .= 'opacity:'.esc_attr($theevent_theme_option['header-background-opacity']).' !important';
						$style .= '}' . "\r\n";
					}
				}
				
				
				//Background As Pattern
				if(isset($theevent_theme_option['header-background-image'])){
					if($theevent_theme_option['header-background-image'] <> ''){
						if( !empty($theevent_theme_option['header-background-image']) && is_numeric($theevent_theme_option['header-background-image']) ){
							$alt_text = get_post_meta($theevent_theme_option['header-background-image'] , '_wp_attachment_image_alt', true);	
							$image_src = wp_get_attachment_image_src($theevent_theme_option['header-background-image'], 'full');
							
							$style .= '.kode_header_7 .kode_top_eng, .kode_top_strip{background:url(' . esc_url($image_src[0]).')}';
						}else if( !empty($theevent_theme_option['body-background-pattern']) ){
							$style .= '.kode_header_7 .kode_top_eng, .kode_top_strip{background:url(' . $theevent_theme_option['header-background-image'].')}';
						}
					}
				}
				
				//Background As Pattern
				if(isset($theevent_theme_option['kode-body-style'])){
					if($theevent_theme_option['kode-body-style'] == 'body-pattern'){
						if($theevent_theme_option['body-background-pattern'] <> ''){
							if( !empty($theevent_theme_option['body-background-pattern']) && is_numeric($theevent_theme_option['body-background-pattern']) ){
								// $alt_text = get_post_meta($theevent_theme_option['body-background-pattern'] , '_wp_attachment_image_alt', true);	
								// $image_src = wp_get_attachment_image_src($theevent_theme_option['body-background-image'], 'full');
								$style .= 'body{background:url(' . esc_url(THEEVENT_PATH . '/images/pattern/pattern_'.$theevent_theme_option['body-background-pattern'].'.png') . ')}';
							}else if( !empty($theevent_theme_option['body-background-pattern']) ){
								$style .= 'body{background:url(' . esc_url(THEEVENT_PATH . '/images/pattern/pattern_'.$theevent_theme_option['body-background-pattern'].'.png') . ')}';
							}
						}
					}
				}
				
				//Background As Image
				if(isset($theevent_theme_option['kode-body-style'])){
					if($theevent_theme_option['kode-body-style'] == 'body-background'){
						if($theevent_theme_option['body-background-image'] <> ''){
							if( !empty($theevent_theme_option['body-background-image']) && is_numeric($theevent_theme_option['body-background-image']) ){
								$alt_text = get_post_meta($theevent_theme_option['body-background-image'] , '_wp_attachment_image_alt', true);	
								$image_src = wp_get_attachment_image_src($theevent_theme_option['body-background-image'], 'full');
								$style .= 'body{background:url(' . esc_url($image_src[0]) . ')}';
								if($theevent_theme_option['kode-body-position'] == 'body-scroll'){
									$style .= 'body{background-attachment:scroll !important}';
								}else{
									$style .= 'body{background-attachment:fixed !important}';
								}
							}else if( !empty($theevent_theme_option['body-background-image']) ){
								$style .= 'body{background:url(' . esc_url($theevent_theme_option['body-background-image']) . ')}';
								if($theevent_theme_option['kode-body-position'] == 'body-scroll'){
									$style .= 'body{background-attachment:scroll !important}';
								}else{
									$style .= 'body{background-attachment:fixed !important}';
								}
							}
						}
					}
				}
				
				//Background As Color
				if(isset($theevent_theme_option['body-bg-color'])){
					if($theevent_theme_option['body-bg-color'] <> ''){
						$style .= 'body { ';
						$style .= 'background-color: ' . esc_attr($theevent_theme_option['body-bg-color']) . ';  }' . "\r\n";
					}
				}
				

				if(!empty($theevent_theme_option['enable-boxed-style'])){
					if($theevent_theme_option['enable-boxed-style'] == 'boxed-style'){
						if( !empty($theevent_theme_option['boxed-background-image']) && is_numeric($theevent_theme_option['boxed-background-image']) ){
							$alt_text = get_post_meta($theevent_theme_option['boxed-background-image'] , '_wp_attachment_image_alt', true);	
							$image_src = wp_get_attachment_image_src($theevent_theme_option['boxed-background-image'], 'full');
							$style .= 'body{background:url(' . esc_url($image_src[0]) . ')}';
						}else if( !empty($theevent_theme_option['boxed-background-image']) ){
							$style .= 'body{background:url(' . esc_url($theevent_theme_option['boxed-background-image']) . ')}';
						}
						
						$style .= '.logged-in.admin-bar .body-wrapper .kode-header-1{';
						$style .= 'margin-top:0px !important;';
						$style .= '}' . "\r\n";
						$style .= '.body-wrapper .kode-topbar:before{width:25%;}';
						$style .= '.body-wrapper #footer-widget .kode-widget-bg-footer:before{width:23em;}';
						$style .= '.body-wrapper .eccaption{top:40%;}';
						$style .= '.body-wrapper .main-content{background:#fff;}';
						$style .= '.body-wrapper {';
						$style .= 'background:#fff;width: 1200px;overflow:hidden; margin: 0 auto; margin-top: 40px; margin-bottom: 40px; box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.2);position:relative;';
						$style .= '}' . "\r\n";
					}
				}
				
				
				if(isset($theevent_theme_option['footer-background-opacity'])){
					if($theevent_theme_option['footer-background-opacity'] <> ''){
						$style .= 'footer:before{ ';
						$style .= ' content: "";
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;opacity: ' . esc_attr($theevent_theme_option['footer-background-opacity']) . ';  }' . "\r\n";
					}
				}
				

				
				//Background As Color
				if(isset($theevent_theme_option['footer-background-color'])){				
					if($theevent_theme_option['footer-background-color'] <> ''){
						
						$style .= 'footer:before{ ';
						$style .= 'background-color: ' . esc_attr($theevent_theme_option['footer-background-color'] ). ';  }' . "\r\n";
						
					}
				}
				
				if(!empty($theevent_theme_option['footer-background-image'])){
					if( !empty($theevent_theme_option['footer-background-image']) && is_numeric($theevent_theme_option['footer-background-image']) ){
						$alt_text = get_post_meta($theevent_theme_option['footer-background-image'] , '_wp_attachment_image_alt', true);	
						$image_src = wp_get_attachment_image_src($theevent_theme_option['footer-background-image'], 'full');
						$style .= 'footer{background:url(' . esc_url($image_src[0]) . ');background-size: cover;float: left;position: relative;width: 100%;}';
					}else if( !empty($theevent_theme_option['footer-background-image']) ){
						$style .= 'footer{background:url(' . esc_url($theevent_theme_option['footer-background-image']) . ');background-size: cover;float: left;position: relative;width: 100%;}';
					}
				}
				
				if(isset($theevent_theme_option['logo-bottom-margin'])){
					if($theevent_theme_option['logo-bottom-margin'] <> ''){
						$style .= 'a.logo{';
						$style .= 'margin-bottom: ' . esc_attr($theevent_theme_option['logo-bottom-margin']) . 'px;  }' . "\r\n";
					}
				}
				
				if(isset($theevent_theme_option['logo-top-margin'])){
					if($theevent_theme_option['logo-top-margin'] <> ''){
						$style .= 'a.logo{';
						$style .= 'margin-top: ' . esc_attr($theevent_theme_option['logo-top-margin']) . 'px;  }' . "\r\n";
					}
				}
				
				if(isset($theevent_theme_option['kode-top-bar-trans'])){
					if($theevent_theme_option['kode-top-bar-trans'] == 'colored'){
						if(isset($theevent_theme_option['top-bar-background-color'])){
							if($theevent_theme_option['top-bar-background-color'] <> ''){
								$style .= '.top-strip{';
								$style .= 'background-color: ' . esc_attr($theevent_theme_option['top-bar-background-color']) . ';  }' . "\r\n";
							}
						}
					}
				}
				
				if(isset($theevent_theme_option['color-scheme-one'])){
					if(empty($theevent_theme_option['color-scheme-one'])){
						$theevent_theme_option['color-scheme-one'] = '#03AF14';
					}
				}
				
				
				if(isset($theevent_theme_option['color-scheme-one'])){
					if($theevent_theme_option['color-scheme-one'] <> ''){
						
						$style .= '/*
============================================================
		  				 Font Color
 ============================================================
*/
.kf_testimonial_des p:before, .aside_widget_hdg h6 span, .kf_blog_detail_des ul li a span, .kf_comment_des > a, .kf_blog_detail_des ul li:hover a, .kf_comment_des > ul > li:hover > a > h6, .kf_comment_des > ul > li:hover > a, .kf_accordian_hdg h5:before, /*Amir Code Classes*/
.blog-2-side-bar ul li:hover strong, .kode-fig-capstion > a:hover, .kode-team-blog figure > a :hover, .kode-thumb-caption li:hover a i, .our-speaker-social-section:hover > span, .our-speaker-social-section a:hover, .blog-2-side-bar strong:hover, .kode-fig-capstion > a:hover, .kode-team-blog figure > a :hover, .pag.pagination > li > a:hover, .kode-thumb-caption li:hover a i, .kode-loading-link > a:hover, .our-speaker-social-section:hover > span, .our-speaker-social-section a:hover, .contactusform h3, .main-service-box:hover .icon-srvc, .auditorium a:hover, .auditorium li.active a, .scheduletabs .panel-heading, .scheduletabs .announcer, .overcontent ul a, 
.kode-price-box:hover .kode-price-header h3, 
.active .kode-price-box .kode-price-header h3, 
.kode-blog-newcontent .meta-event, 
.user-small h5 a,
.kode-footer-text .title,
.address-nav .fa, 
.kf_accordian_hdg.accordion-open:before,
.twitterfeed-nav a, 
.kode-error > h3, 
.main-service-box .icon-srvc i,
.kode-price-content a:hover,
.kf_action3_wrap span i,
.kf_pet_callus ul li i,
.kf_pet_callus ul li:hover a,
.kode-health-over li i,
.accordion_des_capstion strong i, .accordion_des_capstion span i, .accordion_capstion_heading h5 i,
.ticket-column:hover li span,
footer strong.title,
.kf_event_testi_wrap:hover h4 > a,
.kf_action3_wrap:hover span i,
.caption-holder a:hover,
.kf_event_member > a:hover,
.kode_event_list:hover a,
.kode-blog-newcontent .post-meta-data .blog-info:hover a,
.kf_experience_list:hover > a,
.kode-next.thcolorhover.next-nav.inner-post a:hover{';
						$style .= 'color: ' . esc_attr($theevent_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";

						$style .= '/*
============================================================
		   			Background Color
============================================================
*/
.kode-pagination .page-numbers.current,
.kf_speaker_socil, .kf_heading_1 i:before, .kf_experience_list:before, .kf_experience_list span, .owl-theme.kf_testimonial_bg .owl-controls .owl-page.active span, .owl-theme.kf_testimonial_bg .owl-controls .owl-page:hover span, .kf_aside_twitter, .kode_calender .fc-toolbar, .blog_img_year, .kf_reply_field input[type="submit"], .kf_reply_field button, .kode_about_bg, .register-button, #kode-gallery-causel .owl-controls .owl-buttons div:hover, .kf_heading_3 span:before, #kode-testimonial-causel .owl-controls .owl-buttons div:hover, 
.back-to-top,
.col-right .button-member, .button.active, .button:hover, .kode_about_bg, .kode-thumb-caption span a:hover, 
.kode-blog-2 figure:after, 
.kode-contact-area > a, 
.register-button:hover, 
.kode-comments-area form.comment-form p.form-submit .submit,
.eventless-today,
.kf_new_conf_btn_link a,
.em-booking-submit,
#em_wp-submit,
.first-letter,.kode_wel_outr_wrap p:first-letter, 
.kf_heading_4 h2::before,
.pag .pagination > li:hover a, 
.kf_gallery figure, 
.col-right .button-member, 
.button.active, .button:hover, 
.book_event_form_wrap input[type="button"],
.kode-tickets-title,
#em-booking-submit, #em_wp-submit:hover,
.kode_about_bg, .kode-thumb-caption span a:hover, 
.speaker-event-content ul li a:hover, .speaker-event-content ul li a:focus, .speaker-event-content ul li.active a,
.kode-blog-2 figure:after, .kode-contact-area > a, 
.register-button:hover, 
.first-letter, .kode-nav li.active > a, 
.kf_faq_banner .bx-wrapper .bx-controls-direction a:hover, 
.button-animate span::before, 
.button-animate span, .main-service-box:hover, 
.button-slide:hover, .button-slide:focus, 
.button-slide.active, .scheduletabs .nav-tabs > li.active > a, .scheduletabs .nav-tabs > li.active > a:hover, .scheduletabs .nav-tabs > li.active > a:focus, .scheduletabs .nav-tabs > li.active > a:after, .scheduletabs .nav-tabs > li.active > a:hover:after, .scheduletabs .nav-tabs > li.active > a:focus:after, 
.auditorium li.active a:after,
.auditorium a:hover:after, 
.find-us-heder, 
.subscribe-form input[type="submit"], 
.new-ourspeaker.over-state, 
.kode-price-footer a:hover, 
.kode-price-box:hover .kode-price-footer a, 
.kode-event-testimonial .border-left, 
.time-newevent, 
.kode_button_link_3:hover,
.kode-newblog-footer a:hover, 
.kode-event-contact .border-left, 
.contactusform input[type="submit"], 
.kode-ask-question a, 
.footer-copyright, 
.kode-welcome .icon-srvc, 
.col-right .button-member, 
.event-slider-box .bx-wrapper .bx-pager.bx-default-pager a:hover,
.event-slider-box .bx-wrapper .bx-pager.bx-default-pager a.active,
.event-slider-box .bx-wrapper .bx-pager.bx-default-pager a:focus, 
.bottom-border, 
.overcontent:before, 
.kode-error > a:hover, 
.filter-item li a.active,
.filter-item li a:hover,
.filter-item li a:focus, 
.lode-more, 
.counter-meta,
.kode-loading-link, 
.kode-health-caption > a, 
#mobile-header:hover, .header-social a:hover, 
.kode-btn-style-1, 
.thbg-color,
.widget_categories ul li:hover,
.kf_heading_2 span::before,
.kf_faq_accordian .kf_accordian_hdg.accordion.accordion-open,
.kode-pagination > span:hover,
.kode-pagination > a:hover,
#kodecomments ul li .text a.comment-reply-link:hover,
.em-calendar thead,
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus,
#calendar table th,
.kf_action3_wrap:hover,
.kd-table table thead,
.ticket-column:hover li a,
.menu-shortcodes-container ul.menu li:hover,
.kode_progress .progress .progress-bar,
.evt-blog-cols figure figcaption a,
.accordian_schedule_wrap .accordion-open:before,
.kode-search label,
.kf_event_member > a,
.kode-item.k-content.kode_news_detail .wpcf7-form-control.wpcf7-submit,
.book_event_heading,
.footer_2 .kode-widget h3:before,
.interval_wrap::before, .accordian_schedule_wrap .accordion.accordion-open, 
.dl-menuwrapper button,
.widget_pages ul li:hover > a, .widget_recent_entries ul li:hover a, .widget_nav_menu ul li:hover > a,
#em_wp-submit em-booking-submit:hover {';
						$style .= 'background-color: ' . esc_attr($theevent_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						
						
						$style .= '/*
============================================================
		   			Border Color
============================================================
*/
.kf_blog_detail figure, .blog_img_year:before, #kode-testimonial-causel .owl-controls .owl-buttons div:hover, /*Amir Code Classes*/
.kode-thumb-caption > span:hover, .button.active, .button:hover, .kode-blog-2:hover .kode-thumb-caption::before, .kode-blog-2:hover .kode-thumb-caption::after, .blog-2-side-bar ul li a::after, .blog-2-side-bar ul li:hover a:after, .kode-event-blog3:hover figure:after, .kode-thumb-caption > span:hover, .button.active, .button:hover, .kode-blog-2:hover .kode-thumb-caption::before, .kode-blog-2:hover .kode-thumb-caption::after, .blog-2-side-bar ul li a::after, .blog-2-side-bar ul li a:hover::after, .kode-event-blog3:hover figure::after, .kode-price-box:hover .kode-price-header, .active .kode-price-box .kode-price-header, .kf_faq_banner .bx-wrapper .bx-controls-direction a:hover, .button-slide:hover, .button-slide:focus, .button-slide.active, 
.subscribe-form input[type="submit"], 

.kode-newspeaker:hover .new-ourspeaker.over-state, .kode-newspeaker:hover, 
.kode-newblog-footer a:hover, 
.kode-error > form.kf_404_form .placeholder2:focus,
.kode-error > a, 
.kode-error input[type="search"]:focus, 
.user-small figure,
.clints-brand:hover,
.speaker-event-content ul li a:hover, .speaker-event-content ul li a:focus, .speaker-event-content ul li.active a,
.filter-item li a.active,
.main-service-box .icon-srvc,
.filter-item li a:hover,
.filter-item li a:focus, 
#mobile-header:hover, .header-social a:hover,
.kode-event-blog3 .kode-thumb-caption span a:hover,
#kodecomments ul li .text a.comment-reply-link:hover{';
						$style .= 'border-color: ' . esc_attr($theevent_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						
						$style .= '.kf_pet_navigation ul li a.current{';
						$style .= 'border-bottom-color: ' . esc_attr($theevent_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						
						
						$style .= 'blockquote{ ';
						$style .= 'border-left-color: ' . esc_attr($theevent_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						$style .= '.accordian_schedule_wrap .accordion-open::before{ ';
						$style .= 'border-color: ' . esc_attr($theevent_theme_option['color-scheme-one']) . ' transparent transparent !important;  }' . "\r\n";
						
						$style .= '.kode_politician_des:before, .kode_politician_des:before{ ';
						$style .= 'border-top: 10px solid ' . esc_attr($theevent_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						
						if(isset($theevent_theme_option['navi-font-weight']) && $theevent_theme_option['navi-font-weight'] <> ''){
							$style .= '.kode-nav ul li a{ ';
							$style .= 'font-weight: ' . esc_attr($theevent_theme_option['navi-font-weight']) . ' !important;  }' . "\r\n";
						}
						
						// if(isset($theevent_theme_option['navi-font-weight']) && $theevent_theme_option['navi-font-weight'] <> ''){
							// $style .= '.kode-nav ul li a{ ';
							// $style .= 'font-weight: ' . esc_attr($theevent_theme_option['navi-font-weight']) . ' !important;  }' . "\r\n";
						// }
						
						if(isset($theevent_theme_option['color-scheme-one']) && $theevent_theme_option['color-scheme-one'] <> ''){
							$style .= '.kode_pet_navigation ul.nav li a.current{';
							$style .= 'border-top-color: ' . esc_attr($theevent_theme_option['color-scheme-one'] ). ' !important;  }' . "\r\n";
						}
						
						if(isset($theevent_theme_option['caption-btn-color-hover']) && $theevent_theme_option['caption-btn-color-hover'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover{';
							$style .= 'color: ' . esc_attr($theevent_theme_option['caption-btn-color-hover']) . ' !important;  }' . "\r\n";
						}
						if(isset($theevent_theme_option['caption-btn-color']) && $theevent_theme_option['caption-btn-color'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out{';
							$style .= 'color: ' . esc_attr($theevent_theme_option['caption-btn-color']) . ' !important;  }' . "\r\n";
						}
						
						if(isset($theevent_theme_option['caption-btn-color-border']) && $theevent_theme_option['caption-btn-color-border'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out{';
							$style .= 'outline-color: ' . esc_attr($theevent_theme_option['caption-btn-color-border']) . ' !important;  }' . "\r\n";
						}
						
						if(isset($theevent_theme_option['caption-btn-color-bg']) && $theevent_theme_option['caption-btn-color-bg'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out{';
							$style .= 'background-color: ' . esc_attr($theevent_theme_option['caption-btn-color-bg']) . ' !important;  }' . "\r\n";
						}
						
						if(isset($theevent_theme_option['caption-btn-hover-bg']) && $theevent_theme_option['caption-btn-hover-bg'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover{';
							$style .= 'background-color: ' . esc_attr($theevent_theme_option['caption-btn-hover-bg']) . ' !important;  }' . "\r\n";		
						}

						if(isset($theevent_theme_option['caption-btn-arrow-color']) && $theevent_theme_option['caption-btn-arrow-color'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out i{';
							$style .= 'color: ' . esc_attr($theevent_theme_option['caption-btn-arrow-color']) . ' !important;  }' . "\r\n";		
						}
						
						if(isset($theevent_theme_option['caption-btn-arrow-hover']) && $theevent_theme_option['caption-btn-arrow-hover'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover i{';
							$style .= 'color: ' . esc_attr($theevent_theme_option['caption-btn-arrow-hover']) . ' !important;  }' . "\r\n";		
						}
						
						if(isset($theevent_theme_option['bx-arrow']) && $theevent_theme_option['bx-arrow'] == 'disable'){
							$style .= '.kode-item.kode-slider-item .kode-bxslider .bx-controls-direction{';
							$style .= 'display:none !important;  }' . "\r\n";		
						}
						
						if(isset($theevent_theme_option['caption-btn-arrow-bg']) && $theevent_theme_option['caption-btn-arrow-bg'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out:before{';
							$style .= 'background-color: ' . esc_attr($theevent_theme_option['caption-btn-arrow-bg']) . ' !important;  }' . "\r\n";		
						}
						
						if(isset($theevent_theme_option['caption-btn-arrow-hover-bg']) && $theevent_theme_option['caption-btn-arrow-hover-bg'] <> ''){
							$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover:before{';
							$style .= 'background-color: ' . esc_attr($theevent_theme_option['caption-btn-arrow-hover-bg']) . ' !important;  }' . "\r\n";	
						}
						if(isset($theevent_theme_option['sidebar-tbn']) && $theevent_theme_option['sidebar-tbn'] == 'enable'){
							if(isset($theevent_theme_option['sidebar-bg-color'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'background-color: ' . esc_attr($theevent_theme_option['sidebar-bg-color']) . ' !important;float:left;width:100%;}' . "\r\n";
							}
							
							if(isset($theevent_theme_option['sidebar-padding-top'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-top: ' . esc_attr($theevent_theme_option['sidebar-padding-top']) . 'px !important;}' . "\r\n";
							}
							if(isset($theevent_theme_option['sidebar-padding-bottom'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-bottom: ' . esc_attr($theevent_theme_option['sidebar-padding-bottom']) . 'px !important;}' . "\r\n";
							}
							if(isset($theevent_theme_option['sidebar-padding-left'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-left: ' . esc_attr($theevent_theme_option['sidebar-padding-left']) . 'px !important;}' . "\r\n";
							}
							if(isset($theevent_theme_option['sidebar-padding-right'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-right: ' . esc_attr($theevent_theme_option['sidebar-padding-right']) . 'px !important;}' . "\r\n";
							}
						}
						if(isset($theevent_theme_option['enable-meet-btn']) && $theevent_theme_option['enable-meet-btn'] == 'enable'){
							if(isset($theevent_theme_option['meet-btn-background-color'])){
								$style .= '
								#kode-header .button-member{';
								$style .= 'background-color: ' . esc_attr($theevent_theme_option['meet-btn-background-color']) . ' !important;}' . "\r\n";
							}
							if(isset($theevent_theme_option['meet-btn-hover-background-color'])){
								$style .= '
								#kode-header .button-member:hover{';
								$style .= 'background-color: ' . esc_attr($theevent_theme_option['meet-btn-hover-background-color']) . ' !important;}' . "\r\n";
							}
							if(isset($theevent_theme_option['meet-btn-color'])){
								$style .= '
								#kode-header .button-member{';
								$style .= 'color: ' . esc_attr($theevent_theme_option['meet-btn-color']) . ' !important;}' . "\r\n";
							}
							if(isset($theevent_theme_option['meet-btn-hover-txt-color'])){
								$style .= '
								#kode-header .button-member:hover{';
								$style .= 'color: ' . esc_attr($theevent_theme_option['meet-btn-hover-txt-color']) . ' !important;}' . "\r\n";
							}
						}

						// if(isset($theevent_theme_option['font-heading-h1']) && $theevent_theme_option['font-heading-h1'] <> ''){
							// $style .= '
								// h1{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['font-heading-h1']) . ' !important;}' . "\r\n";					
						// }
						// if(isset($theevent_theme_option['font-heading-h2']) && $theevent_theme_option['font-heading-h2'] <> ''){
							// $style .= '
								// h2{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['font-heading-h2']) . ' !important;}' . "\r\n";					
						// }
						
						// if(isset($theevent_theme_option['font-heading-h3']) && $theevent_theme_option['font-heading-h3'] <> ''){
							// $style .= '
								// h3{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['font-heading-h3']) . ' !important;}' . "\r\n";					
						// }
						// if(isset($theevent_theme_option['font-heading-h4']) && $theevent_theme_option['font-heading-h4'] <> ''){
							// $style .= '
								// h4{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['font-heading-h4']) . ' !important;}' . "\r\n";					
						// }
						// if(isset($theevent_theme_option['font-heading-h5']) && $theevent_theme_option['font-heading-h5'] <> ''){
							// $style .= '
								// h5{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['font-heading-h5']) . ' !important;}' . "\r\n";					
						// }
						// if(isset($theevent_theme_option['font-heading-h1']) && $theevent_theme_option['font-heading-h6'] <> ''){
							// $style .= '
								// h6{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['font-heading-h6']) . ' !important;}' . "\r\n";					
						// }
						
						// if(isset($theevent_theme_option['text-color']) && $theevent_theme_option['text-color'] <> ''){
							// $style .= '
								// body{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['text-color']) . ' !important;}' . "\r\n";					
						// }
						// if(isset($theevent_theme_option['link-color']) && $theevent_theme_option['link-color'] <> ''){
							// $style .= '
								// a{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['link-color']) . ' !important;}' . "\r\n";					
						// }
						// if(isset($theevent_theme_option['link-color']) && $theevent_theme_option['link-color'] <> ''){
							// $style .= '
								// a{';
								// $style .= 'color: ' . esc_attr($theevent_theme_option['link-color']) . ' !important;}' . "\r\n";					
						// }
						
						if(isset($theevent_theme_option['woo-post-title']) && $theevent_theme_option['woo-post-title'] == 'disable'){
							$style .= '.woocommerce-content-item .product .product_title.entry-title{ display:none;}';							
						}
						
						if(isset($theevent_theme_option['woo-post-price']) && $theevent_theme_option['woo-post-price'] == 'disable'){
							$style .= '.woocommerce-content-item div[itemprop="offers"]{ display:none;}';
						}
						
						if(isset($theevent_theme_option['woo-post-variable-price']) && $theevent_theme_option['woo-post-price'] == 'disable'){
							$style .= '.woocommerce-content-item div[itemprop="offers"]{ display:none;}';
						}
						
						if(isset($theevent_theme_option['woo-post-related']) && $theevent_theme_option['woo-post-related'] == 'disable'){
							$style .= '.woocommerce-content-item .product .related.products{ display:none;}';
						}
						
						if(isset($theevent_theme_option['woo-post-sku']) && $theevent_theme_option['woo-post-sku'] == 'disable'){
							$style .= '.woocommerce-content-item .product .sku_wrapper{ display:none;}';
						}
						
						if(isset($theevent_theme_option['woo-post-category']) && $theevent_theme_option['woo-post-category'] == 'disable'){
							$style .= '.woocommerce-content-item .product .posted_in{ display:none; !important;}';
						}
						
						if(isset($theevent_theme_option['woo-post-tags']) && $theevent_theme_option['woo-post-tags'] == 'disable'){
							$style .= '.woocommerce-content-item .product .tagged_as{ display:none !important;}';
						}
						
						if(isset($theevent_theme_option['woo-post-outofstock']) && $theevent_theme_option['woo-post-outofstock'] == 'disable'){
							
						}
						
						if(isset($theevent_theme_option['woo-post-saleicon']) && $theevent_theme_option['woo-post-saleicon'] == 'disable'){
							$style .= '.woocommerce-content-item .product .onsale{ display:none;}';
						}

					}
				}
				
				
				
				return $style;
				
				
				// echo $style .='</style>';
				//echo wp_kses( $style, $theevent_allowed_html_array );
				
			}
		}