<?php
	/*	
	*	Kodeforest Blog Item Management File
	*	---------------------------------------------------------------------
	*	This file contains functions that help you get blog item
	*	---------------------------------------------------------------------
	*/
	
		
	if( !function_exists('theevent_get_blog_item') ){
		function theevent_get_blog_item( $settings = array() ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-blog-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			//$ret  = theevent_get_item_title($settings);
			$ret = '';
			$ret .= '<div class="blog-item-wrapper"  ' . $item_id . $margin_style . '>';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			if( !empty($settings['category']) || !empty($settings['tag']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}
				if( !empty($settings['tag']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'post_tag', 'field'=>'slug'));
				}				
			}

			// if( $settings['enable-sticky'] == 'enable' ){
				// if( get_query_var('paged') <= 1 ){
					// $sticky_args = $args;
					// $sticky_args['post__in'] = get_option('sticky_posts');
					// if( !empty($sticky_args['post__in']) ){
						// $sticky_query = new WP_Query($sticky_args);	
					// }
				// }
				// $args['post__not_in'] = get_option('sticky_posts', '');
			// }else{
				// $args['ignore_sticky_posts'] = 1;
			// }
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : get_query_var('page');
			$args['paged'] = empty($args['paged'])? 1: $args['paged'];
			$query = new WP_Query( $args );
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];

			// merge query
			// if( !empty($sticky_query) ){
				// $query->posts = array_merge($sticky_query->posts, $query->posts);
				// $query->post_count = $sticky_query->post_count + $query->post_count;
			// }

			// set the excerpt length
			if( !empty($settings['num-excerpt']) ){
				global $theevent_excerpt_length; $theevent_excerpt_length = $settings['num-excerpt'];
				add_filter('excerpt_length', 'theevent_set_excerpt_length');
			} 
			
			// get blog by the blog style
			global $theevent_post_settings, $theevent_lightbox_id;
			$theevent_lightbox_id++;
			$theevent_post_settings['excerpt'] = intval($settings['num-excerpt']);
			$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];	
			$theevent_post_settings['blog-style'] = $settings['blog-style'];			
			$ret .= '<div class="blog-item-holder">';
			if($settings['blog-style'] == 'blog-full'){
				$ret .= '<div class="kode-blog-list kode-large-blog row">';
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= theevent_get_blog_full($query);
				$ret .= '</div>';
			}else if($settings['blog-style'] == 'blog-medium'){
				$ret .= '<div class="kode-blog-list kode-list-blog row">';
				$blog_size = $settings['blog-size'];	
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= theevent_get_blog_list($query, $blog_size, '');					
				$ret .= '</div>';
			}else if(strpos($settings['blog-style'], 'blog-grid') !== false){
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$blog_size = $settings['blog-size'];	
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-blog-list kode-grid-blog row">';
				$ret .= theevent_get_blog_grid($query, $blog_size, '');			
				$ret .= '</div>';
			}else if(strpos($settings['blog-style'], 'blog-modern-grid') !== false){
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$blog_size = $settings['blog-size'];
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-blog-list kode-press-blog row">';
				$ret .= theevent_get_blog_modern_grid($query, $blog_size, '');			
				$ret .= '</div>';		
			}else if(strpos($settings['blog-style'], 'blog-small') !== false){
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$blog_size = $settings['blog-size'];
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-blog-list kode-box-blog row">';
				$ret .= theevent_get_blog_small($query, $blog_size, '');
				$ret .= '</div>';		
			}else{
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$blog_size = $settings['blog-size'];
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-blog-list kode-grid-blog row">';
				$ret .= theevent_get_blog_grid($query, $blog_size, '');			
				$ret .= '</div>';
			}
			$ret .= '</div>';
			
			if( $settings['pagination'] == 'enable' ){
				$ret .= theevent_get_pagination($query->max_num_pages, $args['paged']);
			}
			$ret .= '</div>'; // blog-item-wrapper
			
			remove_filter('excerpt_length', 'theevent_set_excerpt_length');
			return $ret;
		}
	}
	
	
	if( !function_exists('theevent_get_news_item') ){
		function theevent_get_news_item( $settings = array() ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-blog-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			//$ret  = theevent_get_item_title($settings);
			$ret = '';
			$ret .= '<div class="blog-item-wrapper"  ' . $item_id . $margin_style . '>';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			if( !empty($settings['category']) || !empty($settings['tag']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}
				if( !empty($settings['tag']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'post_tag', 'field'=>'slug'));
				}				
			}
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : get_query_var('page');
			$args['paged'] = empty($args['paged'])? 1: $args['paged'];
			$query = new WP_Query( $args );
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];

			// merge query
			// if( !empty($sticky_query) ){
				// $query->posts = array_merge($sticky_query->posts, $query->posts);
				// $query->post_count = $sticky_query->post_count + $query->post_count;
			// }

			// set the excerpt length
			if( !empty($settings['num-excerpt']) ){
				global $theevent_excerpt_length; $theevent_excerpt_length = $settings['num-excerpt'];
				add_filter('excerpt_length', 'theevent_set_excerpt_length');
			} 
			
			// get blog by the blog style
			global $theevent_post_settings, $theevent_lightbox_id;
			$theevent_lightbox_id++;
			$theevent_post_settings['excerpt'] = intval($settings['num-excerpt']);
			$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];	
			$theevent_post_settings['blog-style'] = $settings['blog-style'];			
			$ret .= '<div class="blog-item-holder">';
			if($settings['blog-style'] == 'blog-full'){
				$ret .= '<div class="kode-blog-list kode-large-blog row">';
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= theevent_get_news_full($query);
				$ret .= '</div>';
			}else if(strpos($settings['blog-style'], 'blog-grid') !== false){
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$blog_size = $settings['blog-size'];	
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-blog-list kode-grid-blog row">';
				$ret .= theevent_get_news_grid($query, $blog_size, '');			
				$ret .= '</div>';
			}else{
				$theevent_post_settings['thumbnail-size'] = $settings['kode-blog-thumbnail-size'];
				$blog_size = $settings['blog-size'];
				$theevent_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-blog-list kode-grid-blog row">';
				$ret .= theevent_get_news_grid($query, $blog_size, '');			
				$ret .= '</div>';
			}
			$ret .= '</div>';
			
			if( $settings['pagination'] == 'enable' ){
				$ret .= theevent_get_pagination($query->max_num_pages, $args['paged']);
			}
			$ret .= '</div>'; // blog-item-wrapper
			
			remove_filter('excerpt_length', 'theevent_set_excerpt_length');
			return $ret;
		}
	}

	if( !function_exists('theevent_get_blog_info') ){
		function theevent_get_blog_info( $array = array(), $wrapper = true, $sep = '',$custom_wrap='div' ){
			global $theevent_theme_option; $ret = '';
			if( empty($array) ) return $ret;
			$exclude_meta = empty($theevent_theme_option['post-meta-data'])? array(): esc_attr($theevent_theme_option['post-meta-data']);
			
			foreach($array as $post_info){
				if( in_array($post_info, $exclude_meta) ) continue;
				if( !empty($sep) ) $ret .= $sep;
				
				switch( $post_info ){
					case 'time':
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-time"><i class="fa fa-clock-o"></i>';
						$ret .= '<a href="' . esc_url(get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))) . '">';
						$ret .= esc_attr(get_the_time());
						$ret .= '</a>';
						$ret .= '</'.esc_attr($custom_wrap).'>';	
						break;
					case 'date':
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-date"><i class="fa fa-calendar-o"></i>';
						$ret .= '<a href="' . esc_url(get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))) . '">';
						$ret .= esc_attr(get_the_date());
						$ret .= '</a>';
						$ret .= '</'.esc_attr($custom_wrap).'>';	
						break;
					case 'tag':
						$tag = get_the_term_list(get_the_ID(), 'post_tag', '', '<span class="sep">,</span> ' , '' );
						if(empty($tag)) break;					
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-tag"><i class="fa fa-tag"></i>';
						$ret .= $tag;						
						$ret .= '</'.esc_attr($custom_wrap).'>';						
						break;
					case 'category':
						$category = get_the_term_list(get_the_ID(), 'category', '', '<span class="sep">,</span> ' , '' );
						if(empty($category)) break;
						
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-category"><i class="fa fa-list"></i>';
						$ret .= $category;					
						$ret .= '</'.esc_attr($custom_wrap).'>';					
						break;
					case 'comment':
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-comment"><i class="fa fa-comment-o"></i>';
						if(isset($themall_theme_option['blog-comments']) && $themall_theme_option['blog-comments'] <> ''){
						$ret .= '<a href="' . esc_url(get_permalink()) . '#respond" >' . esc_attr(get_comments_number()) . ' ' . sprintf(__('%s','the-event'),$themall_theme_option['blog-comments']).'</a>';						
						}
						$ret .= '</'.esc_attr($custom_wrap).'>';							
						break;
					case 'views':
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-views"><i class="fa fa-eye"></i>';
						$ret .= '<a href="' . esc_url(get_permalink()) . '" >' . esc_attr(theevent_get_post_views(get_the_ID())) . ' ' . esc_html__('Views','the-event').'</a>';
						$ret .= '</'.esc_attr($custom_wrap).'>';							
						break;	
					case 'author':
						ob_start();
						the_author_posts_link();
						$author = ob_get_contents();
						ob_end_clean();
						
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-author"><i class="fa fa-user"></i>';
						$ret .= $author;
						$ret .= '</'.esc_attr($custom_wrap).'>';						
						break;						
				}
			}
			
			
			if($wrapper && !empty($ret)){
				return '<div class="kode-blog-info kode-info">' . $ret . '<div class="clear"></div></div>';
			}else if( !empty($ret) ){
				return $ret;
			}
			return '';
		}
	}
	
	
	if( !function_exists('theevent_get_news_full') ){
		function theevent_get_news_full($query){
			$ret = ''; $current_size = 0;
			$size = 1;
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-blog-full">';
				$ret .= '<div class="kode-ux kode-blog-full-ux">';
				ob_start();
				
				get_template_part('single/news');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
			
		}
	}	
	
	if( !function_exists('theevent_get_news_grid') ){
		function theevent_get_news_grid($query, $size){
			$ret = ''; $current_size = 0;
			
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-blog-grid-default">';
				$ret .= '<div class="kode-ux kode-blog-grid-default-ux">';
				ob_start();
				
				get_template_part('single/news');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
			
		}
	}	
	
	
	if( !function_exists('theevent_get_blog_simple') ){
		function theevent_get_blog_simple($query, $size){
			$ret = ''; $current_size = 0;
			
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-blog-simple">';
				$ret .= '<div class="kode-ux kode-blog-simple-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
			
		}
	}	
	
	if( !function_exists('theevent_get_blog_small') ){
		function theevent_get_blog_small($query, $size){
			$ret = ''; $current_size = 0;
			
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-blog-simple">';
				$ret .= '<div class="kode-ux kode-blog-simple-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
			
		}
	}
	

	if( !function_exists('theevent_get_blog_widget') ){
		function theevent_get_blog_widget($query, $size){
			$ret = ''; $current_size = 0;
			
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-blog-widget">';
				$ret .= '<div class="kode-ux kode-blog-widget-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
		}
	}
	

	if( !function_exists('theevent_get_news_medium') ){
		function theevent_get_news_medium($query, $size){
			
			$ret = ''; $current_size = 0;
			
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="news-listing kd-mediumview">';
				$ret .= '<div class="kode-item kode-news-grid">';
				ob_start();
				
				get_template_part('single/content_news');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';
			//$ret .= '</div>'; // close the kode-isotope
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	if( !function_exists('theevent_get_news_full') ){
		function theevent_get_news_full($query, $size){
			
			$ret = ''; $current_size = 0;
			$size = 3;
			//$ret .= '<div class="kode-isotope" data-type="blog" data-layout="' . $blog_layout  . '" >';
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-ux kode-news-grid-ux">';
				ob_start();
				
				get_template_part('single/content_news');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux		
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';
			//$ret .= '</div>'; // close the kode-isotope
			wp_reset_postdata();
			
			return $ret;
		}
	}	
	
	if( !function_exists('theevent_get_news_grid') ){
		function theevent_get_news_grid($query, $size){
			
			$ret = ''; $current_size = 0;
			
			//$ret .= '<div class="kode-isotope" data-type="blog" data-layout="' . $blog_layout  . '" >';
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="news-listing">';
				$ret .= '<div class="kode-ux kode-item">';
				ob_start();
				
				get_template_part('single/content_news');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // news-listing		
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';
			//$ret .= '</div>'; // close the kode-isotope
			wp_reset_postdata();
			
			return $ret;
		}
	}	
	
	if( !function_exists('theevent_get_blog_grid') ){
		function theevent_get_blog_grid($query, $size, $blog_layout = 'fitRows'){
			// if($blog_layout == 'carousel'){ return theevent_get_blog_grid_carousel($query, $size); }
		
			$ret = ''; $current_size = 0;			
			//$ret .= '<div class="kode-isotope" data-type="blog" data-layout="' . $blog_layout  . '" >';
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clearfix clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-ux kode-blog-grid-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux				
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';
			//$ret .= '</div>'; // close the kode-isotope
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	
	if( !function_exists('theevent_get_blog_modern_grid') ){
		function theevent_get_blog_modern_grid($query, $size, $blog_layout = 'fitRows'){
			
			$ret = ''; $current_size = 0;
			$class_item = '';			
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clearfix clear"></div>';
					$class_item = 'kode_item_style_1';
				}else{
					$class_item = 'kode_item_style_2';
				}
				
				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-ux kode-blog-small-ux '.$class_item.'">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux				
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';			
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	
	if( !function_exists('theevent_get_blog_grid_carousel') ){
		function theevent_get_blog_grid_carousel($query, $size){
			$ret = ''; 
			
			$ret .= '<div class="kode-blog-carousel-item kode-item" >';
			$ret .= '<div class="flexslider" data-type="carousel" data-nav-container="blog-item-holder" data-columns="' . esc_attr($size) . '" >';	
			$ret .= '<ul class="slides" >';			
			while($query->have_posts()){ $query->the_post();
				$ret .= '<li class="kode-item kode-blog-grid">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();					
				$ret .= '</li>'; // kode-item
			}
			$ret .= '</ul>';
			$ret .= '<div class="clear"></div>';
			$ret .= '</div>'; // close the flexslider
			$ret .= '</div>'; // close the kode-item
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	if( !function_exists('theevent_get_blog_list') ){
		function theevent_get_blog_list($query, $size, $blog_layout = 'fitRows'){
			$ret = '';$current_size = 0;			
			// if($blog_layout == 'carousel'){ return theevent_get_blog_grid_carousel($query, $size); }
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-blog-medium">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	if( !function_exists('theevent_get_blog_full') ){
		function theevent_get_blog_full($query){
			$ret = '';$current_size = 0;
		
			$size = 1;
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}
				$ret .= '<div class="' . esc_attr(theevent_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-blog-full ">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux
				$ret .= '</div>'; // kode-item
				$current_size++;
			}
			wp_reset_postdata();
			
			return $ret;
		}
	}	

?>