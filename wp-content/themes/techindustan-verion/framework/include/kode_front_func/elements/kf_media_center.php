<?php
	/*	
	*	Kodeforest Media Center File
	*	---------------------------------------------------------------------
	*	This file contains functions that manage the media in the theme
	*	---------------------------------------------------------------------
	*/	
	
	if( !function_exists('theevent_get_social_shares') ){
		function theevent_get_social_shares(){	
			global $theevent_theme_option;
			$thumbnail = array();
			$page_title = rawurlencode(get_the_title());
			$current_url = THEEVENT_HTTP . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];?>
			<ul class="kode-team-network-kick">
				<?php if($theevent_theme_option['facebook-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-facebook" href="http://www.facebook.com/share.php?u=<?php echo esc_url($current_url); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if($theevent_theme_option['digg-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-digg" href="http://digg.com/submit?url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Digg"></a></li><?php }?>
				<?php if($theevent_theme_option['google-plus-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-google-plus" href="https://plus.google.com/share?url=<?php echo esc_url($current_url); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if($theevent_theme_option['linkedin-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-linkedin" href="http://www.linkedin.com/shareArticle?mini=true&#038;url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if($theevent_theme_option['my-space-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-steam" href="http://www.myspace.com/Modules/PostTo/Pages/?u=<?php echo esc_url($current_url); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if($theevent_theme_option['pinterest-share'] == 'enable'){ $thumbnail_id = get_post_thumbnail_id( get_the_ID() );$thumbnail = wp_get_attachment_image_src( $thumbnail_id , 'large' ); ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo esc_url($current_url); ?>&media=<?php echo esc_url($thumbnail[0]); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if($theevent_theme_option['reddit-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-reddit" href="http://reddit.com/submit?url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if($theevent_theme_option['stumble-upon-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-stumbleupon" href="http://www.stumbleupon.com/submit?url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if($theevent_theme_option['twitter-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-twitter" href="http://twitter.com/home?status=<?php echo esc_attr(str_replace('%26%23038%3B', '%26', $page_title)) . ' - ' . esc_url($current_url); ?>" data-original-title="Facebook"></a></li><?php }?>
			</ul>
			<?php 
		}
	}
	
	$theevent_header_social_icon = array(
		'delicious'		=> esc_html__('Delicius','the-event'), 
		'digg'			=> esc_html__('Digg','the-event'),
		'facebook'		=> esc_html__('Facebook','the-event'), 
		'flickr'		=> esc_html__('Flickr','the-event'),
		'google-plus' 	=> esc_html__('Google Plus','the-event'),						
		'linkedin' 		=> esc_html__('Linkedin','the-event'),		
		'pinterest' 	=> esc_html__('Pinterest','the-event'),		
		'skype'			=> esc_html__('Skype','the-event'),
		'stumble-upon' 	=> esc_html__('Stumble Upon','the-event'),
		'tumblr' 		=> esc_html__('Tumblr','the-event'),
		'twitter' 		=> esc_html__('Twitter','the-event'),
		'vimeo' 		=> esc_html__('Vimeo','the-event'),
		'youtube' 		=> esc_html__('Youtube','the-event'),
	);	
	
	add_filter('theevent_themeoption_panel', 'theevent_register_header_social_option');
	if( !function_exists('theevent_register_header_social_option') ){
		function theevent_register_header_social_option( $array ){		
			if( empty($array['overall-elements']['options']) ) return $array;
			
			global $theevent_header_social_icon;
			$header_social = array( 									
				'title' => esc_html__('Header Social', 'the-event'),
				'options' => array(
				)
			);
				
			foreach( $theevent_header_social_icon as $social_slug => $social_name ){
				$header_social['options'][$social_slug . '-header-social'] = array(
					'title' => $social_name . ' ' . esc_html__('Header Social', 'the-event'),
					'type' => 'text',
					'description' => 'Enter URL of your social profile here.'										
				);
				
			}
			
			$array['overall-elements']['options']['header-social'] = $header_social;
			return $array;
		}
	}
	
	
	
	if( !function_exists('theevent_print_header_social') ){
		function theevent_print_header_social(){
			global $theevent_header_social_icon, $theevent_theme_option;
			$type = empty($theevent_theme_option['header-social-type'])? 'dark': $theevent_theme_option['header-social-type'];
			
			foreach( $theevent_header_social_icon as $social_slug => $social_name ){
				if( !empty($theevent_theme_option[$social_slug . '-header-social']) ){ ?>
				<div class="social-icon">
					<a href="<?php echo esc_attr($theevent_theme_option[$social_slug . '-header-social']); ?>" target="_blank" >
						<img width="32" height="32" src="<?php echo THEEVENT_PATH . '/images/' . $type . '/social-icon/' . $social_slug . '.png'; ?>" alt="<?php echo esc_attr($social_name); ?>" />
					</a>
				</div>
				<?php				
				}
			}
			echo '<div class="clear"></div>';
		}
	}	
	
	if( !function_exists('theevent_print_header_social_icon') ){
		function theevent_print_header_social_icon($class=''){
			global $theevent_header_social_icon, $theevent_theme_option;
			$type = empty($theevent_theme_option['header-social-type'])? 'dark': esc_attr($theevent_theme_option['header-social-type']); ?>
			<ul class="<?php echo esc_attr($type).' '.$class;?> kode-team-network ">
				<?php if($theevent_theme_option['delicious-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['delicious-header-social']);?>"><i class="fa fa-delicious"></i></a></li><?php }?>
				<?php if($theevent_theme_option['digg-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['digg-header-social']);?>"><i class="fa fa-digg"></i></a></li><?php }?>
				<?php if($theevent_theme_option['facebook-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['facebook-header-social']);?>"><i class="fa fa-facebook"></i></a></li><?php }?>
				<?php if($theevent_theme_option['flickr-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['flickr-header-social']);?>"><i class="fa fa-flickr"></i></a></li><?php }?>
				<?php if($theevent_theme_option['google-plus-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['google-plus-header-social']);?>"><i class="fa fa-google-plus"></i></a></li><?php }?>
				<?php if($theevent_theme_option['linkedin-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['linkedin-header-social']);?>"><i class="fa fa-linkedin"></i></a></li><?php }?>
				<?php if($theevent_theme_option['pinterest-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['pinterest-header-social']);?>"><i class="fa fa-pinterest"></i></a></li><?php }?>
				<?php if($theevent_theme_option['skype-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['skype-header-social']);?>"><i class="fa fa-skype"></i></a></li><?php }?>
				<?php if($theevent_theme_option['stumble-upon-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['stumble-upon-header-social']);?>"><i class="fa fa-stumbleupon"></i></a></li><?php }?>
				<?php if($theevent_theme_option['tumblr-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['tumblr-header-social']);?>"><i class="fa fa-tumblr"></i></a></li><?php }?>
				<?php if($theevent_theme_option['twitter-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['twitter-header-social']);?>"><i class="fa fa-twitter"></i></a></li><?php }?>
				<?php if($theevent_theme_option['vimeo-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['vimeo-header-social']);?>"><i class="fa fa-vimeo-square"></i></a></li><?php }?>
				<?php if($theevent_theme_option['youtube-header-social'] <> ''){ ?><li><a href="<?php echo esc_url($theevent_theme_option['youtube-header-social']);?>"><i class="fa fa-youtube"></i></a></li><?php }?>
			</ul>	
			<?php				
		}
	}
	
	if( !function_exists('theevent_print_header_social_icon_unechod') ){
		function theevent_print_header_social_icon_unechod($class=''){
			global $theevent_header_social_icon, $theevent_theme_option;
			$type = empty($theevent_theme_option['header-social-type'])? 'dark': esc_attr($theevent_theme_option['header-social-type']); 
			$html_social = '<ul class="'.esc_attr($type).' '.$class.' kode-team-network ">';
				if($theevent_theme_option['delicious-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['delicious-header-social']).'"><i class="fa fa-delicious"></i></a></li>';
				}
				if($theevent_theme_option['digg-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['digg-header-social']).'"><i class="fa fa-digg"></i></a></li>';
				}
				if($theevent_theme_option['flickr-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['flickr-header-social']).'"><i class="fa fa-flickr"></i></a></li>';
				}
				if($theevent_theme_option['google-plus-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['google-plus-header-social']).'"><i class="fa fa-google-plus"></i></a></li>';
				}
				if($theevent_theme_option['facebook-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['facebook-header-social']).'"><i class="fa fa-facebook"></i></a></li>';
				}
				if($theevent_theme_option['linkedin-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['linkedin-header-social']).'"><i class="fa fa-linkedin"></i></a></li>';
				}
				if($theevent_theme_option['pinterest-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['pinterest-header-social']).'"><i class="fa fa-pinterest"></i></a></li>';
				}
				if($theevent_theme_option['skype-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['skype-header-social']).'"><i class="fa fa-skype"></i></a></li>';
				}
				if($theevent_theme_option['stumble-upon-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['stumble-upon-header-social']).'"><i class="fa fa-stumble-upon"></i></a></li>';
				}
				if($theevent_theme_option['tumblr-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['tumblr-header-social']).'"><i class="fa fa-tumblr"></i></a></li>';
				}
				if($theevent_theme_option['twitter-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['twitter-header-social']).'"><i class="fa fa-twitter"></i></a></li>';
				}
				if($theevent_theme_option['vimeo-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['vimeo-header-social']).'"><i class="fa fa-vimeo"></i></a></li>';
				}
				if($theevent_theme_option['youtube-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($theevent_theme_option['youtube-header-social']).'"><i class="fa fa-youtube"></i></a></li>';
				}				
			$html_social .= '</ul>';
			
			return $html_social;
			
		}
	}
	
	add_filter('theevent_themeoption_panel', 'theevent_register_social_shares_option');
	if( !function_exists('theevent_register_social_shares_option') ){
		function theevent_register_social_shares_option( $array ){	
			if( empty($array['overall-elements']['options']) ) return $array;
			
			$theevent_social_shares = array(
				'digg'			=> esc_html__('Digg','the-event'),			
				'facebook'		=> esc_html__('Facebook','the-event'), 
				'google-plus' 	=> esc_html__('Google Plus','the-event'),	
				'linkedin' 		=> esc_html__('Linkedin','the-event'),
				'my-space' 		=> esc_html__('My Space','the-event'),
				'pinterest' 	=> esc_html__('Pinterest','the-event'),
				'reddit' 		=> esc_html__('Reddit','the-event'),
				'stumble-upon' 	=> esc_html__('Stumble Upon','the-event'),
				'twitter' 		=> esc_html__('Twitter','the-event'),
			);	
			$header_social = array( 									
				'title' => esc_html__('Social Shares', 'the-event'),
				'options' => array(
					'enable-social-share'=> array(
						'title' => esc_html__('Enable Social Share' ,'the-event'),
						'type' => 'checkbox',
						'description' => 'Enable this option to show the social shares below each post'
					)
				)
			);
				
			foreach( $theevent_social_shares as $social_slug => $social_name ){
				$header_social['options'][$social_slug . '-share'] = array(
					'title' => $social_name,
					'type' => 'checkbox',
					'description' => 'Enable this option to show the social Icon under post.'					
				);
			}
			
			$array['overall-elements']['options']['social-shares'] = $header_social;
			return $array;
		}
	}	
	
	if( !function_exists('theevent_get_video') ){
		function theevent_get_video($video, $size = 'full'){
			if( empty($video) ) return '';
			
			$video_size = theevent_get_video_size($size);
			$width = $video_size['width']; 
			$height = $video_size['height']; 

			// video shortcode
			if(preg_match('#^\[video\s.+\[/video\]#', $video, $match)){ 
				return do_shortcode($match[0]);
				
			// embed shortcode
			}else if(preg_match('#^\[embed.+\[/embed\]#', $video, $match)){ 
				global $theevent_wp_embed; 
				return $theevent_wp_embed->run_shortcode($match[0]);
				
			// youtube link
			}else if(strpos($video, 'youtube') !== false){
				preg_match('#[?&]v=([^&]+)(&.+)?#', $video, $id);
				$id[2] = empty($id[2])? '': $id[2];
				return '<iframe src="http://www.youtube.com/embed/' . esc_attr($id[1]) . '?wmode=transparent' . esc_attr($id[2]) . '" width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" ></iframe>';
			
			// youtu.be link
			}else if(strpos($video, 'youtu.be') !== false){
				preg_match('#youtu.be\/([^?&]+)#', $video, $id);
				return '<iframe src="http://www.youtube.com/embed/' . esc_attr($id[1]) . '?wmode=transparent" width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" ></iframe>';
			
			// vimeo link
			}else if(strpos($video, 'vimeo') !== false){
				preg_match('#https?:\/\/vimeo.com\/(\d+)#', $video, $id);
				return '<iframe src="http://player.vimeo.com/video/' . esc_attr($id[1]) . '?title=0&amp;byline=0&amp;portrait=0" width="' . esc_attr($width) . '" height="' . esc_attr($height) . '"></iframe>';
			
			// another link
			}else if(preg_match('#^https?://\S+#', $video, $match)){ 	
				$path_parts = pathinfo($match[0]);
				if( !empty($path_parts['extension']) ){
					return do_shortcode('[video width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" src="' . esc_url($match[0]) . '" ][/video]');
				}else{
					global $theevent_wp_embed; 
					$video_embed = '[embed width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" ]' . esc_url($match[0]) . '[/embed]';
					return $theevent_wp_embed->run_shortcode($video_embed);
				}				
			}
			return '';
		}
	}	
	
	// use for printing the image from  image id
	if( !function_exists('theevent_get_image') ){
		function theevent_get_image($image, $size = 'full', $link = array(), $attr = ''){
			if( empty($image) ) return '';
		
			if( is_numeric($image) ){
				$alt_text = get_post_meta($image , '_wp_attachment_image_alt', true);	
				$image_src = wp_get_attachment_image_src($image, $size);	
				if( empty($image_src) ) return '';
				
				if( $link === true ){ 
					$image_full = wp_get_attachment_image_src($image, 'full');
					$link = array('url'=>esc_url($image_full[0]));
				}else if( !empty($link) && empty($link['url']) ){
					$image_full = wp_get_attachment_image_src($image, 'full');
					$link['url'] = esc_url($image_full[0]);				
				}
				$ret = '<img src="' . esc_url($image_src[0]) . '" alt="' . esc_attr($alt_text) . '" width="' . esc_attr($image_src[1]) .'" height="' . esc_attr($image_src[2]) . '" ' . $attr . '/>';
			}else{
				if( $link === true ){ 
					$link = array('url'=>esc_url($image)); 
				}else if( !empty($link) && empty($link['url']) ){
					$link['url'] = esc_url($image);		
				}
				$ret = '<img src="' . esc_url($image) . '" alt="" ' . $attr . ' />';
			}
			
			if( !empty($link) ){
				$pretty_photo  = '<a href="' . esc_url($link['url']) . '" ';
				$pretty_photo .= (empty($link['id']))? '': 'data-pretty-group="kode-gal-' . $link['id'] . '" ';
				$pretty_photo .= (!empty($link['type']) && $link['type'] == 'link')? '': 'data-rel="prettyphoto[]" ';
				$pretty_photo .= (!empty($link['type']) && $link['type'] == 'video')? 'data-pretty-type="iframe" ': '';
				$pretty_photo .= (!empty($link['new-tab']) && $link['new-tab'] == 'enable')? 'target="_blank" ': '';
				$pretty_photo .= '>' . $ret;
				$pretty_photo .= (!empty($link['close-tag']))? '': '</a>';
				return $pretty_photo;
			}
			return $ret;
		}
	}
	
	if( !function_exists('theevent_get_attachment_info') ){
		function theevent_get_attachment_info($attachment_id, $type = '') {
			$attachment = get_post($attachment_id);
			if( !empty($attachment) ){
				$ret = array(
					'caption' => $attachment->post_excerpt,
					'description' => $attachment->post_content,
					'title' => $attachment->post_title
				);
				
				if( !empty($type) ) return $ret[$type];
				return $ret;
			}
			return array();
		}	
	}	
	
	// use for printing slider
	if( !function_exists('theevent_get_slider_item') ){
		function theevent_get_slider_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' . esc_attr($settings['element-item-id']) . '" ';
			$settings['element-item-class'] = empty($settings['element-item-class'])? '': $settings['element-item-class'];

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			$settings['thumbnail-size'] = 'full';
			$ret  = '<div class="'.esc_attr($settings['element-item-class']).' kode-item kode-slider-item" ' . $item_id . $margin_style . ' >';
			$ret .= theevent_get_slider($settings['slider'], $settings['thumbnail-size'], $settings['slider-type']);
			$ret .= '</div>';
			return $ret;
		}
	}
	
	// use for printing post slider
	if( !function_exists('theevent_get_post_slider_item') ){
		function theevent_get_post_slider_item( $settings ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			$slide_order = array();
			$slide_data = array();
			
			// query posts section
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['ignore_sticky_posts'] = 1;

			if( is_numeric($settings['category']) ){
				$args['category'] = (empty($settings['category']))? '': $settings['category'];	
			}else{ 
				if( !empty($settings['category']) || !empty($settings['tag']) ){
					$args['tax_query'] = array('relation' => 'OR');
					
					if( !empty($settings['category']) ){
						array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
					}
					if( !empty($settings['tag']) ){
						array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'post_tag', 'field'=>'slug'));
					}				
				}	
			}
			$query = new WP_Query( $args );	
			
			// set the excerpt length
			global $theevent_theme_option, $theevent_excerpt_length, $theevent_excerpt_read_more; 
			$theevent_excerpt_read_more = false;
			$theevent_excerpt_length = $settings['num-excerpt'];
			add_filter('excerpt_length', 'theevent_set_excerpt_length');

			global $post;
			while($query->have_posts()){ $query->the_post();
				$image_id = get_post_thumbnail_id();
				
				if( !empty($image_id) ){
					$slide_order[] = $image_id;
					$slide_data[$image_id] = array(	
						'title'=> esc_attr(get_the_title()),
						'slide-link'=> 'url',
						'url'=> esc_url(get_permalink()),
						'new-tab'=> 'disable',
						'caption-position'=>$settings['caption-style']
					);
					
					if( $settings['style'] == 'no-excerpt' ){
						$slide_data[$image_id]['caption']  = '<div class="kode-caption-date" >';
						$slide_data[$image_id]['caption'] .= '<i class="fa fa-calendar"></i>';
						$slide_data[$image_id]['caption'] .= esc_attr(get_the_time(get_option('date_format')));				
						$slide_data[$image_id]['caption'] .= '</div>';				
						
						$slide_data[$image_id]['caption'] .= '<div class="kode-title-link" >';
						$slide_data[$image_id]['caption'] .= '<i class="fa fa-angle-right" ></i>';
						$slide_data[$image_id]['caption'] .= '</div>';		
					}else{
						$slide_data[$image_id]['caption']  = '<div class="blog-info blog-date"><i class="fa fa-calendar"></i>';
						$slide_data[$image_id]['caption'] .= esc_attr(get_the_time(get_option('date_format')));		
						$slide_data[$image_id]['caption'] .= '</div>';
						$slide_data[$image_id]['caption'] .= '<div class="blog-info blog-comment"><i class="fa fa-comment"></i>';
						$slide_data[$image_id]['caption'] .= esc_attr(get_comments_number());
						$slide_data[$image_id]['caption'] .= '</div>';					
						$slide_data[$image_id]['caption'] .= '<div class="clear"></div>';					
						$slide_data[$image_id]['caption'] .= '<p>'.esc_attr(get_the_excerpt()).'</p>';
					}
				}
			}	
			
			$theevent_excerpt_read_more = true;
			remove_filter('excerpt_length', 'theevent_set_excerpt_length');
			
			if( $settings['style'] == 'no-excerpt' ){
				$settings['caption-style'] = 'no-excerpt';
			}
			
			$ret  = '<div class="kode-item kode-post-slider-item style-' . $settings['caption-style'] . '" ' . $item_id . $margin_style . ' >';
			$ret .= theevent_get_slider(array($slide_order, $slide_data), $settings['thumbnail-size'], 'bxslider');
			$ret .= '</div>';
			return $ret;
		}
	}	
	
	// use for printing slider
	if( !function_exists('theevent_get_slider') ){
		function theevent_get_slider( $slider_data, $thumbnail_size, $slider_type = 'flexslider' ){
			if( is_array($slider_data) ){
				$slide_order = $slider_data[0];
				$slide_data = $slider_data[1];
			}else{
				$slider_option = json_decode($slider_data, true);
				$slide_order = $slider_option[0];
				$slide_data = $slider_option[1];			
			}
			
			$slides = array();
			$slide_order = empty($slide_order)? array(): $slide_order;
			foreach($slide_order as $slide){
				$slides[$slide] = $slide_data[$slide];
			}
				
			if($slider_type == 'flexslider'){
				return theevent_get_flex_slider($slides, array('size'=> $thumbnail_size));
			}else if($slider_type == 'nivoslider'){
				return theevent_get_nivo_slider($slides, array('size'=> $thumbnail_size));
			}else if($slider_type == 'bxslider'){
				return theevent_get_bx_slider($slides, array('size'=> $thumbnail_size));
			}else{
				return 'slider is not defined';
			}
			
		}
	}	
	
	
	
	
	// use for printing flex slider
	if( !function_exists('theevent_get_flex_slider') ){
		function theevent_get_flex_slider($slides, $settings = array()){
			global $theevent_theme_option, $theevent_gallery_id; $theevent_gallery_id++;
			
			$ret  = '<div class="flexslider" ';
			$ret .= empty($settings['pausetime'])? 'data-pausetime="' . esc_attr($theevent_theme_option['flex-pause-time']) . '" ': 
						'data-pausetime="' . esc_attr($settings['pausetime']) . '" ';
			$ret .= empty($settings['slidespeed'])? 'data-slidespeed="' . esc_attr($theevent_theme_option['flex-slide-speed']) . '" ': 
						'data-slidespeed="' . esc_attr($settings['slidespeed']) . '" ';			
			$ret .= empty($settings['effect'])? 'data-effect="' . esc_attr($theevent_theme_option['flex-slider-effects']) . '" ': 
						'data-effect="' . esc_attr($settings['effect']) . '" ';	
						
			$ret .= empty($settings['columns'])? '': 'data-columns="' . esc_attr($settings['columns']) . '" ';
			$ret .= empty($settings['carousel'])? '': 'data-type="carousel" ';
			$ret .= empty($settings['nav-container'])? '': 'data-nav-container="' . esc_attr($settings['nav-container']) . '" ';
			$ret .= '>';
			$ret .= '<ul class="slides" >';
			$title_font_size = '';
			if(isset($theevent_theme_option['title-font-size'])){
				if($theevent_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($theevent_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($theevent_theme_option['caption-font-size'])){
				if($theevent_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($theevent_theme_option['caption-font-size']).'px';
				}
			}
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){
				$ret .= '<li>';
				
				if( is_array($slide) ){

					// flex slider caption
					$caption = '';
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
					
						$caption .= '<div class="kode-caption-wrapper position-' . esc_attr($slide['caption-position']) . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($theevent_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))) . '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($theevent_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div style="color:'.esc_attr($theevent_theme_option['caption-btn-color']).';background:'.esc_attr($theevent_theme_option['caption-btn-color-bg']).'" class="kode_button_event_slider">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-slider-caption
						$caption .= '</div>'; // kode-slider-caption-wrapper
						$caption .= '</div>';
					}				
				
					// flex slider link
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size'])) . $caption;
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab']), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}
				}else{
					$ret .= theevent_get_image(esc_attr($slide), esc_attr($settings['size']), array('id'=>esc_attr($theevent_gallery_id)));
				}
				$ret .= '</li>';
			}
			$ret .= '</ul>';
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	
	
	// breaking news item
	if( !function_exists('theevent_get_wp_post_slider') ){
		function theevent_get_wp_post_slider( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			if( !empty($settings['category']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}			
			}
			$ret = '';
			$slider_class = '';
			$settings['thumbnail-size'];
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			
			$settings['num-fetch'] = (empty($settings['num-fetch']))? '20': $settings['num-fetch'];
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];
			
			$query = new WP_Query( $args );
	
			$ret .= '
			<div '.$margin_style.' class="kode_banner kode_post_slider kode-slider-item">                	
				<ul data-mode="fade" data-min="1" data-max="1" class="bxslider '.esc_attr($slider_class).'">';
					while($query->have_posts()){
					$query->the_post();
					global $post;							
					$ret .= '
					<li>
					'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
					<div class="kode-caption">
						'.theevent_get_blog_info(array('category'), false, '','h6').'
						<h2 class="kode-caption-title"><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h2>
						<p class="kode-caption-text">'.esc_attr(substr(get_the_content(),0,100)).'</p>
						'.do_shortcode('[button class="kode-linksection hvr-radial-out" style="type-1" icon="fa fa-angle-right" iconcolor="#000000" iconbgcolor="#dd9933" url="'.esc_url(get_permalink()).'" target="_blank" color="#ffffff" bgcolor="#dd3333" size="small" ]Read More[/button]').'
					</div>
					</li>';
					}
				$ret .= '    
				</ul>
			</div>';
			
			wp_reset_postdata();
			
			return $ret;
		}
	}	
	
	// use for printing flex slider
	if( !function_exists('theevent_get_wp_post_slider') ){
		function theevent_get_wp_post_slider($settings = array()){
			global $theevent_theme_option, $theevent_gallery_id; $theevent_gallery_id++;
			
			$ret .= '<ul class="slides" >';
			$title_font_size = '';
			if(isset($theevent_theme_option['title-font-size'])){
				if($theevent_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($theevent_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($theevent_theme_option['caption-font-size'])){
				if($theevent_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($theevent_theme_option['caption-font-size']).'px';
				}
			}
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){
				$ret .= '<li>';
				
				if( is_array($slide) ){

					// flex slider caption
					$caption = '';
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
					
						$caption .= '<div class="kode-caption-wrapper position-' . esc_attr($slide['caption-position']) . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($theevent_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))) . '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($theevent_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div style="color:'.esc_attr($theevent_theme_option['caption-btn-color']).';background:'.esc_attr($theevent_theme_option['caption-btn-color-bg']).'" class="kode_button_event_slider">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-slider-caption
						$caption .= '</div>'; // kode-slider-caption-wrapper
						$caption .= '</div>';
					}				
				
					// flex slider link
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size'])) . $caption;
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab']), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}
				}else{
					$ret .= theevent_get_image(esc_attr($slide), esc_attr($settings['size']), array('id'=>esc_attr($theevent_gallery_id)));
				}
				$ret .= '</li>';
			}
			$ret .= '</ul>';
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	
	// use for printing bx slider
	if( !function_exists('theevent_get_bx_slider') ){
		function theevent_get_bx_slider($slides, $settings = array()){
			global $theevent_theme_option, $theevent_gallery_id; $theevent_gallery_id++;
			
			$ret  = '<div class="kode-bxslider" ';
			$ret .= empty($settings['pausetime'])? 'data-pausetime="' . esc_attr($theevent_theme_option['bx-pause-time']) . '" ': 
						'data-pausetime="' . esc_attr($settings['pausetime']) . '" ';
			$ret .= empty($settings['slidespeed'])? 'data-slidespeed="' . esc_attr($theevent_theme_option['bx-slide-speed']) . '" ': 
						'data-slidespeed="' . esc_attr($settings['slidespeed']) . '" ';			
			$ret .= empty($settings['effect'])? 'data-effect="' . esc_attr($theevent_theme_option['bx-slider-effects']) . '" ': 
						'data-effect="' . esc_attr($settings['effect']) . '" ';	
						
			$ret .= empty($settings['columns'])? '': 'data-columns="' . esc_attr($settings['columns']) . '" ';
			$ret .= empty($settings['carousel'])? '': 'data-type="carousel" ';
			//$ret .= empty($settings['nav-container'])? '': 'data-nav-container="' . $settings['nav-container'] . '" ';
			$ret .= '>';
			if($theevent_theme_option['bx-slider-effects'] == 'slide'){
				$ret .= '<ul data-min="'.esc_attr($theevent_theme_option['bx-min-slide']).'" data-max="'.esc_attr($theevent_theme_option['bx-max-slide']).'" data-margin="'.esc_attr($theevent_theme_option['bx-slide-margin']).'" data-mode="horizontal" class="bxslider" >';
			}else{
				$ret .= '<ul data-mode="fade" class="bxslider">';
			}
			$title_font_size = '';
			if(isset($theevent_theme_option['title-font-size'])){
				if($theevent_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($theevent_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($theevent_theme_option['caption-font-size'])){
				if($theevent_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($theevent_theme_option['caption-font-size']).'px';
				}
			}
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){
				$ret .= '<li>';
				
				if( is_array($slide) ){

					// flex slider caption
					$caption = '';
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
					
						$caption .= '<div class="kode-caption-wrapper position-' . esc_attr($slide['caption-position']) . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($theevent_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))). '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($theevent_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div style="color:'.esc_attr($theevent_theme_option['caption-btn-color']).';background:'.esc_attr($theevent_theme_option['caption-btn-color-bg']).'" class="kode_button_event_slider">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-slider-caption
						$caption .= '</div>'; // kode-slider-caption-wrapper
						$caption .= '</div>';
					}				
				
					// bx slider link
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size'])) . $caption;
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab']), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= theevent_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($theevent_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}
				}else{
					$ret .= theevent_get_image($slide, esc_attr($settings['size']), array('id'=>esc_attr($theevent_gallery_id)));
				}
				$ret .= '</li>';
			}
			$ret .= '</ul>';
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	// use for printing nivo slider
	if( !function_exists('theevent_get_nivo_slider') ){
		function theevent_get_nivo_slider($slides, $settings = array()){
			global $theevent_theme_option, $theevent_gallery_id; $theevent_gallery_id++;
			
			$i = 0; $caption = '';
			$ret  = '<div class="nivoSlider-wrapper">';
			$ret .= '<div class="nivoSlider" ';
			$ret .= empty($settings['pausetime'])? 'data-pausetime="' . esc_attr($theevent_theme_option['nivo-pause-time']) . '" ': 
						'data-pausetime="' . esc_attr($settings['pausetime']) . '" ';
			$ret .= empty($settings['slidespeed'])? 'data-slidespeed="' . esc_attr($theevent_theme_option['nivo-slide-speed']) . '" ': 
						'data-slidespeed="' . esc_attr($settings['slidespeed']) . '" ';			
			$ret .= empty($settings['effect'])? 'data-effect="' . esc_attr($theevent_theme_option['nivo-slider-effects']) . '" ': 
						'data-effect="' . esc_attr($settings['effect']) . '" ';
			$ret .= '>';
			$title_font_size = '';
			if(isset($theevent_theme_option['title-font-size'])){
				if($theevent_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($theevent_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($theevent_theme_option['caption-font-size'])){
				if($theevent_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($theevent_theme_option['caption-font-size']).'px';
				}
			}
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){ 
				if( is_array($slide) ){

					// nivo slider caption
					$id = 'nivo-caption' . $theevent_gallery_id . '-' . $i; $i++;
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
						
						$caption .= '<div class="kode-nivo-caption" id="' . $id . '" >';
						$caption .= '<div class="kode-caption-wrapper position-' . $slide['caption-position'] . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($theevent_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))) . '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($theevent_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('div'=>array('id'=>array(),'class'=>array()),'h3'=>array('id'=>array(),'class'=>array()),'p'=>array('id'=>array(),'class'=>array()),'h2'=>array('id'=>array(),'class'=>array()),'a'=>array('id'=>array(),'class'=>array()),'span'=>array('id'=>array(),'class'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div style="color:'.esc_attr($theevent_theme_option['caption-btn-color']).';background:'.esc_attr($theevent_theme_option['caption-btn-color-bg']).'" class="kode_button_event_slider">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-caption
						$caption .= '</div>'; // kode-caption-inner
						$caption .= '</div>'; // kode-caption-wrapper
						$caption .= '</div>'; // kode-nivo-caption
					}				
					
					// flex slider link
					$attr = ' title="#' . $id . '" '; 
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= theevent_get_image($slide_id, esc_attr($settings['size']), array(), $attr);
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= theevent_get_image($slide_id, esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])), $attr);
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= theevent_get_image($slide_id, esc_attr($settings['size']), 
							array('id'=>esc_attr($theevent_gallery_id)), $attr);
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= theevent_get_image($slide_id, esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id)), $attr);
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= theevent_get_image($slide_id, esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($theevent_gallery_id)), $attr);
					}
				}else{
					$ret .= theevent_get_image($slide, esc_attr($settings['size']), array('id'=>$theevent_gallery_id), $attr);
				}
			}
			$ret .= '</div>'; // nivoSlider
			$ret .= $caption;
			$ret .= '</div>'; // nivoSlider-wrapper
			
			return $ret;
		}
	}	
	
	if( !function_exists('theevent_get_work_gallery') ){
		function theevent_get_work_gallery($theevent_post_option,$col="",$num_fetch=""){
			global $theevent_gallery_id, $theevent_spaces; $theevent_gallery_id++; 
			$ret = '';
			$slider_option = json_decode($theevent_post_option['image-gallery'], true);
			$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
			$num_page = ceil(sizeof($theevent_post_option['image-gallery']) / $num_fetch);
			$slide_order = $slider_option[0];
			$slide_data = $slider_option[1];					
			
			$slides = array();
			if(!empty($slide_order)){
				foreach( $slide_order as $slide_id ){
					$slides[$slide_id] = $slide_data[$slide_id];
				}
			}
			$theevent_post_option['slider'] = $slides;
			$gallery_print = '';
			if(isset($theevent_post_option['slider']) && $theevent_post_option['slider'] <> ''){
				$gallery_print  = '<div class="kode_project_flick_wrap gallery-item"><ul>';
			
				$current_size = 0;
				foreach($theevent_post_option['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $num_fetch) &&
						($current_size < ($paged) * $num_fetch) ){

						if( !empty($current_size) && ($current_size % $col == 0) ){
							$gallery_print .= '<li class="clear"></li>';
						}			
						$image_src = wp_get_attachment_image_src($slide_id, 'full');	
						$gallery_print .= '
						<li>
							<a href="'.esc_url($image_src[0]).'" data-gal="prettyphoto[]">';
								if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
									$gallery_print .= theevent_get_image($slide_id, esc_attr($theevent_post_option['thumbnail-size']));
								}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
									$gallery_print .= theevent_get_image($slide_id, esc_attr($theevent_post_option['thumbnail-size']), 
										array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
								}else if($slide['slide-link'] == 'current'){
									$gallery_print .= theevent_get_image($slide_id, esc_attr($theevent_post_option['thumbnail-size']), 
										array('id'=>esc_attr($theevent_gallery_id)));
								}else if($slide['slide-link'] == 'image'){
									$gallery_print .= theevent_get_image($slide_id, esc_attr($theevent_post_option['thumbnail-size']), 
										array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id)));
								}else if($slide['slide-link'] == 'video'){
									$gallery_print .= theevent_get_image($slide_id, esc_attr($theevent_post_option['thumbnail-size']), 
										array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$theevent_gallery_id));
								}
								$image_src = wp_get_attachment_image_src($slide_id, 'full');	
								if(!isset($slide['gallery_caption'])){
									$slide['gallery_caption'] = '';
								}
								if(!isset($slide['gallery_title'])){
									$slide['gallery_title'] = '';
								}
							$gallery_print .= '	
							</a>
						</li>';
						
					}
				$current_size ++;
				}					
			$gallery_print .= '</ul></div>  '; // kode-gallery-item
			
			}
			return $gallery_print;
		}
	}
	
	// gallery item
	if( !function_exists('theevent_get_gallery_item') ){
		function theevent_get_gallery_item( $settings ){
			// title section	
			//$ret .= theevent_get_item_title($settings);		
			$ret = '';
			$slider_option = json_decode($settings['slider'], true);
			
			$slide_order = $slider_option[0];
			$slide_data = $slider_option[1];					
			
			$slides = array();
			if(!empty($slide_order)){
				foreach( $slide_order as $slide_id ){
					$slides[$slide_id] = $slide_data[$slide_id];
				}
			}
			$settings['slider'] = $slides;
			
			//if( $settings['gallery-style'] == 'thumbnail' ) return theevent_get_gallery_thumbnail($settings);
			return $ret . theevent_get_gallery($settings);
		}
	}	
	
	
	
	
	// print gallery function
	if( !function_exists('theevent_get_gallery') ){
		function theevent_get_gallery( $settings ){
			global $theevent_gallery_id, $theevent_spaces; $theevent_gallery_id++; 

			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			// start printing gallery
			$current_size = 0;
			$settings['num-fetch'] = empty($settings['num-fetch'])? 9999: intval($settings['num-fetch']);
			$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
			$num_page = ceil(sizeof($settings['slider']) / $settings['num-fetch']);
			
			$ret  = '<div class="'.esc_attr($settings['element-item-class']).' kode-style-gal-'.esc_attr($settings['style']).' kode-gallery kode-gutter-gallery bottom-spacer">';
			
			if($settings['style'] == 'simple-gallery'){
				if($settings['layout'] == 'without-space'){
					$ret  .= '<ul class="row kode-item kode-padding-free" ' . $item_id . $margin_style . '>';	
				}else{
					$ret  .= '<ul class="row kode-item" ' . $item_id . $margin_style . '>';	
				}
				foreach($settings['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $settings['num-fetch']) &&
						($current_size < ($paged) * $settings['num-fetch']) ){

						if( !empty($current_size) && ($current_size % $settings['gallery-columns'] == 0) ){
							$ret .= '<li class="clear"></li>';
						}			
						$ret .= '<li class="gallery-item ' . theevent_get_column_class('1/' . $settings['gallery-columns']) . '">';
							$ret .= '<div class="kf_gallery kode-item">
								<figure class="effect-hera kode-ux">';
									if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']));
									}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
									}else if($slide['slide-link'] == 'current'){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('id'=>esc_attr($theevent_gallery_id)));
									}else if($slide['slide-link'] == 'image'){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id)));
									}else if($slide['slide-link'] == 'video'){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$theevent_gallery_id));
									}
									$image_src = wp_get_attachment_image_src($slide_id, 'full');	
									if(!isset($slide['gallery_caption'])){
										$slide['gallery_caption'] = '';
									}
									if(!isset($slide['gallery_title'])){
										$slide['gallery_title'] = '';
									}
									$ret .= '
								</figure>';
								if($settings['show-caption'] != 'no'){
									$ret .= '
									<div class="kode_galry_des">
										<h5><a class="search-portfolio" data-rel="prettyphoto[]" href="'.esc_url($image_src[0]).'">' . $slide['gallery_title'] . '</a></h5>
										<p>' . $slide['gallery_caption'] . '</p>
									</div>';
								}
							$ret .= '</div>'; // gallery item
						$ret .= '</li>'; // gallery column				
					}
					$current_size ++;
				}
				$ret .= '<li class="clear"></li>';
				$ret .= '</ul>'; // kode-gallery-item
			}else if($settings['style'] == 'normal-gallery'){
				if($settings['layout'] == 'without-space'){
					$ret  .= '<ul class="row kode-item kode-padding-free" ' . $item_id . $margin_style . '>';	
				}else{
					$ret  .= '<ul class="row kode-item" ' . $item_id . $margin_style . '>';	
				}
				foreach($settings['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $settings['num-fetch']) &&
						($current_size < ($paged) * $settings['num-fetch']) ){

						if( !empty($current_size) && ($current_size % $settings['gallery-columns'] == 0) ){
							$ret .= '<li class="clear"></li>';
						}			
						$ret .= '<li class="gallery-item ' . theevent_get_column_class('1/' . $settings['gallery-columns']) . '">';
							$ret .= '<div class="kode_portfoiol_wrap kode-item">
								<figure class="effect-hera kode-ux">';
									if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']));
									}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
									}else if($slide['slide-link'] == 'current'){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('id'=>esc_attr($theevent_gallery_id)));
									}else if($slide['slide-link'] == 'image'){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id)));
									}else if($slide['slide-link'] == 'video'){
										$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$theevent_gallery_id));
									}
									$image_src = wp_get_attachment_image_src($slide_id, 'full');	
									if(!isset($slide['gallery_caption'])){
										$slide['gallery_caption'] = '';
									}
									if(!isset($slide['gallery_title'])){
										$slide['gallery_title'] = '';
									}
									$ret .= '
									<figcaption class="kode_fortfolio_detail">
										<div class="caption-holder">
											<a class="search-portfolio" data-rel="prettyphoto[]" href="'.esc_url($image_src[0]).'">
												<i class="fa fa-search"></i>
											</a>
											<a class="link-portfolio" href="'.esc_url($slide['url']).'">
												<i class="fa fa-link"></i>
											</a>
											<strong>' . $slide['gallery_title'] . '</strong>
										</div>
									</figcaption>
								</figure>';
								// if($settings['show-caption'] != 'no'){
									// $ret .= '
									// <div class="kode_galry_des">
										// <h5>' . $slide['gallery_title'] . '</h5>
										// <p>' . $slide['gallery_caption'] . '</p>
									// </div>';
								// }
							$ret .= '</div>'; // gallery item
						$ret .= '</li>'; // gallery column				
					}
					$current_size ++;
				}
				$ret .= '<li class="clear"></li>';
				$ret .= '</ul>'; // kode-gallery-item
				
			}else if($settings['style'] == 'gallery-slider'){
				if($settings['layout'] == 'without-space'){
					$ret  .= '<div data-slide="'.esc_attr($settings['gallery-columns']).'" data-small-slide="'.esc_attr($settings['gallery-columns']).'" class="owl-no-space owl-theme kode_video_list" ' . $item_id . $margin_style . '>';
				}else{
					$ret  .= '<div data-slide="'.esc_attr($settings['gallery-columns']).'" data-small-slide="'.esc_attr($settings['gallery-columns']).'" class="owl-carousel owl-theme kode_video_list" ' . $item_id . $margin_style . '>';
				}
				foreach($settings['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $settings['num-fetch']) &&
						($current_size < ($paged) * $settings['num-fetch']) ){

						if( !empty($current_size) && ($current_size % $settings['gallery-columns'] == 0) ){
							$ret .= '';
						}
						$ret .= '<div class="item">';
							$ret .= '<div class="kode-newgallery">
										<div class="ourgallery-pic">';
											if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
												$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']));
											}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
												$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
													array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
											}else if($slide['slide-link'] == 'current'){
												$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
													array('id'=>esc_attr($theevent_gallery_id)));
											}else if($slide['slide-link'] == 'image'){
												$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
													array('url'=>esc_url($slide['url']), 'id'=>esc_attr($theevent_gallery_id)));
											}else if($slide['slide-link'] == 'video'){
												$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
													array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$theevent_gallery_id));
											}
											$image_src = wp_get_attachment_image_src($slide_id, 'full');	
											$ret .= '
										</div>
										<div class="overcontent">
											<ul>
												<li><a href="'.esc_url($slide['url']).'"><i class="fa fa-link"></i></a></li>
												<li><a class="search-portfolio" data-rel="prettyphoto[]" href="'.esc_url($image_src[0]).'"><i class="fa fa-search"></i></a></li>
											</ul>';
											if(isset($slide['gallery_title'])){
												$ret .= '<h3>'.$slide['gallery_title'].'</h3>';
											}
											if(isset($slide['gallery_caption'])){
												$ret .= '<h4>'.$slide['gallery_caption'].'</h4>';
											}
											$ret .= '
										</div>
									</div>';
							$ret .= '</div>'; // gallery item
					}
					$current_size ++;
				}
				//$ret .= '<div class="clear"></div>';
				$ret .= '</div>'; // kode-gallery-item
				
			}
			if( $settings['pagination'] == 'enable' ){
				$ret .= theevent_get_pagination($num_page, $paged);
			}
			$ret .= '</div>'; // kode-gallery-item
			
			
			return $ret;
		}
	}
	if( !function_exists('theevent_get_gallery_thumbnail') ){
		function theevent_get_gallery_thumbnail( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' . esc_attr($settings['element-item-id']) . '" ';

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . esc_attr($margin) . '" ': '';			
			
			$ret  = '<div class="kode-gallery-item kode-item kode-gallery-thumbnail" ' . $item_id . $margin_style . '>';
			
			// full image
			$ret .= '<div class="kode-gallery-thumbnail-container">';
			foreach($settings['slider'] as $slide_id => $slide){
				$ret .= '<div class="kode-gallery-thumbnail" data-id="' . $slide_id . '" >';
				$ret .= theevent_get_image($slide_id);
				if($settings['show-caption'] != 'no'){
					$ret .= '<div class="gallery-caption-wrapper">';
					$ret .= '<span class="gallery-caption">';
					$ret .= theevent_get_attachment_info($slide_id, 'caption');
					$ret .= '</span>';
					$ret .= '</div>';
				}
				$ret .= '</div>';
			}
			$ret .= '</div>';
			
			// start printing gallery
			$current_size = 0;
			foreach($settings['slider'] as $slide_id => $slide){
				if( !empty($current_size) && ($current_size % $settings['gallery-columns'] == 0) ){
					$ret .= '<div class="clear"></div>';
				}			
			
				$ret .= '<div class="gallery-column ' . theevent_get_column_class('1/' . $settings['gallery-columns']) . '">';
				$ret .= '<div class="gallery-item" data-id="' . $slide_id . '" >';
				$ret .= theevent_get_image($slide_id, esc_attr($settings['thumbnail-size']));
				$ret .= '</div>'; // gallery item
				$ret .= '</div>'; // gallery column
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';
			
			$ret .= '</div>'; // kode-gallery-item
			
			return $ret;
		}
	}	
	
	
	
		


?>