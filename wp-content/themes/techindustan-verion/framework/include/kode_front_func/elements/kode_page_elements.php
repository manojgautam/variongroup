<?php
	/*	
	*	Kodeforest Theme File
	*	---------------------------------------------------------------------
	*	This file contains the function use to print the elements of the theme
	*	---------------------------------------------------------------------
	*/
	
	// print title
	if( !function_exists('theevent_get_item_title') ){
		function theevent_get_item_title( $atts ){
			$ret = '';
			
			$atts['title-type'] = (empty($atts['title-type']))? $atts['type']: $atts['title-type'];
		
			if( !empty($atts['title-type']) && $atts['title-type'] != 'none' ){
				$item_class  = 'pos-' . str_replace('-divider', '', $atts['title-type']);
				$item_class .= (!empty($atts['carousel']))? ' kode-nav-container': '';
				
				$ret .= '<div class="kode-item-title-wrapper kode-item ' . esc_attr($item_class) . ' ">';
				
				$ret .= '<div class="kode-item-title-head">';
				$ret .= !empty($atts['carousel'])? '<i class="icon-angle-left kode-flex-prev"></i>': '';
				if(!empty($atts['title'])){
					$ret .= '<h3 class="kode-item-title kode-skin-title kode-skin-border">' . esc_attr($atts['title']) . '</h3>';
				}
				$ret .= !empty($atts['carousel'])? '<i class="icon-angle-right kode-flex-next"></i>': '';
				$ret .= '<div class="clear"></div>';
				$ret .= '</div>';
				
				$ret .= (strpos($atts['title-type'], 'divider') > 0)? '<div class="kode-item-title-divider"></div>': '';
				
				if(!empty($atts['caption'])){
					$ret .= '<div class="kode-item-title-caption kode-skin-info">' . esc_attr($atts['caption']) . '</div>';
				}
				
				if(!empty($atts['right-text']) && !empty($atts['right-text-link'])){
					$ret .= '<a class="kode-item-title-link" href="' . esc_url($atts['right-text-link']) . '" >' . esc_attr($atts['right-text']) . '</a>';
				}
				
				$ret .= '</div>'; // kode-item-title-wrapper
			}
			return $ret;
		}
	}
		// print title
		
	if( !function_exists('theevent_get_maintenace_mode') ){
		function theevent_get_maintenace_mode( ){
			global $theevent_theme_option;
			$theevent_theme_option['maintenance-mode'];
			//echo $theevent_theme_option['maintenance-logo'];
			$maintenance_logo = wp_get_attachment_image_src($theevent_theme_option['maintenance-logo'], 'full');			
			$maintenance_bg = wp_get_attachment_image_src($theevent_theme_option['maintenance-background'], 'full');			
			$theevent_theme_option['maintenance-background'];
			$theevent_theme_option['maintenance-title'];
			$theevent_theme_option['maintenance-caption'];
			$theevent_theme_option['maintenance-newsletter'];
			$theevent_theme_option['maintenance-social'];
			
			$event_year = date('Y',strtotime($theevent_theme_option['maintenance-date']));
			$event_month = date('m',strtotime($theevent_theme_option['maintenance-date']));
			$event_day = date('d',strtotime($theevent_theme_option['maintenance-date']));
			$event_start_time = date("G:i:s", strtotime($theevent_theme_option['maintenance-date']));
			$html = '
			<!--Comming Soon Wrap Start-->
			<section class="kf_comming_bg" style="background:url('.esc_url($maintenance_bg[0]).')">
				<div class="container">
					<div class="kf_comming_wrap">
						<a href="#"><img src="'.esc_url($maintenance_logo[0]).'" alt="'.esc_attr($theevent_theme_option['maintenance-title']).'"></a>
					</div>
					<div class="kf_comming_counter">
						<h2>'.esc_attr($theevent_theme_option['maintenance-title']).'</h2>
						<h4>'.esc_attr($theevent_theme_option['maintenance-caption']).'</h4>
						<ul data-year="'.esc_attr($event_year).'" data-month="'.esc_attr($event_month).'" data-day="'.esc_attr($event_day).'" data-time="'.esc_attr($event_start_time).'" class="downcount">
							<li>
								<span class="days">00</span>
								<p class="days_ref">Week</p>
							</li>
							<li>
								<span class="hours">00</span>
								<p class="hours_ref">Days</p>
							</li>
							<li>
								<span class="minutes">00</span>
								<p class="minutes_ref">Hours</p>
							</li>
							<li>
								<span class="seconds last">00</span>
								<p class="seconds_ref">Second</p>
							</li>
						</ul>
					</div>
					
					<div class="kf_comming_form">
						<form>
							<input type="text" placeholder="Your Mail">
							<button>keep me updated</button>
						</form>
					</div>
					
					<div class="kf_comming_social">
						'.theevent_print_header_social_icon_unechod('kode_social_icon').'
					</div>
					
				</div>
			</section>
			<!--Comming Soon Wrap Start-->
			';
			
			return $html;
		}
	}	
	
	
	
		

	if( !function_exists('theevent_get_column_donate_item') ){
		function theevent_get_column_donate_item($settings){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';		
			
			if( is_numeric($settings['donate-image']) ){
				$theevent_image = wp_get_attachment_image_src($settings['donate-image'], 'full');
				$theevent_image = esc_url($theevent_image[0]);
			}else{
				$theevent_image = esc_url($settings['donate-image']);
			}
			
			$title_donate = empty($settings['title-donate'])? '': '<h2>' .esc_attr( $settings['title-donate'] ). '</h3>';
			$desc_donate = empty($settings['desc-donate'])? '': '<p>' .esc_attr( $settings['desc-donate'] ). '</p>';
			$html_donate = '';
			$html_donate = '<div class="kode-cause">
				<div class="row">
					<div class="col-md-6">
						<img alt="'.esc_attr($settings['title-donate']).'" src="'.esc_attr($theevent_image).'">
					</div>
					<div class="col-md-6">
						'.$title_donate.'
						'.$desc_donate.'
						<a class="read-more" href="'.esc_url($settings['link']).'">'.esc_attr($settings['link-text']).'</a>
						<!--RANGE SLIDER START-->
						<div class="kode-range">
							Min: $ <span class="range-slider">500</span>
							<input class="range" type="text" data-slider-min="0" data-slider-max="2000" data-slider-step="1" data-slider-value="500"/>
							<span id="ex6CurrentSliderValLabel"></span>
						</div>
						<!--RANGE SLIDER END-->
						<!--DONATE TEXT START-->
						<div class="donate-text">
							<p>'.esc_attr($settings['sub-donate']).'</p>
							<a class="btn-filled" href="'.esc_url($theevent_theme_option['donatation_page']).'">Donate Now</a>
						</div>               
						<!--DONATE TEXT END-->             
					</div>
				</div>
				</div>';
			
			return $html_donate;
			
		}
	}

	// title item
	if( !function_exists('theevent_get_title_item') ){
		function theevent_get_title_item( $settings ){	
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';		
			
			$ret  = '<div class="kode-title-item" ' . $item_id . $margin_style . ' >';
			//$ret .= theevent_get_item_title($settings);			
			$ret .= '</div>';
			return $ret;
		}
	}
	
	// sidebar item
	if( !function_exists('theevent_get_sidebar_item') ){
		function theevent_get_sidebar_item( $settings ){
			$ret = '';
			return dynamic_sidebar($settings['widget']);			
		}
	}	
	
	// accordion item
	if( !function_exists('theevent_get_accordion_item') ){
		function theevent_get_accordion_item( $settings ){
			global $theevent_counter;
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			$accordion = is_array($settings['accordion'])? esc_attr($settings['accordion']): json_decode($settings['accordion'], true);

			//$ret  = theevent_get_item_title($settings);	
			$ret = '';
			$ret .= '<div class="kode-item kd-accordion kode-accordion-item" ' . $item_id . $margin_style . ' >';
			$ret .= '
			<script type="text/javascript">
			jQuery(document).ready(function($){
				/* ---------------------------------------------------------------------- */
				/*	Accordion Script
				/* ---------------------------------------------------------------------- */
				if($(".accordion-'.esc_attr($theevent_counter).'").length){
					//custom animation for open/close
					$.fn.slideFadeToggle = function(speed, easing, callback) {
					  return this.animate({opacity: "toggle", height: "toggle"}, speed, easing, callback);
					};

					$(".accordion-'.esc_attr($theevent_counter).'").accordion({
					  defaultOpen: "section_1'.esc_attr($theevent_counter).'",
					  cookieName: "nav",
					  speed: "slow",
					  animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
						elem.next().stop(true, true).slideFadeToggle(opts.speed);
					  },
					  animateClose: function (elem, opts) { //replace the standard slideDown with custom function
						elem.next().stop(true, true).slideFadeToggle(opts.speed);
					  }
					});
				}
			});
			</script>
			';

			$current_tab = 0;
			foreach( $accordion as $tab ){  $current_tab++;
				$ret .= '<div class="kf_faq_accordian">';
				$ret .= '<div id="';
				$ret .= ($current_tab == intval($settings['initial-state']))? 'section_1'.esc_attr($theevent_counter).'"': 'section_'.esc_attr($current_tab).esc_attr($theevent_counter).'"';
				$ret .= ' class="kf_accordian_hdg accordion-'.esc_attr($theevent_counter).'" ';
				//$ret .= empty($tab['kdf-tab-title-id'])? '': 'id="' . esc_attr($tab['kdf-tab-title-id']) . '" ';
				$ret .= '>' . theevent_text_filter($tab['kdf-tab-title']) . '<span class="fa ';
				$ret .= ($current_tab == intval($settings['initial-state']))? 'fa-minus': 'fa-minus';
				$ret .= '"></span></div>';
				$ret .= '<div class="kf_accordian_des">' . theevent_content_filter($tab['kdf-tab-content']) . '</div></div>';			
			}
			$ret .= '</div>';
			
			return $ret;
		}
	}	
	
	// breaking news item
	if( !function_exists('theevent_get_breaking_news') ){
		function theevent_get_breaking_news( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			$settings['element-item-class'] = (empty($settings['element-item-class']))? '': $settings['element-item-class'];
			if( !empty($settings['category']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}			
			}
			
			$ret = '<div '.$item_id.' class="'.esc_attr($settings['element-item-class']).'">';
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$query = new WP_Query( $args );
			$settings['num-fetch'] = (empty($settings['num-fetch']))? '20': $settings['num-fetch'];
			$ret .= '<div class="kode_breaking_news">
				<div class="kode_breaking_hdg"><h6>'.esc_attr($settings['kode-news-title']).'</h6></div>
					<div class="kode_brekg_news_des"><div data-ticker="true" data-slidespeed="500" data-pausetime="10000" data-auto="true" data-tickerHover="false" data-mode="horizontal" data-min="1" data-max="1" class="bxslider">';
					while($query->have_posts()){ $query->the_post();				
						$ret .= '<div class="item"><p>'.substr(get_the_content(),0,120).'</p></div>';
					}
					$ret .= '
					</div>
				</div></div>';
			wp_reset_postdata();
			$ret .= '</div>';
			return $ret;
		}
	}	
	
	// breaking news item
	if( !function_exists('theevent_get_headings_item') ){
		function theevent_get_headings_item( $settings ){
			global $theevent_counter;
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$settings['element-item-class'] = (empty($settings['element-item-class']))? '': $settings['element-item-class'];
			$settings['title'] = (empty($settings['title']))? '': $settings['title'];
			$settings['caption'] = (empty($settings['caption']))? '': $settings['caption'];
			$settings['title-color'] = (empty($settings['title-color']))? '': $settings['title-color'];
			$settings['line-color'] = (empty($settings['line-color']))? '': $settings['line-color'];
			$settings['caption-color'] = (empty($settings['caption-color']))? '': $settings['caption-color'];
			
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$ret = '<div class="kode-simple-heading '.esc_attr($settings['element-item-class']).'" '.$margin_style.'>';
			if(isset($settings['element-style']) && $settings['element-style'] == 'style-1'){
				if(isset($settings['title']) && $settings['title'] <> ''){
					$ret .= '
					<style scoped>
					#kode-headings-'.esc_attr($theevent_counter).' h4::before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div id="kode-headinsg-'.esc_attr($theevent_counter).'" class="kf_heading_2">
						<h3 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h3>
						<span style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</span>
					</div>';
				}
			}else if(isset($settings['element-style']) && $settings['element-style'] == 'style-2'){				
				if(isset($settings['title']) && $settings['title'] <> ''){
				$ret .= '
					<style scoped>
					#kode-headings-'.esc_attr($theevent_counter).' h4::before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div id="kode-headings-'.esc_attr($theevent_counter).'" class="kf_heading_1">
						<h2 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h2>
						<span style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</span>
					</div>';
				}	
			}else if(isset($settings['element-style']) && $settings['element-style'] == 'style-3'){				
				if(isset($settings['title']) && $settings['title'] <> ''){
				$ret .= '
					<style scoped>
					#kode-headings-'.esc_attr($theevent_counter).' h2::before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div id="kode-headings-'.esc_attr($theevent_counter).'" class="kode-heading-overview">
						<h2 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h2>
						<p style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</p>
					</div>';
				}	
			}else if(isset($settings['element-style']) && $settings['element-style'] == 'style-4'){				
				if(isset($settings['title']) && $settings['title'] <> ''){
				$ret .= '
					<style scoped>
					#kode-headings-'.esc_attr($theevent_counter).' h2::before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>					
					<div id="kode-headings-'.esc_attr($theevent_counter).'" class="kf_heading_4">
						<h2 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h2>
						<span style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</span>
					</div>';
				}	
			}else{
				if(isset($settings['title']) && $settings['title'] <> ''){
					$ret .= '
					<style scoped>
					#kode-headings-'.esc_attr($theevent_counter).' h4::before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div id="kode-headings-'.esc_attr($theevent_counter).'" class="kf_heading_2 kf_heading_new_2">
						<h3 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h3>
						<span style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</span>
					</div>';
				}
			}
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	// breaking news item
	if( !function_exists('theevent_get_contact_detail_item') ){
		function theevent_get_contact_detail_item( $settings ){
			global $theevent_counter;
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$settings['element-item-class'] = (empty($settings['element-item-class']))? '': $settings['element-item-class'];
			$settings['office-1'] = (empty($settings['office-1']))? '': $settings['office-1'];
			$settings['icon-1'] = (empty($settings['icon-1']))? '': $settings['icon-1'];
			$settings['icon-2'] = (empty($settings['icon-2']))? '': $settings['icon-2'];
			$settings['icon-3'] = (empty($settings['icon-3']))? '': $settings['icon-3'];
			$settings['title-1'] = (empty($settings['title-1']))? '': $settings['title-3'];
			$settings['title-2'] = (empty($settings['title-2']))? '': $settings['title-2'];
			$settings['title-3'] = (empty($settings['title-3']))? '': $settings['title-3'];
			$settings['address-1'] = (empty($settings['address-1']))? '': $settings['address-1'];
			$settings['address-2'] = (empty($settings['address-2']))? '': $settings['address-2'];
			$settings['address-3'] = (empty($settings['address-3']))? '': $settings['address-3'];
			$settings['office-2'] = (empty($settings['office-2']))? '': $settings['office-2'];
			
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) &&
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$ret = '<div class="kode-contact-detail '.esc_attr($settings['element-item-class']).'" '.$margin_style.'>';
			
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	
	
	// breaking news item
	if( !function_exists('theevent_get_blog_slider_item') ){
		function theevent_get_blog_slider_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			if( !empty($settings['category']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}			
			}
			$ret = '';
			$slider_class = '';
			$settings['thumbnail-size'];
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			
			$settings['num-fetch'] = (empty($settings['num-fetch']))? '20': $settings['num-fetch'];
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];
			$settings['heading'] = (empty($settings['heading']))? '20': $settings['heading'];
			$settings['caption'] = (empty($settings['caption']))? '20': $settings['caption'];
			$settings['element-style'] = (empty($settings['element-style']))? 'style-1': $settings['element-style'];
			$settings['element-type'] = (empty($settings['element-type']))? 'slider': $settings['element-type'];
			if($settings['element-type'] == 'simple-post'){$slider_class = '';$args['posts_per_page'] = 1;}else{$slider_class = 'bxslider';}
			
			$query = new WP_Query( $args );
			if($settings['element-style'] == 'style-1'){
				$ret .= '
					<div '.$margin_style.' class="kode_latst_post_lst">                	
                    	<ul data-mode="fade" class="post_bxslider '.esc_attr($slider_class).'">';
                            while($query->have_posts()){ $query->the_post();global $post;
							global $post;							
							$ret .= '
							<li class="kode_latest_blog kode-item">
                                <figure class="kode-ux">
									'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
                                </figure>
                                <h6>'.theevent_get_blog_info(array('category'), false, '','span').'</h6>
                                <div class="kode_latest_blog_des">
                                    <h6> <a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h6>
                                    <ul>
										'.theevent_get_blog_info(array('comment','views','author'), false, '','li').'
                                    </ul>
                                </div>
                            </li> ';
							}
                        $ret .= '    
                        </ul>
                    </div>';
			}else if($settings['element-style'] == 'style-2'){
				$ret .= '
				<div '.$margin_style.' data-mode="fade" class="kode-item-post-slider '.esc_attr($slider_class).'">';
				while($query->have_posts()){ $query->the_post();global $post;
					$ret .= '
					<div class="kode_achment_wrap kode-item">
						<figure class="kode-ux">
							'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
							<figcaption>
								<h6>'.esc_attr(get_the_date('D M, Y')).'</h6>
							</figcaption>
						</figure>
						<div class="kode_achment_des">
							<h5><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h5>
						</div>
					</div>';
				}
				$ret .= '</div>';
			}else if($settings['element-style'] == 'style-3'){
				$ret .= '<div class="kode_latest_blog_wrap">
				<ul '.$margin_style.' data-margin="30" data-width="570" data-min="2" data-max="4" data-mode="horizontal" class="'.esc_attr($slider_class).'">';
				while($query->have_posts()){ $query->the_post();global $post;				
				$ret .= '
					<li>
						<div class="kode_blog">
							<figure class="kode-ux">
								'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
							</figure>
							<div class="kode-caption">
								<div class="kode-user">
									<div class="kode-thumb">
										'.get_avatar(get_the_author_meta('ID'), 90).'									
									</div>
									<div class="kode_blog_posting">
										<h6>'.esc_attr(get_the_date()).'</h6>
										<h5>John Doe</h5>
									</div>
								</div>
								<div class="kode-text">
									<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h4>
									<p>'.substr(get_the_content(),0,50).'</p>
									<a class="view" href="'.esc_url(get_permalink()).'">'.esc_attr__('View Blog','the-event').'</a>
								</div>
							</div>
						</div>						
					</li>';
				}
				$ret .= '</ul></div>';
				
			}else{}
					wp_reset_postdata();
			return $ret;
		}
	}	
	
	
	
	
	// Simple Column item
	if( !function_exists('theevent_get_simple_column_item') ){
		function theevent_get_simple_column_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$settings['element-item-class'] = (empty($settings['element-item-class']))? '': $settings['element-item-class'];

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			$ret  = '<div '.$item_id.' class="simple-column '.esc_attr($settings['element-item-class']).' " '.$margin_style.'>'.theevent_content_filter($settings['content']).'</div>';
			return $ret;
		}
	}	
	
	// column service item
	if( !function_exists('theevent_get_column_service_item') ){
		function theevent_get_column_service_item( $settings ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' . esc_attr($settings['page-item-id']) . '" ';
			
			
			
			global $theevent_spaces,$theevent_allowed_html_array;
			
			$theevent_allowed_html_array = array(
				'a' => array(
					'href' => array(),
					'title' => array()
				),
				'ul' => array(),
				'li' => array(),
				'br' => array(),
				'span' => array(),
				'em' => array(),
				'strong' => array(),
				'&gt;' => array(),
				'style' => array(
					'scoped'=>array(),
					'id'=>array(),
				),
			);	
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$settings['style'] = empty($settings['style'])? 'type-1': $settings['style'];
			$settings['icon_type'] = empty($settings['icon_type'])? 'fa fa-lock': $settings['icon_type'];
			if($settings['style'] == 'type-1'){
				$thumbnail = wp_get_attachment_image_src( $settings['service-image-box'] , $settings['thumbnail-size'] );
			$ret = '
			<div '.$item_id.' class="main-service-box kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>
					<div class="icon-srvc">';
						if($settings['icon_type'] <> ''){
							$ret .= '<i class="' . esc_attr($settings['icon_type']) . '"></i>';
						}
					$ret .= '</div>
					<div class="srvc-content">
						<h3>' . theevent_text_filter($settings['title']) . '</h3>
						<p>'.wp_kses($settings['content'],$theevent_allowed_html_array).'</p>
						<strong class="bottom-border"></strong>
					</div>
				</div>';
			}else if($settings['style'] == 'type-2'){
			$ret = '
			<div '.$item_id.' class="kode-welcome-overview kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>';
				$ret .= '
				<div class="kf_overview_list">';
					if($settings['icon_type'] <> ''){
						$ret .= '<i class="icon_color_1 ' . esc_attr($settings['icon_type']) . '"></i>';
					}
					 $ret .= '
					 <div class="kode-text">	
						<h5>' . theevent_text_filter($settings['title']) . '</h5>
						<p>'.wp_kses($settings['content'],$theevent_allowed_html_array).'</p>
					 </div>
				</div>
			</div>';
			}else if($settings['style'] == 'type-3'){
				$ret = '
				<div '.$item_id.' class="kode-welcome kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>
					<div class="kode_focus_wrap">
						<div class="icon_captions">
							<div class="icon-srvc">';
							if($settings['icon_type'] <> ''){
								$ret .= '<i class="' . esc_attr($settings['icon_type']) . '"></i>';
							}
							$ret .= '
							</div>
							<div class="srvc-content">
								<h4>' . theevent_text_filter($settings['title']) . '</h4>
								<p>'.wp_kses(theevent_content_filter($settings['content']),$theevent_allowed_html_array).'</p>
							</div>
						</div>
					</div>	
				</div>';
			}else if($settings['style'] == 'type-4'){
				$thumbnail = wp_get_attachment_image_src( $settings['service-image-box'] , $settings['thumbnail-size']);
				$ret = '
				<div '.$item_id.' class="kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>
					<div class="kf_action3_wrap border-top">
						';
						if($settings['icon_type'] <> ''){
							$ret .= '<span><i class="' . esc_attr($settings['icon_type']) . '"></i></span>';
						}
						$ret .= '
						<div class="kf_action3_des">
							<h6><a href="'.esc_url($settings['link']).'">' . theevent_text_filter($settings['title']) . '</a></h6>
							<p>'.wp_kses(theevent_content_filter($settings['content']),$theevent_allowed_html_array).'</p>
						</div>
					</div>
				</div>';
			}else if($settings['style'] == 'type-5'){
				$ret = '
				<div class="kf_party_seat_detail">
					<a href="#"><i class="' . esc_attr($settings['icon_type']) . '"></i></a>
					<div class="kf_party_seat_des">
						<h5>' . theevent_text_filter($settings['title']) . '</h5>
						<p>'.wp_kses(theevent_content_filter($settings['content']),$theevent_allowed_html_array).'</p>
					</div>
				</div>';
				
			}else{
				
			}
			
			return $ret;
		}
	}
	
	
	// Become A Volunteer item
	if( !function_exists('theevent_get_volunteer') ){
		function theevent_get_volunteer($settings){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];
			$settings['heading'] = (empty($settings['heading']))? '20': $settings['heading'];
			$settings['caption'] = (empty($settings['caption']))? '20': $settings['caption'];
			$settings['kode-volunteer-title'] = (empty($settings['kode-volunteer-title']))? '20': $settings['kode-volunteer-title'];
			$settings['kode-volunteer-caption'] = (empty($settings['kode-volunteer-caption']))? '20': $settings['kode-volunteer-caption'];
			$settings['volunteer-icon'] = empty($settings['volunteer-icon'])? 'fa fa-lock': $settings['volunteer-icon'];
			$settings['volunteer-icon-2'] = empty($settings['volunteer-icon-2'])? 'fa fa-lock': $settings['volunteer-icon-2'];
			$settings['volunteer-content'] = (empty($settings['volunteer-content']))? '20': $settings['volunteer-content'];
			
			$ret = '<div '.$margin_style.' class="kode_volunteer_bg">';
				$ret .= '
				<div class="kode_hdg_3">
                    <h6>'.esc_attr($settings['kode-volunteer-title']).'</h6>
                    <h4>'.esc_attr($settings['kode-volunteer-caption']).'</h4>
                </div>';
				
				$ret .= '<div class="kode_volunteer">
                	<p>'.esc_attr($settings['volunteer-content']).'</p>
                </div>';
				
				$ret .= '<div class="kode_volunteer_btn">
                	<a href="'.esc_url($settings['volunteer-link']).'" class="donate">
                    	<i class="'.esc_attr($settings['volunteer-icon']).'"></i>
                    	<span>'.esc_attr($settings['volunteer-link-text']).'</span>
                    </a>
                   <a href="'.esc_url($settings['volunteer-link-2']).'"  class="kf_volunteer">
						<i class="'.esc_attr($settings['volunteer-icon-2']).'"></i>
                    	<span>'.esc_attr($settings['volunteer-link-text-2']).'</span>	
                    </a>
                </div>';

			
			
			$ret .= '</div>'; // End Element Container
			return $ret;
			
		}
	}
	// content item
	if( !function_exists('theevent_get_content_item') ){
		function theevent_get_content_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$ret = '<div '.$item_id.' class="k-content-container" '.$margin_style.'>';
			while ( have_posts() ){ the_post();
				$content = theevent_content_filter(get_the_content(), true); 
				
				//$ret .= '<div class="container">';
					//Show Title
					if( empty($settings['show-title']) || $settings['show-title'] != 'disable' ){
						$ret .= '<div class="kode-item k-title"><h2>';
							$ret .= get_the_title();
						$ret .= '</h2></div>';
					}
					//Show Content
					if( empty($settings['show-content']) || $settings['show-content'] != 'disable' ){
						if(!empty($content)){
							$ret .= '<div class="kode-item k-content kode_news_detail">';
								$ret .= $content;
							$ret .= '</div>';
						}
					}
					// if ( comments_open() || get_comments_number() ) :
						// comments_template();
					// endif;
				//$ret .= '</div>'; // Grid Container
			} // WHile Loop End
			$ret .= '</div>'; // End Element Container
			return $ret;
		}
	}	
	
	// content item
	if( !function_exists('theevent_get_default_content_item') ){
		function theevent_get_default_content_item( $settings ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' .esc_attr( $settings['page-item-id'] ). '" ';
			?>
			<div <?php echo esc_attr($item_id);?> class="k-content-container">
			<?php
			while ( have_posts() ){ the_post();
				$content = theevent_content_filter(get_the_content(), true); 
					//Show Title
					if( empty($settings['show-title']) || $settings['show-title'] != 'disable' ){ ?>
						<div class="kode-item k-title"><h2>
							<?php echo esc_attr(get_the_title());?>
						</h2></div>
					<?php }
					//Show Content
					if( empty($settings['show-content']) || $settings['show-content'] != 'disable' ){
						if(!empty($content)){ ?>
							<div class="kode-item k-content kode_news_detail">
								<?php echo $content; ?>
							</div>
							<?php
						}
					}?>
					<div class="kode-single-detail">
						<div class="kode-admin-post">
							<figure><a href="#"><?php echo get_avatar(get_the_author_meta('ID'), 90); ?></a></figure>
							<div class="admin-info">
								<h4><?php the_author_posts_link(); ?></h4>
								<p><?php echo esc_attr(get_the_author_meta('description')); ?></p>
							</div>
						</div>
					</div>
					<?php
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				// Grid Container
			} // WHile Loop End
			?>
			</div>
		<?php
		}
	}

	// tab item
	if( !function_exists('theevent_get_tab_item') ){
		function theevent_get_tab_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';

			global $theevent_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $theevent_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr( $settings['margin-bottom'] ) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			$tabs = is_array($settings['tab'])? $settings['tab']: json_decode($settings['tab'], true);			
			$current_tab = 0;
			$ret = '';
			//$ret  = kode_get_item_title($settings);	
			$ret .= '<div class="kode-item kode-tab-item '  . esc_attr($settings['style']) . '" ' . $item_id . $margin_style . '>';
			$ret .= '<div class="tab-title-wrapper" >';
			foreach( $tabs as $tab ){  $current_tab++;
				$ret .= '<h4 class="tab-title';
				$ret .= ($current_tab == intval($settings['initial-state']))? ' active" ': '" ';
				$ret .= empty($tab['kdf-tab-title-id'])? '>': 'id="' . esc_attr($tab['kdf-tab-title-id']) . '" >';
				$ret .= empty($tab['kdf-tab-icon-title'])? '': '<i class="' . esc_attr($tab['kdf-tab-icon-title']) . '" ></i>';				
				$ret .= '<span>' . theevent_text_filter($tab['kdf-tab-title']) . '</span></h4>';				
			}
			$ret .= '</div>';
			
			$current_tab = 0;
			$ret .= '<div class="tab-content-wrapper" >';
			foreach( $tabs as $tab ){  $current_tab++;
				$ret .= '<div class="tab-content';
				$ret .= ($current_tab == intval($settings['initial-state']))? ' active" >': '" >';
				$ret .= theevent_content_filter($tab['kdf-tab-content']) . '</div>';
							
			}	
			$ret .= '</div>';	
			$ret .= '<div class="clear"></div>';
			$ret .= '</div>'; // kode-tab-item 
			
			return $ret;
		}
	}		

	if( !function_exists('theevent_get_divider_item') ){
		function theevent_get_divider_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			
			global $theevent_spaces;
			
			$margin = (!empty($settings['margin-bottom']))? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			//$style = empty($settings['size'])? '': ' style="width: ' . $settings['size'] . ';" ';
			$ret  = '<div class="clear"></div>';
			$ret .= '<div class="kode-item kode-divider-item" ' . $item_id . $margin_style . ' >';
			$ret .= '<div class="kode-divider"></div>';
			$ret .= '</div>';					
			
			return $ret;
		}
	}
	

?>