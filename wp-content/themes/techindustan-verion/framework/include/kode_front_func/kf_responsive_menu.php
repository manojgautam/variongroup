<?php
	/*	
	*	Kodeforest Menu Management File
	*	---------------------------------------------------------------------
	*	This file use to include a necessary script / function for the 
	* 	navigation area
	*	---------------------------------------------------------------------
	*/
	
	// add action to enqueue superfish menu
	add_filter('theevent_enqueue_scripts', 'theevent_register_dlmenu');
	if( !function_exists('theevent_register_dlmenu') ){
		function theevent_register_dlmenu($array){	
			$array['style']['dlmenu'] = THEEVENT_PATH . '/framework/assets/dl-menu/component.css';
			
			$array['script']['modernizr'] = THEEVENT_PATH . '/framework/assets/dl-menu/modernizr.custom.js';
			$array['script']['dlmenu'] = THEEVENT_PATH . '/framework/assets/dl-menu/jquery.dlmenu.js';

			return $array;
		}
	}
	
	// creating the class for outputing the custom navigation menu
	if( !class_exists('theevent_dlmenu_walker') ){
		
		// from wp-includes/nav-menu-template.php file
		class theevent_dlmenu_walker extends Walker_Nav_Menu{		

			function start_lvl( &$output, $depth = 0, $args = array() ) {
				$indent = str_repeat("\t", $depth);
				$output .= "\n$indent<ul class=\"dl-submenu\">\n";
			}

		}
		
	}

?>