<?php
	/*	
	*	WP_FileSystem Management
	*	---------------------------------------------------------------------
	*	This file contains functions that help you read/write files
	*	---------------------------------------------------------------------
	*/
	
	
	// Run System
	if( !function_exists('theevent_init_filesystem') ){
		function theevent_init_filesystem($url){	
			if (false === ($creds = request_filesystem_credentials($url, '', false, false, null) ) ) {
				return false;
			}
			
			if (!WP_Filesystem($creds)){
				request_filesystem_credentials($url, '', true, false, null);
				return false;
			}
		}
	}
	
	//Write File
	if( !function_exists('theevent_write_filesystem') ){
		function theevent_write_filesystem($current_page, $url, $data){	
			theevent_init_filesystem($current_page);
			
			global $wp_filesystem;
			if (!$wp_filesystem->put_contents($url, $data, FS_CHMOD_FILE)){
				return false;
			}
			return true;
		}
	}	
	
	
	// get remote file
	if( !function_exists('theevent_get_remote_file') ){
		function theevent_get_remote_file($url){
			$response = wp_remote_get($url);
			
			if( is_wp_error( $response ) ) {
				return array('success'=>false, 'error'=>$response->get_error_message());
			}else if( is_array($response) ) {
				return array('success'=>true, 'data'=>$response['body']);
			}
		}
	}
		
	//Read File
	if( !function_exists('theevent_read_filesystem') ){
		function theevent_read_filesystem($current_page, $url){	
			theevent_init_filesystem($current_page);
			
			global $wp_filesystem;
			return $wp_filesystem->get_contents($url);
		}
	}		

	
	

