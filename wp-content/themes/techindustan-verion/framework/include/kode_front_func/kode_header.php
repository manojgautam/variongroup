<?php
	/*	
	*	Kodeforest Header File
	*	---------------------------------------------------------------------
	*	This file contains utility function in the theme
	*	---------------------------------------------------------------------
	*/
	
	
	if( !function_exists('theevent_get_woo_cart') ){	
		function theevent_get_woo_cart(){
			if(class_exists('Woocommerce')){
				global $post,$post_id,$product,$woocommerce;	
				$currency = get_woocommerce_currency_symbol();
				if($woocommerce->cart->cart_contents_count <> 0){ 
					return $shopping_cart_div = '<div class="widget_shopping_cart_content"></div>';
				}else{ 
					return $shopping_cart_div = '<div class="widget_shopping_cart_content"></div>';
				}
			}
		}
	}
	
	
	if( !function_exists('theevent_get_selected_header') ){	
		function theevent_get_selected_header($theevent_post_option,$theevent_theme_option) {
			if(isset($theevent_theme_option['enable-header-option'])){
				if($theevent_theme_option['enable-header-option'] == 'disable'){
					if(isset($theevent_post_option['kode-header-style'])){
						$theevent_theme_option['kode-header-style'] = $theevent_post_option['kode-header-style'];
						theevent_get_header($theevent_theme_option);	
					}else{
						theevent_get_header($theevent_theme_option);	
					}
				}else{
				
					theevent_get_header($theevent_theme_option);	
				}
			}else{
				if(isset($theevent_post_option['kode-header-style'])){
					$theevent_theme_option['kode-header-style'] = $theevent_post_option['kode-header-style'];
					theevent_get_header($theevent_theme_option);						
				}else{
					theevent_get_header($theevent_theme_option);	
				}
			}
		}
	}	
	
	if( !function_exists('theevent_get_selected_header_class') ){	
		function theevent_get_selected_header_class($theevent_post_option,$theevent_theme_option) {	
			if(isset($theevent_theme_option['enable-header-option'])){
				if($theevent_theme_option['enable-header-option'] == 'disable'){
					if(isset($theevent_post_option['kode-header-style'])){
						$theevent_theme_option['kode-header-style'] = $theevent_post_option['kode-header-style'];
						return $theevent_theme_option['kode-header-style'];
					}else{						
						return $theevent_theme_option['kode-header-style'];
					}
				}else{
					return $theevent_theme_option['kode-header-style'];
				}
			}else{
				if(isset($theevent_post_option['kode-header-style'])){
					$theevent_theme_option['kode-header-style'] = $theevent_post_option['kode-header-style'];
					return $theevent_theme_option['kode-header-style'];
				}else{
					//return $theevent_theme_option['kode-header-style'];
					return 'default';
				}
			}
		}
	}
	
	
	if( !function_exists('theevent_get_header') ){	
		function theevent_get_header ($theevent_theme_option) {
			if(isset($theevent_theme_option['kode-header-style']) && $theevent_theme_option['kode-header-style'] == 'header-style-1'){ ?>
				<!--Header Wrap Start-->				
				<header class="<?php if(isset($theevent_theme_option['enable-sticky-menu']) && $theevent_theme_option['enable-sticky-menu'] == 'enable'){echo 'kode-navigation-sticky';}?>" id="kode-header">
					<strong class="kode-logo">
						<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" >
							<?php 
								if(empty($theevent_theme_option['logo'])){ 
									echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
								}else{
									if(theevent_get_image($theevent_theme_option['logo']) <> ''){
										echo theevent_get_image($theevent_theme_option['logo']);	
									}else{
										echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
									}
								}
							?>						
						</a>
					</strong>
					<?php
					// mobile navigation
					if( class_exists('theevent_dlmenu_walker') && has_nav_menu('main_menu') &&
						( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
						echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
						echo '<button class="dl-trigger">Open Menu</button>';
						wp_nav_menu( array(
							'theme_location'=>'main_menu', 
							'container'=> '', 
							'menu_class'=> 'dl-menu kode-main-mobile-menu',
							'walker'=> new theevent_dlmenu_walker() 
						) );						
						echo '</div>';
					}	
					?>	
					<!--Navigation Wrap Start-->
					<nav id="navigation" class="kode-nav">
						<?php get_template_part( 'header', 'nav' ); ?>	
					</nav>
					<!--Navigation Wrap End-->		
					<div class="col-right">
						<?php theevent_print_header_social_icon('header-social'); 
						//meet Button
						if(isset($theevent_theme_option['enable-meet-btn']) && $theevent_theme_option['enable-meet-btn'] == 'enable'){
							if(isset($theevent_theme_option['meet-page']) && $theevent_theme_option['meet-page'] != ''){ ?>
								<?php if(get_permalink($theevent_theme_option['meet-page']) <> ''){ ?>
									<a href="<?php echo esc_url(get_permalink($theevent_theme_option['meet-page']));?>" class="button-member"><?php echo wp_kses($theevent_theme_option['meet-btn-text'],array('i'=>array('class'=>array())));?></a>
								<?php 
								}
							}
						}?>
					</div>
				</header>
				
			<?php
			}else if(isset($theevent_theme_option['kode-header-style']) && $theevent_theme_option['kode-header-style'] == 'header-style-2'){ ?>
				<header class="kf_event_header_2">
					<!--Top Bar Wrap Start-->
					<div class="kf_pet_topbar">
						<div class="container">
						
							<div class="kf_pet_callus">
								<?php 
									if( !empty($theevent_theme_option['top-bar-left-text']) ) {
										echo do_shortcode($theevent_theme_option['top-bar-left-text']); 
									}	
								?>
							</div>
							
							<div class="kf_pet_ui_element">
								<?php theevent_print_header_social_icon('header-social'); ?>								
							</div>
							
						</div>
					</div>
					<!--Top Bar Wrap End-->
					
					<!--Logo & Navigation Wrap Start-->
					<div class="kf_pet_logo_nav <?php if(isset($theevent_theme_option['enable-sticky-menu']) && $theevent_theme_option['enable-sticky-menu'] == 'enable'){echo 'kode-navigation-sticky';}?>">
						<div class="container">
						
							<!--Logo Wrap Start-->
							<div class="kf_pet_logo">
								<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" >
									<?php 
										if(empty($theevent_theme_option['logo'])){ 
											echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
										}else{
											if(theevent_get_image($theevent_theme_option['logo']) <> ''){
												echo theevent_get_image($theevent_theme_option['logo']);	
											}else{
												echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
											}
										}
									?>						
								</a>
							</div>
							<!--Logo Wrap End-->
							
							<!--Member Wrap Start-->
							<div class="kf_event_member">
							<?php 
								//meet Button
								if(isset($theevent_theme_option['enable-meet-btn']) && $theevent_theme_option['enable-meet-btn'] == 'enable'){
									if(isset($theevent_theme_option['meet-page']) && $theevent_theme_option['meet-page'] != ''){ ?>
										<?php if(get_permalink($theevent_theme_option['meet-page']) <> ''){ ?>
											<a href="<?php echo esc_url(get_permalink($theevent_theme_option['meet-page']));?>" class="button-member"><?php echo wp_kses($theevent_theme_option['meet-btn-text'],array('i'=>array('class'=>array())));?></a>
										<?php 
										}
									}
								}?>								
							</div>
							<!--Member Wrap End-->
							<?php
							// mobile navigation
							if( class_exists('theevent_dlmenu_walker') && has_nav_menu('main_menu') &&
								( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
								echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
								echo '<button class="dl-trigger">Open Menu</button>';
								wp_nav_menu( array(
									'theme_location'=>'main_menu', 
									'container'=> '', 
									'menu_class'=> 'dl-menu kode-main-mobile-menu',
									'walker'=> new theevent_dlmenu_walker() 
								) );						
								echo '</div>';
							}	
							?>	
							<!--Navigation Wrap Start-->
							<div class="kf_pet_navigation">
								<?php get_template_part( 'header', 'nav' ); ?>	
							</div>
							<!--Navigation Wrap Start-->
							
						</div>
					</div>
					<!--Logo & Navigation Wrap End-->
					
				</header>
						
			<?php
			}else if(isset($theevent_theme_option['kode-header-style']) && $theevent_theme_option['kode-header-style'] == 'header-style-3'){ ?>
				<header class="kode_header_3 header4_bg kode-header-lawyer">
					<div class="kode_top_strip2">
						<div class="container">
							<div class="kode_loc">
								<?php 
									if( !empty($theevent_theme_option['top-bar-right-text']) ) {
										echo do_shortcode($theevent_theme_option['top-bar-right-text']); 
									}	
								?>	
							</div>
							<div class="kode_social_icon">
								<?php theevent_print_header_social_icon('kode-team-network'); ?>		
							</div>
						</div>
					</div>
					
					<div class="kode_nav_1">
						<div class="container">
							<!--Logo Wrap Start-->
							<div class="kode_logo_2">
								 <a class="logo" href="<?php echo esc_url(home_url('/')); ?>" >
								<?php 
									if(empty($theevent_theme_option['logo'])){ 
										echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
									}else{
										if(theevent_get_image($theevent_theme_option['logo']) <> ''){
											echo theevent_get_image($theevent_theme_option['logo']);	
										}else{
											echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
										}
									}
								?>						
								</a>
							</div>
							<!--Logo Wrap End-->
							
							<!--Navigation Wrap Start-->
							<?php
								// mobile navigation
								if( class_exists('theevent_dlmenu_walker') && has_nav_menu('main_menu') &&
									( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
									echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
									echo '<button class="dl-trigger">Open Menu</button>';
									wp_nav_menu( array(
										'theme_location'=>'main_menu', 
										'container'=> '', 
										'menu_class'=> 'dl-menu kode-main-mobile-menu',
										'walker'=> new theevent_dlmenu_walker() 
									) );						
									echo '</div>';
								}	
							?>	
							<?php get_template_part( 'header', 'nav' ); ?>	
							<!--Navigation Wrap End-->
						</div>
					</div>
					
				</header>
			<?php
			}else if(isset($theevent_theme_option['kode-header-style']) && $theevent_theme_option['kode-header-style'] == 'header-style-4'){ ?>
			<header class="kode_header_5 kode-header-lawyer">
				<div class="kode_top_strip2">
					<div class="container">
						<div class="kode_coun">
							<div class="dropdown">
								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">English <i class="fa fa-flag"></i><i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>
							<?php 
								if( !empty($theevent_theme_option['top-bar-right-text']) ) {
									echo do_shortcode($theevent_theme_option['top-bar-right-text']); 
								}	
							?>	
						</div>
						<div class="kode_social_icon">
							<?php theevent_print_header_social_icon('kode-team-network'); ?>		
						</div>
						<div class="kode_cart_wrap">
							<ul>
								<li><a href="#"><i class="fa fa-heart-o"></i> My Account</a></li>
								<li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="kode_nav_1">
					<div class="container">
						<!--Logo Wrap Start-->
						<div class="kode_logo_2">
							 <a class="logo" href="<?php echo esc_url(home_url('/')); ?>" >
							<?php 
								if(empty($theevent_theme_option['logo'])){ 
									echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
								}else{
									if(theevent_get_image($theevent_theme_option['logo']) <> ''){
										echo theevent_get_image($theevent_theme_option['logo']);	
									}else{
										echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
									}
								}
							?>						
							</a>
						</div>
						<!--Logo Wrap End-->				 
						
						<!--Search Wrap Start-->
						<div class="kode_search">
							<a href="#"><i class="fa fa-search"></i></a>
						</div>
						<!--Search Wrap End-->
						 <?php
							// mobile navigation
							if( class_exists('kode_dlmenu_walker') && has_nav_menu('main_menu') &&
								( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
								echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
								echo '<button class="dl-trigger">Open Menu</button>';
								wp_nav_menu( array(
									'theme_location'=>'main_menu', 
									'container'=> '', 
									'menu_class'=> 'dl-menu kode-main-mobile-menu',
									'walker'=> new theevent_dlmenu_walker() 
								) );						
								echo '</div>';
							}	
						?>	
						<?php get_template_part( 'header', 'nav' ); ?>							
					</div>
				</div>
			</header>
			<?php
			}else if(isset($theevent_theme_option['kode-header-style']) && $theevent_theme_option['kode-header-style'] == 'header-style-5'){ ?>
				<header class="kode_header_5">
					<?php if(isset($theevent_theme_option['enable-top-bar']) && $theevent_theme_option['enable-top-bar'] == 'enable'){	?>
					<div class="kode_top_strip2">
						<div class="container">
							<div class="kode_coun">
								<?php 
									if( !empty($theevent_theme_option['top-bar-left-text']) ) {
										echo do_shortcode($theevent_theme_option['top-bar-left-text']); 
									}	
								?>
							</div>
							<?php theevent_print_header_social_icon('theevent_social_icon'); ?>
							<div class="kode_cart_wrap">
								<?php 
									if( !empty($theevent_theme_option['top-bar-right-text']) ) {
										echo do_shortcode($theevent_theme_option['top-bar-right-text']); 
									}	
								?>
							</div>
						</div>
					</div>
					<?php }?>
					<div class="kode_nav_1 header_sticky_nav">
						<div class="container">
							<!--Logo Wrap Start-->
							<div class="kode_logo_2">
								<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" >
								<?php 
									if(empty($theevent_theme_option['logo'])){ 
										echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
									}else{
										echo theevent_get_image($theevent_theme_option['logo']);
									}
								?>						
								</a>
							</div>
							<!--Logo Wrap End-->
							<!--Cart Wrap Start-->
							<div class="kode_cart_fill">
								<?php 
								if(class_exists('Woocommerce')){
									global $woocommerce;?>									
									<?php echo theevent_get_woo_cart(); ?>
									<span>(<?php echo esc_attr($woocommerce->cart->cart_contents_count);?>)</span>
								<?php }?>
							</div>
							<!--Cart Wrap End-->
							<!--Search Wrap Start-->
							<div class="kode_search">
								<a href="#"><i class="fa fa-search"></i></a>
							</div>
							<!--Search Wrap End-->
							<div id="kode_search" class="kode_search hide">
								<form class="kode_search-form">
								<form class="kode-search kode_search-form" method="get" id="searchform" action="<?php  echo esc_url(home_url('/')); ?>/">
									<?php
										$search_val = get_search_query();
										if( empty($search_val) ){
											$search_val = esc_html__("Type keywords..." , "the-event");
										}
									?>
									<input class="kode_search kode_search-input" type="text" name="s" id="s" autocomplete="off" data-default="<?php echo esc_attr($search_val); ?>" />
									<button class="kode_search-submit" type="submit"><i class="fa fa-search"></i></button>
								</form>
								<span class="kode_search-close"></span>
							</div><!-- /kode_search -->
							<div class="overlay"></div>
							<!--Navigation Wrap Start-->
							<?php
								// mobile navigation
								if( class_exists('kode_dlmenu_walker') && has_nav_menu('main_menu') &&
									( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
									echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
									echo '<button class="dl-trigger">Open Menu</button>';
									wp_nav_menu( array(
										'theme_location'=>'main_menu', 
										'container'=> '', 
										'menu_class'=> 'dl-menu kode-main-mobile-menu',
										'walker'=> new theevent_dlmenu_walker() 
									) );						
									echo '</div>';
								}	
							?>	
							<?php get_template_part( 'header', 'nav' ); ?>	
							<!--Navigation Wrap End-->
						</div>
					</div>
				</header>
			<?php
			}else if(isset($theevent_theme_option['kode-header-style']) && $theevent_theme_option['kode-header-style'] == 'header-style-6'){
				?>
				<header class="kode_header_7 header_sticky_nav">
					<div class="kode_top_eng">
						<div class="container">
						<?php if(isset($theevent_theme_option['enable-top-bar']) && $theevent_theme_option['enable-top-bar'] == 'enable'){	?>
							<div class="kode_welcome">
								<?php 
									if( !empty($theevent_theme_option['top-bar-left-text']) ) {
										echo do_shortcode($theevent_theme_option['top-bar-left-text']); 
									}	
								?>
							</div>
							<div class="kode_select_opt">
								
								<!--Items DropDown Start-->
								<div class="kode_umeed_list">
									<?php 
										if( !empty($theevent_theme_option['top-bar-right-text']) ) {
											echo do_shortcode($theevent_theme_option['top-bar-right-text']); 
										}	
									?>
								</div>
								<!--Items DropDown End-->
								
							</div>
						<?php }?>
							<div class="kode_sec_strip">
								<div class="row">
									<div class="col-md-4">
										<div class="kode_logo">
											<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" >
											<?php 
												if(empty($theevent_theme_option['logo'])){ 
													echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
												}else{
													echo theevent_get_image($theevent_theme_option['logo']);
												}
											?>						
											</a>
										</div>
									</div>
									
									<div class="col-md-4">
										<div class="kode_search_bar">
											<form class="kode-search" method="get" id="searchform" action="<?php  echo esc_url(home_url('/')); ?>/">
												<?php
													$search_val = get_search_query();
													if( empty($search_val) ){
														$search_val = esc_html__("Type keywords..." , "the-event");
													}
												?>
												<input class="kode_search" type="text" name="s" id="s" autocomplete="off" data-default="<?php echo esc_attr($search_val); ?>" />
												<button><i class="fa fa-search"></i></button>
											</form>
										</div>
									</div>
									
									<div class="col-md-4">
										<div class="kode_call">
											<div class="kode_ph_no">
												<p><?php esc_html_e('Call Us With Hotline 24/7','the-event')?></p>
												<h4><?php esc_html_e('(123) 878 545','the-event')?></h4>
											</div>
											<div class="kode_left_thumb">
												<i class="fa fa-phone"></i>
											</div>
										</div>
									</div>
									
								</div>
							</div> 
						</div>
						
						<!--Navigation Wrap Start-->
						<div class="kode_nav_1 ">
							<div class="container">
								<!--Navigation Wrap Start-->
								<?php
									// mobile navigation
									if( class_exists('theevent_dlmenu_walker') && has_nav_menu('main_menu') &&
										( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
										echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
										echo '<button class="dl-trigger">Open Menu</button>';
										wp_nav_menu( array(
											'theme_location'=>'main_menu', 
											'container'=> '', 
											'menu_class'=> 'dl-menu kode-main-mobile-menu',
											'walker'=> new theevent_dlmenu_walker() 
										) );						
										echo '</div>';
									}	
								?>	
								<?php get_template_part( 'header', 'nav' ); ?>	
								<!--Navigation Wrap End-->
								
								<!--Donate Now Button Start-->
								<div class="kode_donate_btn">
									<?php
									$mega_menu = get_option('mega_main_menu_options');
									if(is_array($mega_menu)){
										if(!in_array('main_menu',$mega_menu['mega_menu_locations'])){
											//Donation Button
											if(isset($theevent_theme_option['enable-donation-btn']) && $theevent_theme_option['enable-donation-btn'] == 'enable'){
												if(isset($theevent_theme_option['donatation_page']) && $theevent_theme_option['donatation_page'] != ''){ ?>
													<?php if(get_permalink($theevent_theme_option['donatation_page']) <> ''){ ?>
														<a href="<?php echo esc_url(get_permalink($theevent_theme_option['donatation_page']));?>" class="kode-donate-btn thbg-color"><?php echo esc_attr($theevent_theme_option['donation-btn-text']);?></a>
													<?php 
													}
												}
											}
										}
									}else{
										//Donation Button
										if(isset($theevent_theme_option['enable-donation-btn']) && $theevent_theme_option['enable-donation-btn'] == 'enable'){
											if(isset($theevent_theme_option['donatation_page']) && $theevent_theme_option['donatation_page'] != ''){ ?>
												<?php if(get_permalink($theevent_theme_option['donatation_page']) <> ''){ ?>
													<a href="<?php echo esc_url(get_permalink($theevent_theme_option['donatation_page']));?>"><?php echo esc_attr($theevent_theme_option['donation-btn-text']);?></a>
												<?php 
												}
											}
										}
									} ?>									
								</div>
								<!--Donate Now Button End-->
							</div>
						</div>
						<!--Navigation Wrap End-->
					</div>
				</header>				
			<?php }else if(isset($theevent_theme_option['kode-header-style']) && $theevent_theme_option['kode-header-style'] == 'header-style-7'){ ?>
				<header id="mainheader" class="kode-header-absolute kode-header-1">
					<?php if(isset($theevent_theme_option['enable-top-bar']) && $theevent_theme_option['enable-top-bar'] == 'enable'){	?>
					<!--// TopBaar //-->
					<div class="kode-topbar-kodeforest">
						<div class="container">
							<div class="row">
								<div class="col-md-6 kode_bg_white">
								<?php
								//latest news script
								if(isset($theevent_theme_option['top_latest_news_btn']) && $theevent_theme_option['top_latest_news_btn'] == 'enable'){
									$args = array( 'posts_per_page' => 5, 'category' => $theevent_theme_option['top_latest_news'] );
									$theevent_posts = get_posts($args);
									if(!empty($theevent_posts)){
										echo '<ul class="bxslider">';
											foreach($theevent_posts as $theevent_post){
												echo '<li><span class="kode-barinfo">';
												if(isset($theevent_theme_option['top_latest_news_title'])){
													echo '<strong>'.esc_attr($theevent_theme_option['top_latest_news_title']).' : </strong> ';
												}
												echo '<a href="'.esc_url(get_permalink($theevent_post->ID)).'">'.esc_attr($theevent_post->post_title).'</a></span></li>';
											}
										echo '</ul>';
									}
								}
								?>	
								</div>
								<div class="col-md-6">							
									<ul class="kode-userinfo">
										<?php 
										if(class_exists('Woocommerce')){
											global $woocommerce;?>									
											<li class="cart-option woocommerce">
											<span><i class="fa fa-shopping-cart"></i> <?php esc_html_e('Cart','the-event');?> (<?php echo esc_attr($woocommerce->cart->cart_contents_count);?>)</span>
											<?php echo theevent_get_woo_cart(); ?>
											</li>
										<?php }?>
										<?php if (is_user_logged_in()) {
											global $current_user;?>
											<li><a href=""><?php esc_attr_e('Welcome ','the-event');?><?php echo esc_attr($current_user->display_name);?></a></li>
											<li><a data-original-title="Facebook" href="<?php echo esc_url(wp_logout_url( home_url() )); ?>"><i class="fa fa-user"></i> <?php esc_attr_e('Logout','the-event');?></a></li>
										<?php }else{ ?>
											<li><?php theevent_signin_form()?></li>
											<li><?php theevent_signup_form()?></li>
										<?php }?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!--// TopBaar //-->
					<?php }?>
					<div class="header-8">
						<div class="container">
							<!--NAVIGATION START-->
							<?php
								// mobile navigation
								if( class_exists('theevent_dlmenu_walker') && has_nav_menu('main_menu') &&
									( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
									echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
									echo '<button class="dl-trigger">Open Menu</button>';
									wp_nav_menu( array(
										'theme_location'=>'main_menu', 
										'container'=> '', 
										'menu_class'=> 'dl-menu kode-main-mobile-menu',
										'walker'=> new theevent_dlmenu_walker() 
									) );						
									echo '</div>';
								}	
							?>	
							<div class="kode-navigation pull-left">
								<!--Navigation Wrap Start-->
								<?php get_template_part( 'header', 'left' ); ?>	
								<!--Navigation Wrap End-->
							</div>
							<!--NAVIGATION END--> 
							<!--LOGO START-->	
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" >
								<?php 
									if(empty($theevent_theme_option['logo'])){ 
										echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
									}else{
										if(theevent_get_image($theevent_theme_option['logo']) <> ''){
											echo theevent_get_image($theevent_theme_option['logo']);
										}else{
											echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
										}
										
									}
								?>						
								</a>
							</div>
							<!--LOGO END-->	
							<!--NAVIGATION START-->
							<div class="kode-navigation">					
								<!--Navigation Wrap Start-->
								<?php get_template_part( 'header', 'right' ); ?>	
								<!--Navigation Wrap End-->
							</div>
						</div>
					</div>
				</header>				
			<?php }else{ ?>
				<!--Header Wrap Start-->				
				<header id="kode-header">
					<strong class="kode-logo">
						<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" >
							<?php 
								if(empty($theevent_theme_option['logo'])){ 
									echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
								}else{
									if(theevent_get_image($theevent_theme_option['logo']) <> ''){
										echo theevent_get_image($theevent_theme_option['logo']);	
									}else{
										echo theevent_get_image(THEEVENT_PATH . '/images/logo.png');
									}
								}
							?>						
						</a>
					</strong>
					<?php
					// mobile navigation
					if( class_exists('theevent_dlmenu_walker') && has_nav_menu('main_menu') &&
						( empty($theevent_theme_option['enable-responsive-mode']) || $theevent_theme_option['enable-responsive-mode'] == 'enable' ) ){
						echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
						echo '<button class="dl-trigger">Open Menu</button>';
						wp_nav_menu( array(
							'theme_location'=>'main_menu', 
							'container'=> '', 
							'menu_class'=> 'dl-menu kode-main-mobile-menu',
							'walker'=> new theevent_dlmenu_walker() 
						) );						
						echo '</div>';
					}	
					?>	
					<!--Navigation Wrap Start-->
					<nav id="navigation" class="kode-nav">
						<?php get_template_part( 'header', 'nav' ); ?>	
					</nav>
					<!--Navigation Wrap End-->		
					<div class="col-right">
						<?php theevent_print_header_social_icon('header-social'); 
						//meet Button
						if(isset($theevent_theme_option['enable-meet-btn']) && $theevent_theme_option['enable-meet-btn'] == 'enable'){
							if(isset($theevent_theme_option['meet-page']) && $theevent_theme_option['meet-page'] != ''){ ?>
								<?php if(get_permalink($theevent_theme_option['meet-page']) <> ''){ ?>
									<a href="<?php echo esc_url(get_permalink($theevent_theme_option['meet-page']));?>" class="button-member"><?php echo wp_kses($theevent_theme_option['meet-btn-text'],array('i'=>array('class'=>array())));?></a>
								<?php 
								}
							}
						}?>
					</div>
				</header>
			<?php
			}
		}
	}

	