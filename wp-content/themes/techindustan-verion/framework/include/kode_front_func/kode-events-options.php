<?php
	/*	
	*	Kodeforest Event Option file
	*	---------------------------------------------------------------------
	*	This file creates all post options to the post page
	*	---------------------------------------------------------------------
	*/
	
	// add a post admin option
	// add a post option to post page
	if( is_admin() ){ add_action('init', 'theevent_create_event_options'); }
	if( !function_exists('theevent_create_event_options') ){
	
		function theevent_create_event_options(){
			global $theevent_theme_option;
			
			if( !class_exists('theevent_page_options') ) return;
			new theevent_page_options( 
				
				
					  
				// page option settings
				array(
					'page-layout' => array(
						'title' => esc_attr__('Page Layout', 'the-event'),
						'options' => array(
							'sidebar' => array(
								'title' => esc_attr__('Sidebar Template' , 'the-event'),
								'type' => 'radioimage',
								'options' => array(
									'no-sidebar'=>		THEEVENT_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
									'both-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
									'right-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
									'left-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
								),
								'default' => 'default-sidebar'
							),	
							'left-sidebar' => array(
								'title' => esc_attr__('Left Sidebar' , 'the-event'),
								'type' => 'combobox_sidebar',
								'options' => $theevent_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper left-sidebar-wrapper both-sidebar-wrapper'
							),
							'right-sidebar' => array(
								'title' => esc_attr__('Right Sidebar' , 'the-event'),
								'type' => 'combobox_sidebar',
								'options' => $theevent_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper right-sidebar-wrapper both-sidebar-wrapper'
							),						
						)
					),
					
					'page-option' => array(
						'title' => esc_attr__('Page Option', 'the-event'),
						'options' => array(
							'page-title' => array(
								'title' => esc_attr__('Post Title' , 'the-event'),
								'type' => 'text',
								'description' => esc_attr__('Leave this field blank to use the default title from admin panel > general > blog style section.', 'the-event')
							),
							'page-caption' => array(
								'title' => esc_attr__('Post Caption' , 'the-event'),
								'type' => 'textarea'
							),
							// 'team_member' => array(
								// 'title' => esc_attr__('Team Member' , 'the-event'),
								// 'type' => 'multi-combobox',
								// 'options' => theevent_get_post_list_id('team'),
								// 'description'=> esc_attr__('Select Team Member.', 'the-event')
							// ),	
							// 'theevent_project' => array(
								// 'title' => esc_attr__('Select Project' , 'the-event'),
								// 'type' => 'combobox',
								// 'options' => theevent_get_post_list_id('ignition_product'),
								// 'description'=> esc_attr__('Select Crowd Funding Project.', 'the-event')
							// ),
							'add_team_member' => array(
								'title' => esc_attr__('Speakers at Conference' , 'the-event'),
								'type' => 'multi-fields',
								'wrapper-class' => 'kode-multi-fields',
								'data-array' => array(
									'team_speaker_member' => array(
										'title' => esc_attr__('Team Member' , 'the-event'),
										'type' => 'combobox',
										'options' => theevent_get_post_lunch_list_id('team'),
										'wrapper-class' => 'columns three',
										'description'=> esc_attr__('Select Team Member.', 'the-event')
									),
									'team_speaker_topic' => array(
										'title' => esc_attr__('Speaker Topic Title' , 'the-event'),
										'type' => 'text',
										'wrapper-class' => 'columns three',
										'default'=>'--blank--',
										'description' => esc_attr__('Add team speaker topic title.', 'the-event')
									),
									'team_speaker_start' => array(
										'title' => esc_attr__('Speaker Start Time' , 'the-event'),
										'type' => 'text',
										'wrapper-class' => 'columns three',
										'default'=>'--blank--',
										'description' => esc_attr__('Add team speaker Start Time.', 'the-event')
									),
									'team_speaker_end' => array(
										'title' => esc_attr__('Speaker End Time' , 'the-event'),
										'type' => 'text',
										'wrapper-class' => 'columns three',
										'default'=>'--blank--',
										'description' => esc_attr__('Add team speaker End Time.', 'the-event')
									),
									'team_speaker_topic_desc' => array(
										'title' => esc_attr__('Speaker Topic Description' , 'the-event'),
										'type' => 'textarea',
										'wrapper-class' => 'columns full-width-sec',
										'default'=>'--blank--',
										'description' => esc_attr__('Add team speaker topic description.', 'the-event')
									),
								),								
							),	
							'add-more-team' => array(
								'title' => esc_attr__('Add Speaker' , 'the-event'),
								'type' => 'multi-fields-button',
								'button_slug' => 'kode-multi-fields',
								'description' => esc_attr__('Add Button Slug of the sibling class which you want to add multiple times by clicking this button.', 'the-event')
							),
						)
					),

				),
				
				// page option attribute
				array(
					'post_type' => array('event'),
					'meta_title' => esc_attr__('Kodeforest Event Option', 'the-event'),
					'meta_slug' => 'kodeforest-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);
			
		}
	}	
	
	
	// add work in page builder area
	add_filter('theevent_page_builder_option', 'theevent_register_event_item');
	if( !function_exists('theevent_register_event_item') ){
		function theevent_register_event_item( $page_builder = array() ){
			global $theevent_spaces;
			$page_builder['content-item']['options']['events'] = array(
				'title'=> esc_attr__('Events', 'the-event'), 
				'icon'=>'fa-calendar',
				'type'=>'item',
				'options'=> array(					
					'element-item-id' => array(
						'title' => esc_html__('Page Item ID', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item id.', 'the-event')
					),
					'element-item-class' => array(
						'title' => esc_html__('Page Item Class', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item class.', 'the-event')
					),
					'title-num-excerpt'=> array(
						'title'=> esc_attr__('Title Num Length (Word)' ,'the-event'),
						'type'=> 'text',	
						'default'=> '15',
						'description'=> esc_attr__('This is a number of word (decided by spaces) that you want to show on the event title. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'the-event')
					),	
					'category'=> array(
						'title'=> esc_attr__('Category' ,'the-event'),
						'type'=> 'multi-combobox',
						'options'=> theevent_get_term_list('event-categories'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'the-event')
					),	
					'tag'=> array(
						'title'=> esc_attr__('Tag' ,'the-event'),
						'type'=> 'multi-combobox',
						'options'=> theevent_get_term_list('event-tags'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'the-event')
					),	
					'num-excerpt'=> array(
						'title'=> esc_attr__('Num Excerpt (Word)' ,'the-event'),
						'type'=> 'text',	
						'default'=> '25',
						'description'=> esc_attr__('This is a number of word (decided by spaces) that you want to show on the post excerpt. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'the-event')
					),	
					'num-fetch'=> array(
						'title'=> esc_attr__('Num Fetch' ,'the-event'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> esc_attr__('Specify the number of posts you want to pull out.', 'the-event')
					),	
					'thumbnail-size' => array(
						'title' => esc_html__('Event List Thumbnail Size', 'the-event'),
						'type'=> 'combobox',
						'options'=> theevent_get_thumbnail_list(),
						'default'=> ''
					),						
					'event-style'=> array(
						'title'=> esc_attr__('Event Style' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'event-simple-view' => esc_attr__('Event Simple View', 'the-event'),							
							'event-grid-view' => esc_attr__('Event Grid View', 'the-event'),
							// 'event-medium-view' => esc_attr__('Event Medium View', 'the-event'),
							// 'event-full-view' => esc_attr__('Event Full Normal', 'the-event'),
							'event-calendar-view' => esc_attr__('Event Calendar View', 'the-event'),
							'event-marker-view' => esc_attr__('Event Google Markers View', 'the-event'),
						),
						'default'=>'event-grid-simple'
					),	
					'event-size'=> array(
						'title'=> esc_html__('Event Size' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'1' => esc_html__('1 Column', 'the-event'),
							'2' => esc_html__('2 Column', 'the-event'),
							'3' => esc_html__('3 Column', 'the-event'),
							'4' => esc_html__('4 Column', 'the-event'),
						),						
						'default'=>'3'
					),					
					'scope'=> array(
						'title'=> esc_attr__('Event Scope' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'past' => esc_attr__('Past Events', 'the-event'), 
							'future' => esc_attr__('Upcoming Events', 'the-event'), 
							'all' => esc_attr__('All Events', 'the-event'), 
						)
					),
					'order'=> array(
						'title'=> esc_attr__('Order' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>esc_attr__('Descending Order', 'the-event'), 
							'asc'=> esc_attr__('Ascending Order', 'the-event'), 
						)
					),									
					'pagination'=> array(
						'title'=> esc_attr__('Enable Pagination' ,'the-event'),
						'type'=> 'checkbox'
					),										
					'margin-bottom' => array(
						'title' => esc_attr__('Margin Bottom', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_attr__('Spaces after ending of this item', 'the-event')
					),					
				)
			);
			
			$page_builder['content-item']['options']['upcoming-event'] = array(
				'title'=> esc_attr__('Upcoming Event', 'the-event'), 
				'icon'=>'fa-calendar-check-o',
				'type'=>'item',
				'options'=> array(					
					'element-item-id' => array(
						'title' => esc_html__('Page Item ID', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item id.', 'the-event')
					),
					'element-item-class' => array(
						'title' => esc_html__('Page Item Class', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item class.', 'the-event')
					),
					'heading'=> array(
						'title'=> esc_attr__('Element Heading' ,'the-event'),
						'type'=> 'text',	
						'default'=> '',
						'description'=> esc_attr__('Please add element heading text which will be shown above the element.', 'the-event')
					),	
					'caption'=> array(
						'title'=> esc_attr__('Element Caption' ,'the-event'),
						'type'=> 'text',	
						'default'=> '',
						'description'=> esc_attr__('Please add Element Caption text here which will be shwon above the element.', 'the-event')
					),	
					'title-num-excerpt'=> array(
						'title'=> esc_attr__('Title Num Length (Word)' ,'the-event'),
						'type'=> 'text',	
						'default'=> '15',
						'description'=> esc_attr__('This is a number of word (decided by spaces) that you want to show on the event title. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'the-event')
					),	
					'category'=> array(
						'title'=> esc_attr__('Category' ,'the-event'),
						'type'=> 'multi-combobox',
						'options'=> theevent_get_term_list('event-categories'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'the-event')
					),	
					'margin-bottom' => array(
						'title' => esc_attr__('Margin Bottom', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_attr__('Spaces after ending of this item', 'the-event')
					),					
				)
			);
			
			$page_builder['content-item']['options']['next-event'] = array(
				'title'=> esc_attr__('Next Event', 'the-event'), 
				'icon'=>'fa-calendar-check-o',
				'type'=>'item',
				'options'=> array(					
					'element-item-id' => array(
						'title' => esc_html__('Page Item ID', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item id.', 'the-event')
					),
					'element-item-class' => array(
						'title' => esc_html__('Page Item Class', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item class.', 'the-event')
					),
					'next-event-find-text'=> array(
						'title'=> esc_attr__('Next Event Near To You' ,'the-event'),
						'type'=> 'textarea',	
						'default'=> '',
						'description'=> esc_attr__('Please add title for the left find us next event. (html tags allowed: <code><h2>, <P>, <small></code>)', 'the-event')
					),	
					'next-event-button-text'=> array(
						'title'=> esc_attr__('Next Event Button Text' ,'the-event'),
						'type'=> 'text',
						'default'=> 'Contact Us',
						'description'=> esc_attr__('Please Next Event Button Text', 'the-event')
					),	
					'next-event-button-url'=> array(
						'title'=> esc_attr__('Next Event Button URL' ,'the-event'),
						'type'=> 'text',
						'default'=> '#',
						'description'=> esc_attr__('Please Next Event Button URL', 'the-event')
					),	
					'next-event-button-description'=> array(
						'title'=> esc_attr__('Next Event Button Description' ,'the-event'),
						'type'=> 'text',
						'default'=> 'or Connect Us Directly',
						'description'=> esc_attr__('Please Next Event Button Description', 'the-event')
					),
					'next-event-title'=> array(
						'title'=> esc_attr__('Next Event Title' ,'the-event'),
						'type'=> 'text',	
						'default'=> 'Next Event Start IN',
						'description'=> esc_attr__('Please add title for the next event.', 'the-event')
					),	
					'next_event'=> array(
						'title'=> esc_attr__('Select Category of Next Event' ,'the-event'),
						'type'=> 'multi-combobox',
						'options'=> theevent_get_term_list('event-categories'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'the-event')
					),	
					'next-event-subscribe-txt'=> array(
						'title'=> esc_attr__('Next Event Subscribe Text' ,'the-event'),
						'type'=> 'text',	
						'default'=> 'SUBSCRIBE FOR UPDATES',
						'description'=> esc_attr__('Please add title for the subscribe field.', 'the-event')
					),	
					'margin-bottom' => array(
						'title' => esc_attr__('Margin Bottom', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_attr__('Spaces after ending of this item', 'the-event')
					),					
				)
			);
			
			$page_builder['content-item']['options']['conference'] = array(
				'title'=> esc_attr__('Conference', 'the-event'), 
				'icon'=>'fa-calendar-check-o',
				'type'=>'item',
				'options'=> array(					
					'element-item-id' => array(
						'title' => esc_html__('Page Item ID', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item id.', 'the-event')
					),
					'element-item-class' => array(
						'title' => esc_html__('Page Item Class', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item class.', 'the-event')
					),
					'style'=> array(
						'title'=> esc_attr__('Conference Style' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'accordion-style'=>esc_attr__('Accordion Style', 'the-event'), 
							'tab-style'=> esc_attr__('Tab Style', 'the-event'), 
							'tab-style-2'=> esc_attr__('Tab Style 2', 'the-event'), 
						)
					),
					'heading'=> array(
						'title'=> esc_attr__('Element Heading' ,'the-event'),
						'type'=> 'text',	
						'default'=> '',
						'description'=> esc_attr__('Please add element heading text which will be shown above the element.', 'the-event')
					),	
					'caption'=> array(
						'title'=> esc_attr__('Element Caption' ,'the-event'),
						'type'=> 'text',	
						'default'=> '',
						'description'=> esc_attr__('Please add Element Caption text here which will be shwon above the element.', 'the-event')
					),
					'num-fetch'=> array(
						'title'=> esc_attr__('Num Fetch' ,'the-event'),
						'type'=> 'text',	
						'default'=> '4',
						'description'=> esc_attr__('Select Num of Fetch Events in Conference.', 'the-event')
					),						
					'event-title-type'=> array(
						'title'=> esc_attr__('Event Title Type' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'day-wise' => esc_attr__('Day Wise', 'the-event'),
							'title-wise' => esc_attr__('Title Wise', 'the-event'),
						),
						'default'=>'event-grid-simple'
					),	
					'title-num-excerpt'=> array(
						'title'=> esc_attr__('Title Num Length (Word)' ,'the-event'),
						'type'=> 'text',	
						'default'=> '15',
						'description'=> esc_attr__('This is a number of word (decided by spaces) that you want to show on the event title. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'the-event')
					),	
					'category'=> array(
						'title'=> esc_attr__('Category' ,'the-event'),
						'type'=> 'combobox',
						'options'=> theevent_get_term_list('event-categories'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'the-event')
					),	
					'order'=> array(
						'title'=> esc_attr__('Order' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>esc_attr__('Descending Order', 'the-event'), 
							'asc'=> esc_attr__('Ascending Order', 'the-event'), 
						)
					),	
					'margin-bottom' => array(
						'title' => esc_attr__('Margin Bottom', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => esc_attr__('Spaces after ending of this item', 'the-event')
					),					
				)
			);
			
			
			
			return $page_builder;
		}
	}
	
	if( !function_exists('theevent_get_event_info') ){
		function theevent_get_event_info( $event_id='', $array = array(), $wrapper = true, $sep = '',$div_wrap = 'div' ){
			global $theme_option; $ret = '';
			if( empty($array) ) return $ret;
			//$exclude_meta = empty($theme_option['post-meta-data'])? array(): esc_attr($theme_option['post-meta-data']);
			
			foreach($array as $post_info){
				
				if( !empty($sep) ) $ret .= $sep;

				switch( $post_info ){
					case 'date':
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-date"><i class="fa fa-clock-o"></i>';
						$ret .= '<a href="' . esc_url(get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))) . '">';
						$ret .= esc_attr(get_the_time());
						$ret .= '</a>';
						$ret .= '</'.esc_attr($div_wrap).'>';
						break;
					case 'tag':
						$tag = get_the_term_list($event_id, 'event-tags', '', '<span class="sep">,</span> ' , '' );
						if(empty($tag)) break;					
						
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-tags"><i class="fa fa-tag"></i>';
						$ret .= $tag;						
						$ret .= '</'.esc_attr($div_wrap).'>';					
						break;
					case 'category':
						$category = get_the_term_list($event_id, 'event-categories', '', '<span class="sep">,</span> ' , '' );
						if(empty($category)) break;
						
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-category"><i class="fa fa-list"></i>';
						$ret .= $category;					
						$ret .= '</'.esc_attr($div_wrap).'>';			
						break;
					case 'comment':
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-comment"><i class="fa fa-comment-o"></i>';
						$ret .= '<a href="' . esc_url(get_permalink($event_id)) . '#respond" >' . esc_attr(get_comments_number()) . ' Comments</a>';						
						$ret .= '</'.esc_attr($div_wrap).'>';					
						break;
					case 'author':
						ob_start();
						the_author_posts_link();
						$author = ob_get_contents();
						ob_end_clean();
						
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-author"><i class="fa fa-user"></i>';
						$ret .= $author;
						$ret .= '</'.esc_attr($div_wrap).'>';			
						break;						
				}
			}
			
			
			if($wrapper && !empty($ret)){
				return '<div class="kode-event-info kode-info">' . $ret . '<div class="clear"></div></div>';
			}else if( !empty($ret) ){
				return $ret;
			}
			return '';			
		}
	}
	
	if( !function_exists('theevent_get_events_conference') ){
		function theevent_get_events_conference( $settings ){
			global $theevent_counter;
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$margin = (!empty($settings['margin-bottom']))? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$settings['element-item-class'] = empty($settings['element-item-class'])? '' : $settings['element-item-class'];
			$settings['style'] = empty($settings['style'])? '' : $settings['style'];
			$evn = '<div '.$margin_style.' '.$item_id.' class="'.esc_attr($settings['element-item-class']).' kode_event_conference">';
			$order = 'DESC';
			$limit = 10;//Default limit
			$offset = '';		
			$rowno = 0;
			$event_count = 0;
			//category_image_id
						
			$category = get_term_by( 'slug', $settings['category'], 'event-categories' );
			
			$category = em_get_category($category->term_id);
			$cat_img = $category->get_image_url();
			
			$EM_Events = EM_Events::get( array('category'=>$settings['category'], 'group'=>'this','scope'=>'future', 'limit' => $settings['num-fetch'], 'order' => 'ASC') );
			$events_count = count ( $EM_Events );	
			$current_size = 0;
			$size = 1;
			if($settings['style'] == 'tab-style'){
				$evn =  '
				<div class="schedule-banner '.esc_attr($settings['style']).'">
				<img src="'.esc_url($cat_img).'" alt="'.esc_attr($settings['category']).'">
				<div class="scheduletabs">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">';
					$event_hd = 0;
					$current_session_day = 0;
					$current_session_date = '';
					foreach ( $EM_Events as $event ) {
						$event_year = date('Y',$event->start);
						$event_month = date('m',$event->start);
						$event_day = date('d',$event->start);
						$session_date = date_i18n(get_option('date_format'), $event->start);
						$event_start_time = date("G,i,s", strtotime($event->start_time));
						if( $current_session_date != $session_date ){
							$current_session_day++;
							$current_session_date = $session_date;		
							if($event_hd == 0){
								$active_hd_class = 'active';
							}else{
								$active_hd_class = '';
							}
							if($settings['event-title-type'] == 'title-wise'){
								$evn .=  '<li role="presentation" class="'.esc_attr($active_hd_class).'"><a href="#event-'.esc_attr($event->post_id).'" role="tab" data-toggle="tab"><span>'.esc_attr($event->post_title).'</span>'.date('M.d.Y',$event->start).'</a></li>';
							}else{
								$evn .=  '<li role="presentation" class="'.esc_attr($active_hd_class).'"><a href="#event-'.esc_attr($event->post_id).'" role="tab" data-toggle="tab"><span>'.esc_attr__('Day ','the-event').$current_session_day.' </span>'.date('M.d.Y',$event->start).'</a></li>';
							}
						}$event_hd++;
					}
					$evn .=  '
					</ul>
					<ul class="auditorium">';
					$current_session_day_loc = 0;
					$current_session_date_loc = '';
					foreach ( $EM_Events as $event ) {
						$session_date = date_i18n(get_option('date_format'), $event->start);
						if( $current_session_date_loc != $session_date ){
							$current_session_day_loc++;
							$current_session_date_loc = $session_date;	
							$location = esc_attr($event->get_location()->location_address);								
							$evn .=  '
							<li><a href="'.esc_url($event->guid).'">'.esc_attr($location).'</a></li>';
						}
					}	
						$evn .=  '
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">';
					$eve_count = 0;
					$current_session_day = 0;
					$current_session_date = '';
					foreach ( $EM_Events as $event ) {
						
					$event_year = date('Y',$event->start);
					$event_month = date('m',$event->start);
					$event_day = date('d',$event->start);
					$event_start_time = date("G,i,s", strtotime($event->start_time));
					$location = esc_attr($event->get_location()->name);
					$team_speaker_data = get_post_meta($event->post_id,'_team_speaker_data',true);
						$session_date = date_i18n(get_option('date_format'), $event->start);
						if( $current_session_date != $session_date ){
							$current_session_day++;
							$current_session_date = $session_date;
							if($eve_count == 0){
								$active_class = 'active';
							}else{
								$active_class = '';
							}
							$evn .=  '
							<div role="tabpanel" class="tab-pane '.esc_attr($active_class).'" id="event-'.esc_attr($event->post_id).'">';
								$evn .= '
								<div class="panel-group" id="accordion-'.esc_attr($event->post_id).'" role="tablist" aria-multiselectable="true">';
								$team_counter = 0;
								$a_active_class = '';
								$a_active_in = '';
								if(isset($team_speaker_data) && $team_speaker_data <> ''){
									foreach($team_speaker_data as $data){
										$team_speaker = explode(',',$data);
										if($team_counter == 0){
											$a_active_class = 'collapsed';
											$a_active_in = 'in';
										}else{
											$a_active_class = '';
											$a_active_in = '';
										}
										$evn .= '
										<!-- Kode-accordian postt End -->
										<div class="panel panel-default">
											<div class="announcer-pic">'.get_the_post_thumbnail($team_speaker[0], 'full').'</div>
											<div class="overflow">
												<div class="panel-heading" role="tab" id="headingThree-'.esc_attr($team_counter).esc_attr($event->post_id).'">
													<div class="sch-time"><i class="fa fa-clock-o"></i> '.esc_attr($team_speaker[3]).'</div>
													<h4 class="panel-title">
														<a class="'.esc_attr($a_active_class).'" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree-'.esc_attr($team_counter).esc_attr($event->post_id).'" aria-expanded="false">'.esc_attr($team_speaker[1]).'</a>
													</h4>
												</div>
											
											</div>
										</div>
										<!-- Kode-accordian postt End -->';
										$team_counter++;
									}
								}
								$evn .=  '</div>
							</div>';
						}	
							$eve_count++;
					}	
				$evn .= '</div></div></div>';
			}else if($settings['style'] == 'tab-style-2'){
				$evn =  '
				<div class="schedule-banner '.esc_attr($settings['style']).'">
				<img src="'.esc_url($cat_img).'" alt="'.esc_attr($settings['category']).'">
				<div class="scheduletabs">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">';
					$event_hd = 0;
					$current_session_day = 0;
					$current_session_date = '';
					foreach ( $EM_Events as $event ) {
						$event_year = date('Y',$event->start);
						$event_month = date('m',$event->start);
						$event_day = date('d',$event->start);
						$session_date = date_i18n(get_option('date_format'), $event->start);
						$event_start_time = date("G,i,s", strtotime($event->start_time));
						if( $current_session_date != $session_date ){
							$current_session_day++;
							$current_session_date = $session_date;		
							if($event_hd == 0){
								$active_hd_class = 'active';
							}else{
								$active_hd_class = '';
							}
							if($settings['event-title-type'] == 'title-wise'){
								$evn .=  '<li role="presentation" class="'.esc_attr($active_hd_class).'"><a href="#event-'.esc_attr($event->post_id).'" role="tab" data-toggle="tab"><span>'.esc_attr($event->post_title).'</span>'.date('M.d.Y',$event->start).'</a></li>';
							}else{
								$evn .=  '<li role="presentation" class="'.esc_attr($active_hd_class).'"><a href="#event-'.esc_attr($event->post_id).'" role="tab" data-toggle="tab"><span>'.esc_attr__('Day ','the-event').$current_session_day.' </span>'.date('M.d.Y',$event->start).'</a></li>';
							}
						}$event_hd++;
					}
					$evn .=  '
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
					<script>
						jQuery(document).ready(function($){
						/*
						  ==============================================================
							   Accordian Script Start
						  ============================================================== */  
						  
						  if($(".kf_new_conf_hdg").length){
							//custom animation for open/close
							$.fn.slideFadeToggle = function(speed, easing, callback) {
							  return this.animate({opacity: "toggle", height: "toggle"}, speed, easing, callback);
							};

							$(".kf_new_conf_hdg").accordion({
							  defaultOpen: "kode-accor-0",
							  cookieName: "nav",
							  speed: "slow",
							  animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
								elem.next().stop(true, true).slideFadeToggle(opts.speed);
							  },
							  animateClose: function (elem, opts) { //replace the standard slideDown with custom function
								elem.next().stop(true, true).slideFadeToggle(opts.speed);
							  }
							});
						}
						});
					</script>
					';
					$eve_count = 0;
					$current_session_day = 0;
					$current_session_date = '';
					foreach ( $EM_Events as $event ) {
						
					$event_year = date('Y',$event->start);
					$event_month = date('m',$event->start);
					$event_day = date('d',$event->start);
					$event_start_time = date("G,i,s", strtotime($event->start_time));
					$location = esc_attr($event->get_location()->name);
					$team_speaker_data = get_post_meta($event->post_id,'_team_speaker_data',true);
						$session_date = date_i18n(get_option('date_format'), $event->start);
						if( $current_session_date != $session_date ){
							$current_session_day++;
							$current_session_date = $session_date;
							if($eve_count == 0){
								$active_class = 'active';
							}else{
								$active_class = '';
							}
							$evn .=  '
							<div role="tabpanel" class="tab-pane '.esc_attr($active_class).'" id="event-'.esc_attr($event->post_id).'">';
								$evn .= '
								<div class="panel-group" id="accordion-'.esc_attr($event->post_id).'" role="tablist" aria-multiselectable="true">';
								$team_counter = 0;
								$a_active_class = '';
								$a_active_in = '';
								foreach($team_speaker_data as $data){
									
									$team_speaker = explode(',',$data);
									$theevent_post_option_team = theevent_decode_stopbackslashes(get_post_meta($team_speaker[0], 'post-option', true ));
									if( !empty($theevent_post_option_team) ){
										$theevent_post_option_team = json_decode( $theevent_post_option_team, true );					
									}
									if($team_counter == 0){
										$a_active_class = 'collapsed';
										$a_active_in = 'in';
									}else{
										$a_active_class = '';
										$a_active_in = '';
									}
									if($team_speaker[0] == 'lunch-break'){
										$evn .= '
										<div class="kf_new_con_wrap kode-lunch-break">
											<div class="kf_new_conf_time">
												<h5>'.esc_attr($team_speaker[2]).'</h5>
											</div>
											<div class="kf_new_conf_des_wrap">
												<div class="kf_new_conf_hdg" id="kode-accor-'.esc_attr($team_counter).'">
													<h5>'.esc_attr($team_speaker[1]).'</h5>
													<i class="fa fa-angle-down"></i>
												</div>
												<div class="kf_new_conf_open_wrap">
													<div class="kf_new_conf_des">
														<p>'.esc_attr($team_speaker[4]).'</p>
													</div>
												</div>
											</div>
										</div>';
									}else{
										$evn .= '
										<div class="kf_new_con_wrap">
											<div class="kf_new_conf_time">
												<h5>'.esc_attr($team_speaker[2]).'</h5>
											</div>
											<div class="kf_new_conf_des_wrap">
												<div class="kf_new_conf_hdg" id="kode-accor-'.esc_attr($team_counter).'">
													<h5>'.esc_attr($team_speaker[1]).'</h5>
													<i class="fa fa-angle-down"></i>
												</div>
												<div class="kf_new_conf_open_wrap">
													<div class="kf_new_conf_des">
														<p>'.esc_attr($team_speaker[4]).'</p>
													</div>
												
													<div class="kf_new_conf_orgnizr">
														<figure>
															<a href="'.esc_url(get_permalink($team_speaker[0])).'" >'.get_the_post_thumbnail($team_speaker[0], 'full').'</a>
														</figure>
														<div class="kf_new_conf_orgnizr_name">
															<h6><a href="'.esc_url(get_permalink($team_speaker[0])).'" >'.esc_attr(get_the_title($team_speaker[0])).'</a></h6>
															<span>'.esc_attr($theevent_post_option_team['designation']).'</span>
														</div>
													</div>
												
													<div class="kf_new_conf_btn_link">
														<a href="'.esc_url($event->guid).'">'.esc_attr($team_speaker[2]).' - '.esc_attr($team_speaker[3]).'</a>
														<a href="'.esc_url($event->guid).'"><i class="fa fa-play"></i>'.esc_attr__('Details About The Event','the-event').'</a>
													</div>
												</div>
											</div>
										</div>';
									}
									
									$team_counter++;
								}
								$evn .=  '</div>
							</div>';
						}	
							$eve_count++;
					}	
				$evn .= '</div></div></div>';
			}else{
				$event_hd = 0;
				$theevent_script = '
				<script type="text/JavaScript">
				jQuery( document ).ready(function($) {
					if($(".accordion-'.esc_attr($theevent_counter).'").length){
						//custom animation for open/close
						$.fn.slideFadeToggle = function(speed, easing, callback) {
							return this.animate({opacity: "toggle", height: "toggle"}, speed, easing, callback);
						};

						$(".accordion-'.esc_attr($theevent_counter).'").accordion({
							defaultOpen: "section-'.esc_attr($event_hd).esc_attr($theevent_counter).'",
							cookieName: "nav",
							speed: "slow",
							animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
								elem.next().stop(true, true).slideFadeToggle(opts.speed);
							},
							animateClose: function (elem, opts) { //replace the standard slideDown with custom function
								elem.next().stop(true, true).slideFadeToggle(opts.speed);
							}
						});
					}
				});
				</script>';
				$evn .=  '
				<div class="schedule-banner '.esc_attr($settings['style']).'">'.$theevent_script.'
					<img src="'.esc_url($cat_img).'" alt="'.esc_attr($settings['category']).'" />
					<!-- accordian_schedule_wrap Start-->
					<div class="accordian_schedule_wrap">';
						
						$current_session_day = 0;
						$current_session_date = '';
						foreach ( $EM_Events as $event ) {
							$event_year = date('Y',$event->start);
							$event_month = date('m',$event->start);
							$event_day = date('d',$event->start);
							$session_date = date_i18n(get_option('date_format'), $event->start);
							if(!empty($event->get_location()->location_id)){
								
								$location = esc_attr($event->get_location()->name);
							}
							$team_speaker_data = get_post_meta($event->post_id,'_team_speaker_data',true);
							$event_start_time = date("G,i,s", strtotime($event->start_time));
							if( $current_session_date != $session_date ){
								$current_session_day++;
								$current_session_date = $session_date;		
								if($event_hd == 0){
									$active_hd_class = 'active';
								}else{
									$active_hd_class = '';
								}
								//if($settings['event-title-type'] == 'title-wise'){
								$evn .=  '
								<div class="kode_goal_wrap">';
									if($settings['event-title-type'] == 'title-wise'){
										$evn .=  '
										<div class="accordion accordion-'.esc_attr($theevent_counter).' '.esc_attr($active_hd_class).'" id="section-'.esc_attr($event_hd).esc_attr($theevent_counter).'">
											<strong>'.esc_attr($event->post_title).'</strong>
											<span>'.date('l',$event->start).' - '.date('d.m.Y',$event->start).'</span>
										</div>';
									}else{
										$evn .=  '
										<div class="accordion accordion-'.esc_attr($theevent_counter).' '.esc_attr($active_hd_class).'" id="section-'.esc_attr($event_hd).esc_attr($theevent_counter).'">
											<strong>'.esc_attr__('Day ','the-event').$current_session_day.' </strong>
											<span>'.date('l',$event->start).' - '.date('d.m.Y',$event->start).'</span>
										</div>';
									}
									$evn .=  '
									<div class="accordion-content">
										<!--accordion_des start-->
										<ul class="accordion_des">';
										$team_counter = 0;
										$a_active_class = '';
										$a_active_in = '';
										foreach($team_speaker_data as $data){
											$team_speaker = explode(',',$data);
											if($team_counter == 0){
												$a_active_class = 'collapsed';
												$a_active_in = 'in';
											}else{
												$a_active_class = '';
												$a_active_in = '';
											}
											if($team_speaker[0] == 'lunch-break'){
												$evn .=  '
												<li>
													<div class="interval_wrap">
														<strong><i class="fa fa-clock-o"></i>'.esc_attr($team_speaker[2]).' - '.esc_attr($team_speaker[3]).'</strong>
														<span>Interval &amp; '.esc_attr($team_speaker[1]).'</span>
													</div>
												</li>';
											}else{
											$evn .=  '
												<li>
													<!--accordion_des_capstion start-->
													<div class="accordion_des_capstion">
														<div class="accordion_capstion_heading">
															<h5><i class="fa fa-microphone"></i><a href="'.esc_url(get_permalink($team_speaker[0])).'">'.esc_attr(get_the_title($team_speaker[0])).'</a></h5>
															<span><i class="fa fa-clock-o"></i>'.esc_attr($team_speaker[2]).' - '.esc_attr($team_speaker[3]).'</span>
														</div>
														<p>'.esc_attr($team_speaker[4]).'</p>
													</div>
													<!--accordion_des_capstion end-->
												</li>';
											}	
												$team_counter++;
										}	
										
										$evn .=  '	
										</ul>
										<!--accordion_des end-->
									</div>
								</div>';
							}
							$event_hd++;		
						}
					$evn .=  '
					</div>
					<!--accordian_schedule_wrap End-->
				</div></div>';			
			}
			return $evn;
		}
	}	
	
	
	//Event Listing
	if( !function_exists('theevent_get_events_item') ){
		function theevent_get_events_item( $settings ){
			global $theevent_spaces,$theevent_theme_option,$theevent_counter;
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$margin = (!empty($settings['margin-bottom']))? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$settings['element-item-class'] = empty($settings['element-item-class'])? '' : $settings['element-item-class'];
			$evn = '<div '.$margin_style.' '.$item_id.' class="'.esc_attr($settings['element-item-class']).' kode_event_upnext_new">';
			// $settings['category'];
			// $settings['tag'];
			// $settings['num-excerpt'];
			// $settings['num-fetch'];
			// $settings['event-style'];
			// $settings['scope'];
			// $settings['order'];
			// $settings['margin-bottom'];			
			$order = 'DESC';
			$limit = 10;//Default limit
			$offset = '';		
			$rowno = 0;
			$event_count = 0;
			$EM_Events = EM_Events::get( array('category'=>$settings['category'], 'group'=>'this','scope'=>$settings['scope'], 'limit' => $settings['num-fetch'], 'order' => $settings['order']) );
			$events_count = count ( $EM_Events );	
			$current_size = 0;
			$size = 1;
			// $evn = '<div class="event-listing">';
			
			$settings['thumbnail-size'];
			$settings['event-size'];
			if($settings['event-style'] == 'event-simple-view'){
				$size = $settings['event-size'];
				$evn = '<div class="event-listing event-simple-view row">';
			}else if($settings['event-style'] == 'event-grid-view'){
				$size = $settings['event-size'];
				$evn = '<div class="event-listing event-grid-view row">';
			}else if($settings['event-style'] == 'event-medium-view'){
				if($settings['event-size'] == 3){
					$size = 2;
				}else if($size == 4){
					$size = 2;
				}else{
					$size = $settings['event-size'];	
				}
				$evn = '<div class="event-listing event-medium-view row">';
			}else if($settings['event-style'] == 'event-full-view'){
				if($settings['event-size'] == 3){
					$size = 2;
				}else if($size == 4){
					$size = 2;
				}else{
					$size = $settings['event-size'];	
				}
				$evn = '<div class="event-listing event-full-view row">';
			}else{
				$size = $settings['event-size'];
				$evn = '<div class="event-listing event-simple-view row">';
			}
			if($settings['event-style'] == 'event-calendar-view'){
			$evn = '
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$("#calendar").fullCalendar({
						defaultDate: "2015-02-12",
						editable: true,
						eventLimit: true, // allow "more" link when too many events
						events: [';
			}
			if($settings['event-style'] == 'event-marker-view'){
				
				$script_html = '
				var icons = [ ';
					foreach ( $EM_Events as $event ) {
						$event_year = date('Y',$event->start);
						$event_month = date('m',$event->start);
						$event_day = date('d',$event->start);
						$event_start_time = date("G,i,s", strtotime($event->start_time));
						$location = esc_attr($event->get_location()->name);
						global $post;
							$script_html .= "'".THEEVENT_PATH.'/images/map-icon-2.png'."',";
					} 
					$script_html .= '
				];';
				
				$evn = '
				<script src="http://maps.google.com/maps/api/js?sensor=false"></script> 
				<script type="text/javascript">
					var geocoder = new google.maps.Geocoder();
					var iconURLPrefix = "images";
					'.$script_html.'
					function theevent_kode_initialize(){
						
						var MY_MAPTYPE_ID = "custom_style";
						var icons_length = icons.length;
						var shadow = {
						  anchor: new google.maps.Point(16,16),
						  url: iconURLPrefix + "msmarker.shadow.png"
						};
						var featureOpts = [
							{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}

						];
						var myOptions = {
						  center: new google.maps.LatLng(16,18),
						  mapTypeId: MY_MAPTYPE_ID,
						  mapTypeControl: true,
						  streetViewControl: true,
						  panControl: true,
						  scrollwheel: false,
						  draggable: true,	  
						  zoom: 3,
						}
						var map = new google.maps.Map(document.getElementById("kode_map_canv"), myOptions);
						var styledMapOptions = {
							name: "Custom Style"
						};

						var customMapType = new google.maps.StyledMapType(
							[
							  {
								stylers: [
								  {hue: "'.esc_attr($theevent_theme_option['color-scheme-one']).'"},
								  {visibility: "simplified"},
								  {gamma: 0.5},
								  {weight: 0.5}
								]
							  },
							  {
								elementType: "labels",
								stylers: [{visibility: "off"}]
							  },
							  {
								featureType: "water",
								stylers: [{color: "'.esc_attr($theevent_theme_option['color-scheme-one']).'"}]
							  }
							], {
							  name: "Custom Style"
						  }
						);

						map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
						
						var infowindow = new google.maps.InfoWindow({
						  maxWidth: 350,
						});
						var marker;
						var markers = new Array();
						var iconCounter = 0;';				
			}
			$settings['title-num-excerpt'] = (empty($settings['title-num-excerpt']))? '15': $settings['title-num-excerpt'];
			$modern_layout = $settings['event-style'];
			$current_size = 0;
			$EM_Events = EM_Events::get( array('category'=>$settings['category'], 'group'=>'this','scope'=>$settings['scope'], 'limit' => $settings['num-fetch'], 'order' => $settings['order']) );
			foreach ( $EM_Events as $event ) {
				// echo '<pre>';print_r($event);
				$event_year = date('Y',$event->start);
				$event_month = date('m',$event->start);
				$event_day = date('d',$event->start);
				$event_start_time = date("G,i,s", strtotime($event->start_time));
				if(isset($event->get_location()->name)){
					$location = esc_attr($event->get_location()->name);
					if($settings['event-style'] <> 'event-calendar-view'){
						if($settings['event-style'] <> 'event-marker-view'){
							if( $current_size % $size == 0 ){
								$evn .= '<div class="clear clearfix"></div>';
							}
						}
					}
				}
				if($settings['event-style'] == 'event-simple-view'){
					$event_html = '';
					if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '
								<li><a href="'.esc_url($event->guid).'"><i class="fa fa-map-marker"></i>
								'.$location.'</a></li>
							';
						}
					}else{
						$event_html = '';				
					}
					$evn .= '<div class="kode-ux ' . esc_attr(theevent_get_column_class('1/' . $size)) . '"><div class="kode-health-conference">
                            <div class="kf_heading_2">
                                <h3>'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</h3>
                                <span>'.esc_attr(strip_tags(substr($event->post_content,0,15))).'</span>
                            </div>
                            <figure>
                                '.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
                                <ul class="kode-health-over">
                                    <li>
                                        <a href="#"><i class="fa fa-calendar"></i>'.esc_attr(date('d M Y',$event->start)).'</a></li>';
										if($event->event_all_day <> 1){
											$evn .= '<li><span> <a href="'.esc_url($event->guid).'"> <i class="fa fa-clock-o"></i> '.esc_attr(date('H:i:s',strtotime($event->start_time))).' - '.esc_attr(date('H:i:s',strtotime($event->end_time))).' </a> </span> </li>	';
										}else{
											$evn .= '<li> <a href="'.esc_url($event->guid).'"> <i class="fa fa-clock-o"></i>All Day</a></li>';
										}
										$evn .= '
                                        '.$event_html.'
                                    
                                </ul>
                            </figure>
                            <!--KODE-HEALTH-CAPSTION-START-->
                            <div class="kode-health-caption">
                                <p>'.esc_attr(strip_tags(substr($event->post_content,0,169))).'</p>';
								if(isset($theevent_theme_option['event-read-more']) && $theevent_theme_option['event-read-more'] <> ''){
									$evn .= '<a href="'.esc_url($event->guid).'">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-read-more']).'</a>';
								}
                            $evn .= '</div>
                            <!--KODE-HEALTH-CAPSTION-END-->
                        </div>
						</div>';
				}else if($settings['event-style'] == 'event-grid-view'){
					 $event_html = '';
					 if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<span>
								<i class="fa fa-map-marker"></i>
								'.$location.'
							</span>';
						}
					}else{
						$event_html = '';				
					}
					$evn .= '
					<div class="kode-ux  event-grid-view ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">
						<div class="kode-event-blog3">
							<figure>
								'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
								<div class="kode-fig-capstion">
									<a href="#">Conference Hall</a>
									<p>'.esc_attr(strip_tags(substr($event->post_content,0,30))).'</p>
								</div>
							</figure>
							<div class="kode-thumb-caption">
								<h4>'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</h4>
								<ul>
									<li><a href="'.esc_url($event->guid).'"><i class="fa fa-calendar"></i>'.esc_attr($event_month).' '.esc_attr($event_day).' / '.esc_attr($event_year).'</a></li>
									<li><a href="'.esc_url($event->guid).'">'.$event_html.'</a></li>
								</ul>';
								if(isset($theevent_theme_option['event-read-more']) && $theevent_theme_option['event-read-more'] <> ''){
								$evn .= '<span><a href="'.esc_url($event->guid).'">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-read-more']).'</a></span>';
								}
							$evn .= '</div>
						</div>
					</div>';				
				}else if($settings['event-style'] == 'event-medium-view'){
					 if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<p>
								<i class="fa fa-map-marker"></i>
								'.$location.'
							</p>';
						}
					}else{
						$event_html = '';				
					}
					$evn .= '
					<div class="kode-ux ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">					
						<div class="kode_event_3">
							<figure>
								'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
							</figure>
							<div class="kode_event3_des">
								<h4>'.esc_attr(date('d',$event->start)).' '.esc_attr(date('M',$event->start)).' - Mauris in erat a ipsum </h4>';
								if($event->event_all_day <> 1){
									$evn .= '<span><i class="fa fa-clock-o"></i>'.esc_attr(date('H:i:S',strtotime($event->start_time))).' - '.esc_attr(date('H:i:S',strtotime($event->end_time))).'</span>';
								}else{
									$evn .= '<span><i class="fa fa-clock-o"></i>'.esc_attr__('All Day','the-event').'</span>';
								}
								$evn .= $event_html.'
								<p>'.esc_attr(strip_tags(substr($event->post_content,0,169))).'</p>';
								if(isset($theevent_theme_option['event-read-more']) && $theevent_theme_option['event-read-more'] <> ''){
								$evn .= '<a href="'.esc_url($event->guid).'">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-read-more']).'</a>';
								}
							$evn .= '</div>
						</div>
					</div>';
				}else if($settings['event-style'] == 'event-full-view'){
                  if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<span>
								<i class="fa fa-map-marker"></i>
								'.$location.'
							</span>';
						}
					}else{
						$event_html = '';				
					}
				  $evn .= '
					  <div class="kode-ux ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">					  
						  <div class="kode_event_full">
							<figure>
								'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
								<div class="event_full_date">
									<div class="kode_aside_event_wrap">
										<div class="aside_event_date">
											<span>'.esc_attr(date('M Y',$event->start)).'</span>
											<h4>'.esc_attr(date('d',$event->start)).'</h4>
										</div>
										<div class="event_time">';
											if($event->event_all_day <> 1){
												$evn .= '<span> '.esc_attr(date('H:i:S',strtotime($event->start_time))).' - '.esc_attr(date('H:i:S',strtotime($event->end_time))).'</span>';
											}else{
												$evn .= '<span>'.esc_attr__('All Day','the-event').'</span>';
											}
											$evn .= '
										</div>
									</div>
								</div>
								<div class="blog_counter">
									<ul data-year="'.esc_attr($event_year).'" data-month="'.esc_attr($event_month).'" data-day="'.esc_attr($event_day).'" data-time="'.esc_attr($event_start_time).'" class="event_countdown">
										<li>
											<span class="days">00</span>';
										if(isset($theevent_theme_option['event-week']) && $theevent_theme_option['event-week'] <> ''){
											$evn .= '<p class="days_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-week']).'</p>';
										}	
										$evn .= '</li>
										<li>
											<span class="hours">00</span>';
										if(isset($theevent_theme_option['event-days']) && $theevent_theme_option['event-days'] <> ''){
											$evn .= '<p class="hours_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-days']).'</p>';
										}	
										$evn .= '</li>
										<li>
											<span class="minutes">00</span>';
										if(isset($theevent_theme_option['event-hours']) && $theevent_theme_option['event-hours'] <> ''){		
											$evn .= '<p class="minutes_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-hours']).'</p>';
										}
										$evn .= '</li>
										<li>
											<span class="seconds last">00</span>';
										if(isset($theevent_theme_option['event-sec']) && $theevent_theme_option['event-sec'] <> ''){		
											$evn .= '<p class="seconds_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-sec']).'</p>';
										}
										$evn .= '</li>
									</ul>
								</div>
							</figure>
							<div class="event_full_des">
								<h4>'.esc_attr(get_the_title()).'</h4>
								'.$event_html.'
								<p>'.esc_attr(strip_tags(substr($event->post_content,0,169))).'</p>';
								if(isset($theevent_theme_option['event-read-more']) && $theevent_theme_option['event-read-more'] <> ''){
								$evn .= '<a href="'.esc_url($event->guid).'">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-read-more']).'</a>';
								}		
							$evn .= '</div>
						</div>
					</div>';
				}else if($settings['event-style'] == 'event-calendar-view'){
                  if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<span>
								<i class="fa fa-map-marker"></i>
								'.$location.'
							</span>';
						}
					}else{
						$event_html = '';				
					}
				  $evn .= '
					{
						id: '.$event->post_id.',
						title: "'.esc_attr(get_the_title()).'",
						start: "'.esc_attr(date('Y-d-m',$event->start)).'T'.esc_attr(date('H:i:s',strtotime($event->start_time))).'",
						end: "'.esc_attr(date('Y-d-m',$event->end)).'",
						url: "'.esc_url($event->guid).'",
					},';
				}else if($settings['event-style'] == 'event-marker-view'){
                  if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_loc = $location;
						}
					}else{
						$event_loc = '';				
					}
					if($event_loc <> ''){
					$html_ev = '<div class="col-md-12"><div class="kode-event-list-map ">'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'<div class="kode-event-caption"><h2><a href="'.esc_url($event->guid).'">'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</a></h2><p>'.esc_attr(date('D M d Y',$event->start)).'</p><a class="btn-borderd" href="'.esc_url($event->guid).'">'.esc_attr__('Read More','the-event').'</a></div></div></div>';
					
					$evn .= "
						var i = ".esc_attr($current_size).";
						geocoder.geocode( { 'address': '".esc_attr($event_loc)."'}, function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {

								marker = new google.maps.Marker({
									position: results[0].geometry.location,
									map: map,
									icon : icons[iconCounter],
									shadow: shadow
								});

							markers.push(marker);
							google.maps.event.addListener(marker, 'click', (function(marker, i) {
								return function() {
									infowindow.setContent('".$html_ev."');
									infowindow.open(map, marker);
								}
							})(marker, i));
							  
							  iconCounter++;
							  // We only have a limited number of possible icon colors, so we may have to restart the counter
							  if(iconCounter >= icons_length){
								iconCounter = 0;
							  }
							}
							
						});";
					}	
				}else{
				
				}
				$current_size++;
			}
			
			if($settings['event-style'] == 'event-marker-view'){
				$evn .= '}
						google.maps.event.addDomListener(window, "load", theevent_kode_initialize);
				</script><div class="row"><div class="ev_map_canvas" id="kode_map_canv"></div></div>';
			}
			if($settings['event-style'] == 'event-calendar-view'){
				$evn .= '
							]
						});
					})
				</script>
				<div id="calendar"></div>
				';
			}
			
			if($modern_layout == 'event-grid-modern'){
				$evn .= '</div>';	
			}else if($modern_layout == 'event-grid-simple'){
				$evn .= '</ul></div>';
			}else if($settings['event-style'] == 'event-marker-view'){
				
			}else if($settings['event-style'] == 'event-calendar-view'){
				
			}else{
				$evn .= '</div>';
			}
			
		return $evn;
		}
	}
	
	function theevent_get_upnext_event( $settings ){
		
		global $theevent_spaces;
		$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
		$margin = (!empty($settings['margin-bottom']))? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
		$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
		$settings['element-item-class'] = empty($settings['element-item-class'])? '' : $settings['element-item-class'];
		$evn = '<div '.$margin_style.' '.$item_id.' class="'.esc_attr($settings['element-item-class']).' kode-register-holder">';
		
		$settings['heading'];
		$settings['caption'];
		$settings['category'];
		$settings['title-num-excerpt'];		
		$order = 'DESC';
		$limit = 10;//Default limit
		$offset = '';		
		$rowno = 0;
		$event_count = 0;
		$EM_Events = EM_Events::get( array('category'=>$settings['category'], 'group'=>'this','scope'=>'future', 'limit' => '1', 'order' => 'DESC') );
		$events_count = count ( $EM_Events );	
		foreach ( $EM_Events as $event ) {
			$event_year = date('Y',$event->start);
			$event_month = date('m',$event->start);
			$event_month_m = date('M',$event->start);
			$event_day = date('d',$event->start);
			$event_start_time = date("G:i:s", strtotime($event->start_time));
			$location = esc_attr($event->get_location()->name);
			$thumbnail_id = get_post_thumbnail_id( $event->post_id );
			$thumbnail = wp_get_attachment_image_src( $thumbnail_id , 'large' );
			$evn .= '			
			<div class="container">
				<div class="register-button">
					<div class="kode-register-detail-left">
						<div class="register-time"><span>'.esc_attr($event_day).'</span> '.esc_attr($event_month_m).'</div>
						<a class="register-content" href="'.esc_url($event->guid).'">
							<h3>'.esc_attr($event->post_title).'</h3>
							<p>'.strip_tags(substr($event->post_content,0,30)).'</p>
						</a>
					</div>
					<div class="kode-register-detail-right">
						<ul data-year="'.esc_attr($event_year).'" data-month="'.esc_attr($event_month).'" data-day="'.esc_attr($event_day).'" data-time="'.esc_attr($event_start_time).'" class="downcount">
							<li>
									<span class="days">00</span>';
								if(isset($theevent_theme_option['event-week']) && $theevent_theme_option['event-week'] <> ''){	
									$evn .= '<p class="days_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-week']).'</p>';
								}	
								$evn .= '</li>
								<li>
									<span class="hours">00</span>';
								if(isset($theevent_theme_option['event-days']) && $theevent_theme_option['event-days'] <> ''){
									$evn .= '<p class="hours_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-days']).'</p>';
								}	
								$evn .= '</li>
								<li>
									<span class="minutes">00</span>';
								if(isset($theevent_theme_option['event-hours']) && $theevent_theme_option['event-hours'] <> ''){		
									$evn .= '<p class="minutes_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-hours']).'</p>';
								}
								$evn .= '</li>
								<li>
									<span class="seconds last">00</span>';
								if(isset($theevent_theme_option['event-sec']) && $theevent_theme_option['event-sec'] <> ''){		
									$evn .= '<p class="seconds_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-sec']).'</p>';
								}
								$evn .= '</li>
						</ul>
					</div>
				</div>
			</div>';
			
		}	
		$evn .= '</div>';
		return $evn;
	}
	
	if( !function_exists('theevent_next_event_starts') ){
		function theevent_next_event_starts($settings = array()){
			global $theevent_allowed_html_tags;
			$ret = '
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-5">
					<div class="find-us-box">
						<div class="find-us-heder">
							<div class="content-blue">
								'.wp_kses($settings['next-event-find-text'],$theevent_allowed_html_tags).'
							</div>
						</div>
						<div class="content-black">
							<p>'.$settings['next-event-button-description'].'</p>
							<a href="'.esc_url($settings['next-event-button-url']).'" class="contactus-button">'.esc_attr($settings['next-event-button-text']).'</a>
						</div>
					</div>
				</div>
				<!-- Event Counter -->
				<div class="col-lg-8 col-md-8 col-sm-7">
					<div class="next-event-counter">
						<h2>'.esc_attr($settings['next-event-title']).'</h2>';
						$evn = '';
						$order = 'DESC';
						$limit = 10;//Default limit
						$offset = '';		
						$rowno = 0;
						$event_count = 0;
						$EM_Events = EM_Events::get( array('category'=>$settings['next_event'], 'group'=>'this','scope'=>'future', 'limit' => '1', 'order' => 'DESC') );
						$events_count = count ( $EM_Events );	
						foreach ( $EM_Events as $event ) {
							$event_year = date('Y',$event->start);
							$event_month = date('m',$event->start);
							$event_day = date('d',$event->start);
							$event_start_time = date("G:i:s", strtotime($event->start_time));
							$location = esc_attr($event->get_location()->name);
							$thumbnail_id = get_post_thumbnail_id( $event->post_id );
							$thumbnail = wp_get_attachment_image_src( $thumbnail_id , 'large' );
							$ret .= '
							<ul data-year="'.esc_attr($event_year).'" data-month="'.esc_attr($event_month).'" data-day="'.esc_attr($event_day).'" data-time="'.esc_attr($event_start_time).'" class="downcount">
								<li>
										<span class="days">00</span>';
									if(isset($theevent_theme_option['event-week']) && $theevent_theme_option['event-week'] <> ''){	
										$evn .= '<p class="days_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-week']).'</p>';
									}	
									$evn .= '</li>
									<li>
										<span class="hours">00</span>';
									if(isset($theevent_theme_option['event-days']) && $theevent_theme_option['event-days'] <> ''){
										$evn .= '<p class="hours_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-days']).'</p>';
									}	
									$evn .= '</li>
									<li>
										<span class="minutes">00</span>';
									if(isset($theevent_theme_option['event-hours']) && $theevent_theme_option['event-hours'] <> ''){		
										$evn .= '<p class="minutes_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-hours']).'</p>';
									}
									$evn .= '</li>
									<li>
										<span class="seconds last">00</span>';
									if(isset($theevent_theme_option['event-sec']) && $theevent_theme_option['event-sec'] <> ''){		
										$evn .= '<p class="seconds_ref">'.sprintf(__('%s','kf_event'),$theevent_theme_option['event-sec']).'</p>';
									}
							$evn .= '</li>
							</ul>';
						}
						$ret .= '
						<h3>'.esc_attr($settings['next-event-subscribe-txt']).'</h3>
						<form action="#" class="subscribe-form">
							<input type="email" placeholder="Email Address">
							<label><input type="submit" name="submit" value=""></label>
						</form>
					</div>
				</div>
			</div>';
			
			return $ret;
			
		}
	}
	
	
	
	//Event Listing
	if( !function_exists('theevent_get_event_table_calendar') ){
		function theevent_get_event_table_calendar( $settings ){
			
			// $settings['category'];
			// $settings['tag'];
			// $settings['num-excerpt'];
			// $settings['num-fetch'];
			// $settings['event-style'];
			// $settings['scope'];
			// $settings['order'];
			// $settings['margin-bottom'];
			
			$settings['prayer-calendar-heading'];
			$settings['category'];
			$settings['tag'];
			$settings['scope'];
			$settings['prayer-table-heading'];
			$settings['fajar-time'];
			$settings['duhur-time'];
			$settings['asar-time'];
			$settings['mugrib-time'];
			$settings['asha-time'];
			$evn = '';
			$order = 'DESC';
			$limit = 10;//Default limit
			$offset = '';		
			$rowno = 0;
			$event_count = 0;
			$EM_Events = EM_Events::get( array('category'=>$settings['category'], 'group'=>'this','scope'=>$settings['scope'], 'limit' => $settings['num-fetch'], 'order' => $settings['order']) );
			$events_count = count ( $EM_Events );	
			$current_size = 0;
			$size = 1;
			
			$size = 1;
			$evn = '<div class="event-listing row">';
			
			$evn = '
			
					<!--Calender Wrap Start-->
                	<div class="col-md-6">
                    	'.do_shortcode('[heading style="islamic-style" title="'.$settings['prayer-calendar-heading'].'" title_color="#fff" caption="" caption_color="" item_margin="30" ][/heading]').'
                    	<div class="kode_moq_calndr">                        
							<script type="text/javascript">
							jQuery(document).ready(function($){
								$("#calendar").fullCalendar({
									defaultDate: "2015-02-12",
									editable: true,
									eventLimit: true, // allow "more" link when too many events
									events: [';
						
									$settings['title-num-excerpt'] = (empty($settings['title-num-excerpt']))? '15': $settings['title-num-excerpt'];
									$modern_layout = $settings['event-style'];
									
									foreach ( $EM_Events as $event ) {
										$event_year = date('Y',$event->start);
										$event_month = date('m',$event->start);
										$event_day = date('d',$event->start);
										$event_start_time = date("G,i,s", strtotime($event->start_time));
										$location = esc_attr($event->get_location()->name);
										
										
										  $evn .= '
											{
												id: '.$event->post_id.',
												title: "'.esc_attr(get_the_title()).'",
												start: "'.esc_attr(date('Y-d-m',$event->start)).'T'.esc_attr(date('H:i:s',strtotime($event->start_time))).'",
												end: "'.esc_attr(date('Y-d-m',$event->end)).'",
												url: "'.esc_url($event->guid).'",
											},';
										
										$current_size++;
									}
									$evn .= '
										]
									});
								})
							</script>
							<div id="calendar"></div>
						</div>
					</div>
					<!--Calender Wrap End-->

			<!--Namaz Time Wrap Start-->
					<div class="col-md-6">
						'.do_shortcode('[heading style="islamic-style" title="'.$settings['prayer-table-heading'].'" title_color="#000" caption="" caption_color="" item_margin="30" ][/heading]').'
						<div class="kode_moq_nmz_time">
							<div class="kode_moq_nmz_clck">
								<ul id="analog-clock" class="analog">	
									<li class="hour"></li>
									<li class="min"></li>
									<li class="sec"></li>
									<li></li>
								</ul>
								<a href="#">Upcomming Namaz Asar</a>
							</div>
							
							<div class="kode_moq_namz_tble">
								<ul>
									<li>
										<a href="#">
											<span>Fajar</span>
											<p>'.$settings['fajar-time'].'</p>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span>Djuhur</span>
											<p>'.$settings['duhur-time'].'</p>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span>Asar</span>
											<p>'.$settings['asar-time'].'</p>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span>Magrib</span>
											<p>'.$settings['mugrib-time'].'</p>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span>Asha</span>
											<p>'.$settings['asha-time'].'</p>
										</a>
									</li>
									
								</ul>
							</div>
							'.do_shortcode('[button link="#" type="islamic" target="_self" color="" bgcolor="" size="small" ]Islamic Event[/button]').'
							'.do_shortcode('[button link="#" type="islamic" target="_self" color="" bgcolor="" size="small" ]Hijri Event[/button]').'
						</div>
					</div>
					<!--Namaz Time Wrap End-->';
			
			
			
			$evn .= '';
			
			
		return $evn;
		}
	}
	
	add_action('pre_post_update', 'theevent_save_event_meta_option');
	if( !function_exists('theevent_save_event_meta_option') ){
		function theevent_save_event_meta_option( $post_id ){
			if( get_post_type() == 'event' && isset($_POST['post-option']) ){
				$post_option = theevent_stopbackslashes(theevent_stripslashes($_POST['post-option']));
				$post_option = json_decode(theevent_decode_stopbackslashes($post_option), true);
				
				//print_r($_POST['team_speaker_member']);
				$_POST['team_speaker_topic'];
				$_POST['team_speaker_start'];
				$_POST['team_speaker_end'];
				$_POST['team_speaker_topic_desc'];
		
				if(!empty($_POST['team_speaker_topic']) && $_POST['team_speaker_topic'] != ''){
					$i = 0;
					foreach($_POST['team_speaker_member'] as $team_speaker_member){
						if($_POST['team_speaker_topic'][$i] <> '--blank--'){
							$post_data[] = $_POST['team_speaker_member'][$i].','.$_POST['team_speaker_topic'][$i].','.$_POST['team_speaker_start'][$i].','.$_POST['team_speaker_end'][$i].','.$_POST['team_speaker_topic_desc'][$i];
							update_post_meta($post_id, '_team_speaker_data', $post_data);
						}
						$i++;
					}
				}else{
					delete_post_meta($post_id, '_team_speaker_data');
				}
				
				if(!empty($_POST['team_speaker_member'])){
					update_post_meta($post_id, '_team_speaker_member', $_POST['team_speaker_member']);
				}else{
					delete_post_meta($post_id, '_team_speaker_member');
				}
				if(!empty($_POST['team_speaker_topic'])){
					update_post_meta($post_id, '_team_speaker_topic', $_POST['team_speaker_topic']);
				}else{
					delete_post_meta($post_id, '_team_speaker_topic');
				}
				if(!empty($_POST['team_speaker_start'])){
					update_post_meta($post_id, '_team_speaker_start', $_POST['team_speaker_start']);
				}else{
					delete_post_meta($post_id, '_team_speaker_start');
				}
				if(!empty($_POST['team_speaker_end'])){
					update_post_meta($post_id, '_team_speaker_end', $_POST['team_speaker_end']);
				}else{
					delete_post_meta($post_id, '_team_speaker_end');
				}
				if(!empty($_POST['team_speaker_topic_desc'])){
					update_post_meta($post_id, '_team_speaker_topic_desc', $_POST['team_speaker_topic_desc']);
				}else{
					delete_post_meta($post_id, '_team_speaker_topic_desc');
				}
			}
		}
	}
	
?>