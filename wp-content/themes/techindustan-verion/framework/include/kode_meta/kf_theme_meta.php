<?php
	/*	
	*	Kodeforest Admin Panel
	*	---------------------------------------------------------------------
	*	This file create the class that help you create the controls admin  
	*	option for custom theme
	*	---------------------------------------------------------------------
	*/	
	
	$theevent_countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
	
	
	
	if( !class_exists('theevent_generate_admin_html') ){
		
		class theevent_generate_admin_html{
			
			
			public $theevent_allowed_html_tags = array(
				'a' => array(
					'href' => array(),
					'class' => array(),
					'id' => array(),
					'title' => array()
				),
				'textarea' => array(
					'name' => array(),
					'class' => array(),
					'data-slug' => array(),
					'id' => array(),
					'title' => array()
				),
				'input' => array(
					'type' => array(),
					'value' => array(),
					'class' => array(),
					'data-slug' => array(),
					'id' => array(),
					'title' => array()
				),
				'ul' => array(),
				'h1' => array(
					'class' => array(),
					'id' => array(),
				),
				'h2' => array(
					'class' => array(),
					'id' => array(),
				),
				'h3' => array(
					'class' => array(),
					'id' => array(),
				),
				'h4' => array(
					'class' => array(),
					'id' => array(),
				),
				'h5' => array(
					'class' => array(),
					'id' => array(),
				),
				'h6' => array(
					'class' => array(),
					'id' => array(),
				),
				'p' => array(),
				'div' => array(
					'class' => array(),
					'id' => array(),
				),
				'li' => array(),
				'br' => array(),
				'em' => array(),
				'strong' => array(),
				'&gt;' => array(),
				'style' => array(
					'scoped'=>array(),
					'id'=>array(),
				),
			);
			
			// decide to generate each option by type
			function theevent_generate_html($settings = array()){
				echo '<div class="kode-option-wrapper ';
				echo (isset($settings['wrapper-class']))? $settings['wrapper-class'] : '';
				echo '">';
				
				$description_class = empty($settings['description'])? '': 'with-description';
				echo '<div class="kode-option ' . esc_attr($description_class) . '">';
				
				// option title
				if( !empty($settings['title']) ){
					echo '<div class="kode-option-title">' . esc_attr($settings['title']) . '</div>';
				}
				
				// input option
				switch ($settings['type']){
					case 'text': $this->show_text_input($settings); break;
					case 'server-config': $this->show_simple_text($settings); break;
					case 'textarea': $this->show_textarea($settings); break;
					case 'combobox': $this->show_combobox($settings); break;					
					case 'combobox_sidebar': $this->show_combobox_sidebar($settings); break;					
					case 'multi-combobox': $this->show_multi_combobox($settings); break;
					case 'checkbox': $this->show_checkbox($settings); break;
					case 'radioimage': $this->show_radio_image($settings); break;
					case 'radioheader': $this->show_radio_header_image($settings); break;
					case 'colorpicker': $this->show_color_picker($settings); break;					
					case 'sliderbar': $this->show_slider_bar($settings); break;
					case 'slider': $this->show_slider($settings); break;
					case 'gallery': $this->show_gallery($settings); break;
					case 'sidebar': $this->theevent_show_sidebar_data($settings); break;
					case 'multi-fields': $this->theevent_show_add_fields($settings); break;
					case 'multi-fields-button': $this->theevent_show_add_fields_button($settings); break;
					case 'font_option': $this->print_font_combobox($settings); break;
					case 'upload': $this->show_upload_box($settings); break;
					case 'header': $this->show_header_box($settings); break;
					case 'custom': $this->show_custom_option($settings); break;
					case 'player-lineup': $this->show_players_lineup($settings); break;
					case 'player-lineup-second': $this->show_players_lineup_second($settings); break;
					
					case 'date-picker': $this->show_date_picker($settings); break;
				}
				
				// input descirption
				if( !empty($settings['description']) ){
					echo '<div class="kode-input-description"><span>' . esc_attr($settings['description']) . '<span></div>';
					echo '<div class="clear"></div>';
				}
				
				echo '</div>'; // kode-option
				echo '</div>'; // kode-option-wrapper				
			}
			
			function theevent_show_sidebar_data($settings = array()){
				global $theevent_theme_option;				
				
				echo '<div class="kode-option-input">';
				echo '<input type="text" class="kode-upload-box-input" data-slug="' . esc_attr($settings['slug']) . '" value="' . esc_attr($settings['placeholder']) . '" rel="' . esc_attr($settings['placeholder']) . '">';
				echo '<div id="' . esc_attr($settings['slug']) . '" class="kdf-button">'.esc_html__('Add','the-event').'</div>';
				
				echo '<div class="clear"></div>';
				echo '</div>';
					
				echo '<div id="selected-sidebar" class="sidebar-default-k">';
					echo '<div class="default-sidebar-item" id="sidebar-item">';
						echo '<div class="panel-delete-sidebar"></div>';
						echo '<div class="slider-item-text"></div>';
						echo '<input type="hidden" id="sidebar">';
					echo '</div>';
					if(!empty($theevent_theme_option['sidebar-element'])){
						if(isset($settings['value'] )){
							foreach($settings['value'] as $item){
								echo '
								<div id=" " class="sidebar-item" style="">
									<div class="panel-delete-sidebar"></div>
									<div class="slider-item-text">'.esc_attr($item).'</div>
									<input type="hidden" id="sidebar" name="' . esc_attr($settings['slug']) . '[]" data-slug="' . esc_attr($settings['slug']) . '[]" value="'.esc_attr($item).'">
								</div>';
							}
						}
					}
				echo '</div>';	

			
			}
			
			function theevent_show_add_fields($settings = array()){
				global $post;
				$option_value = theevent_decode_stopbackslashes(get_post_meta( $post->ID, 'post-option', true ));
				if( !empty($option_value) ){
					$option_value = json_decode( $option_value, true );					
				}
				$i = 0;
				// $values_slug = array();
				// foreach( $settings['data-array'] as $option_slug => $option_settings ){
					// $option_settings['slug'] = $option_slug.'[]';
					// $option_settings['name'] = $option_slug.'[]';
					// $values_slug[] = get_post_meta(get_the_ID(),'_'.$option_slug,true);
					
				// }
				
				
				$team_speaker_member = get_post_meta(get_the_ID(),'_team_speaker_member',true);
				$team_speaker_topic = get_post_meta(get_the_ID(),'_team_speaker_topic',true);
				$team_speaker_start = get_post_meta(get_the_ID(),'_team_speaker_start',true);
				$team_speaker_end = get_post_meta(get_the_ID(),'_team_speaker_end',true);
				$team_speaker_topic_desc = get_post_meta(get_the_ID(),'_team_speaker_topic_desc',true);
				$team_speaker_data = get_post_meta(get_the_ID(),'_team_speaker_data',true);
			
					foreach( $settings['data-array'] as $option_slug => $option_settings ){
						
						echo '<div class="kode-option-wrapper-child ';
						echo (isset($option_settings['wrapper-class']))? $option_settings['wrapper-class'] : '';
						echo '">';
						
						$description_class = empty($option_settings['description'])? '': 'with-description';
						echo '<div class="kode-option ' . esc_attr($description_class) . '">';
						
						
						if( !empty($option_settings['title']) ){
							echo '<div class="kode-option-title">' . esc_attr($option_settings['title']) . '</div>';
						}
						$option_settings['slug'] = $option_slug.'[]';
						$option_settings['name'] = $option_slug.'[]';
						$values_slug = get_post_meta(get_the_ID(),'_team_speaker_data',true);
						$option_settings['value'] = '--blank--';
						$option_settings['title'] = '';
						$option_settings['wrapper-class'] = '';
						
						echo $this->theevent_generate_html($option_settings);	
						echo '</div>';
						echo '</div>';
						
					}
					
					echo '</div>';
					echo '</div>';
					
					echo '<div class="kode-option-wrapper kode-multi-item">';
					echo '<div class="kode-option">';
					// print_r($settings['data-array']);
					$counter = 0;
					if(!empty($team_speaker_data)){
						foreach($team_speaker_data as $team_data){
							$data[] = explode(',',$team_data);
							$count = 0;
							$newdata = $data[$counter];
							echo '<div class="kode-element-wrapper-item">';
							echo '<div class="kode-option-title">'.$settings['title'].'</div>';
							foreach( $settings['data-array'] as $option_slug => $option_settings ){
								$option_settings['slug'] = $option_slug.'[]';
								$option_settings['name'] = $option_slug.'[]';
								$values_slug = get_post_meta(get_the_ID(),'_team_speaker_data',true);															
								$option_settings['value'] = $newdata[$count];
								echo '<div class="kode-option-wrapper-child ';
								echo (isset($option_settings['wrapper-class']))? $option_settings['wrapper-class'] : '';
								echo '">';
								
								$description_class = empty($option_settings['description'])? '': 'with-description';
								echo '<div class="kode-option ' . esc_attr($description_class) . '">';							
								$option_settings['title'] = '';
								$option_settings['wrapper-class'] = '';
								$this->theevent_generate_html($option_settings);	
								echo '</div>';
								echo '</div>';
									
								$count++;
							}
							echo '<div class="up-arrow"></div>';
							echo '<div class="down-arrow"></div>';
							echo '<div class="kode-delete-item"></div>';
							echo '</div>';
							
							$counter++;
						}
					}
					
				//echo '<div id="' . esc_attr($settings['slug']) . '" class="kdf-button kode-team-button">'.esc_attr($settings['button_label']).'</div>';
				
			
			}
			
			function theevent_show_add_fields_button($settings = array()){
				echo '<div id="' . esc_attr($settings['slug']) . '" data-slug="' . esc_attr($settings['button_slug']) . '" class="kdf-button kode-team-button">'.esc_attr($settings['title']).'</div>';
			}
			
			// print custom option
			function show_simple_text($settings = array()){
				echo '<div class="kode-simple-text">';
				 if ( function_exists( 'wp_get_theme' ) ) {
					$theme_data = wp_get_theme();
					$item_uri = $theme_data->get( 'ThemeURI' );
					$theme_name = $theme_data->get( 'Name' );
					$version = $theme_data->get( 'Version' );
				}
				echo '<div class="kode-system-diagnose"><ul>';
					echo'<li><strong>Theme Name:</strong><span>'.$theme_name.'</span></li>';
					echo '<li><strong>Theme Version:</strong><span>'.$version.'</span></li>';
					echo'<li><strong>Site URL:</strong><span>'.home_url().'</span></li>';
					echo '<li><strong>Author URL:</strong><span>'.$item_uri.'</span></li>';

					if ( is_multisite() ) {
						echo '<li><strong>WordPress Version:</strong><span>'. 'WPMU ' . get_bloginfo( 'version' ).'</span></li>';
					} else {
						echo '<li><strong>WordPress Version:</strong><span>'. 'WP ' . get_bloginfo( 'version' ).'</span></li>';
					}
					echo '<li><strong>Web Server Info:</strong><span>'.esc_html( $_SERVER['SERVER_SOFTWARE'] ).'</span></li>';
					if ( function_exists( 'phpversion' ) ) {
						echo '<li><strong>PHP Version:</strong><span>'. esc_html( phpversion() ).'</span></li>';
					}
					if ( function_exists( 'size_format' ) ) {
						echo '<li><strong>WP Memory Limit:</strong>';
						$mem_limit = WP_MEMORY_LIMIT;
						if ( $mem_limit < 67108864 ) {
							echo '<span class="error">' . size_format( $mem_limit ) .' - Recommended memory limit should be at least 64MB. Please refer to : <a target="_blank" href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP">Increasing memory allocated to PHP</a> for more information</span>';
						} else {
							echo '<span>' . size_format( $mem_limit ) . '</span>';
						}
						echo '</li>';
						echo'<li><strong>WP Max Upload Size:</strong><span>'. size_format( wp_max_upload_size() ) .'</span></li>';
					}
					if ( function_exists( 'ini_get' ) ) {
						echo '<li><strong>PHP Time Limit:</strong><span>'. ini_get( 'max_execution_time' ) .'</span></li>';
					}
					if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
						echo '<li><strong>WP Debug Mode:</strong><span>Enabled</span></li>';
					} else {
						echo '<li><strong>WP Debug Mode:</strong><span class="error">Disabled</span></li>';
					}
					echo '</ul></div>';
				echo '</div>';
			}
			
			
			// print custom option
			function show_custom_option($settings = array()){
				global $theevent_allowed_html_tags;
				echo '<div class="kode-option-input">';
				echo wp_kses($settings['option'],$this->theevent_allowed_html_tags);
				echo '</div>';
			}
			
			
			
			// print the input text
			function show_text_input($settings = array()){
				echo '<div class="kode-option-input">';
				echo '<input type="text" class="kdf-text-input" name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
				if( isset($settings['value']) ){
					echo 'value="' . esc_attr($settings['value']) . '" ';
				}else if( !empty($settings['default']) ){
					echo 'value="' . esc_attr($settings['default']) . '" ';
				}
				echo '/>';
				echo '</div>';
			}
			
			//Header Box
			function show_header_box($settings = array()){
				echo '<div class="page-builder-head-wrapper">
					<h4 class="page-builder-head add-content">'.$settings['header_title'].'</h4>
				</div>';
			}
			
			
			// print the date picker
			function show_date_picker($settings = array()){
				echo '<div class="kode-option-input">';
				echo '<input type="text" class="kdf-text-input kode-date-picker" name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
				if( isset($settings['value']) ){
					echo 'value="' . esc_attr($settings['value']) . '" ';
				}else if( !empty($settings['default']) ){
					echo 'value="' . esc_attr($settings['default']) . '" ';
				}
				echo '/>';
				echo '</div>';
			}			
			
			// print the textarea
			function show_textarea($settings = array()){
				echo '<div class="kode-option-input ';
				echo (!empty($settings['class']))? esc_attr($settings['class']): '';
				echo '">';
				
				echo '<textarea name="' . esc_attr($settings['slug']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
				echo (!empty($settings['class']))? 'class="' . esc_attr($settings['class']) . '"': '';
				echo '>';
				if( isset($settings['value']) ){
					echo esc_attr($settings['value']);
				}else if( !empty($settings['default']) ){
					echo esc_attr($settings['default']);
				}
				echo '</textarea>';
				echo '</div>';
			}		

			// print the combobox
			function show_combobox($settings = array()){
				global $post;
				echo '<div class="kode-option-input">';
				
				$value = '';
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}
				if(isset($settings['ajax']) && !empty($settings['ajax'])){
					echo '<div data-settings="'.esc_attr($settings['settings']).'" data-ajax="' . esc_url(THEEVENT_AJAX_URL) . '" data-id="' . esc_attr($post->ID) . '" data-action="'.esc_attr($settings['ajax']).'" class="kode-combobox-wrapper">';	
				}else{
					echo '<div class="kode-combobox-wrapper">';	
				}
				
				echo '<select name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['slug']) . '" >';
				foreach($settings['options'] as $slug => $name ){
					echo '<option value="' . esc_attr($slug) . '" ';
					echo ($value == $slug)? 'selected ': '';
					echo '>' . esc_attr($name) . '</option>';
				
				}
				echo '</select>';
				echo '</div>'; // kode-combobox-wrapper
				
				echo '</div>';
			}
			
			// print the combobox
			function show_combobox_sidebar($settings = array()){
				echo '<div class="kode-option-input">';
				
				$value = '';
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}
				
				echo '<div class="kode-combobox-wrapper">';
				echo '<select name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['name']) . '" >';
				foreach($settings['options'] as $slug => $name ){
					echo '<option value="' . esc_attr($name) . '" ';
					echo ($value == $name)? 'selected ': '';
					echo '>' . esc_attr($name) . '</option>';
				
				}
				echo '</select>';
				echo '</div>'; // kode-combobox-wrapper
				
				echo '</div>';
			}
			
			// print the combobox
			function show_multi_combobox($settings = array()){
				echo '<div class="kode-option-input">';

				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}else{
					$value = array();
				}

				echo '<div class="kode-multi-combobox-wrapper">';
				echo '<select name="' . esc_attr($settings['name']) . '[]" data-slug="' . esc_attr($settings['slug']) . '" multiple >';
				foreach($settings['options'] as $slug => $name ){
					echo '<option value="' . esc_attr($slug) . '" ';
					echo (in_array($slug, $value))? 'selected ': '';
					echo '>' . esc_attr($name) . '</option>';
				
				}
				echo '</select>';
				echo '</div>'; // kode-combobox-wrapper
				
				echo '</div>';
			}			

			
			// print the checkbox ( enable / disable )
			function show_checkbox($settings = array()){
				echo '<div class="kode-option-input">';
				
				$value = 'enable';
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}
				echo '
					<div class="onoffswitch primary inline-block">
						<div class="checkbox-appearance ' . esc_attr($value) . '" > </div>
						<input type="hidden" name="' . esc_attr($settings['name']) . '" value="disable" />
						<input type="checkbox" name="' . esc_attr($settings['name']) . '" class="onoffswitch-checkbox" id="' . esc_attr($settings['slug']) . '-id" data-slug="' . esc_attr($settings['slug']) . '" ';
						echo ($value == 'enable')? 'checked': '';
						echo ' value="enable">
						<label class="onoffswitch-label" for="' . esc_attr($settings['slug']) . '-id">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>';
			}		

			// print the radio image
			function show_radio_image($settings = array()){
				echo '<div class="kode-option-input">';
				
				$value = '';
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}
				
				$i = 0;
				foreach($settings['options'] as $slug => $name ){
					echo '<label for="' . esc_attr($settings['slug']) . '-id' . $i . '" class="radio-image-wrapper ';
					echo ($value == $slug)? 'active ': '';
					echo '">';
					echo '<img src="' . esc_url($name) . '" alt="" />';
					echo '<div class="selected-radio"></div>';

					echo '<input type="radio" name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
					echo 'id="' . esc_attr($settings['slug']) . '-id' . $i . '" value="' . esc_attr($slug) . '" ';
					echo ($value == $slug)? 'checked ': '';
					echo ' />';
					
					echo '</label>';
					
					$i++;
				}
				
				echo '<div class="clear"></div>';
				echo '</div>';
			}
			
			
			// print the radio image
			function show_radio_header_image($settings = array()){
				echo '<div class="kode-option-input">';
				
				$value = '';
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}
				
				$i = 0;
				foreach($settings['options'] as $slug => $name ){
					echo '<label for="' . esc_attr($settings['slug']) . '-id' . $i . '" class="radio-image-wrapper radio-header-wrapper ';
					echo ($value == $slug)? 'active ': '';
					echo '">';
					echo '<img src="' . esc_url($name) . '" alt="" />';
					echo '<div class="selected-radio"></div>';

					echo '<input type="radio" name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
					echo 'id="' . esc_attr($settings['slug']) . '-id' . $i . '" value="' . esc_attr($slug) . '" ';
					echo ($value == $slug)? 'checked ': '';
					echo ' />';
					
					echo '</label>';
					
					$i++;
				}
				
				echo '<div class="clear"></div>';
				echo '</div>';
			}
			
			

			// print color picker
			function show_color_picker($settings = array()){
				echo '<div class="kode-option-input">';
				
				echo '<input type="text" class="wp-color-picker" name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
				if( !empty($settings['value']) ){
					echo 'value="' . esc_attr($settings['value']) . '" ';
				}else if( !empty($settings['default']) ){
					echo 'value="' . esc_attr($settings['default']) . '" ';
				}
				
				if( !empty($settings['default']) ){
					echo 'data-default-color="' . esc_attr($settings['default']) . '" ';
				}
				echo '/>';
				
				echo '</div>';
			}	
			
			
			// print slider bar
			function show_slider_bar($settings = array()){
				echo '<div class="kode-option-input">';
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}
				
				// create a blank box for javascript
				echo '<div class="kode-sliderbar" data-value="' . esc_attr($value) . '" ></div>';
				
				echo '<input type="text" class="kode-sliderbar-text-hidden" name="' . esc_attr($settings['name']) . '" ';
				echo 'data-slug="' . esc_attr($settings['slug']) . '" value="' . esc_attr($value) . '" />';
				
				// this will be the box that shows the value
				echo '<div class="kode-sliderbar-text">' . esc_attr($value) . 'px</div>';
				
				echo '<div class="clear"></div>';
				echo '</div>';			
			}

			// print slider
			function show_slider($settings = array()){
				echo '<div class="kode-option-input ';
				echo (!empty($settings['class']))? esc_attr($settings['class']): '';
				echo '">';
				
				echo '<textarea name="' . esc_attr($settings['slug']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
				echo 'class="kode-input-hidden kode-slider-selection" data-overlay="true" data-caption="true" >';
				if( isset($settings['value']) ){
					echo esc_attr($settings['value']);
				}else if( !empty($settings['default']) ){
					echo esc_attr($settings['default']);
				}
				echo '</textarea>';
				echo '</div>';
			}	

			// print Gallery
			function show_gallery($settings = array()){
				echo '<div class="kode-option-input ';
				echo (!empty($settings['class']))? esc_attr($settings['class']): '';
				echo '">';
				
				echo '<textarea name="' . esc_attr($settings['slug']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
				echo 'class="kode-input-hidden kode-gallery-selection" data-overlay="true" data-caption="true" >';
				if( isset($settings['value']) ){
					echo esc_attr($settings['value']);
				}else if( !empty($settings['default']) ){
					echo esc_attr($settings['default']);
				}
				echo '</textarea>';
				echo '</div>';
			}				
			
			// print upload box
			function show_upload_box($settings = array()){
				echo '<div class="kode-option-input">';
				
				$value = ''; $file_url = '';
				$settings['data-type'] = empty($settings['data-type'])? 'image': $settings['data-type'];
				$settings['data-type'] = ($settings['data-type']=='upload')? 'image': $settings['data-type'];
				
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}
				
				if( is_numeric($value) ){ 
					$file_url = wp_get_attachment_url($value);
				}else{
					$file_url = $value;
				}
				
				// example image url
				echo '<img class="kode-upload-img-sample ';
				echo (empty($file_url) || $settings['data-type'] != 'image')? 'blank': '';
				echo '" ';
				echo (!empty($file_url) && $settings['data-type'] == 'image')? 'src="' . esc_url($file_url) . '" ': ''; 
				echo '/>';
				echo '<div class="clear"></div>';
				
				// input link url
				echo '<input type="text" class="kode-upload-box-input" value="' . esc_url($file_url) . '" />';					
				
				// hidden input
				echo '<input type="hidden" class="kode-upload-box-hidden" ';
				echo 'name="' . esc_attr($settings['name']) . '" data-slug="' . esc_attr($settings['slug']) . '" ';
				echo 'value="' . esc_attr($value) . '" />';
				
				// upload button
				echo '<input type="button" class="kode-upload-box-button kdf-button" ';
				echo 'data-title="' . esc_attr($settings['title']) . '" ';
				echo 'data-type="' . esc_attr($settings['data-type']) . '" ';				
				echo 'data-button="';
				echo (empty($settings['button']))? esc_html__('Insert Image', 'the-event'):$settings['button'];
				echo '" ';
				echo 'value="' . esc_html__('Upload', 'the-event') . '"/>';
				
				echo '<div class="clear"></div>';
				echo '</div>';
			}			

			// print the font combobox
			function print_font_combobox($settings = array()){
				echo '<div class="kode-option-input">';
				
				$value = '';
				if( !empty($settings['value']) ){
					$value = $settings['value'];
				}else if( !empty($settings['default']) ){
					$value = $settings['default'];
				}
				
				echo '<input class="kode-sample-font" ';
				echo 'value="' . esc_attr( esc_html__('Sample Font', 'the-event') ) . '" ';
				echo (!empty($value))? 'style="font-family: ' . $value . ';" />' : '/>';
				
				echo '<div class="kode-combobox-wrapper">';
				echo '<select name="' . $settings['name'] . '" data-slug="' . $settings['slug'] . '" class="kode-font-combobox" >';
				do_action('theevent_print_all_font_list', $value);
				echo '</select>';
				echo '</div>'; // kode-combobox-wrapper
				
				echo '</div>';
			}	
			
			
		}

	}
		
?>