<?php
	/*	
	*	kf_event Framework File
	*	---------------------------------------------------------------------
	*	This file contains the admin option setting 
	*	---------------------------------------------------------------------
	*	http://stackoverflow.com/questions/2270989/what-does-apply-filters-actually-do-in-wordpress
	*/
	
	// create the main admin option
	if( is_admin() ){
		add_action('after_setup_theme', 'theevent_create_themeoption');
	}
	
	if( !function_exists('theevent_create_themeoption') ){
	
		function theevent_create_themeoption(){
		
			global $theevent_theme_option;
			if(!isset($theevent_theme_option['sidebar-element'])){$theevent_theme_option['sidebar-element'] = array('blog','contact');}
			// Theme Options Default data
			$default_data_array = array(
				'page_title' => esc_html__('Theme Options', 'the-event'),
				'menu_title' => 'Theme Options',
				'menu_slug' => 'the-event',
				'save_option' => 'theevent_admin_option',
				'role' => 'edit_theme_options'
			);
			
			
			new theevent_themeoption_panel(
				
				// Theme Options Default data
				$default_data_array,
				
				// Theme Options setting
				apply_filters('theevent_themeoption_panel',
					array(
						// general menu
						'general' => array(
							'title' => esc_html__('General Settings', 'the-event'),
							'icon' => 'fa fa-diamond',
							'options' => array(
								'header-logo' => array(
									'title' => esc_html__('Logo Settings', 'the-event'),
									'options' => array(
										'logo' => array(
											'title' => esc_html__('Upload Logo', 'the-event'),
											'button' => esc_html__('Set As Logo', 'the-event'),
											'type' => 'upload',
											'description' => 'Please upload your logo supported for example jpeg, gif, png',
										),
										'logo-width' => array(
											'title' => esc_html__('Logo Width', 'the-event'),
											'type' => 'text',
											'description' => 'Please enter the width of the logo in Numbers E.g 20',
											'default' => '0',
											//'selector' => '.kode-logo{ margin-top: #kode#; }',
											'data-type' => 'pixel'
											
										),
										'logo-height' => array(
											'title' => esc_html__('Logo Height', 'the-event'),
											'type' => 'text',
											'description' => 'Please enter the height of the logo in Numbers E.g 20',
											'default' => '0',
											//'selector' => '.kode-logo{ margin-bottom: #kode#; }',
											'data-type' => 'pixel'
										),											
										'logo-top-margin' => array(
											'title' => esc_html__('Logo Top Margin', 'the-event'),
											'type' => 'text',
											'description' => 'Please enter the Top Margin of the logo in Numbers E.g 20',
											'default' => '0',
											'selector' => '.kode-logo{ margin-top: #kode#; }',
											'data-type' => 'pixel'
										),
										'logo-bottom-margin' => array(
											'title' => esc_html__('Logo Bottom Margin', 'the-event'),
											'type' => 'text',
											'description' => 'Please enter the Bottom Margin of the logo in Numbers E.g 20',
											'default' => '0',
											'selector' => '.kode-logo{ margin-bottom: #kode#; }',
											'data-type' => 'pixel'
										),	
										'favicon-id' => array(
											'title' => esc_html__('Upload Favicon ( .ico file )', 'the-event'),
											'button' => esc_html__('Select Icon', 'the-event'),
											'type' => 'upload',
											'description' => 'You can upload the favicon icon from here.',
										),	
										
									)
								),
								'header-section' => array(
									'title' => esc_html__('Header Settings', 'the-event'),
									'options' => array(
										// 'enable-header-option' => array(
											// 'title' => __('Enable Header All Pages', 'the-event'),
											// 'type' => 'checkbox',	
											// 'description'=>__('Click here to Turn On Header Style for All pages.', 'the-event'),
											// 'default' => 'enable'
										// ),
										'kode-header-style' => array(
											'title' => __('Select Header', 'the-event'),
											'type' => 'radioheader',	
											'description'=>__('There are 4 Different Header Styles Available here. Click on the Drop Down menu and select the Header Style here from of your choice.', 'the-event'),
											'options' => array(
												'header-style-1'=>THEEVENT_PATH . '/framework/include/backend_assets/images/headers/2.jpg',
												'header-style-2'=>THEEVENT_PATH . '/framework/include/backend_assets/images/headers/1.jpg',
											),
										),
										'enable-top-bar' => array(
											'title' => esc_html__('Enable Top Bar', 'the-event'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Top Bar from here.',
											'wrapper-class'=> ''											
										),
										
										'enable-social-bar' => array(
											'title' => esc_html__('Enable Social Bar', 'the-event'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Top social icons from here Bar from here.',
											'wrapper-class'=> ''											
										),
										
										'kode-top-bar-trans' => array(
											'title' => __('Header Style', 'the-event'),
											'type' => 'combobox',	
											'wrapper-class'=> 'kode-header-style-wrapper header-style-1-wrapper',
											'options' => array(
												'transparent' => __('Transparent', 'the-event'),
												'colored' => __('Colored', 'the-event'),												
											)
										),
										// 'top-bar-background-color' => array(
											// 'title' => esc_html__('Top Bar Background Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker allows you to change the top bar background color.',
											// 'wrapper-class' => 'colored-wrapper kode-top-bar-trans-wrapper',
											// 'default' => '#ffffff',
										// ),
										'top-bar-left-text' => array(
											'title' => esc_html__('Top Bar Left Text', 'the-event'),
											'type' => 'textarea',	
											'wrapper-class'=> 'kode-header-style-wrapper header-style-2-wrapper',
											'description' => 'Please enter the Top Bar left text here.',
										),	
										// 'enable-search-bar' => array(
											// 'title' => esc_html__('Enable Search Bar', 'the-event'),
											// 'type' => 'checkbox',
											// 'description' => 'You can Enable / Disable the Top Search Bar from here.',
											// 'default' =>'disable'
										// ),
										'enable-meet-btn' => array(
											'title' => __('Enable Member Button', 'the-event'),
											'type' => 'checkbox',		
											'description' => 'You can Switch On/Off the Member Button From here.',	
										),
										'meet-btn-text' => array(
											'title' => __('Member Button Text', 'the-event'),
											'type' => 'text',				
											'default'=>'Become A Member',
											'description'=>__('Please add the Member box text here.', 'the-event'),
										),	
										
										'meet-page' => array(
											'title' => esc_attr__('Meeting BTN Page' , 'the-event'),
											'type' => 'combobox',
											'options' => theevent_get_post_list_id('page'),
											'description'=> esc_attr__('Select Meeting Page from the Drop Down Menu.', 'the-event')
										),
										'meet-btn-background-color' => array(
											'title' => __('Meeting BTN Background Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=>__('Click here to select the Meeting Background Color from the color picker for this header style.', 'the-event'),
											'default' => '#ffffff',
										),
										'meet-btn-hover-background-color' => array(
											'title' => __('Meeting BTN Hover Background Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=>__('Click here to select the Meeting Background Color from the color picker for this header style.', 'the-event'),
											'default' => '#ffffff',
										),
										'meet-btn-color' => array(
											'title' => __('Meeting BTN Text Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=>__('Click here to select the Meeting Background Color from the color picker for this header style.', 'the-event'),
											'default' => '#ffffff',
										),
										'meet-btn-hover-txt-color' => array(
											'title' => __('Meeting BTN Hover Text Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=>__('Click here to select the Meeting Background Color from the color picker for this header style.', 'the-event'),
											'default' => '#ffffff',
										),
										'enable-sticky-menu' => array(
											'title' => esc_html__('Enable Sticky', 'the-event'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Sticky Header Menu.',
											'default' => 'disable'
										),
										'enable-one-page-header-navi' => array(
											'title' => esc_html__('Enable One Page Header Nav', 'the-event'),
											'type' => 'checkbox',
											'description' => 'You can Enable / Disable the One Page Navigation Site option from here. If you want to use Multi Pages site then you need to disable the One Page Navigation option from here.',
											'default' =>'disable'
										),	
										'enable-breadcrumbs' => array(
											'title' => esc_html__('Breadcrumbs', 'the-event'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Breadcrumbs from here.',
											'default' => 'disable'
										)
									)
								),
								'navigation-settings' => array(
									'title' => esc_html__('Navigation Styling', 'the-event'),
									'options' => array(
										'navi-color' => array(
											'title' => esc_html__('Navigation Text Color', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the navigation text color.',
											'default' => '#ffffff',
											//'selector'=> '.navigation ul > li > a, .navbar-nav > li > a{color: #kode#;}'
										),
										'navi-hover-bg' => array(
											'title' => esc_html__('Navigation Text Hover BG', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the navigation Hover BG color.',
											'default' => '#ffffff',
											//'selector'=> '.navigation ul > li::before, .navbar-nav > li::before{background: #kode#;}'
										),
										'navi-hover-color' => array(
											'title' => esc_html__('Navigation Text Hover Color', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the navigation text Hover color.',
											'default' => '#ffffff',
											//'selector'=> '.navigation ul > li:hover a, .navbar-nav > li:hover{color: #kode#;}'
										),	
										'navi-dropdown-bg' => array(
											'title' => esc_html__('Navigation DropDown BG', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the navigation dropdown BG color.',
											'default' => '#ffffff',
											//'selector'=> '.navigation .sub-menu, .navigation .children, .navbar-nav .children{background: #kode#;}'
										),	
										'navi-dropdown-color' => array(
											'title' => esc_html__('Navigation DropDown Text Color', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the navigation Dropdown text color.',
											'default' => '#ffffff',
											//'selector'=> '.navigation .sub-menu, .navigation .children, .navbar-nav .children{color: #kode#;}'
										),
										'navi-dropdown-hover' => array(
											'title' => esc_html__('Navigation DropDown Text Hover', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the navigation dropdown Hover color.',
											'default' => '#ffffff',
											//'selector'=> '.navigation ul li ul li::before, .navbar-nav li ul li::before{background: #kode#;}'
										),										
										'navi-dropdown-hover-bg' => array(
											'title' => esc_html__('Navigation DropDown on Hover Background', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the navigation dropdown Hover color.',
											'default' => '#ffffff',
											//'selector'=> '.navigation ul li ul li::before, .navbar-nav li ul li::before{background: #kode#;}'
										),	
										
									)
								),	
									
								'layout-style' => array(
									'title' => esc_html__('Style & Layouts', 'the-event'),
									'options' => array(
										'color-scheme-one' => array(
											'title' => esc_html__('Color Scheme', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the Overall Color Scheme of your site.',
											'default' => '#ffffff',
											'selector'=> ''
										),
										'enable-boxed-style' => array(
											'title' => esc_html__('Website Layout', 'the-event'),
											'type' => 'combobox',
											'description' => 'You can change your site layout to Full Width / Boxed Style',											
											'options' => array(
												'full-style' => esc_html__('Full Style', 'the-event'),
												'boxed-style' => esc_html__('Boxed Style', 'the-event')
											)
										),
										'kode-body-style' => array(
											'title' => esc_html__('Body Background Style', 'the-event'),
											'type' => 'combobox',	
											'description' => 'You have selected the Boxed Layout. Now you can set your Boxed layout style from here.',
											'options' => array(
												'body-color' => esc_html__('Body Background Color', 'the-event'),
												'body-background' => esc_html__('Body Background Image', 'the-event'),
												'body-pattern' => esc_html__('Body Background Pattern', 'the-event'),
											),
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper'
										),	
										'body-bg-color' => array(
											'title' => esc_html__('Body Background Color', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picket allows you to change Body Background Color for the Boxed Layout.',
											'default' => '#ffffff',
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper body-color-wrapper kode-body-style-wrapper',
											'selector'=> ''
										),
										'body-background-image' => array(
											'title' => esc_html__('Background Image', 'the-event'),
											'type' => 'upload',
											'description' => 'You can upload the background image for your Boxed Layout.',
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper body-background-wrapper kode-body-style-wrapper'
										),	
										'body-background-pattern' => array(
											'title' => esc_html__('Background Pattern', 'the-event'),
											'type' => 'radioimage',
											'description' => 'There are 18 Built in Background patterns available in the theme. Just Select the pattern and save the Changes.',
											'options' => array(
												'1'=>THEEVENT_PATH . '/images/pattern/pattern_1.png',
												'2'=>THEEVENT_PATH . '/images/pattern/pattern_2.png', 
												'3'=>THEEVENT_PATH . '/images/pattern/pattern_3.png',
												'4'=>THEEVENT_PATH . '/images/pattern/pattern_4.png',
												'5'=>THEEVENT_PATH . '/images/pattern/pattern_5.png',
												'6'=>THEEVENT_PATH . '/images/pattern/pattern_6.png',
												'7'=>THEEVENT_PATH . '/images/pattern/pattern_7.png',
												'8'=>THEEVENT_PATH . '/images/pattern/pattern_8.png',
												'9'=>THEEVENT_PATH . '/images/pattern/pattern_9.png',
												'10'=>THEEVENT_PATH . '/images/pattern/pattern_10.png', 
												'11'=>THEEVENT_PATH . '/images/pattern/pattern_11.png',
												'12'=>THEEVENT_PATH . '/images/pattern/pattern_12.png',
												'13'=>THEEVENT_PATH . '/images/pattern/pattern_13.png',
												'14'=>THEEVENT_PATH . '/images/pattern/pattern_14.png',
												'15'=>THEEVENT_PATH . '/images/pattern/pattern_15.png',
												'16'=>THEEVENT_PATH . '/images/pattern/pattern_16.png',
												'17'=>THEEVENT_PATH . '/images/pattern/pattern_17.png',
												'18'=>THEEVENT_PATH . '/images/pattern/pattern_18.png'
											),
											'wrapper-class' => 'boxed-style-wrapper enable-boxed-style-wrapper pattern-size-wrap body-pattern-wrapper kode-body-style-wrapper',
											'default' => '1'
										),	
										'kode-body-position' => array(
											'title' => esc_html__('Body Background Position', 'the-event'),
											'type' => 'combobox',	
											'description' => 'Select the Body Background Position you want to have for your site.',
											'options' => array(
												'body-scroll' => esc_html__('Scroll', 'the-event'),
												'body-fixed' => esc_html__('Fixed', 'the-event')												
											),
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper body-background-wrapper kode-body-style-wrapper'
										),	
										'enable-nice-scroll' => array(
											'title' => esc_html__('Nice Scroll', 'the-event'),
											'type' => 'checkbox',
											'description' => 'You can Enable / Disable the Nice Scroll from here.',											
											'default' => 'enable'
										),
										'nice-scroll-color' => array(
											'title' => esc_html__('Nice Scroll Color', 'the-event'),
											'type' => 'colorpicker',
											'description' => esc_html('Nice Scroll Color','the-event'),
											'default' => '#ffffff',
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
											'selector'=> ''
										),
										'nice-scroll-radius' => array(
											'title' => esc_html__('Scroll Radius', 'the-event'),
											'type' => 'text',				
											'default'=>'5px',
											'description' => esc_html('nice scroll radius','the-event'),
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
										),	
										'nice-scroll-width' => array(
											'title' => esc_html__('Scroll Size', 'the-event'),
											'type' => 'text',				
											'default'=>'12px',
											'description' => esc_html('nice scroll width','the-event'),
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
										),	
										'nice-scroll-touch' => array(
											'title' => esc_html__('Nice Scroll Touch Behavior', 'the-event'),
											'type' => 'combobox',	
											'description' => esc_html('nice scroll width','the-event'),
											'options' => array(
												'true' => esc_html__('True', 'the-event'),
												'false' => esc_html__('False', 'the-event')
											),
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
										),	
										'enable-rtl-layout' => array(
											'title' => esc_html__('Enable RTL', 'the-event'),
											'type' => 'checkbox',
											'description' => 'You can Enable / Disable the RTL Layout of your site.',											
											'default' => 'disable'
										),
										'enable-responsive-mode' => array(
											'title' => esc_html__('Enable Responsive', 'the-event'),
											'description' => 'You can Enable / Disable the Responsive Layout of your site.',
											'type' => 'checkbox',	
											'default' => 'enable'
										),	
										'enable-sticky-sidebar' => array(
											'title' => esc_html__('Enable Sticky Sidebar', 'the-event'),
											'description' => 'You can Enable / Disable the Sticky Sidebar of your site.',
											'type' => 'checkbox',	
											'default' => 'enable'
										),											
										'video-ratio' => array(
											'title' => esc_html__('Default Video Ratio', 'the-event'),
											'type' => 'text',				
											'default'=>'16/9',
											'description'=>esc_html__('Please only fill number/number as default video ratio', 'the-event')
										),										
									)
								),	
								'footer-style' => array(
									'title' => esc_html__('Footer Settings', 'the-event'),
									'options' => array(
										'show-footer' => array(
											'title' => esc_html__('Show Footer', 'the-event'),
											'type' => 'checkbox',
											'description' => 'You can Switch On / Off the Footer From here.',
											'default' => 'enable'
										),
										
										// 'footer-logo' => array(
											// 'title' => esc_html__('Footer Logo', 'the-event'),
											// 'button' => esc_html__('Upload', 'the-event'),
											// 'type' => 'upload',
											// 'description' => 'You can Upload the Footer Logo from here.',
										// ),
										// 'footer-text-description'=> array(
											// 'title'=> esc_html__('Footer Description' ,'the-event'),
											// 'type'=> 'text',											
											// 'description'=> esc_html__('Footer Description which comes right after footer logo.', 'the-event')
										// ),
										'kode-footer-style' => array(
											'title' => esc_attr__('Select Footer', 'the-event'),
											'type' => 'radioheader',	
											'description'=>esc_attr__('There are 3 Different Footer Styles Available here. Click on the Drop Down menu and select the Footer Style here from of your choice.', 'the-event'),
											'options' => array(
												'footer-style-1'=>THEEVENT_PATH . '/framework/include/backend_assets/images/headers/footer-1.jpg',
												'footer-style-2'=>THEEVENT_PATH . '/framework/include/backend_assets/images/headers/footer-2.jpg',
												'footer-style-3'=>THEEVENT_PATH . '/framework/include/backend_assets/images/headers/footer-3.jpg',
											),
										),
										'footer-background-color' => array(
											'title' => esc_html__('Footer Background Color', 'the-event'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker Allows you to change the Footer Background Color.',
											'default' => '#000',											
										),
										'footer-background-opacity'=> array(
											'title'=> esc_html__('Footer Background Opacity' ,'the-event'),
											'type'=> 'text',
											'description' => 'You can Control the footer background image opacity from here.',
											'default' => '0.6',
											'description'=> esc_html__('Adjust footer background opacity from 0 to 1.', 'the-event')
										),
										'footer-background-image' => array(
											'title' => esc_html__('Footer Background Image', 'the-event'),
											'button' => esc_html__('Upload', 'the-event'),
											'type' => 'upload',
											'description' => 'You can Upload the footer Background Image from here.',
										),
										'footer-layout' => array(
											'title' => esc_html__('Footer Layout', 'the-event'),
											'type' => 'radioimage',
											'description' => 'There are 4 Footer Styles available in the theme. You can select any of the style you wish to have.',
											'options' => array(
												'1'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/footer-2col.jpg',
												'2'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/footer-3col.jpg',
												'3'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/footer-4col.jpg',
												'4'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/footer-2col-onebroad.jpg'
											),
											'default' => '2'
										),
										'footer-text-map'=> array(
											'title'=> esc_html__('Footer Map Location' ,'the-event'),
											'type'=> 'text',
											'wrapper-class'=> '4-wrapper footer-layout-wrapper',
											'description'=> esc_html__('Add Location of your office which will come right beside widgets.', 'the-event')
										),
										// 'show-widget-heading-border' => array(
											// 'title' => esc_html__('Show Footer Heading Border', 'the-event'),
											// 'type' => 'checkbox',
											// 'default' => 'enable'
										// ),
										// 'show-newsletter' => array(
											// 'title' => esc_html__('Show Newsletter', 'the-event'),
											// 'type' => 'checkbox',
											// 'description' => 'You can Switch On / Off the newsletter From here.',
											// 'default' => 'enable'
										// ),
										// 'footer-newsletter-title'=> array(
											// 'title'=> esc_html__('Footer Newsletter Title Text' ,'the-event'),
											// 'type'=> 'text',											
											// 'default' => 'Newsletter',
											// 'description'=> esc_html__('Adjust footer newsletter title text from here', 'the-event')
										// ),
										// 'footer-newsletter-caption'=> array(
											// 'title'=> esc_html__('Footer Background Opacity' ,'the-event'),
											// 'type'=> 'text',											
											// 'default' => 'Subcribe Our',
											// 'description'=> esc_html__('Adjust footer newsletter caption text from here', 'the-event')
										// ),
										'show-copyright' => array(
											'title' => esc_html__('Show Copyright', 'the-event'),
											'type' => 'checkbox',
											'description' => 'Use this option to Enable or Disable the Copyright Text for your site.',
											'default' => 'enable'
										),
										'kode-copyright-text' => array(
											'title' => esc_html__('Copyright Text', 'the-event'),
											'type' => 'textarea',
											'description' => 'Use this field to enter your Footer Copyright text here. Note : This field will not render the HTML tags due to the XSS Security Implemented in the theme.',	
											'class' => 'full-width',
										),	
										'kode-back-top' => array(
											'title' => esc_html__('Back To Top', 'the-event'),
											'type' => 'checkbox',
											'description' => 'Use this option to Enable or Disable the Back to Top button.',
											'default' => 'enable'
										),
									)
								),									
								'page-style' => array(
									'title' => esc_html__('Sub Header', 'the-event'),
									'options' => array(
										'default-page-title' => array(
											'title' => esc_html__('Default Page Title Background', 'the-event'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default Page Title Background from here.',
											'selector' => '.kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),	
										'default-post-title-background' => array(
											'title' => esc_html__('Default Post Title Background', 'the-event'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default Post Title Background from here.',
											'selector' => 'body.single .kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),
										'default-search-archive-title-background' => array(
											'title' => esc_html__('Default Search Archive Title Background', 'the-event'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default Search / Archive Page Title Background from here.',
											'selector' => 'body.archive .kode-subheader, body.search .kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),
										'default-404-title-background' => array(
											'title' => esc_html__('Default 404 Title Background', 'the-event'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default 404 Page Title Background from here.',
											'selector' => 'body.error404 .kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),										
									)
								),
								
								
								
								
								//'portfolio-style' => array(),		

									
								'server-configuration' => array(
									'title' => esc_html__('System Diagnostic Information', 'the-event'),
									'options' => array(
										'server-config' => array(
											'title' => esc_html__('Server Configuration', 'the-event'),
											'type' => 'server-config',
											'wrapper-class' => 'server-config',
											'description' => 'Above information is useful to diagnose some of the possible reasons to malfunctions, performance issues or any errors. You can faciliate the process of support by providing below information to our support staff.',
										),
									)
								),
								
								'ticket-form-style' => array(
									'title' => __('Ticket - Paypal Form', 'the-event'),
									'options' => array(
										'ticket-money-format' => array(
											'title' => __('Ticket Money Format', 'the-event'),
											'type' => 'text',
											'default' => 'NUMBER USD',
											'description' => __('The word NUMBER will be replaced with actual number you fill within each price', 'the-event')
										),
										'paypal-action' => array(
											'title' => __('Paypal Request URL', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'paypal' => __('Paypal Site', 'the-event') . ' (https://www.paypal.com/cgi-bin/webscr)',
												'sandbox' => __('Sandbox Test Site', 'the-event') .  ' (https://www.sandbox.paypal.com/cgi-bin/webscr)'
											)						
										),
										'paypal-recipient-name' => array(
											'title' => __('Recipient Name', 'the-event'),
											'type' => 'text',
										),
										'paypal-recipient-email' => array(
											'title' => __('Recipient Email', 'the-event'),
											'type' => 'text',					
										),
										'enable-paypal' => array(
											'title' => __('Allow Payment via Paypal', 'the-event'),
											'type' => 'checkbox',					
										),
										'paypal-currency-code' => array(
											'title' => __('Paypal Currency Code', 'the-event'),
											'type' => 'text',
											'default' => 'USD'
										),
									)
								),
								
								'import-export-option' => array(
									'title' => esc_html__('Import/Export Option', 'the-event'),
									'options' => array(
										'export-option' => array(
											'title' => esc_html__('Export Option', 'the-event'),
											'type' => 'custom',
											'description'=> esc_html__('Here you can copy/download your themes current option settings. Keep this safe as you can use it as a backup should anything go wrong. Or you can use it to restore your settings on this site (or any other site). You also have the handy option to copy the link to yours sites settings. Which you can then use to duplicate on another site.', 'the-event'),
											'option' => 
												'<input type="button" id="kode-export" class="kdf-button" value="' . esc_html__('Export', 'the-event') . '" />' .
												'<textarea class="full-width"></textarea>'
										),
										'import-option' => array(
											'title' => esc_html__('Import Option', 'the-event'),
											'type' => 'custom',
											'description'=> esc_html__('WARNING! This will overwrite any existing options, please proceed with caution!', 'the-event'),
											'option' => 
												'<input type="button" id="kode-import" class="kdf-button" value="' . esc_html__('Import', 'the-event') . '" />' .
												'<textarea class="full-width"></textarea>'
										),										
									)
								),
							)
						),
						
						'blog-style' => array(
							'title' => esc_html__('Blog Settings', 'the-event'),
							'icon' => 'fa fa-cubes',
							'options' => array(
								
								'search-archive-style' => array(
									'title' => esc_html__('Index Search - Archive Style', 'the-event'),
									'options' => array(
										'archive-sidebar-template' => array(
											'title' => esc_html__('Index Search - Archive Sidebar Template', 'the-event'),
											'type' => 'radioimage',
											'description' => 'You can Select the Search / Archive page sidebar position from here.',
											'options' => array(
												'no-sidebar'=>		THEEVENT_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
												'both-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
												'right-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
												'left-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
											),
											'default' => 'no-sidebar'							
										),
										'archive-sidebar-left' => array(
											'title' => esc_html__('Search - Archive Sidebar Left', 'the-event'),
											'type' => 'combobox_sidebar',
											'options' => $theevent_theme_option['sidebar-element'],		
											'wrapper-class'=>'left-sidebar-wrapper both-sidebar-wrapper archive-sidebar-template-wrapper',											
										),
										'archive-sidebar-right' => array(
											'title' => esc_html__('Search - Archive Sidebar Right', 'the-event'),
											'type' => 'combobox_sidebar',
											'options' => $theevent_theme_option['sidebar-element'],
											'wrapper-class'=>'right-sidebar-wrapper both-sidebar-wrapper archive-sidebar-template-wrapper',
										),		
										'archive-blog-style' => array(
											'title' => esc_html__('Search - Archive Blog Style', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'blog-grid' => esc_html__('Blog Grid', 'the-event'),
												'blog-full' => esc_html__('Blog Full', 'the-event'),
											),
											'default' => 'blog-full'							
										),	
										'archive-col-size' => array(
											'title' => esc_html__('Search - Archive Blog Style', 'the-event'),
											'type' => 'combobox',
											'options' => array(
												'2' => esc_html__('2 Column', 'the-event'),
												'3' => esc_html__('3 Column', 'the-event'),
												'4' => esc_html__('4 Column', 'the-event'),
											),
											'default' => 'blog-full'							
										),
										'archive-col-size'=> array(
											'title'=> esc_html__('Column size' ,'the-event'),
											'type'=> 'text',	
											'default'=> '3',
											'wrapper-class'=>'blog-grid-wrapper archive-blog-style-wrapper',
											'description'=> esc_html__('Select the column width of content.', 'the-event')
										),										
										'archive-num-excerpt'=> array(
											'title'=> esc_html__('Search - Archive Num Excerpt (Word)' ,'the-event'),
											'type'=> 'text',	
											'default'=> '25',
											'wrapper-class'=>'blog-full-wrapper archive-blog-style-wrapper',
											'description'=> esc_html__('This is a number of word (decided by spaces) that you want to show on the post excerpt. Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag.', 'the-event')
										),
									)
								),	
								'blog-single' => array(
									'title' => esc_html__('Blog Single', 'the-event'),
									'options' => array(
										// 'post-title' => array(
											// 'title' => 'Sub Header Post Title',
											// 'type' => 'text',	
											// 'description' => 'Sub Header Post Title'
										// ),
										// 'post-caption' => array(
											// 'title' => 'Sub Header Post Caption',
											// 'type' => 'textarea',
											// 'description' => 'Add Sub Header Post Caption'
										// ),
										'theevent-post-thumbnail-size' => array(
											'title' => esc_html__('Single Post Thumbnail Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> 'theevent-post-thumbnail-size'
										),
										'post-sidebar-template' => array(
											'title' => esc_html__('Single Default Sidebar', 'the-event'),
											'type' => 'radioimage',
											'options' => array(
												'no-sidebar'=>		THEEVENT_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
												'both-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
												'right-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
												'left-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
											),
										),
										'post-sidebar-left' => array(
											'title' => esc_html__('Single Default Sidebar Left', 'the-event'),
											'type' => 'combobox_sidebar',
											'wrapper-class' => 'left-sidebar-wrapper both-sidebar-wrapper post-sidebar-template-wrapper',
											'options' => $theevent_theme_option['sidebar-element'],		
										),
										'post-sidebar-right' => array(
											'title' => esc_html__('Single Default Sidebar Right', 'the-event'),
											'type' => 'combobox_sidebar',
											'wrapper-class' => 'right-sidebar-wrapper both-sidebar-wrapper post-sidebar-template-wrapper',
											'options' => $theevent_theme_option['sidebar-element'],
										),	
										'single-post-feature-image' => array(
											'title' => esc_html__('Single Post Feature Image', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('If you do not want to set a featured image (in case of sound post type : Audio player, in case of video post type : Video Player) kindly disable it here.', 'the-event'),
											'default' => 'enable'
										),
										'single-post-date' => array(
											'title' => esc_html__('Single Post Date', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Date.', 'the-event'),											
											'default' => 'enable'
										),
										'single-post-author' => array(
											'title' => esc_html__('Single Post Author', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('You can enable or disable the about author box from here.', 'the-event'),
											'default' => 'enable'											
										),	
										'single-post-comments' => array(
											'title' => esc_html__('Single Post Comments', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Comments.', 'the-event'),		
											'default' => 'enable'
										),
										'single-post-tags' => array(
											'title' => esc_html__('Single Tags', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Tags.', 'the-event'),
											'default' => 'enable'
										),
										'single-post-category' => array(
											'title' => esc_html__('Single Category', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Category.', 'the-event'),
											'default' => 'enable'
										),
										'single-next-pre' => array(
											'title' => esc_html__('Single Next / Previous Button', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can turn on/off the navigation arrows when viewing the blog single page.', 'the-event'),
											'default' => 'enable'
										),
										'single-recommended-post' => array(
											'title' => esc_html__('Single Recommended Post Button', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can turn on/off the Recommended Posts on the blog single page.', 'the-event'),
											'default' => 'enable'
										),
										'kode-recommended-thumbnail-size' => array(
											'title' => esc_html__('Post Recommended Thumbnail Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> 'theevent-post-thumbnail-size'
										),
									)
								),								
								// 'blog-list' => array(
									// 'title' => esc_html__('Blog Listing', 'the-event'),									
									// 'options' => array(	
										// 'blog-read-more' => array(
											// 'title' => esc_html__('Blog Read More', 'the-event'),
											// 'type' => 'checkbox',
											// 'description'=> esc_html__('Using this option you can show/hide Read More button at the blog listing,  Category.', 'the-event'),	
											// 'default' => 'enable'
										// ),
									// )
								// ),
							),
						),
						
						'event-style' => array(
							'title' => esc_html__('Event Settings', 'the-event'),
							'icon' => 'fa-calendar-check-o',
							'options' => array(
								'Event-single' => array(
									'title' => esc_html__('Event Single', 'the-event'),
									'options' => array(
										'single-event-feature-image' => array(
											'title' => esc_html__('Single Event Feature Image', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide the Featured Image on event detail.', 'the-event'),											
											'default' => 'enable'
										),
										'single-event-feature-size' => array(
											'title' => esc_html__('Single Event Feature Image', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> 'theevent-post-thumbnail-size'
										),
										'single-event-date' => array(
											'title' => esc_html__('Single Event Image Date', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide extra information about the Event Date on Featured Image.', 'the-event'),											
											'default' => 'enable'
										),
										'single-event-organizer' => array(
											'title' => esc_html__('Single Event Organizer Information', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('You can enable or disable the Event Organizer Information from here.', 'the-event'),
											'default' => 'enable'											
										),	
										'single-event-tags' => array(
											'title' => esc_html__('Single Event Meta Information', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide the Event Detail Tags.', 'the-event'),
											'default' => 'enable'
										),
										'speaker-thumbnail-size' => array(
											'title' => esc_html__('Speakers Thumbnail Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> ''
										),	
										'single-related-events' => array(
											'title' => esc_html__('Single Related Events', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide the Related Events on Event Detail Page.', 'the-event'),		
											'default' => 'enable'
										),
										'single-related-events-meta' => array(
											'title' => esc_html__('Single Related Events Meta', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide some extra related events meta information on detail page.', 'the-event'),
											'default' => 'enable'
										),
										'single-event-related-size' => array(
											'title' => esc_html__('Single Event Related Image', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'default'=> 'theevent-post-thumbnail-size'
										),
										'single-event-comments' => array(
											'title' => esc_html__('Single Events Comments', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can turn on/off the Author Comments Section on the Event Detail Page.', 'the-event'),
											'default' => 'enable'
										),
									)
								),
							),
						),
						
						
						'team-style' => array(
							'title' => esc_html__('Speaker Settings', 'the-event'),
							'icon' => 'fa-user',
							'options' => array(
								'team-settings' => array(
									'title' => esc_html__('Team Single', 'the-event'),
									'options' => array(
										'single-team-feature-image' => array(
											'title' => esc_html__('Single Team Feature Image', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('If you do not want to set a featured image on Team detail kindly disable it here.', 'the-event'),
											'default' => 'enable',
										),
										'thumbnail-size' => array(
											'title' => esc_html__('Select Image Size', 'the-event'),
											'type'=> 'combobox',
											'options'=> theevent_get_thumbnail_list(),
											'wrapper-class' => 'type-1-wrapper type-4-wrapper style-wrapper',
											'default'=> 'theevent-post-thumbnail-size'
										),
										// 'single-team-layout' => array(
											// 'title' => esc_html__('Single Team Style', 'the-event'),
											// 'type' => 'combobox',
											// 'options' => array(
												// 'lawyer-style'=> 'Lawyer Style',
												// 'politician-style'=> 'Politician Style',												
											// ),
											// 'default' => 'politician-style'							
										// ),
										'single-team-comments' => array(
											'title' => esc_html__('Single Team Comments', 'the-event'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide the Team Comments on Team Detail Page.', 'the-event'),		
											'default' => 'enable'
										),
									)
								),
							),
						),

						// overall elements menu
						'overall-elements' => array(
							'title' => esc_html__('Social Settings', 'the-event'),
							'icon' => 'fa fa-share-square-o',
							'options' => array(

								'header-social' => array(),
								
								'social-shares' => array(),
								
							)				
						),
						
						// Element Color menu
						// 'element-color' => array(
							// 'title' => __('Element Color', 'the-event'),
							// 'icon' => 'fa fa-eyedropper',
							// 'options' => array(
								// 'font-heading-color' => array(
								// 'title' => __('Font Headings Color', 'the-event'),
									// 'options' => array(
										// 'font-heading-h1' => array(
											// 'title' => __('Font Heading H1 Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Font Heading h1 color of the site.',
											// 'default' => '#000',											
										// ),
										// 'font-heading-h2' => array(
											// 'title' => __('Font Heading H2 Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Font Heading h2 color of the site.',
											// 'default' => '#000',											
										// ),
										// 'font-heading-h3' => array(
											// 'title' => __('Font Heading H3 Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Font Heading h3 color of the site.',
											// 'default' => '#000',											
										// ),
										// 'font-heading-h4' => array(
											// 'title' => __('Font Heading H4 Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Font Heading h4 color of the site.',
											// 'default' => '#000',											
										// ),
										// 'font-heading-h5' => array(
											// 'title' => __('Font Heading H5 Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Font Heading h5 color of the site.',
											// 'default' => '#000',											
										// ),		
										// 'font-heading-h6' => array(
											// 'title' => __('Font Heading H6 Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Font Heading h6 color of the site.',
											// 'default' => '#000',											
										// ),
									// ),										
								// ),
								// 'body-color' => array(
									// 'title' => __('Body Color Settings', 'the-event'),
									// 'options' => array(
										// 'text-color' => array(
											// 'title' => __('Text Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the text color of the site.',
											// 'default' => '#000',											
										// ),
										// 'link-color' => array(
											// 'title' => __('Link Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Link color of hyperlink of the site.',
											// 'default' => '#fff',											
										// ),
										// 'link-hover-color' => array(
											// 'title' => __('Link Hover Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Link hover color of hyperlink of the site.',
											// 'default' => '#000',											
										// ),
										// 'button-color' => array(
											// 'title' => __('Button Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the button color of your site.',
											// 'default' => '#f37936',											
										// ),
										// 'button-text-color' => array(
											// 'title' => __('Button Text Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the button text color of your site.',
											// 'default' => '#000',											
										// ),
										// 'button-border-color' => array(
											// 'title' => __('Button Border Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the button border color of your site.',
											// 'default' => '#fff',											
										// ),
									// ),	
								// ),
								// 'blog-color' => array(
									// 'title' => __('Blog Color Settings', 'the-event'),
									// 'options' => array(
										// 'blog-title-color' => array(
											// 'title' => __('Blog title Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the blog title color of the site.',
											// 'default' => '#000',											
										// ),
										// 'blog-date-color' => array(
											// 'title' => __('Blog Date Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Blog Date color of hyperlink of the site.',
											// 'default' => '#000',											
										// ),
										// 'blog-hover-color' => array(
											// 'title' => __('Link Hover Color', 'the-event'),
											// 'type' => 'colorpicker',
											// 'description' => 'This Color Picker Allows you to change the Link hover color of hyperlink of the site.',
											// 'default' => '#f37936',											
										// ),
										
									// ),	
								// ),		

							// )					
						// ),
						
						
						// font setting menu
						'font-settings' => array(
							'title' => esc_html__('Font Settings', 'the-event'),
							'icon' => 'fa fa-font',
							'options' => array(
								'font-family' => array(
								'title' => esc_html__('Font Family', 'the-event'),
									'options' => array(
										'navi-font-family' => array(
											'title' => esc_html__('Navigation Font', 'the-event'),
											'type' => 'font_option',
											'description'=> esc_html__('Please Select the Navigation Font from here.', 'the-event'),
											'default' => 'Arial, Helvetica, sans-serif',
											'data-type' => 'font_option',
											'selector' => '.kode-navigation{ font-family: #gdlr#; }'
										),
										'navi-font-weight' => array(
											'title' => __('Navigation Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',											
										),
										'heading-font-family' => array(
											'title' => esc_html__('Heading Font', 'the-event'),
											'type' => 'font_option',
											'description'=> esc_html__('Please Select the Heading Font Style from here h1, h2, h3, h4, h5, h6.', 'the-event'),
											'default' => 'Arial, Helvetica, sans-serif',
											'data-type' => 'font_option',
											'selector' => 'h1, h2, h3, h4, h5, h6{ font-family: #gdlr#; }'
										),	
																			
										'body-font-family' => array(
											'title' => esc_html__('Content Font', 'the-event'),
											'type' => 'font_option',
											'description'=> esc_html__('Please Select the Body Font Style from here.', 'the-event'),
											'default' => 'Arial, Helvetica, sans-serif',
											'data-type' => 'font_option',
											'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
										),			
										
									),	
								),
								
								// 'font-heading' => array(
									// 'title' => __('Font Heading', 'the-event'),
									// 'options' => array(
										// 'heading-h1-font-family' => array(
											// 'title' => __('Heading H1 Font', 'the-event'),
											// 'type' => 'font_option',
											// 'description'=> esc_html__('Please Select the Heading h1 Font from here.', 'the-event'),
											// 'default' => 'Arial, Helvetica, sans-serif',
											// 'data-type' => 'font_option',
											// 'selector' => '.kode-navigation{ font-family: #gdlr#; }'
										// ),
										// 'heading-h2-font-family' => array(
											// 'title' => __('Heading H2 Font', 'the-event'),
											// 'type' => 'font_option',
											// 'description'=> esc_html__('Please Select the Heading h2 Font from here.', 'the-event'),
											// 'default' => 'Arial, Helvetica, sans-serif',
											// 'data-type' => 'font_option',
											// 'selector' => '.kode-navigation{ font-family: #gdlr#; }'
										// ),
										// 'heading-h3-font-family' => array(
											// 'title' => __('Heading H3 Font', 'the-event'),
											// 'type' => 'font_option',
											// 'description'=> esc_html__('Please Select the Heading h3 Font from here.', 'the-event'),
											// 'default' => 'Arial, Helvetica, sans-serif',
											// 'data-type' => 'font_option',
											// 'selector' => '.kode-navigation{ font-family: #gdlr#; }'
										// ),	
										// 'heading-h4-font-family' => array(
											// 'title' => __('Heading H4 Font', 'the-event'),
											// 'type' => 'font_option',
											// 'description'=> esc_html__('Please Select the Heading h4 Font from here.', 'the-event'),
											// 'default' => 'Arial, Helvetica, sans-serif',
											// 'data-type' => 'font_option',
											// 'selector' => '.kode-navigation{ font-family: #gdlr#; }'
										// ),
										// 'heading-h5-font-family' => array(
											// 'title' => __('Heading H5 Font', 'the-event'),
											// 'type' => 'font_option',
											// 'description'=> esc_html__('Please Select the Heading h5 Font from here.', 'the-event'),
											// 'default' => 'Arial, Helvetica, sans-serif',
											// 'data-type' => 'font_option',
											// 'selector' => '.kode-navigation{ font-family: #gdlr#; }'
										// ),
										
									// ),	
								// ),

								'font-size' => array(
									'title' => esc_html__('Font Size - Weight', 'the-event'),
									'options' => array(
										
										'content-font-size' => array(
											'title' => esc_html__('Content Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('This option will let you increase / decrease the Content Size of the site.', 'the-event'),
											'default' => '14',
											//'selector' => 'body{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),	
										'content-font-weight' => array(
											'title' => __('Content Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',
										),											
										'h1-font-size' => array(
											'title' => esc_html__('H1 Size', 'the-event'),
											'type' => 'sliderbar',
											'default' => '30',
											'description'=> esc_html__('You can increase or decrease the Heading 1 Font size from here.', 'the-event'),
											//'selector' => 'h1{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'h1-font-weight' => array(
											'title' => __('H1 Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',
										),	
										'h2-font-size' => array(
											'title' => esc_html__('H2 Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can increase or decrease the Heading 2 Font size from here.', 'the-event'),
											'default' => '25',
											//'selector' => 'h2{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'h2-font-weight' => array(
											'title' => __('H2 Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',
										),	
										'h3-font-size' => array(
											'title' => esc_html__('H3 Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can increase or decrease the Heading 3 Font size from here.', 'the-event'),
											'default' => '20',
											//'selector' => 'h3{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'h3-font-weight' => array(
											'title' => __('H3 Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',
										),	
										'h4-font-size' => array(
											'title' => esc_html__('H4 Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can increase or decrease the Heading 4 Font size from here.', 'the-event'),
											'default' => '18',
											//'selector' => 'h4{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'h4-font-weight' => array(
											'title' => __('H4 Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',
										),
										'h5-font-size' => array(
											'title' => esc_html__('H5 Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can increase or decrease the Heading 5 Font size from here.', 'the-event'),
											'default' => '16',
											//'selector' => 'h5{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'h5-font-weight' => array(
											'title' => __('H5 Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',
										),
										'h6-font-size' => array(
											'title' => esc_html__('H6 Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can increase or decrease the Heading 6 Font size from here.', 'the-event'),
											'default' => '15',
											//'selector' => 'h6{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'h6-font-weight' => array(
											'title' => __('H6 Font Weight', 'the-event'),
											'type' => 'text',
											'description' => 'Add font weight which can be added in numberic and text format both for example: 100 to 900, Normal, Lighter, Bolder.',
											'default' => 'normal',
										),
										
									)
								),								
							)					
						),
						
						//translation setting menu
						'translate' => array(
							'title' => esc_html__('Translation Settings', 'the-event'),
							'icon' => 'fa-language',
							'options' => array(
								'general-translation' => array(
									'title' => esc_html__('General Translation', 'the-event'),
									'options' => array(
										'error-404'=> array(
											'title'=> esc_html__('Error 404 Title' ,'the-event'),
											'type'=> 'text',											
											'default' => '404',
											'description'=> esc_html__('Adjust 404 text in the Error 404 page.', 'the-event')
										),
										'error-title'=> array(
											'title'=> esc_html__('Error Title' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Oops this page could not be found !',
											'description'=> esc_html__('Adjust 404 Title text in the Error 404 page.', 'the-event')
										),
										'error-sub-title'=> array(
											'title'=> esc_html__('Error 404  Sub Title' ,'the-event'),
											'type'=> 'text',											
											'default' => 'You can use the form below to search for what you need.',
											'description'=> esc_html__('Adjust Error 404 Sub Title text in the Error 404 page.', 'the-event')
										),
										'error-btn'=> array(
											'title'=> esc_html__('Error 404 Button Text' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Return To Home Page',
											'description'=> esc_html__('Adjust Error 404 Home page button text in the Error 404 page.', 'the-event')
										),
										'error-pre-btn'=> array(
											'title'=> esc_html__('Error 404 Previous Button Text' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Back to Previous Page',
											'description'=> esc_html__('Adjust Error 404 previous page button text in the Error 404 page.', 'the-event')
										),
									)
								),
								
								'blog-translation' => array(
									'title' => esc_html__('Blog Translation', 'KodeForest'),
									'options' => array(
										'blog-comments'=> array(
											'title'=> esc_html__('Blog Comments' ,'KodeForest'),
											'type'=> 'text',											
											'default' => 'Comments',
											'description'=> esc_html__('Adjust Blog listing layouts Comments button text here', 'KodeForest')
										),
										'blog-read-more'=> array(
											'title'=> esc_html__('Blog Read More' ,'KodeForest'),
											'type'=> 'text',											
											'default' => 'Read More',
											'description'=> esc_html__('Adjust Read More text in the blog posts.', 'KodeForest')
										),
										'blog-see-more'=> array(
											'title'=> esc_html__('Blog See More' ,'KodeForest'),
											'type'=> 'text',											
											'default' => 'See More',
											'description'=> esc_html__('Adjust see More text in the blog posts.', 'KodeForest')
										),
									)
								),
								
								
								'event-translation' => array(
									'title' => esc_html__('Event Translation', 'the-event'),
									'options' => array(
										'event-days'=> array(
											'title'=> esc_html__('Event Days' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Days',
											'description'=> esc_html__('Adjust Event Days text on the Event Detail page.', 'the-event')
										),
										'event-hours'=> array(
											'title'=> esc_html__('Event Hours' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Hours',
											'description'=> esc_html__('Adjust Event Hours text on the Event Detail page.', 'the-event')
										),
										'event-week'=> array(
											'title'=> esc_html__('Event Weeks' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Week',
											'description'=> esc_html__('Adjust Event Minutes text on the Event Detail page.', 'the-event')
										),
										'event-sec'=> array(
											'title'=> esc_html__('Event Seconds' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Seconds',
											'description'=> esc_html__('Adjust Event Seconds text on the Event Detail page.', 'the-event')
										),
										'event-speakers-of-event'=> array(
											'title'=> esc_html__('Speaker Of Event' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Speaker Of the Event',
											'description'=> esc_html__('Adjust Speaker Of the Event text on the Event Detail page.', 'the-event')
										),
										'event-booking-text'=> array(
											'title'=> esc_html__('Event Booking Text' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Reserve Your Seat Now',
											'description'=> esc_html__('Adjust Reserve Your Seat Now text on the Event Detail page.', 'the-event')
										),
										'event-read-more'=> array(
											'title'=> esc_html__('Event Read More' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Read More',
											'description'=> esc_html__('Adjust Read More text on the Event Detail page.', 'the-event')
										),
									)
								),
								
								// 'event-tickets-translation' => array(
									// 'title' => esc_html__('Tickets Translation', 'the-event'),
									// 'options' => array(
										// 'error-ticket-type'=> array(
											// 'title'=> esc_html__('Event Tickets Type' ,'the-event'),
											// 'type'=> 'text',											
											// 'default' => 'Event Tickets Type',
											// 'description'=> esc_html__('Adjust Event Tickets Type text in the Ticket Tables.', 'the-event')
										// ),
										// 'error-ticket-price'=> array(
											// 'title'=> esc_html__('Price' ,'the-event'),
											// 'type'=> 'text',											
											// 'default' => 'Price',
											// 'description'=> esc_html__('Adjust Price text in the Ticket Tables.', 'the-event')
										// ),
										// 'error-ticket-qty'=> array(
											// 'title'=> esc_html__('Quantity' ,'the-event'),
											// 'type'=> 'text',											
											// 'default' => 'QTY',
											// 'description'=> esc_html__('Adjust Quantity text in the Ticket Tables.', 'the-event')
										// ),
										// 'error-ticket-book'=> array(
											// 'title'=> esc_html__('Book' ,'the-event'),
											// 'type'=> 'text',											
											// 'default' => 'Book',
											// 'description'=> esc_html__('Adjust Book text in the Ticket Tables.', 'the-event')
										// ),
									// )
								// ),
								
								'speaker-translation' => array(
									'title' => esc_html__('Speaker Translation', 'the-event'),
									'options' => array(
										'speaker-website'=> array(
											'title'=> esc_html__('Speaker Website' ,'the-event'),
											'type'=> 'text',											
											'default' => 'address',
											'description'=> esc_html__('Adjust Website text in the Speaker Detail page.', 'the-event')
										),
										'speaker-phone'=> array(
											'title'=> esc_html__('Speaker Detail Phone Number' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Phone',
											'description'=> esc_html__('Adjust Phone Number text in the Speaker Detail page.', 'the-event')
										),
										'speaker-email'=> array(
											'title'=> esc_html__('Speaker Email Address' ,'the-event'),
											'type'=> 'text',											
											'default' => 'Email',
											'description'=> esc_html__('Adjust Email text in the Speaker Detail page.', 'the-event')
										),
									)
								),
							),
						),
							
						
						// plugin setting menu
						'plugin-settings' => array(
							'title' => esc_html__('Slider Settings', 'the-event'),
							'icon' => 'fa fa-sliders ',
							'options' => array(
								'bx-slider' => array(
									'title' => esc_html__('BX Slider', 'the-event'),
									'options' => array(		
										'bx-slider-effects' => array(
											'title' => esc_html__('BX Slider Effect', 'the-event'),
											'type' => 'combobox',
											'description'=> esc_html__('You can select the BX slide effect from the dropdown menu.', 'the-event'),
											'options' => array(
												'fade' => esc_html__('Fade', 'the-event'),
												'slide'	=> esc_html__('Slide', 'the-event')
											)
										),
										'bx-min-slide' => array(
											'title' => esc_html__('BX Min Slide', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the minimum number of slides here.', 'the-event'),
											'wrapper-class' => 'bx-slider-effects-wrapper slidee-wrapper',
											'default' => '1'
										),
										'bx-max-slide' => array(
											'title' => esc_html__('BX Max Slide', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the maximum number of slides here.', 'the-event'),
											'wrapper-class' => 'bx-slider-effects-wrapper slidee-wrapper',
											'default' => '1'
										),
										'bx-slide-margin' => array(
											'title' => esc_html__('BX Slide Margin', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the maximum number of slides here.', 'the-event'),
											'wrapper-class' => 'bx-slider-effects-wrapper slidee-wrapper',
											'default' => '10'
										),
										'bx-arrow' => array(
											'title' => esc_html__('Bxslider Arrows', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Turn on or off bx slider arrows from here enabling this option will enable the arrows on home page.', 'the-event'),	
											'default' => 'enable'
										),
										'bx-pause-time' => array(
											'title' => esc_html__('BX Slider Pause Time', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the BX slider pause time you want to have.', 'the-event'),
											'default' => '7000'
										),
										'bx-slide-speed' => array(
											'title' => esc_html__('BX Slider Animation Speed', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the BX slider animation speed you want to have.', 'the-event'),
											'default' => '600'
										),	
									)
								),
								'flex-slider' => array(
									'title' => esc_html__('Flex Slider', 'the-event'),
									'options' => array(		
										'flex-slider-effects' => array(
											'title' => esc_html__('Flex Slider Effect', 'the-event'),
											'type' => 'combobox',
											'description'=> esc_html__('You can select the Flex slide effect from the dropdown menu.', 'the-event'),
											'options' => array(
												'fade' => esc_html__('Fade', 'the-event'),
												'slide'	=> esc_html__('Slide', 'the-event')
											)
										),
										'flex-pause-time' => array(
											'title' => esc_html__('Flex Slider Pause Time', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Flex slider pause time you want to have.', 'the-event'),
											'default' => '7000'
										),
										'flex-slide-speed' => array(
											'title' => esc_html__('Flex Slider Animation Speed', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Flex slider animation speed you want to have.', 'the-event'),
											'default' => '600'
										),	
									)
								),
								
								'nivo-slider' => array(
									'title' => esc_html__('Nivo Slider', 'the-event'),
									'options' => array(		
										'nivo-slider-effects' => array(
											'title' => esc_html__('Nivo Slider Effect', 'the-event'),
											'type' => 'combobox',
											'description'=> esc_html__('You can select the Nivo slide effect from the dropdown menu.', 'the-event'),
											'options' => array(
												'sliceDownRight'	=> esc_html__('sliceDownRight', 'the-event'),
												'sliceDownLeft'		=> esc_html__('sliceDownLeft', 'the-event'),
												'sliceUpRight'		=> esc_html__('sliceUpRight', 'the-event'),
												'sliceUpLeft'		=> esc_html__('sliceUpLeft', 'the-event'),
												'sliceUpDown'		=> esc_html__('sliceUpDown', 'the-event'),
												'sliceUpDownLeft'	=> esc_html__('sliceUpDownLeft', 'the-event'),
												'fold'				=> esc_html__('fold', 'the-event'),
												'fade'				=> esc_html__('fade', 'the-event'),
												'boxRandom'			=> esc_html__('boxRandom', 'the-event'),
												'boxRain'			=> esc_html__('boxRain', 'the-event'),
												'boxRainReverse'	=> esc_html__('boxRainReverse', 'the-event'),
												'boxRainGrow'		=> esc_html__('boxRainGrow', 'the-event'),
												'boxRainGrowReverse'=> esc_html__('boxRainGrowReverse', 'the-event')
											)
										),
										'nivo-pause-time' => array(
											'title' => esc_html__('Nivo Slider Pause Time', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Nivo slider pause time you want to have.', 'the-event'),
											'default' => '7000'
										),
										'nivo-slide-speed' => array(
											'title' => esc_html__('Nivo Slider Animation Speed', 'the-event'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Nivo slider animation speed you want to have.', 'the-event'),
											'default' => '600'
										),	
									)
								),
								'slider-caption' => array(
									'title' => esc_html__('Slider Caption', 'the-event'),
									'options' => array(		
										'caption-title-color' => array(
											'title' => esc_html__('Caption Title Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Title Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-background-color-switch' => array(
											'title' => esc_html__('Caption Background Color Switch', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('if you have transparent background and dont want to use caption bg you can turn it off.', 'the-event'),	
											'default' => 'enable'
										),
										'caption-background-color' => array(
											'title' => esc_html__('Caption Title Background Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Title Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper caption-background-color-switch-wrapper caption-background-color-switch-enable'
										),
										'title-font-size' => array(
											'title' => esc_html__('Caption Title Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can control the Caption Title Size on the slider from here.', 'the-event'),
											'default' => '30',
											'range_start' => '30',
											'range_end' => '30',
											//'selector' => 'h1{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										
										'caption-desc-color' => array(
											'title' => esc_html__('Caption Description Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Description Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-font-size' => array(
											'title' => esc_html__('Caption Description Size', 'the-event'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can select the Caption Description Font Size on the slider from here.', 'the-event'),
											'default' => '30',
											'range_start' => '30',
											'range_end' => '30',
											//'selector' => 'h1{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'caption-btn-color' => array(
											'title' => esc_html__('Button Text Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),
										'caption-btn-color-hover' => array(
											'title' => esc_html__('Button Hover Text Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-btn-color-border' => array(
											'title' => esc_html__('Button Border Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Border Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),										
										'caption-btn-color-bg' => array(
											'title' => esc_html__('Button Background Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-btn-hover-bg' => array(
											'title' => esc_html__('Button Background Hover Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-btn-arrow-color' => array(
											'title' => esc_html__('Caption Button Arrow Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-btn-arrow-hover' => array(
											'title' => esc_html__('Caption Button Hover Arrow Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),
										'caption-btn-arrow-bg' => array(
											'title' => esc_html__('Caption Button Arrow BG Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),
										'caption-btn-arrow-hover-bg' => array(
											'title' => esc_html__('Caption Button Arrow Hover BG Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'the-event'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),
									) ,
								)
							)					
						),
						
						'sidebar-settings' => array(
							'title' => esc_html__('Sidebar Settings', 'the-event'),
							'icon' => 'fa fa-columns',
							'options' => array(
								'sidebar_element' => array(
									'title' => esc_html__('Sidebar', 'the-event'),
									'options' => array(
										'sidebar-tbn' => array(
											'title' => esc_html__('Sidebar Button', 'the-event'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Sidebar button', 'the-event'),
											'default' => 'enable'
										),
										'sidebar-bg-color' => array(
											'title' => esc_html__('Sidebar Background Color', 'the-event'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Sidebar Background Color.', 'the-event'),
											'default'=> '#ffffff',											
										),
										'sidebar-padding-top' => array(
											'title' => esc_html__('Padding Top', 'the-event'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from top of the sidebar'
										),
										'sidebar-padding-bottom' => array(
											'title' => esc_html__('Padding Bottom', 'the-event'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from Bottom of the sidebar'
										),
										'sidebar-padding-left' => array(
											'title' => esc_html__('Padding Left', 'the-event'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from Left of the sidebar'
										),
										'sidebar-padding-right' => array(
											'title' => esc_html__('Padding Right', 'the-event'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from Right of the sidebar'
										),
										'sidebar-element' => array(
											'title' => esc_html__('Sidebar Name', 'the-event'),
											'description'=> esc_html__('Enter a name for new sidebar. It must be a valid name which starts with a letter. Then Click on the Add Button to create the Custom Sidebar', 'the-event'),
											'placeholder' => esc_html__('type sidebar name', 'the-event'),
											'type' => 'sidebar',
											'btn_title' => 'Add Sidebar'
										)										
									)
								),
							)
						),
						'api-settings' => array(
							'title' => esc_html__('API Settings', 'the-event'),
							'icon' => 'fa fa-gear',
							'options' => array(
								'api_configuration' => array(
									'title' => esc_html__('API Settings', 'the-event'),
									'options' => array(		
										'mail-chimp-api' => array(
											'title' => esc_html__('Mail Chimp API', 'the-event'),
											'type' => 'text',
											'default' => 'API KEY',
											'description' => 'Please add mail chimp API Key here'
										),									
										'mail-chimp-listid' => array(
											'title' => esc_html__('MailChimp List ID', 'the-event'),
											'type' => 'text',
											'description' => 'For getting list id first login to your mail chimp account then click on list > List name and Campaign defaults > you will see list id written on the right side of first section.'
										),
									)
								),
							)
						),
						'seo-settings' => array(
							'title' => esc_html__('SEO Settings', 'the-event'),
							'icon' => 'fa fa-bar-chart',
							'options' => array(
								'seo-settings-config' => array(
									'title' => esc_html__('SEO Config', 'the-event'),
									'options' => array(		
										'home-seo-meta-button' => array(
											'title' => esc_html__('Theme SEO', 'the-event'),
											'type' => 'checkbox',	
											'description' => 'Turn it OFF if you want to use external SEO plugin',
											'default' => 'enable'
										),
										'seo-title-sep' => array(
											'title' => 'Title Separator',
											'type' => 'text',	
											'default' => '-',
											'description' => 'Add Separator for your title by default - is added.'
										),
										'seo-website-slug' => array(
											'title' => 'Website Name Default',
											'type' => 'text',	
											'default' => get_bloginfo( 'name' ),
											'description' => 'Enter your Website Default Name you want to have.',
										),
										'seo-title-transform' => array(
											'title' => esc_html__('Seo Title Transform', 'the-event'),
											'type' => 'combobox',
											'description'=> esc_html__('You can Manage Page Title Case uppercase or lowercase, capitalize.', 'the-event'),
											'options' => array(
												'uppercase'		=> esc_html__('UpperCase', 'the-event'),
												'lowercase'		=> esc_html__('LowerCase', 'the-event'),
												'capitalize'	=> esc_html__('Capitalize', 'the-event'),												
											)
										),
									)
								),
								'seo-settings-meta' => array(
									'title' => esc_html__('WebMaster & Bots', 'the-event'),
									'options' => array(		
										'seo-meta-bots' => array(
											'title' => esc_html__('Allow Bots to Index', 'the-event'),
											'type' => 'checkbox',	
											'description' => 'Turn it OFF if you dont want to index your site.',
											'default' => 'enable'
										),
										'seo-meta-alexa' => array(
											'title' => 'SEO Default Alexa',
											'type' => 'text',	
											'description' => 'You can use the boxes below to verify with the different Webmaster Tools, if your site is already verified, you can just forget about these. <a target="_blank" href="http://www.alexa.com/siteowners/claim">Alexa</a>.'
										),
										'seo-meta-bing' => array(
											'title' => 'SEO Default Bing',
											'type' => 'text',	
											'description' => 'You can use the boxes below to verify with the different Webmaster Tools, if your site is already verified, you can just forget about these. <a target="_blank" href="' . esc_url( 'http://www.bing.com/webmaster/?rfp=1#/Dashboard/?url=' . urlencode( str_replace( 'http://', '', home_url('/') ) ) ) . '">Bing</a>.'
										),
										'seo-meta-google' => array(
											'title' => 'SEO Default Google',
											'type' => 'text',	
											'description' => 'You can use the boxes below to verify with the different Webmaster Tools, if your site is already verified, you can just forget about these. <a target="_blank" href="' . esc_url( 'https://www.google.com/webmasters/verification/verification?hl=en&siteUrl=' . urlencode( home_url('/') ) . '/' ) . '">Google</a>.'
										),
										'seo-meta-yandex' => array(
											'title' => 'SEO Default Yandex',
											'type' => 'text',	
											'description' => 'You can use the boxes below to verify with the different Webmaster Tools, if your site is already verified, you can just forget about these. <a target="_blank" href="http://help.yandex.com/webmaster/service/rights.xml#how-to">Yandex</a>.'
										),
										// 'seo-meta-pinterest' => array(
											// 'title' => 'SEO Default Pinterest',
											// 'type' => 'text',	
											// 'description' => 'You can use the boxes below to verify with the different Webmaster Tools, if your site is already verified, you can just forget about these. <a target="_blank" href="https://help.pinterest.com/en/articles/verify-your-website#meta_tag">Yandex</a>.'
										// ),
										
									)
								),
								'seo-settings-home' => array(
									'title' => esc_html__('Home Page Settings', 'the-event'),
									'options' => array(		
										'home-seo-meta-title' => array(
											'title' => 'SEO Default Title',
											'type' => 'text',	
											'description' => 'Please add Default meta title. These setting may be overridden for single posts & pages.'
										),
										'home-seo-meta-description' => array(
											'title' => 'SEO Default Description',
											'type' => 'textarea',
											'description' => 'Please add Default meta description here. These setting may be overridden for single posts & pages.'
										),
										'home-seo-meta-keyword' => array(
											'title' => 'SEO Default Keyword',
											'type' => 'textarea',
											'description' => 'Please add Default keyword coma separated for example: food, charity, news These setting may be overridden for single posts & pages.'
										),	
									)
								),
								'seo-settings-archive' => array(
									'title' => esc_html__('SEO Archive Page', 'the-event'),
									'options' => array(		
										'archive-seo-meta-title' => array(
											'title' => 'SEO Archive Title',
											'type' => 'text',	
											'description' => 'Please add Archive meta title. These setting may be overridden for single posts & pages.'
										),
										'archive-seo-meta-description' => array(
											'title' => 'SEO Archive Description',
											'type' => 'textarea',
											'description' => 'Please add Archive meta description here. These setting may be overridden for single posts & pages.'
										),
										'archive-seo-meta-keyword' => array(
											'title' => 'SEO Archive Keyword',
											'type' => 'textarea',
											'description' => 'Please add Archive keyword coma separated for example: food, charity, news These setting may be overridden for single posts & pages.'
										),	
									)
								),
								'seo-settings-error' => array(
									'title' => esc_html__('SEO 404 Page', 'the-event'),
									'options' => array(		
										'error-seo-meta-title' => array(
											'title' => 'SEO Error Title',
											'type' => 'text',	
											'description' => 'Please add Error meta title. These setting may be overridden for single posts & pages.'
										),
										'error-seo-meta-description' => array(
											'title' => 'SEO Error Description',
											'type' => 'textarea',
											'description' => 'Please add Error meta description here. These setting may be overridden for single posts & pages.'
										),
										'error-seo-meta-keyword' => array(
											'title' => 'SEO Error Keyword',
											'type' => 'textarea',
											'description' => 'Please add Error keyword coma separated for example: food, charity, news These setting may be overridden for single posts & pages.'
										),	
									)
								),
								'seo-settings-search' => array(
									'title' => esc_html__('SEO Search Page', 'the-event'),
									'options' => array(		
										'search-seo-meta-title' => array(
											'title' => 'SEO Search Title',
											'type' => 'text',	
											'description' => 'Please add Search meta title. These setting may be overridden for single posts & pages.'
										),
										'search-seo-meta-description' => array(
											'title' => 'SEO Search Description',
											'type' => 'textarea',
											'description' => 'Please add Search meta description here. These setting may be overridden for single posts & pages.'
										),
										'search-seo-meta-keyword' => array(
											'title' => 'SEO Search Keyword',
											'type' => 'textarea',
											'description' => 'Please add Search keyword coma separated for example: food, charity, news These setting may be overridden for single posts & pages.'
										),	
									)
								),
								'seo-settings-maintenance' => array(
									'title' => esc_html__('SEO Maintenance Page', 'the-event'),
									'options' => array(		
										'maintenance-seo-meta-title' => array(
											'title' => 'SEO Maintenance Title',
											'type' => 'text',	
											'description' => 'Please add maintenance meta title. These setting may be overridden for single posts & pages.'
										),
										'maintenance-seo-meta-description' => array(
											'title' => 'SEO Maintenance Description',
											'type' => 'textarea',
											'description' => 'Please add maintenance meta description here. These setting may be overridden for single posts & pages.'
										),
										'maintenance-seo-meta-keyword' => array(
											'title' => 'SEO Maintenance Keyword',
											'type' => 'textarea',
											'description' => 'Please add maintenance keyword coma separated for example: food, charity, news These setting may be overridden for single posts & pages.'
										),	
									)
								),
								
							)
						),
						
						
					)
				), 
				
				
				
				$theevent_theme_option
			);
			
		}
		
	}

?>