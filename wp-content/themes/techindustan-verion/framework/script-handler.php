<?php
	/*	
	*	Kodeforest Include Script File
	*	---------------------------------------------------------------------
	*	This file use to include a necessary script when it's requires
	*	---------------------------------------------------------------------
	*/
	
	// Match the values
	if( !function_exists('theevent_match_page_builder') ){
		function theevent_match_page_builder($array, $item_type, $type, $data = array()){
			if(isset($array)){
				foreach($array as $item){
					if($item['item-type'] == $item_type && $item['type'] == $type){
						if(empty($data)){
							return true;
						}else{	
							if( strpos($item['option'][$data[0]], $data[1]) !== false ) return true;
						}
					}
					if($item['item-type'] == 'wrapper'){
						if( theevent_match_page_builder($item['items'], $item_type, $type) ) return true;
					}
				}
			}
			return false;
		}
	}	
	//Add Scripts in Theme
	if(!is_admin()){
		add_action('wp_enqueue_scripts','theevent_register_non_admin_styles');
		add_action('wp_enqueue_scripts','theevent_register_non_admin_scripts');
		add_action( 'after_setup_theme', 'theevent_theme_slug_editor_styles' );
	}
	
	// Register all Css
	if( !function_exists('theevent_register_non_admin_styles') ){
		function theevent_register_non_admin_styles(){	
			
			global $post,$post_id,$theevent_content_raw,$theevent_theme_option,$theevent_font_controller;		
			
			wp_deregister_style('ignitiondeck-base');
			
			wp_deregister_style('flexslider');
			
			
			wp_enqueue_style( 'style', get_stylesheet_uri() );  //Default Stylesheet	
			
			wp_enqueue_style( 'font-awesome', THEEVENT_PATH . '/framework/include/frontend_assets/font-awesome/css/font-awesome.min.css' );  //Font Awesome
			
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'slider', array('slider-type', 'flexslider')) ){
				wp_enqueue_style( 'flexslider', THEEVENT_PATH . '/framework/include/frontend_assets/flexslider/flexslider.css' );  //Font Awesome
			}
			
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'slider', array('slider-type', 'nivo-slider')) ){
				wp_enqueue_style( 'nivo-slider', THEEVENT_PATH . '/framework/include/frontend_assets/nivo-slider/nivo-slider.css' );  //Font Awesome		
			}
			

			
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'gallery') ){
				wp_enqueue_style( 'style-prettyphoto', THEEVENT_PATH . '/framework/include/frontend_assets/default/css/prettyphoto.css' );  //Font Awesome		
			}
			
			
			wp_enqueue_style( 'style-component', THEEVENT_PATH . '/framework/include/frontend_assets/dl-menu/component.css' );  //Font Awesome
			wp_register_script('theevent-modernizr', THEEVENT_PATH.'/framework/include/frontend_assets/dl-menu/modernizr.custom.js', false, '1.0', true);
			wp_enqueue_script('theevent-modernizr');
			wp_register_script('theevent-dlmenu', THEEVENT_PATH.'/framework/include/frontend_assets/dl-menu/jquery.dlmenu.js', false, '1.0', true);
			wp_enqueue_script('theevent-dlmenu');
			
			
			wp_register_script('theevent-sticky-sidebar', THEEVENT_PATH.'/js/theevent-sticky-sidebar.js', false, '1.0', true);
			wp_enqueue_script('theevent-sticky-sidebar');
			
			
			if ( class_exists( 'bbPress' ) ) {
				wp_enqueue_style( 'theevent-bbp-css', THEEVENT_PATH . '/css/theevent_bbpress.css' );  //Font Awesome
			}
			
			
			wp_register_script('kf-timecircles', THEEVENT_PATH.'/framework/include/frontend_assets/timecircles/timecircles.js', false, '1.0', true);
			wp_enqueue_script('kf-timecircles');		
			wp_enqueue_style( 'kf-time-circles', THEEVENT_PATH . '/framework/include/frontend_assets/timecircles/timecircles.css' ); 
			
			wp_enqueue_style( 'theevent-buddy-press', THEEVENT_PATH . '/css/theevent_buddypress.css' );  //Font Awesome
			
			wp_enqueue_style( 'style-responsive', THEEVENT_PATH . '/css/responsive.css' );  //Font Awesome
			
			wp_enqueue_style( 'style-bootstrap', THEEVENT_PATH . '/css/bootstrap.min.css' );  //Font Awesome			
			
			wp_enqueue_style( 'style-typo', THEEVENT_PATH . '/css/themetypo.css' );  //Default Typo
			wp_enqueue_style( 'style-widget', THEEVENT_PATH . '/css/widget.css' );  //Default Widgets
			wp_enqueue_style( 'style-theme-widgets', THEEVENT_PATH . '/css/theme-widgets.css' );  //Theme Widgets
			
			wp_enqueue_style( 'style-svg-icon', THEEVENT_PATH . '/css/svg-icon/svg-icon.css' );  //Font Awesome
			
			wp_enqueue_style( 'style-svg-icon-2', THEEVENT_PATH . '/css/svg-icon-2.css' );  //Font Awesome
			
			wp_enqueue_style( 'style-featherlight', THEEVENT_PATH . '/framework/include/frontend_assets/featherlight/featherlight_min.css' );  //Font Awesome
			wp_register_script('theevent-featherlight', THEEVENT_PATH.'/framework/include/frontend_assets/featherlight/featherlight_min.js', false, '1.0', true);
			wp_enqueue_script('theevent-featherlight');
			
			
			wp_enqueue_style( 'style-color', THEEVENT_PATH . '/css/color.css' );  //Color File Default
			
			wp_enqueue_style( 'style-shortcode', THEEVENT_PATH . '/css/shortcode.css' );  //Font Awesome
			
			if(isset($theevent_theme_option['navi-font-family'])){
				$font_id = str_replace( ' ', '-', $theevent_theme_option['navi-font-family'] );
				wp_enqueue_style( 'style-shortcode-'.$font_id, esc_url_raw($theevent_font_controller->theevent_get_google_font_url($theevent_theme_option['navi-font-family'])));
			}
			
			if(isset($theevent_theme_option['heading-font-family'])){
				$font_id = str_replace( ' ', '-', $theevent_theme_option['heading-font-family'] );
				wp_enqueue_style( 'style-shortcode-'.$font_id, esc_url_raw($theevent_font_controller->theevent_get_google_font_url($theevent_theme_option['heading-font-family'])));
			}
			
			if(isset($theevent_theme_option['body-font-family'])){
				$font_id = str_replace( ' ', '-', $theevent_theme_option['body-font-family'] );
				wp_enqueue_style( 'style-shortcode-'.$font_id, esc_url_raw($theevent_font_controller->theevent_get_google_font_url($theevent_theme_option['body-font-family'])));
			}
		
			
		}
	}
	
	
	if( !function_exists('theevent_theme_slug_editor_styles') ){
		function theevent_theme_slug_editor_styles() {
			global $post,$post_id,$theevent_content_raw,$theevent_theme_option,$theevent_font_controller;		
			if(isset($theevent_theme_option['body-font-family'])){
				$font_id = str_replace( ' ', '-', $theevent_theme_option['body-font-family'] );			
				add_editor_style( array( 'editor-style.css', esc_url_raw($theevent_font_controller->theevent_get_google_font_url($theevent_theme_option['body-font-family'])) ) );
			}
		}
	}
	
	add_action( 'admin_print_styles-appearance_page_custom-header', 'theevent_slug_custom_header_fonts' );
	if( !function_exists('theevent_slug_custom_header_fonts') ){
		function theevent_slug_custom_header_fonts() {
			global $post,$post_id,$theevent_content_raw,$theevent_theme_option,$theevent_font_controller;		
			if(isset($theevent_theme_option['body-font-family'])){
				$font_id = str_replace( ' ', '-', $theevent_theme_option['body-font-family'] );						
				wp_enqueue_style( 'theme-slug-fonts', esc_url_raw($theevent_font_controller->theevent_get_google_font_url($theevent_theme_option['body-font-family'])), array(), null );
			}
		}
	}
	

	
		 
    // Register all scripts
	if( !function_exists('theevent_register_non_admin_scripts') ){
		function theevent_register_non_admin_scripts(){
			
			global $post,$post_id,$theevent_content_raw,$theevent_post_option,$theevent_theme_option;	
			$theevent_content_raw = json_decode(theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'kode_content', true)), true);
			$theevent_content_raw = (empty($theevent_content_raw))? array(): $theevent_content_raw;
			wp_enqueue_script('jquery');
			
			if ( is_singular() && get_option( 'thread_comments' ) ) 	wp_enqueue_script( 'comment-reply' );
				

			
			//BootStrap Script Loaded
			wp_register_script('theevent-bootstrap', THEEVENT_PATH.'/js/bootstrap.min.js', array('jquery'), '1.0', true);
			wp_localize_script('theevent-bootstrap', 'ajax_var', array('url' => admin_url('admin-ajax.php'),'nonce' => wp_create_nonce('ajax-nonce')));
			wp_enqueue_script('theevent-bootstrap');
			wp_enqueue_style( 'theevent-bootstrap-slider', THEEVENT_PATH . '/css/bootstrap-slider.css' );  //Font Awesome
			wp_enqueue_style( 'theevent-custom-style', THEEVENT_PATH . '/css/style-custom.css' );  //Font Awesome
			
			wp_register_script('theevent-bootstrap-slider', THEEVENT_PATH.'/js/bootstrap-slider.js', false, '1.0', true);
			wp_enqueue_script('theevent-bootstrap-slider');
			wp_enqueue_style( 'theevent-events', THEEVENT_PATH . '/css/events.css' );  //events

			
			wp_register_script('theevent-accordion', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/jquery.accordion.js', false, '1.0', true);
			wp_enqueue_script('theevent-accordion');
			
			wp_register_script('theevent-circlechart', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/jquery.circlechart.js', false, '1.0', true);
			wp_enqueue_script('theevent-circlechart');
			
			
			
			
			if(isset($theevent_theme_option['enable-one-page-header-navi'])){
				if($theevent_theme_option['enable-one-page-header-navi'] == 'enable'){
					wp_register_script('theevent-singlepage', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/jquery.singlePageNav.js', false, '1.0', true);
					wp_enqueue_script('theevent-singlepage');
				}
			}
			
			wp_register_script('theevent-filterable', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/filterable.js', false, '1.0', true);
			wp_enqueue_script('theevent-filterable');
			
			
			wp_register_script('theevent-downcount', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/jquery-downcount.js', false, '1.0', true);
			wp_enqueue_script('theevent-downcount');
			
			
			// Product Slider
			if( is_page() &&  theevent_match_page_builder($theevent_content_raw, 'item', 'player-slider') ){
				wp_register_script('owl_carousel', THEEVENT_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', THEEVENT_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Font Awesome
			}	
			
			// Product Slider
			if( is_page() &&  theevent_match_page_builder($theevent_content_raw, 'item', 'portfolio-slider',array('slider-type', 'type-3')) ){
				wp_register_script('owl_carousel', THEEVENT_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', THEEVENT_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Font Awesome
			}
			
			if( is_page() &&  theevent_match_page_builder($theevent_content_raw, 'item', 'gallery',array('style', 'gallery-slider')) ){
				wp_register_script('owl_carousel', THEEVENT_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', THEEVENT_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Font Awesome
			}
			
			if( is_page() &&  theevent_match_page_builder($theevent_content_raw, 'item', 'woo-slider') ){
				wp_register_script('owl_carousel', THEEVENT_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', THEEVENT_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Font Awesome
			}	

			// Product Slider
			if( is_page() &&  theevent_match_page_builder($theevent_content_raw, 'item', 'events', array('event-style', 'event-calendar-view')) ){
				wp_register_script('theevent-moment.min', THEEVENT_PATH.'/framework/include/frontend_assets/calendar/moment.min.js', false, '1.0', true);
				wp_enqueue_script('theevent-moment.min');
				wp_register_script('theevent-fullcalendar', THEEVENT_PATH.'/framework/include/frontend_assets/calendar/fullcalendar.js', false, '1.0', true);
				wp_enqueue_script('theevent-fullcalendar');
				
				wp_enqueue_style( 'theevent-fullcalendar', THEEVENT_PATH . '/framework/include/frontend_assets/calendar/fullcalendar.css' );  //Font Awesome
			}		

			// Product Slider
			if( is_page() &&  theevent_match_page_builder($theevent_content_raw, 'item', 'testimonial', array('testimonial-style', 'simple-view')) ){
				wp_register_script('owl_carousel', THEEVENT_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', THEEVENT_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Font Awesome
			}			
			
			wp_register_script('owl_carousel', THEEVENT_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
			wp_enqueue_script('owl_carousel');
			wp_enqueue_style( 'owl_carousel', THEEVENT_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Font Awesome
			
			if( is_search() || is_archive() || 
				( empty($theevent_theme_option['enable-flex-slider']) || $theevent_theme_option['enable-flex-slider'] != 'disable' ) ||
				( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'blog') ) ||
				( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'post-slider') ) ||
				( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'slider', array('slider-type', 'bxslider')) ) ||
				( is_single() && strpos($theevent_post_option, '"post_media_type":"slider"') )){
				wp_enqueue_style( 'bx-slider', THEEVENT_PATH . '/framework/include/frontend_assets/bxslider/bxslider.css' );  //Font Awesome
				wp_register_script('bx-slider', THEEVENT_PATH.'/framework/include/frontend_assets/bxslider/jquery.bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('bx-slider');
				
			}
			
			
			wp_register_script('theevent-waypoints-min', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/waypoints-min.js', false, '1.0', true);
			wp_enqueue_script('theevent-waypoints-min');
			
			wp_register_script('theevent-bg-moving', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/bg-moving.js', false, '1.0', true);
			wp_enqueue_script('theevent-bg-moving');
			
			
			
			//Custom Script Loaded
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'slider', array('slider-type', 'flexslider')) ){
				wp_enqueue_style( 'flexslider', THEEVENT_PATH . '/framework/include/frontend_assets/flexslider/flexslider.css' );  //Font Awesome
				wp_register_script('theevent-flexslider', THEEVENT_PATH.'/framework/include/frontend_assets/flexslider/jquery.flexslider.js', false, '1.0', true);
				wp_enqueue_script('theevent-flexslider');
			}
			
			wp_enqueue_style( 'flexslider', THEEVENT_PATH . '/framework/include/frontend_assets/flexslider/flexslider.css' );  //Font Awesome
			wp_register_script('theevent-flexslider', THEEVENT_PATH.'/framework/include/frontend_assets/flexslider/jquery.flexslider.js', false, '1.0', true);
			wp_enqueue_script('theevent-flexslider');
			
			//Enable RTL
			if(isset($theevent_theme_option['enable-rtl-layout'])){
				if($theevent_theme_option['enable-rtl-layout'] == 'enable'){
					wp_enqueue_style( 'theevent-rtl', THEEVENT_PATH . '/css/rtl.css' );  //Font Awesome	
				}
			}
			
			
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'wrapper', 'full-size-wrapper', array('type', 'video')) ){
				wp_register_script('theevent-video', THEEVENT_PATH.'/framework/include/frontend_assets/video_background/video.js', false, '1.0', true);
				wp_enqueue_script('theevent-video');
				wp_register_script('theevent-bigvideo', THEEVENT_PATH.'/framework/include/frontend_assets/video_background/bigvideo.js', false, '1.0', true);
				wp_enqueue_script('theevent-bigvideo');
				
			}
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'slider', array('slider-type', 'bxslider')) ){
				wp_enqueue_style( 'bx-slider', THEEVENT_PATH . '/framework/include/frontend_assets/bxslider/bxslider.css' );  //Font Awesome
				wp_register_script('bx-slider', THEEVENT_PATH.'/framework/include/frontend_assets/bxslider/jquery.bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('bx-slider');
			}
			
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'post-slider') ){		
				wp_enqueue_style( 'bx-slider', THEEVENT_PATH . '/framework/include/frontend_assets/bxslider/bxslider.css' );  //Font Awesome
				wp_register_script('bx-slider', THEEVENT_PATH.'/framework/include/frontend_assets/bxslider/jquery.bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('bx-slider');
			}
			
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'gallery') ){
				wp_register_script('theevent-prettyphoto', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('theevent-prettyphoto');
				wp_register_script('theevent-prettypp', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/theevent_pp.js', false, '1.0', true);
				wp_enqueue_script('theevent-prettypp');
			}	
			
			//if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'slider', array('slider-type', 'nivo-slider')) ){
				wp_enqueue_style( 'nivo-slider', THEEVENT_PATH . '/framework/include/frontend_assets/nivo-slider/nivo-slider.css' );  //Font Awesome
				wp_register_script('theevent-nivo-slider', THEEVENT_PATH.'/framework/include/frontend_assets/nivo-slider/jquery.nivo.slider.js', false, '1.0', true);
				wp_enqueue_script('theevent-nivo-slider');
			//}
			
			//CountDown Timer
			if( is_page() && theevent_match_page_builder($theevent_content_raw, 'item', 'upcoming-event') ){
				
				wp_register_script('custom_countdown', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/jquery.countdown.js', false, '1.0', true);
				wp_enqueue_script('custom_countdown');		
				
				wp_enqueue_style( 'custom_countdown', THEEVENT_PATH . '/framework/include/frontend_assets/default/css/countdown.css' );  //Count Down timer
				
			}	
			
			
			
			wp_register_script('theevent-easing', THEEVENT_PATH.'/framework/include/frontend_assets/jquery.easing.js', false, '1.0', true);
			wp_enqueue_script('theevent-easing');
			
			if(isset($theevent_theme_option['enable-nice-scroll'])){
			if($theevent_theme_option['enable-nice-scroll'] == 'enable'){
				wp_register_script('theevent-nicescroll', THEEVENT_PATH.'/framework/include/frontend_assets/default/js/jquery.nicescroll.min.js', false, '1.0', true);
				wp_enqueue_script('theevent-nicescroll');
			}
		}
			
			wp_register_script('theevent-functions', THEEVENT_PATH.'/js/functions.js', false, '1.0', true);
			wp_enqueue_script('theevent-functions');
			
		}
	}
	
	// set the global variable based on the opened page, post, ...
	add_action('wp', 'theevent_define_global_variable');
	if( !function_exists('theevent_define_global_variable') ){
		function theevent_define_global_variable(){
			global $post;		
			if( is_page() ){
				global $theevent_content_raw,$theevent_post_option;				
				$theevent_content_raw = json_decode(theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'kode_content', true)), true);
				$theevent_content_raw = (empty($theevent_content_raw))? array(): $theevent_content_raw;
				$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true));
			}else if( is_single() || (!empty($post)) ){
				global $theevent_post_option;			
				$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true));
			}
			
			
		}
	}