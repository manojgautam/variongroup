<?php
	/*	
	*	Kodeforest Import Variable Setting
	*/

	
	
	// init the $theevent_theme_option value and customizer value upon activation	
	
	add_action( 'after_switch_theme', 'theevent_get_default_themeoption' );
	if( !function_exists('theevent_get_default_themeoption') ){
		function theevent_get_default_themeoption() {
			$theevent_theme_option = get_option('theevent_admin_option', array());
			if(empty($theevent_theme_option)){
				
				$default_file = THEEVENT_LOCAL_PATH . '/framework/external/import_export/theme_options.txt';
				$default_admin_option = 'logo=4&logo-width=&logo-height=&logo-top-margin=&logo-bottom-margin=&favicon-id=&kode-top-bar-trans=transparent&enable-meet-btn=disable&enable-meet-btn=enable&meet-btn-text=Become+a+Member&meet-page=314&meet-btn-background-color=%2300a4ef&meet-btn-hover-background-color=%234abbef&meet-btn-color=%23ffffff&meet-btn-hover-txt-color=%23ffffff&enable-sticky-menu=disable&enable-sticky-menu=enable&enable-one-page-header-navi=disable&enable-breadcrumbs=disable&enable-breadcrumbs=enable&navi-color=%23ffffff&navi-hover-bg=%2300a4ef&navi-hover-color=%23ffffff&navi-dropdown-bg=%23ffffff&navi-dropdown-color=%23000000&navi-dropdown-hover=%23ffffff&navi-dropdown-hover-bg=%2300a4ef&color-scheme-one=%2300a4ef&enable-boxed-style=full-style&kode-body-style=body-color&body-bg-color=%23ffffff&body-background-image=&body-background-pattern=1&kode-body-position=body-scroll&enable-nice-scroll=disable&nice-scroll-color=%23ffffff&nice-scroll-radius=5px&nice-scroll-width=12px&nice-scroll-touch=true&enable-rtl-layout=disable&enable-responsive-mode=disable&enable-responsive-mode=enable&video-ratio=16%2F9&show-footer=disable&show-footer=enable&footer-background-color=%23000&footer-background-opacity=0.6&footer-layout=3&footer-text-map=&show-copyright=disable&show-copyright=enable&kode-copyright-text=All+Rights+Reserved+KODEFOREST.&kode-back-top=disable&kode-back-top=enable&default-page-title=&default-post-title-background=&default-search-archive-title-background=&default-404-title-background=307&ticket-money-format=NUMBER+USD&paypal-action=paypal&paypal-recipient-name=&paypal-recipient-email=&enable-paypal=disable&enable-paypal=enable&paypal-currency-code=USD&archive-sidebar-template=no-sidebar&archive-sidebar-left=blog&archive-sidebar-right=blog&archive-blog-style=blog-full&archive-col-size=3&archive-num-excerpt=25&theevent-post-thumbnail-size=kode-full-slider&post-sidebar-template=no-sidebar&post-sidebar-left=blog&post-sidebar-right=blog&single-post-feature-image=disable&single-post-feature-image=enable&single-post-date=disable&single-post-date=enable&single-post-author=disable&single-post-author=enable&single-post-comments=disable&single-post-comments=enable&single-post-tags=disable&single-post-tags=enable&single-post-category=disable&single-post-category=enable&single-next-pre=disable&single-next-pre=enable&single-recommended-post=disable&single-recommended-post=enable&kode-recommended-thumbnail-size=thumbnail&single-event-feature-image=disable&single-event-feature-image=enable&single-event-feature-size=kode-full-slider&single-event-date=disable&single-event-date=enable&single-event-organizer=disable&single-event-organizer=enable&single-event-tags=disable&single-event-tags=enable&speaker-thumbnail-size=thumbnail&single-related-events=disable&single-related-events=enable&single-related-events-meta=disable&single-related-events-meta=enable&single-event-related-size=theevent-team-size&single-event-comments=disable&single-event-comments=enable&single-team-feature-image=disable&single-team-feature-image=enable&thumbnail-size=theevent-post-thumbnail-size&single-team-comments=disable&single-team-comments=enable&woo-post-title=disable&woo-post-title=enable&woo-post-price=disable&woo-post-price=enable&woo-post-variable-price=disable&woo-post-variable-price=enable&woo-post-related=disable&woo-post-related=enable&woo-post-sku=disable&woo-post-sku=enable&woo-post-category=disable&woo-post-category=enable&woo-post-tags=disable&woo-post-tags=enable&woo-post-outofstock=disable&woo-post-outofstock=enable&woo-post-saleicon=disable&woo-post-saleicon=enable&woo-list-cart-btn=disable&woo-list-cart-btn=enable&woo-list-title=disable&woo-list-title=enable&woo-list-price=disable&woo-list-price=enable&woo-list-rating=disable&woo-list-rating=enable&all-products-per-row=3&all-products-sidebar=no-sidebar&all-products-sidebar-left=blog&all-products-sidebar-right=blog&delicious-header-social=&digg-header-social=&facebook-header-social=%23&flickr-header-social=&google-plus-header-social=%23&linkedin-header-social=%23&pinterest-header-social=%23&skype-header-social=&stumble-upon-header-social=&tumblr-header-social=&twitter-header-social=%23&vimeo-header-social=&youtube-header-social=&enable-social-share=disable&enable-social-share=enable&digg-share=disable&digg-share=enable&facebook-share=disable&facebook-share=enable&google-plus-share=disable&google-plus-share=enable&linkedin-share=disable&linkedin-share=enable&my-space-share=disable&my-space-share=enable&pinterest-share=disable&pinterest-share=enable&reddit-share=disable&reddit-share=enable&stumble-upon-share=disable&stumble-upon-share=enable&twitter-share=disable&twitter-share=enable&font-heading-h1=%23000&font-heading-h2=%23000&font-heading-h3=%23000&font-heading-h4=%23000&font-heading-h5=%23000&text-color=%23000&link-color=%23fff&link-hover-color=%23000&button-color=%23f37936&button-text-color=%23000&button-border-color=%23fff&blog-title-color=%23000&blog-date-color=%23000&navi-font-family=Montserrat&heading-font-family=Montserrat&body-font-family=Open+Sans&content-font-size=14&h1-font-size=30&h2-font-size=25&h3-font-size=20&h4-font-size=18&h5-font-size=16&h6-font-size=15&breadcrumbs-home=Home&pre-page=previous&next-page=next&newsletter-text=Subscribe+Email&newsletter-btn=Subscribe+Now&comments-reply=Leave+a+Reply&comments-btn=Post+Comments&blog-tags=Tags&blog-category=Category&blog-published-by=Published+by&blog-read-more=Read+More&blog-see-more=See+More&blog-comments=Comments&error-title=404&error-subtitle=WE+ARE+REALLY+SORRY&error-text=We+are+not+sure+what+you%22re+looking+for...+would+you+like+to+go+to+the+HOMEPAGE+instead%3F&event-days=Days&event-hours=Hours&event-min=Minutes&event-sec=Seconds&event-start-date=Start+Date&event-end-date=End+Date&error-ticket-type=Event+Tickets+Type&error-ticket-price=Price&error-ticket-qty=QTY&error-ticket-book=Book&speaker-address=address&speaker-phone=Phone&speaker-email=Email&bx-slider-effects=slide&bx-min-slide=1&bx-max-slide=1&bx-slide-margin=0&bx-arrow=disable&bx-arrow=enable&bx-pause-time=7000&bx-slide-speed=600&flex-slider-effects=fade&flex-pause-time=7000&flex-slide-speed=600&nivo-slider-effects=sliceDownRight&nivo-pause-time=7000&nivo-slide-speed=600&caption-title-color=%23ffffff&caption-background-color-switch=disable&caption-background-color=%23ffffff&title-font-size=30&caption-desc-color=%23ffffff&caption-font-size=30&caption-btn-color=%23000000&caption-btn-color-hover=%23ffffff&caption-btn-color-border=%23ffffff&caption-btn-color-bg=%23ffffff&caption-btn-hover-bg=%23ef4a2b&caption-btn-arrow-color=%23ffffff&caption-btn-arrow-hover=%23000000&caption-btn-arrow-bg=%23000000&caption-btn-arrow-hover-bg=%23ffffff&sidebar-tbn=disable&sidebar-tbn=enable&sidebar-bg-color=%23f9f9f9&sidebar-padding-top=30&sidebar-padding-bottom=30&sidebar-padding-left=30&sidebar-padding-right=30&sidebar-element%5B%5D=blog&sidebar-element%5B%5D=contact&sidebar-element%5B%5D=event+detail&sidebar-element%5B%5D=news+list&sidebar-element%5B%5D=Shortcodes&sidebar-element%5B%5D=features&sidebar-element%5B%5D=Widgets+All&mail-chimp-api=f6a297a228a01378bdaa75ce2a617d5c-us1&mail-chimp-listid=b265a7b125&home-seo-meta-button=disable&home-seo-meta-button=enable&seo-title-sep=-&seo-website-slug=Democracy&seo-title-transform=capitalize&seo-meta-bots=disable&seo-meta-bots=enable&seo-meta-alexa=&seo-meta-bing=&seo-meta-google=&seo-meta-yandex=&home-seo-meta-title=Democracy+Political+Wordpress+Theme&home-seo-meta-description=&home-seo-meta-keyword=&archive-seo-meta-title=&archive-seo-meta-description=&archive-seo-meta-keyword=&error-seo-meta-title=&error-seo-meta-description=&error-seo-meta-keyword=&search-seo-meta-title=&search-seo-meta-description=&search-seo-meta-keyword=&maintenance-seo-meta-title=&maintenance-seo-meta-description=&maintenance-seo-meta-keyword=';
				parse_str(theevent_stripslashes($default_admin_option), $k_option ); 
				$k_option = theevent_stripslashes($k_option);	
				update_option('theevent_admin_option', $k_option);
			}
		}
	}
	
	
	
	if( !function_exists('theevent_get_default_reset') ){
		function theevent_get_default_reset() {
			
			$default_file = THEEVENT_LOCAL_PATH . '/framework/external/import_export/theme_options.txt';
			$default_admin_option = 'logo=4&logo-width=&logo-height=&logo-top-margin=&logo-bottom-margin=&favicon-id=&kode-top-bar-trans=transparent&enable-meet-btn=disable&enable-meet-btn=enable&meet-btn-text=Become+a+Member&meet-page=314&meet-btn-background-color=%2300a4ef&meet-btn-hover-background-color=%234abbef&meet-btn-color=%23ffffff&meet-btn-hover-txt-color=%23ffffff&enable-sticky-menu=disable&enable-sticky-menu=enable&enable-one-page-header-navi=disable&enable-breadcrumbs=disable&enable-breadcrumbs=enable&navi-color=%23ffffff&navi-hover-bg=%2300a4ef&navi-hover-color=%23ffffff&navi-dropdown-bg=%23ffffff&navi-dropdown-color=%23000000&navi-dropdown-hover=%23ffffff&navi-dropdown-hover-bg=%2300a4ef&color-scheme-one=%2300a4ef&enable-boxed-style=full-style&kode-body-style=body-color&body-bg-color=%23ffffff&body-background-image=&body-background-pattern=1&kode-body-position=body-scroll&enable-nice-scroll=disable&nice-scroll-color=%23ffffff&nice-scroll-radius=5px&nice-scroll-width=12px&nice-scroll-touch=true&enable-rtl-layout=disable&enable-responsive-mode=disable&enable-responsive-mode=enable&video-ratio=16%2F9&show-footer=disable&show-footer=enable&footer-background-color=%23000&footer-background-opacity=0.6&footer-layout=3&footer-text-map=&show-copyright=disable&show-copyright=enable&kode-copyright-text=All+Rights+Reserved+KODEFOREST.&kode-back-top=disable&kode-back-top=enable&default-page-title=&default-post-title-background=&default-search-archive-title-background=&default-404-title-background=307&ticket-money-format=NUMBER+USD&paypal-action=paypal&paypal-recipient-name=&paypal-recipient-email=&enable-paypal=disable&enable-paypal=enable&paypal-currency-code=USD&archive-sidebar-template=no-sidebar&archive-sidebar-left=blog&archive-sidebar-right=blog&archive-blog-style=blog-full&archive-col-size=3&archive-num-excerpt=25&theevent-post-thumbnail-size=kode-full-slider&post-sidebar-template=no-sidebar&post-sidebar-left=blog&post-sidebar-right=blog&single-post-feature-image=disable&single-post-feature-image=enable&single-post-date=disable&single-post-date=enable&single-post-author=disable&single-post-author=enable&single-post-comments=disable&single-post-comments=enable&single-post-tags=disable&single-post-tags=enable&single-post-category=disable&single-post-category=enable&single-next-pre=disable&single-next-pre=enable&single-recommended-post=disable&single-recommended-post=enable&kode-recommended-thumbnail-size=thumbnail&single-event-feature-image=disable&single-event-feature-image=enable&single-event-feature-size=kode-full-slider&single-event-date=disable&single-event-date=enable&single-event-organizer=disable&single-event-organizer=enable&single-event-tags=disable&single-event-tags=enable&speaker-thumbnail-size=thumbnail&single-related-events=disable&single-related-events=enable&single-related-events-meta=disable&single-related-events-meta=enable&single-event-related-size=theevent-team-size&single-event-comments=disable&single-event-comments=enable&single-team-feature-image=disable&single-team-feature-image=enable&thumbnail-size=theevent-post-thumbnail-size&single-team-comments=disable&single-team-comments=enable&woo-post-title=disable&woo-post-title=enable&woo-post-price=disable&woo-post-price=enable&woo-post-variable-price=disable&woo-post-variable-price=enable&woo-post-related=disable&woo-post-related=enable&woo-post-sku=disable&woo-post-sku=enable&woo-post-category=disable&woo-post-category=enable&woo-post-tags=disable&woo-post-tags=enable&woo-post-outofstock=disable&woo-post-outofstock=enable&woo-post-saleicon=disable&woo-post-saleicon=enable&woo-list-cart-btn=disable&woo-list-cart-btn=enable&woo-list-title=disable&woo-list-title=enable&woo-list-price=disable&woo-list-price=enable&woo-list-rating=disable&woo-list-rating=enable&all-products-per-row=3&all-products-sidebar=no-sidebar&all-products-sidebar-left=blog&all-products-sidebar-right=blog&delicious-header-social=&digg-header-social=&facebook-header-social=%23&flickr-header-social=&google-plus-header-social=%23&linkedin-header-social=%23&pinterest-header-social=%23&skype-header-social=&stumble-upon-header-social=&tumblr-header-social=&twitter-header-social=%23&vimeo-header-social=&youtube-header-social=&enable-social-share=disable&enable-social-share=enable&digg-share=disable&digg-share=enable&facebook-share=disable&facebook-share=enable&google-plus-share=disable&google-plus-share=enable&linkedin-share=disable&linkedin-share=enable&my-space-share=disable&my-space-share=enable&pinterest-share=disable&pinterest-share=enable&reddit-share=disable&reddit-share=enable&stumble-upon-share=disable&stumble-upon-share=enable&twitter-share=disable&twitter-share=enable&font-heading-h1=%23000&font-heading-h2=%23000&font-heading-h3=%23000&font-heading-h4=%23000&font-heading-h5=%23000&text-color=%23000&link-color=%23fff&link-hover-color=%23000&button-color=%23f37936&button-text-color=%23000&button-border-color=%23fff&blog-title-color=%23000&blog-date-color=%23000&navi-font-family=Montserrat&heading-font-family=Montserrat&body-font-family=Open+Sans&content-font-size=14&h1-font-size=30&h2-font-size=25&h3-font-size=20&h4-font-size=18&h5-font-size=16&h6-font-size=15&breadcrumbs-home=Home&pre-page=previous&next-page=next&newsletter-text=Subscribe+Email&newsletter-btn=Subscribe+Now&comments-reply=Leave+a+Reply&comments-btn=Post+Comments&blog-tags=Tags&blog-category=Category&blog-published-by=Published+by&blog-read-more=Read+More&blog-see-more=See+More&blog-comments=Comments&error-title=404&error-subtitle=WE+ARE+REALLY+SORRY&error-text=We+are+not+sure+what+you%22re+looking+for...+would+you+like+to+go+to+the+HOMEPAGE+instead%3F&event-days=Days&event-hours=Hours&event-min=Minutes&event-sec=Seconds&event-start-date=Start+Date&event-end-date=End+Date&error-ticket-type=Event+Tickets+Type&error-ticket-price=Price&error-ticket-qty=QTY&error-ticket-book=Book&speaker-address=address&speaker-phone=Phone&speaker-email=Email&bx-slider-effects=slide&bx-min-slide=1&bx-max-slide=1&bx-slide-margin=0&bx-arrow=disable&bx-arrow=enable&bx-pause-time=7000&bx-slide-speed=600&flex-slider-effects=fade&flex-pause-time=7000&flex-slide-speed=600&nivo-slider-effects=sliceDownRight&nivo-pause-time=7000&nivo-slide-speed=600&caption-title-color=%23ffffff&caption-background-color-switch=disable&caption-background-color=%23ffffff&title-font-size=30&caption-desc-color=%23ffffff&caption-font-size=30&caption-btn-color=%23000000&caption-btn-color-hover=%23ffffff&caption-btn-color-border=%23ffffff&caption-btn-color-bg=%23ffffff&caption-btn-hover-bg=%23ef4a2b&caption-btn-arrow-color=%23ffffff&caption-btn-arrow-hover=%23000000&caption-btn-arrow-bg=%23000000&caption-btn-arrow-hover-bg=%23ffffff&sidebar-tbn=disable&sidebar-tbn=enable&sidebar-bg-color=%23f9f9f9&sidebar-padding-top=30&sidebar-padding-bottom=30&sidebar-padding-left=30&sidebar-padding-right=30&sidebar-element%5B%5D=blog&sidebar-element%5B%5D=contact&sidebar-element%5B%5D=event+detail&sidebar-element%5B%5D=news+list&sidebar-element%5B%5D=Shortcodes&sidebar-element%5B%5D=features&sidebar-element%5B%5D=Widgets+All&mail-chimp-api=f6a297a228a01378bdaa75ce2a617d5c-us1&mail-chimp-listid=b265a7b125&home-seo-meta-button=disable&home-seo-meta-button=enable&seo-title-sep=-&seo-website-slug=Democracy&seo-title-transform=capitalize&seo-meta-bots=disable&seo-meta-bots=enable&seo-meta-alexa=&seo-meta-bing=&seo-meta-google=&seo-meta-yandex=&home-seo-meta-title=Democracy+Political+Wordpress+Theme&home-seo-meta-description=&home-seo-meta-keyword=&archive-seo-meta-title=&archive-seo-meta-description=&archive-seo-meta-keyword=&error-seo-meta-title=&error-seo-meta-description=&error-seo-meta-keyword=&search-seo-meta-title=&search-seo-meta-description=&search-seo-meta-keyword=&maintenance-seo-meta-title=&maintenance-seo-meta-description=&maintenance-seo-meta-keyword=';
			parse_str(theevent_stripslashes($default_admin_option), $k_option ); 
			return $k_option = theevent_stripslashes($default_admin_option);	
			// update_option(THEEVENT_SMALL_TITLE . '_admin_option', $k_option);			
		}
	}
	
	
	if( !function_exists('theevent_get_widget_area') ){
		function theevent_get_widget_area(){
			$development = get_option('sidebars_widgets');
			unset($development['array_version']);
				foreach($development as $key=>$value){
					$newval = str_replace('wp_inactive_widgets','',$key);
					$widgets[] = str_replace('orphaned_widgets_1','',$newval);
				}
				return array_filter($widgets);
				
		}
	}
	
	if( !function_exists('theevent_str_before') ){
		function theevent_str_before($subject, $needle)
		{
			$p = strpos($subject, $needle);
			return substr($subject, 0, $p);
		}
	}
	
	if( !function_exists('theevent_get_widget_name_value') ){
		function theevent_get_widget_name_value(){
			$development_myval = get_option('sidebars_widgets');
			foreach(theevent_get_widget_area() as $val){
				foreach($development_myval[$val] as $keys=>$values){
					$string_val = theevent_str_before($values, "-");
					$wid_val[$string_val] = 'widget_'.$string_val;
				}
				
			}
			return $wid_val;
				
		}
	}
	
	// $widget_array = theevent_get_widget_name_value();
	// foreach($widget_array as $widget){
		// echo "'".$widget."'".',';
	// }	

	// $widget_data = array();
	// $widget_list = array('sidebars_widgets', 'widget_search','widget_recent','widget_archives','widget_text','widget_kode_question_widget','widget_nav_menu');
	// foreach($widget_list as $widget){
		// $widget_data[$widget] = get_option($widget);
	// }
	// $widget_file = THEEVENT_LOCAL_PATH . '/framework/external/import_export/kodeforest-importer-widget.txt';
	// $file_stream = @fopen($widget_file, 'w');
	// fwrite($file_stream, serialize($widget_data));
	// fclose($file_stream);	
	
	// echo '$show_on_front = ';
	// echo get_option('show_on_front').';';
	
	// echo '$page_on_front = ';
	// echo get_option('page_on_front').';';
	
	// $theme_name = 'theme_mods_'.get_option('template');
	// echo '$'.$theme_name.' = ';
	// var_export(get_option($theme_name)).';';