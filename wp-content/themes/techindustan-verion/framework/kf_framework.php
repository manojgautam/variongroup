<?php 
	/*	
	*	Kodeforest Framework File
	*	---------------------------------------------------------------------
	*	This file includes the functions to run the plugins - Theme Options
	*	---------------------------------------------------------------------
	*/

	if(is_ssl()){
		define('THEEVENT_HTTP', 'https://');
	}else{
		define('THEEVENT_HTTP', 'http://');
	}
	
	//Data validation HTMl
	if( !function_exists('theevent_esc_html') ){	
		function theevent_esc_html ($html) {
			return esc_html($html);
		}
	}	
	
	//Data Validation
	if( !function_exists('theevent_esc_html_excerpt') ){	
		function theevent_esc_html_excerpt ($html) {
			return esc_html(strip_tags($html));
		}
	}
	
	
	$theevent_theme_option = get_option('theevent_admin_option', array());	
	
	
	
	//Define the content width - Clearing the ThemeCheck
	$theevent_theme_option['content-width'] = 960;
	
	//Default Variables
	$theevent_gallery_id = 0;
	$theevent_lightbox_id = 0;
	$theevent_crop_video = false;
	$theevent_excerpt_length = 55;
	$theevent_excerpt_read_more = true;
	$theevent_wrapper_id = 0;
	
	
	
	$theevent_thumbnail_size = array(
		'theevent-full-slider' => array('width'=>1600, 'height'=>900, 'crop'=>true),
		'theevent-post-thumbnail-size' => array('width'=>385, 'height'=>545, 'crop'=>true),
		'theevent-team-size' => array('width'=>350, 'height'=>350, 'crop'=>true),
		'theevent-serv-size' => array('width'=>345, 'height'=>315, 'crop'=>true),
		'theevent-small-serv-size' => array('width'=>260, 'height'=>325, 'crop'=>true),
		'theevent-blog-size' => array('width'=>570, 'height'=>300, 'crop'=>true),
		'theevent-post-slider-side' => array('width'=>770, 'height'=>330, 'crop'=>true),
		'theevent-blog-post-size' => array('width'=>1170, 'height'=>350, 'crop'=>true),
	);
	
	$theevent_thumbnail_size = apply_filters('custom-thumbnail-size', $theevent_thumbnail_size);
	// Create Sizes on the theme activation
	add_action( 'after_setup_theme', 'theevent_define_thumbnail_size' );
	if( !function_exists('theevent_define_thumbnail_size') ){
		function theevent_define_thumbnail_size(){
			add_theme_support( 'post-thumbnails' );
		
			global $theevent_thumbnail_size;		
			foreach($theevent_thumbnail_size as $theevent_size_slug => $theevent_size){
				add_image_size($theevent_size_slug, $theevent_size['width'], $theevent_size['height'], $theevent_size['crop']);
			}
		}
	}
	
	// add the image size filter to ThemeOptions
	add_filter('image_size_names_choose', 'theevent_define_custom_size_image');
	function theevent_define_custom_size_image( $sizes ){	
		$additional_size = array();
		
		global $theevent_thumbnail_size;
		foreach($theevent_thumbnail_size as $theevent_size_slug => $theevent_size){
			$additional_size[$theevent_size_slug] = $theevent_size_slug;
		}
		
		return array_merge($sizes, $additional_size);
	}
	
	// Get All The Sizes
	function theevent_get_thumbnail_list(){
		global $theevent_thumbnail_size;
		
		$sizes = array();
		foreach( get_intermediate_image_sizes() as $size ){
			if(in_array( $size, array( 'thumbnail', 'medium', 'large' )) ){
				$sizes[$size] = $size . ' -- ' . get_option($size . '_size_w') . 'x' . get_option($size . '_size_h');
			}else if( !empty($theevent_thumbnail_size[$size]) ){
				$sizes[$size] = $size . ' -- ' . $theevent_thumbnail_size[$size]['width'] . 'x' . $theevent_thumbnail_size[$size]['height'];
			}else{
			
			}
		}
		$sizes['full'] = esc_html__('full size (Real Images)', 'the-event');
		
		return $sizes;
	}	
	
	
	// create page builder
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kf_function_utility.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/kf_function_regist.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kode-demo-options.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kf-file-system.php');

	
	
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kf_function_utility.php');	
	
	
	// Create Theme Options
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kf_pagebuilder.php');	
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kf_themeoption.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/kode-include-script.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/kode_google_fonts.php');
	
	// Frontend Assets & functions
		
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kode_loadstyle.php');
	

	//Events
	if(class_exists('EM_Events')){
		include_once( THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kode-events-options.php');
	}
	//WooCommerce
	// if(class_exists('WooCommerce')){
		// include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kode-woo-options.php');
	// }
	//IgnitionDeck
	if(class_exists('Deck')){
		include_once( THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kode-igni-options.php');
	}
	
	
	// create page options
	include_once( THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/kode-post-options.php');
	
	//Frontend
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/elements/kf_media_center.php');
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/elements/kode_page_elements.php');
	include_once( THEEVENT_LOCAL_PATH . '/framework/include/kode_front_func/elements/kf_blogging.php');		
	
	// page builder template
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/kf_themeoptions_html.php');	
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/kf_theme_meta.php');
	
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/kf_pagebuilder_backend.php');	
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/kf_pagebuilder_meta.php');	
	include_once(THEEVENT_LOCAL_PATH . '/framework/include/kode_meta/kf_pagebuilder_scripts.php');	
	
	
?>