<?php
if( !function_exists('theevent_get_post_news') ){
	function theevent_get_post_news(){	
		if( in_array(get_post_format(), array('aside', 'link', 'quote')) ){
			get_template_part('single/content', get_post_format());
		}else{
			
			global $theevent_post_settings;
			if( empty($theevent_post_settings['blog-style']) || $theevent_post_settings['blog-style'] == 'blog-full' ){
				get_template_part('single/content-news-full');
			}else if( strpos($theevent_post_settings['blog-style'], 'blog-grid') !== false ){
				get_template_part('single/content-news-grid');
			}else if( strpos($theevent_post_settings['blog-style'], 'blog-medium') !== false ){
				get_template_part('single/content-news-medium');
			}else{
				get_template_part('single/content-news-grid');
			}
			
		} 
	}
}	
theevent_get_post_news();