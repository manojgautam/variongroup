<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('theevent_get_post_modern_grid') ){
	function theevent_get_post_modern_grid(){
	global $theevent_post_settings,$post,$theevent_post_option,$theevent_theme_option; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('kode-item kode-blog-2'); ?>>
		<div class="blog-2-side-bar">
			<ul>
				<li>
					<strong><?php echo get_the_date('d')?></strong>	
					<a href="<?php echo esc_url(get_permalink())?>"><?php echo get_the_date('m Y')?></a>
				</li>
				<li>
					<strong><i class="fa fa-comment-o"></i></strong>
					<?php echo theevent_get_blog_info(array('comment'), false, '','span');?>				
				</li>
				<li>
					<strong><i class="fa fa-heart-o"></i></strong>
					<?php echo theevent_get_blog_info(array('views'), false, '','span');?>
				</li>
			</ul>
		</div>
		<div class="kode-blog-style">
			<figure>
				<?php get_template_part('single/thumbnail', get_post_format()); ?>
				<a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-plus"></i></a>
			</figure>
			<div class="kode-thumb-caption">
				<h4><?php the_title();?></h4>
				<?php 
				if( is_single() || $theevent_post_settings['excerpt'] < 0 || $theevent_post_settings['excerpt'] == 'full'){
					echo '<div class="kode-blog-content">';
					echo theevent_content_filter($theevent_post_settings['content'], true);
					wp_link_pages( array( 
						'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-event' ) . '</span>', 
						'after' => '</div>', 
						'link_before' => '<span>', 
						'link_after' => '</span>' )
					);
					echo '</div>';
				}else if( $theevent_post_settings['excerpt'] != 0 ){
					echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . esc_attr(get_the_excerpt()) . '</p><span><a href="'.esc_url(get_permalink()).'">View More</a></span>
				</div>';
				}
			?>	
			</div>
		</div>
		<div class="newevent-header">
			<?php get_template_part('single/thumbnail', get_post_format()); ?>
			<div class="time-newevent">Feb <span>21</span></div>
		</div>
	</article>
<?php }

}
theevent_get_post_modern_grid();