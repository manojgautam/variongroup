<?php
/**
 * The template for displaying audio post format
 */
 
if( !function_exists('theevent_get_post_audio') ){
	function theevent_get_post_audio(){
		
		global $theevent_post_settings, $theevent_theme_option,$post;
		
		
		$post_format_data = '';
		$content = trim(get_the_content(esc_html__('Read More', 'the-event')));		
		if(preg_match('#^https?://\S+#', $content, $match)){ 				
			$post_format_data = do_shortcode('[audio src="' . $match[0] . '" ][/audio]');
			$theevent_post_settings['content'] = substr($content, strlen($match[0]));					
		}else if(preg_match('#^\[audio\s.+\[/audio\]#', $content, $match)){ 
			$post_format_data = do_shortcode($match[0]);
			$theevent_post_settings['content'] = substr($content, strlen($match[0]));
		}else{
			$theevent_post_settings['content'] = $content;
		}	

		if ( !empty($post_format_data) ){
			echo '<div class="kode-blog-thumbnail kode-audio">' . $post_format_data . '</div>';
		}else{
			$k_post_option = json_decode(get_post_meta($post->ID, 'post-option', true), true);		
			
			$thumbnail_size = (empty($theevent_post_settings['thumbnail-size']))? $theevent_theme_option['theevent-post-thumbnail-size']: $theevent_post_settings['thumbnail-size'];
			
			//Get Thumbnail Width and Height
			$theevent_img_size = theevent_get_video_size($thumbnail_size);
			$theevent_width = $theevent_img_size['width'];
			$theevent_height = $theevent_img_size['height'];
			
			// Get Slider Images
			$k_post_option['slider'] = (empty($k_post_option['slider']))? ' ': $k_post_option['slider'];
			$raw_slider_data = theevent_decode_stopbackslashes($k_post_option['slider']);	
			$filter_slider_data = json_decode(theevent_stripslashes($raw_slider_data), true);		
			
			//Get Media Type Selected
			$k_post_option['post_media_type'] = (empty($k_post_option['post_media_type']))? ' ': $k_post_option['post_media_type'];
			if(!empty($k_post_option['theevent_audio'])){
				if(strpos($k_post_option['theevent_audio'],'soundcloud')){
					echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[soundcloud url="'.esc_url($k_post_option['theevent_audio']).'" comments="yes" auto_play="no" color="#ff7700" width="100%" height="'.esc_attr($theevent_height).'px"][/soundcloud]') . '</div>';				
				}else{				
					$theevent_get_image = theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
					if(!empty($theevent_get_image)){
						echo '<div class="kode-blog-thumbnail">';
							if( is_single() ){
									echo theevent_get_image(get_post_thumbnail_id(), $thumbnail_size, true);	
									echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['theevent_audio'].'"][/audio]') . '</div>';
							}else{
								$temp_option = json_decode(get_post_meta(get_the_ID(), 'post-option', true), true);
								echo theevent_get_image(get_post_thumbnail_id(), $thumbnail_size,true);
								echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['theevent_audio'].'"][/audio]') . '</div>';							
								if( is_sticky() ){
									echo '<div class="kode-sticky-banner">';
									echo '<i class="fa fa-bullhorn" ></i>';
									echo esc_html__('Sticky Post', 'the-event');
									echo '</div>';
								}
							}						
						echo '</div>';
					}else{
						if(!empty($k_post_option['theevent_audio'])){
							echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['theevent_audio'].'"][/audio]') . '</div>';
						}
					}
					
				}
			}
		} 
	}		
}
theevent_get_post_audio();