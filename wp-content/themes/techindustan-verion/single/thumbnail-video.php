<?php
/**
 * The template for displaying video post format
 */
if( !function_exists('theevent_get_content_video') ){
	function theevent_get_content_video(){
		if( !is_single() ){ 
			global $theevent_post_settings,$post,$theevent_theme_option,$theevent_post_option; 
			$theevent_post_settings['content'] = get_the_content();
		}else{
			global $theevent_post_settings, $theevent_theme_option, $theevent_post_option,$post;
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			$theevent_post_settings = $theevent_post_option;
		}

		$post_format_data = '';
		$content = trim(get_the_content(esc_html__('Read More', 'the-event')));	
		if(preg_match('#^https?://\S+#', $content, $match)){ 		
			if( is_single() || $theevent_post_settings['blog-style'] == 'blog-full' ){
				$post_format_data = theevent_get_video($match[0], 'full');
			}else{
				$post_format_data = theevent_get_video($match[0], $theevent_post_settings['thumbnail-size']);
				
			}
			$theevent_post_settings['content'] = substr($content, strlen($match[0]));				
		}else if(preg_match('#^\[video\s.+\[/video\]#', $content, $match)){ 
			$post_format_data = do_shortcode($match[0]);
			$theevent_post_settings['content'] = substr($content, strlen($match[0]));
		}else if(preg_match('#^\[embed.+\[/embed\]#', $content, $match)){ 
			global $theevent_wp_embed; 
			$post_format_data = $theevent_wp_embed->run_shortcode($match[0]);
			$theevent_post_settings['content'] = substr($content, strlen($match[0]));
		}else{
			$theevent_post_settings['content'] = $content;
		}

		if ( !empty($post_format_data) ){
			echo '<div class="kode-blog-thumbnail kode-video">' . $post_format_data . '</div>';
		}else{
			$k_post_option = json_decode(get_post_meta($post->ID, 'post-option', true), true);		
			
			$thumbnail_size = (empty($theevent_post_settings['thumbnail-size']))? $theevent_theme_option['theevent-post-thumbnail-size']: $theevent_post_settings['thumbnail-size'];
			
			//Get Thumbnail Width and Height
			$theevent_img_size = theevent_get_video_size($thumbnail_size);
			$theevent_width = $theevent_img_size['width'];
			$theevent_height = $theevent_img_size['height'];
			
			// Get Slider Images
			$k_post_option['slider'] = (empty($k_post_option['slider']))? ' ': $k_post_option['slider'];
			$raw_slider_data = theevent_decode_stopbackslashes($k_post_option['slider']);	
			$filter_slider_data = json_decode(theevent_stripslashes($raw_slider_data), true);		
			
			//Get Media Type Selected
			$k_post_option['post_media_type'] = (empty($k_post_option['post_media_type']))? ' ': $k_post_option['post_media_type'];
			if(!empty($k_post_option['theevent_video'])){
				echo '<div class="kode-blog-thumbnail kode-video">' . theevent_get_video($k_post_option['theevent_video'],$thumbnail_size).'</div>';	
			}
		}
	}	
}
theevent_get_content_video();