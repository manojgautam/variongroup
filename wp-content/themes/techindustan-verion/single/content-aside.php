<?php
/**
 * The template for displaying posts in the Aside post format
 */
if( !function_exists('theevent_get_content_aside') ){
	function theevent_get_content_aside(){
	if( !is_single() ){ 
		global $theevent_post_settings;
	}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('kode-blog-info'); ?>>
	<div class="kode-blog-content">
		<?php 
			if( is_single() || $theevent_post_settings['excerpt'] < 0 ){
				echo theevent_content_filter(get_the_content(esc_html__( 'Read More', 'the-event' )), true); 
			}else{
				echo theevent_content_filter(get_the_content(esc_html__( 'Read More', 'the-event' ))); 
			}
		?>
	</div>
</article>
<?php }
}
theevent_get_content_aside();