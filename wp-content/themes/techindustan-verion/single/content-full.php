<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('theevent_get_full') ){
	function theevent_get_full(){
	if( !is_single() ){ 
		global $theevent_post_settings; 
		if($theevent_post_settings['excerpt'] < 0) global $more; $more = 0;
	}else{
		global $theevent_post_settings, $theevent_theme_option;
	}
	$theevent_post_settings['content'] = get_the_content();
	if(!isset($theevent_post_settings['title-num-fetch']) && empty($theevent_post_settings['title-num-fetch'])){
		$theevent_post_settings['title-num-fetch'] = '100';
	}
	$thumbnail_size = (empty($theevent_post_settings['thumbnail-size']))? $theevent_theme_option['theevent-post-thumbnail-size']: $theevent_post_settings['thumbnail-size'];
	
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('kode-item blog-full-element'); ?>>
	<div class="kode-team-blog">
	<?php $theevent_get_image = theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
		if(!empty($theevent_get_image)){ ?>
		<figure>
			<?php get_template_part('single/thumbnail', get_post_format()); ?>
			<a href="<?php echo esc_attr(get_permalink());?>"><i class="fa fa-plus"></i></a>
		</figure>
		<?php }?>			
		<div class="kode-thumb-caption">
			<h4><a href="<?php echo esc_attr(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$theevent_post_settings['title-num-fetch']);?></a></h4>		
			<ul class="kode-post-meta-one">
				<?php echo theevent_get_blog_info(array('author'), false, '','li');?>
				<?php echo theevent_get_blog_info(array('date'), false, '','li');?>
			</ul>
			<?php 
			if( is_single() || $theevent_post_settings['excerpt'] < 0 || $theevent_post_settings['excerpt'] == 'full'){
				echo '<div class="kode-blog-content">';
				echo theevent_content_filter($theevent_post_settings['content'], true);
				wp_link_pages( array( 
					'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-event' ) . '</span>', 
					'after' => '</div>', 
					'link_before' => '<span>', 
					'link_after' => '</span>' )
				);
				echo '</div>';
			}else if( $theevent_post_settings['excerpt'] != 0 ){
				echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . esc_attr(get_the_excerpt()) . '</p>';
				if(isset($theevent_theme_option['blog-see-more']) && $theevent_theme_option['blog-see-more'] <> ''){
				echo '<span><a href="' . esc_url(get_permalink()) . '" class="hvr-sweep-to-right">'. sprintf(__('%s','the-event'),$theevent_theme_option['blog-see-more']).'</a></span>';
				}
			echo '</div>';
				
			}
			?>	
		</div>
	</div>
</article>
<?php }

}
theevent_get_full();