<?php
/**
 * The template for displaying video post format
 */
 
if( !function_exists('theevent_get_post_gallery') ){
	function theevent_get_post_gallery(){
		global $theevent_post_settings,$theevent_theme_option; 
		
		$post_format_data = '';
		$content = trim(get_the_content(esc_html__( 'Read More', 'the-event' )));	
		if(preg_match('#\[gallery[^\]]+]#', $content, $match)){ 
			if( is_single() ){
				$post_format_data = do_shortcode($match[0]);
			}else{
				preg_match('#\[gallery.+ids\s?=\s?\"([^\"]+).+]#', $match[0], $match2);
				$post_format_data = theevent_get_bx_slider(explode(',', $match2[1]), array('size'=>$theevent_post_settings['thumbnail-size']));
			}
			$theevent_post_settings['content'] = substr($content, strlen($match[0]));
		}else{
			$theevent_post_settings['content'] = $content;
		}

		if ( !empty($post_format_data) ){
			echo '<div class="kode-blog-thumbnail kode-gallery">' . $post_format_data . '</div>';
		}else{
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			$thumbnail_size = (empty($theevent_post_settings['thumbnail-size']))? $theevent_theme_option['theevent-post-thumbnail-size']: $theevent_post_settings['thumbnail-size'];
			// Get Slider Images
			$theevent_post_option['slider'] = (empty($theevent_post_option['slider']))? ' ': $theevent_post_option['slider'];
			$raw_slider_data = theevent_decode_stopbackslashes($theevent_post_option['slider']);	
			$filter_slider_data = json_decode(theevent_stripslashes($raw_slider_data), true);		
			echo '<div class="kode-blog-thumbnail">';
			echo theevent_get_slider( $filter_slider_data, $thumbnail_size, 'bxslider' );
			echo '</div>';
		}
	}
}
theevent_get_post_gallery();
			