<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('theevent_get_post_small') ){
	function theevent_get_post_small(){
	global $theevent_post_settings,$post,$theevent_post_option,$new_class;?>
	<article id="blog-press-<?php the_ID(); ?>" <?php post_class('blog-press kode-item'); ?>>
		<div class="kode_press_news margin_bottom">
			<figure class="kode_pres2_style">
				<?php get_template_part('single/thumbnail', get_post_format());?>
				<figcaption><a href="extra-images/press-news-09.jpg" data-rel="prettyPhoto[]"><i class="icon-add52"></i></a></figcaption>
			</figure>
			<div class="kode_news_date">
				<ul>
					<?php echo theevent_get_blog_info(array('date'), false, '','li');?>
					<?php echo theevent_get_blog_info(array('views'), false, '','li');?>
				</ul>
			</div>
			<div class="kode_news_des">
				<h6><a href="<?php echo esc_url(get_permalink()); ?>"><?php echo esc_attr(substr(get_the_title(),0,$theevent_post_settings['title-num-fetch'])); ?></a></h6>
				<?php 
					if( is_single() || $theevent_post_settings['excerpt'] < 0 || $theevent_post_settings['excerpt'] == 'full'){
						echo '<div class="kode-blog-content">';
						echo theevent_content_filter($theevent_post_settings['content'], true);
						wp_link_pages( array( 
							'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-event' ) . '</span>', 
							'after' => '</div>', 
							'link_before' => '<span>', 
							'link_after' => '</span>' )
						);
						echo '</div>';
					}else if( $theevent_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content"><p>' . esc_attr(get_the_excerpt()) . '</p><a href="'.esc_url(get_permalink()).'">Read More <i class="fa fa-long-arrow-right"></i></a></div>';
					}
				?>	
			</div>
		</div>
	</article>
<?php }

}
theevent_get_post_small();