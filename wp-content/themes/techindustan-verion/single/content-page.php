<?php 
if( !function_exists('theevent_get_post_page') ){
	function theevent_get_post_page(){ 	
		while ( have_posts() ){ the_post();
			$content = theevent_content_filter(get_the_content(), true); 
			if(!empty($content)){
				?>
				<div class="container">
					<div class="kode-item k-content">
						<?php echo esc_attr($content); ?>
					</div>
				</div>
				<?php
			}
		}
	}
}	
theevent_get_post_page();