<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('theevent_get_post_medium') ){
	function theevent_get_post_medium(){
	global $theevent_post_settings; ?>
	<article id="blog-list-<?php the_ID(); ?>" <?php post_class('evt-blog-cols'); ?>>		
		<!---events column img here---->
		<figure>
			<?php get_template_part('single/thumbnail', get_post_format()); ?>
			<!---events column caption---->
			<figcaption>
				<a href="<?php echo esc_url(get_permalink());?>"><?php esc_attr_e('Blog','the-event');?></a>
				<a href="<?php echo esc_url(get_permalink());?>"><h4><?php echo esc_attr(get_the_title());?></h4></a>
				<span><i class="fa fa-calendar-o"></i><small><?php echo esc_attr(get_the_date('d M Y'));?></small></span>
				<?php 
					if( is_single() || $theevent_post_settings['excerpt'] < 0 || $theevent_post_settings['excerpt'] == 'full'){
						echo '<div class="kode-blog-content">';
						echo theevent_content_filter($theevent_post_settings['content'], true);
						wp_link_pages( array( 
							'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-event' ) . '</span>', 
							'after' => '</div>', 
							'link_before' => '<span>', 
							'link_after' => '</span>' )
						);
						echo '</div>';
					}else if( $theevent_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content"><p>' . esc_attr(get_the_excerpt()) . '</p>';				
						echo '</div>';
					}
				?>	
			</figcaption>
		</figure>
		<!---events column small user info---->
		<div class="user-small">
			<!---events column small user image here---->
			<figure>
				<?php echo get_avatar(get_the_author_meta('ID'), 90);?>
			</figure>
			<!---events column small user caption---->
			<div class="evt-caption">
				<h5><a href="#">Roy Sorah</a></h5>
				<h6>Posted By <a href="#">Admin</a></h6>
			</div>
		</div>
		<!---info ends---->		
	</article>
<?php }

}
theevent_get_post_medium();