<?php 
if( !function_exists('theevent_get_content_thumbnail') ){
	function theevent_get_content_thumbnail(){
		if( !is_single() ){ 
			global $theevent_post_settings,$post,$theevent_theme_option,$theevent_post_option; 
			$theevent_post_settings['content'] = get_the_content();
		}else{
			global $theevent_post_settings, $theevent_theme_option, $theevent_post_option,$post;
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			$theevent_post_settings = $theevent_post_option;
		}
		
		if ( has_post_thumbnail() && ! post_password_required() ){
			
			$k_post_option = json_decode(get_post_meta($post->ID, 'post-option', true), true);		
			
			$thumbnail_size = (empty($theevent_post_settings['thumbnail-size']))? $theevent_theme_option['theevent-post-thumbnail-size']: $theevent_post_settings['thumbnail-size'];
			
			//Get Thumbnail Width and Height
			$theevent_img_size = theevent_get_video_size($thumbnail_size);
			$theevent_width = $theevent_img_size['width'];
			$theevent_height = $theevent_img_size['height'];
			
			// Get Slider Images
			$k_post_option['slider'] = (empty($k_post_option['slider']))? ' ': $k_post_option['slider'];
			$raw_slider_data = theevent_decode_stopbackslashes($k_post_option['slider']);	
			$filter_slider_data = json_decode(theevent_stripslashes($raw_slider_data), true);		
			
			//Get Media Type Selected
			$k_post_option['post_media_type'] = (empty($k_post_option['post_media_type']))? ' ': $k_post_option['post_media_type'];
			
			//Condition for Audio, Video, Slider
			if($k_post_option['post_media_type'] == 'audio'){
				if(strpos($k_post_option['theevent_audio'],'soundcloud')){				
					echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[soundcloud url="'.esc_url($k_post_option['theevent_audio']).'" comments="yes" auto_play="no" color="#ff7700" width="100%" height="'.esc_attr($theevent_height).'px"][/soundcloud]') . '</div>';				
				}else{				
					$theevent_get_image = theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
					if(!empty($theevent_get_image)){
						echo '<div class="kode-blog-thumbnail kode-ux">';
							if( is_single() ){
									echo theevent_get_image(get_post_thumbnail_id(), $thumbnail_size, true);	
									echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['theevent_audio'].'"][/audio]') . '</div>';
							}else{
								$temp_option = json_decode(get_post_meta(get_the_ID(), 'post-option', true), true);
								echo theevent_get_image(get_post_thumbnail_id(), $thumbnail_size,true);
								echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['theevent_audio'].'"][/audio]') . '</div>';							
								
							}						
						echo '</div>';
					}else{
						echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['theevent_audio'].'"][/audio]') . '</div>';
					}
					
				}
			}else if($k_post_option['post_media_type'] == 'video'){
				echo '<div class="kode-blog-thumbnail kode-video">' . theevent_get_video($k_post_option['theevent_video'],$thumbnail_size).'</div>';
			}else if($k_post_option['post_media_type'] == 'slider'){
				echo theevent_get_slider( $filter_slider_data, esc_attr($thumbnail_size), 'bxslider' );
			}else{
				$theevent_get_image = theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
				if(!empty($theevent_get_image)){
					echo '<div class="kode-blog-thumbnail kode-ux">';
						if( is_single() ){
								echo theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);						
						}else{
							$temp_option = json_decode(get_post_meta(get_the_ID(), 'post-option', true), true);
							
							
							echo theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size));
							
						}
					echo '</div>';
				}
			}
		} 
	}
}	
theevent_get_content_thumbnail();