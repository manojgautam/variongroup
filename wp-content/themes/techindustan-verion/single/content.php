<?php
	if( !function_exists('theevent_get_post_format') ){
		function theevent_get_post_format(){
			if( in_array(get_post_format(), array('aside', 'link', 'quote')) ){
				get_template_part('single/content', get_post_format());
			}else{
				
				global $theevent_post_settings;
				if( empty($theevent_post_settings['blog-style']) || $theevent_post_settings['blog-style'] == 'blog-full' ){
					get_template_part('single/content-full');
				}else if( $theevent_post_settings['blog-style'] == 'blog-medium' ){
					get_template_part('single/content-medium');
				}else if( $theevent_post_settings['blog-style'] == 'blog-modern-grid' ){
					get_template_part('single/content-modern-grid');
				}else if( strpos($theevent_post_settings['blog-style'], 'blog-press') !== false ){
					get_template_part('single/content-small');
				}else if( strpos($theevent_post_settings['blog-style'], 'blog-small') !== false ){
					get_template_part('single/content-simple');
				}else{
					get_template_part('single/content-grid');
				}
				
			} 
		}
	}
	theevent_get_post_format();
?>