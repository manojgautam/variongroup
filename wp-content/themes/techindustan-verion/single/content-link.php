<?php
/**
 * The template for displaying link post format
 */
if( !function_exists('theevent_get_post_link') ){
	function theevent_get_post_link(){
	if( !is_single() ){ 
		global $theevent_post_settings; 
		if($theevent_post_settings['excerpt'] < 0) global $more; $more = 0;
	}	
	global $theevent_theme_option;
	$post_format_data = '';
	$content = trim(get_the_content(esc_html__( 'Read More', 'the-event' )));
	if(preg_match('#^<a.+href=[\'"]([^\'"]+).+</a>#', $content, $match)){ 
		$post_format_data = $match[1];
		$content = substr($content, strlen($match[0]));
	}else if(preg_match('#^https?://\S+#', $content, $match)){
		$post_format_data = $match[0];
		$content = substr($content, strlen($match[0]));
	}	
	
	if( !is_single() ){ 
		global $theevent_post_settings; 
		if($theevent_post_settings['excerpt'] < 0) global $more; $more = 0;
	}else{
		global $theevent_post_settings, $theevent_theme_option;
	}
	
	if(!isset($theevent_post_settings['title-num-fetch']) && empty($theevent_post_settings['title-num-fetch'])){
		$theevent_post_settings['title-num-fetch'] = '21';
	}?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="kode-blog-info">
			<h6><?php echo esc_attr(get_the_date('D'));?> <?php echo esc_attr(get_the_date('d'));?>, <?php echo esc_attr(get_the_date('M'));?>, <?php echo esc_attr(get_the_date('Y'));?></h6>
			<?php get_template_part('single/thumbnail', get_post_format()); ?>
			<div class="kode_pet_blog_des">
				<h5 class="kode-post-title"><a href="<?php echo esc_attr(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$theevent_post_settings['title-num-fetch']);?></a></h5>
				<?php if(isset($theevent_theme_option['blog-post-meta']) && $theevent_theme_option['blog-post-meta'] == 'enable'){ ?>
					<ul>
						<?php echo theevent_get_blog_info(array('author','comment','category'), false, '','li');?>			
					</ul>
				<?php }?>
				<?php 
					if( is_single() || $theevent_post_settings['excerpt'] < 0 || $theevent_post_settings['excerpt'] == 'full'){
						echo '<div class="kode-blog-content">';
						echo theevent_content_filter($theevent_post_settings['content'], true);
						wp_link_pages( array( 
							'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-event' ) . '</span>', 
							'after' => '</div>', 
							'link_before' => '<span>', 
							'link_after' => '</span>' )
						);
						echo '</div>';
					}else if( $theevent_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . esc_attr(get_the_excerpt()) . '</p>';
						if(isset($theevent_theme_option['blog-see-more']) && $theevent_theme_option['blog-see-more'] <> ''){
						echo '<a href="' . esc_url(get_permalink()) . '" class="hvr-sweep-to-right">'. sprintf(__('%s','the-event'),$theevent_theme_option['blog-see-more']).'</a>';
						}
					echo '</div>';
						
					}
				?>			
			</div>
		</div>
	</article>
<?php }

}
theevent_get_post_link();