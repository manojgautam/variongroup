<?php
/**
 * The template for displaying quote post format
 */
if( !function_exists('theevent_get_post_quote') ){
	function theevent_get_post_quote(){ 
		if( !is_single() ){ 
			global $theevent_post_settings;
		}
		global $theevent_allowed_html_tags;
		$post_format_data = '';
		$content = trim(get_the_content(esc_html__( 'Read More', 'the-event' )));	
		if(preg_match('#^\[kode_quote[\s\S]+\[/kode_quote\]#', $content, $match)){ 
			$post_format_data = theevent_content_filter($match[0]);
			$content = substr($content, strlen($match[0]));
		}else if(preg_match('#^<blockquote[\s\S]+</blockquote>#', $content, $match)){ 
			$post_format_data = theevent_content_filter($match[0]);
			$content = substr($content, strlen($match[0]));
		}?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('kode-blog-info'); ?>>
			<div class="kode-blog-content">
				<div class="kode-top-quote">
					<?php echo wp_kses($post_format_data,$theevent_allowed_html_tags); ?>
				</div>
				<div class="kode-quote-author">
				<?php 
					if( is_single() || $theevent_post_settings['excerpt'] < 0 ){
						echo theevent_content_filter($content, true); 
					}else{
						echo theevent_content_filter($content); 
					}
				?>	
				</div>
			</div>
		</article><!-- #post -->
<?php }

}
theevent_get_post_quote();