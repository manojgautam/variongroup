<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('theevent_get_post_grid') ){
	function theevent_get_post_grid(){
		global $theevent_post_settings,$post,$theevent_post_option,$theevent_theme_option; 
		$thumbnail_size = (empty($theevent_post_settings['thumbnail-size']))? $theevent_theme_option['theevent-post-thumbnail-size']: $theevent_post_settings['thumbnail-size'];
		?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('kode-item kodeevent-blog-post'); ?>>
			<?php $theevent_get_image = theevent_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
			if(!empty($theevent_get_image)){ ?>
			<div class="newevent-header">
				<?php get_template_part('single/thumbnail', get_post_format()); ?>
				<div class="time-newevent">Feb <span>21</span></div>
			</div>
			<?php }?>	
			<div class="kode-blog-newcontent">
				<h3><a href="<?php echo esc_attr(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$theevent_post_settings['title-num-fetch']);?></a></h3>		
				<div class="post-meta-data">
					<?php echo theevent_get_blog_info(array('author'), false, '','span');?>
					<?php echo theevent_get_blog_info(array('category'), false, '','span');?>
				</div>
				<?php 
				if( is_single() || $theevent_post_settings['excerpt'] < 0 || $theevent_post_settings['excerpt'] == 'full'){
					echo '<div class="kode-blog-content">';
					echo theevent_content_filter($theevent_post_settings['content'], true);
					wp_link_pages( array( 
						'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-event' ) . '</span>', 
						'after' => '</div>', 
						'link_before' => '<span>', 
						'link_after' => '</span>' )
					);
					echo '</div>';
				}else if( $theevent_post_settings['excerpt'] != 0 ){
					echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . esc_attr(get_the_excerpt()) . '</p>
				</div>';
					
				}
				?>	
			</div>
			<div class="kode-newblog-footer">
			<?php if(isset($theevent_theme_option['blog-read-more']) && $theevent_theme_option['blog-read-more'] <> ''){ ?>
				<a href="<?php echo esc_url(get_permalink());?>"><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['blog-read-more'])?></a>
			<?php } ?>	
			</div>
		</article>
<?php }

}	
theevent_get_post_grid();