<?php
/**
 * The template for displaying 404 pages (Not Found).
 */
get_header(); 
$theevent_theme_option = get_option('theevent_admin_option', array());
?>
	<!--// Main Content //-->
	<div class="container">
		<div class="kode-content">
			<div class="kode-error">
			<?php if(isset($theevent_theme_option['error-404']) && $theevent_theme_option['error-404'] <> ''){ ?>
				<h3><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['error-404'])?></h3>
			<?php } ?>	
			<?php if(isset($theevent_theme_option['error-title']) && $theevent_theme_option['error-title'] <> ''){ ?>
				<h4><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['error-title'])?></h4>
			<?php } ?>	
				<?php echo get_search_form();?>
			<?php if(isset($theevent_theme_option['error-sub-title']) && $theevent_theme_option['error-title'] <> ''){ ?>	
				<p><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['error-sub-title'])?></p>
			<?php } ?>	
			<?php if(isset($theevent_theme_option['error-btn']) && $theevent_theme_option['error-btn'] <> ''){ ?>	
				<a href="<?php echo esc_url(home_url('/'));?>"><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['error-btn'])?></a>
			<?php } ?>	
			<?php if(isset($theevent_theme_option['error-btn']) && $theevent_theme_option['error-btn'] <> ''){ ?>
				<a href="<?php echo esc_url(home_url('/'));?>"><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['error-btn'])?></a>
			<?php } ?>	
			</div>
		</div>
	</div>
	<!--// Main Content //-->
<?php get_footer(); ?>