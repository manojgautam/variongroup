<?php
/*
 * The template for displaying a header title section
 */
	

	$theevent_theme_option = get_option('theevent_admin_option', array());
	$header_selected_class = '';
	$header_background = '';
	$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
	if( !empty($theevent_post_option) ){
		$theevent_post_option = json_decode( $theevent_post_option, true );					
	}
	if(!isset($theevent_post_option['header-background'])){
		$theevent_post_option['header-background'] = $theevent_theme_option['default-post-title-background'];
	}
	if(isset($theevent_post_option['header-background'])){
		if( is_numeric($theevent_post_option['header-background']) ){
			$image_src = wp_get_attachment_image_src($theevent_post_option['header-background'], 'full');	
			$header_background = esc_url($image_src[0]);		
		}else{
			$header_background = esc_url($theevent_post_option['header-background']);
		}
	}else{
		$header_background = '';
	}
	$theevent_theme_option['kode-header-style'] = theevent_get_selected_header_class($theevent_post_option,$theevent_theme_option);
	$page_caption = '';
	
	if( is_page() && (empty($theevent_post_option['show-sub']) || $theevent_post_option['show-sub'] != 'disable') ){ 
	$page_background = ''; $page_title = get_the_title(); 
	if(!empty($theevent_post_option['page-caption'])){ $page_caption = $theevent_post_option['page-caption'];}

		if(isset($theevent_post_option['header-background'])){
			if( is_numeric($theevent_post_option['header-background']) ){
				$image_src = wp_get_attachment_image_src($theevent_post_option['header-background'], 'full');	
				$header_background = esc_url($image_src[0]);		
			}else{
				$header_background = esc_url($theevent_post_option['header-background']);
			}
		}else{
			$header_background = '';
		}
		if(isset($header_background) && $header_background <> ''){
			$header_background = 'style="background-image: url('.esc_url($header_background).');"';
		}
		
	?>
<?php $pageid= get_the_ID();?>
<?php if ($pageid=='18'){?>
	<div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header1"><?php echo do_shortcode('[rev_slider alias="summit1"]');?></div><?php } elseif ($pageid=='10'){?>
<div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header"><img src="http://agiledevelopers.in/VerionGroupNew/wp-content/uploads/2016/09/new3.jpg"></div>
<?php } else { ?>
<div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header"></div>
<?php } ?>
	<div  class="kode_about_bg subheader-height subheader <?php echo esc_attr($theevent_theme_option['kode-header-style']);?>">
		<div class="container">
			<!--ROW START-->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2><?php echo esc_attr(theevent_text_filter($page_title)); ?></h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<?php if($theevent_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
						<?php theevent_get_breadcumbs();?>
					<?php }?>
				</div>
			</div>
			<!--ROW END-->
		</div>
	</div>
	<?php }else if( is_single() && $post->post_type == 'post' ){ 
	
		if( !empty($theevent_post_option['page-title']) ){
			$page_title = $theevent_post_option['page-title'];
			$page_caption = $theevent_post_option['page-caption'];
		}else{
			$page_title = '';
			$page_caption = '';
		} 
		if($page_title == ''){
			$page_title = get_the_title();
		}
		
		if(!isset($theevent_post_option['header-background'])){
			$theevent_post_option['header-background'] = $theevent_theme_option['default-post-title-background'];
		}
		if(isset($theevent_post_option['header-background'])){
			if( is_numeric($theevent_post_option['header-background']) ){
				$image_src = wp_get_attachment_image_src($theevent_post_option['header-background'], 'full');	
				$header_background = esc_url($image_src[0]);		
			}else{
				$header_background = esc_url($theevent_post_option['header-background']);
			}
		}else{
			$header_background = '';
		}
	?>
	<div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header"></div>
	<div class="kode_about_bg subheader-height subheader <?php echo esc_attr($theevent_theme_option['kode-header-style']);?>">
		<div class="container">
			<!--ROW START-->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2><?php echo esc_attr(theevent_text_filter($page_title)); ?></h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<?php if($theevent_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
						<?php theevent_get_breadcumbs();?>
					<?php }?>
				</div>
			</div>
			<!--ROW END-->
		</div>
	</div>
	<?php }else if( is_single() ){ // for custom post type
		
		if( !empty($theevent_post_option['page-title']) ){
			$page_title = $theevent_post_option['page-title'];
			$page_caption = $theevent_post_option['page-caption'];
		}else{
			$page_title = '';
			$page_caption = '';
		} 
		if($page_title == ''){
			$page_title = get_the_title();
		}
		
		if(!isset($theevent_post_option['header-background'])){
			$theevent_post_option['header-background'] = $theevent_theme_option['default-post-title-background'];
		}
		if(isset($theevent_post_option['header-background'])){
			if( is_numeric($theevent_post_option['header-background']) ){
				$image_src = wp_get_attachment_image_src($theevent_post_option['header-background'], 'full');	
				$header_background = esc_url($image_src[0]);		
			}else{
				$header_background = esc_url($theevent_post_option['header-background']);
			}
		}else{
			$header_background = '';
		}
	?><div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header"></div>
	<div class="kode_about_bg subheader-height subheader <?php echo esc_attr($theevent_theme_option['kode-header-style']);?>">
		<div class="container">
			<!--ROW START-->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2><?php echo esc_attr(theevent_text_filter($page_title)); ?></h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<?php if($theevent_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
						<?php theevent_get_breadcumbs();?>
					<?php }?>
				</div>
			</div>
			<!--ROW END-->
		</div>
	</div>
	<?php }else if( is_404() ){ 
		$page_title = '404 Error Page';
		if(!isset($theevent_post_option['header-background'])){
			$theevent_post_option['header-background'] = $theevent_theme_option['default-404-title-background'];
		}
		if(isset($theevent_post_option['header-background'])){
			if( is_numeric($theevent_post_option['header-background']) ){
				$image_src = wp_get_attachment_image_src($theevent_post_option['header-background'], 'full');	
				$header_background = esc_url($image_src[0]);		
			}else{
				$header_background = esc_url($theevent_post_option['header-background']);
			}
		}else{
			$header_background = '';
		}
	?><div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header"></div>
	<div  class="kode_about_bg subheader-height subheader <?php echo esc_attr($theevent_theme_option['kode-header-style']);?>">
		<div class="container">
			<!--ROW START-->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2><?php echo esc_attr(theevent_text_filter($page_title)); ?></h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<?php if($theevent_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
						<?php theevent_get_breadcumbs();?>
					<?php }?>
				</div>
			</div>
			<!--ROW END-->
		</div>
	</div>
	<?php }else if( is_archive() || is_search() ){
		if( is_search() ){
			$page_title = esc_html__('Search Results', 'the-event');
			$caption = get_search_query();
		}else if( is_category() || is_tax('work_category') || is_tax('product_cat') ){
			$page_title = esc_html__('Category','the-event');
			$caption = single_cat_title('', false);
		}else if( is_tag() || is_tax('work_tag') || is_tax('product_tag') ){
			$page_title = esc_html__('Tag','the-event');
			$caption = single_cat_title('', false);
		}else if( is_day() ){
			$page_title = esc_html__('Day','the-event');
			$caption = get_the_date('F j, Y');
		}else if( is_month() ){
			$page_title = esc_html__('Month','the-event');
			$caption = get_the_date('F Y');
		}else if( is_year() ){
			$page_title = esc_html__('Year','the-event');
			$caption = get_the_date('Y');
		}else if( is_author() ){
			$page_title = esc_html__('By','the-event');
			
			$author_id = get_query_var('author');
			$author = get_user_by('id', $author_id);
			$caption = $author->display_name;					
		}else if( is_post_type_archive('product') ){
			$page_title = esc_html__('Shop', 'the-event');
			$caption = '';
		}else{
			$page_title = get_the_title();
			$caption = '';
		}
		if(!isset($theevent_post_option['header-background'])){
			$theevent_post_option['header-background'] = $theevent_theme_option['default-search-archive-title-background'];
		}
		if(isset($theevent_post_option['header-background'])){
			if( is_numeric($theevent_post_option['header-background']) ){
				$image_src = wp_get_attachment_image_src($theevent_post_option['header-background'], 'full');	
				$header_background = esc_url($image_src[0]);		
			}else{
				$header_background = esc_url($theevent_post_option['header-background']);
			}
		}else{
			$header_background = '';
		}
	?><div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header"></div>
	<div class="kode_about_bg subheader-height subheader <?php echo esc_attr($theevent_theme_option['kode-header-style']);?>">
		<div class="container">
			<!--ROW START-->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2><?php echo esc_attr(theevent_text_filter($page_title)); ?></h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<?php if($theevent_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
						<?php theevent_get_breadcumbs();?>
					<?php }?>
				</div>
			</div>
			<!--ROW END-->
		</div>
	</div>
	<?php } ?>
	<!-- is search -->