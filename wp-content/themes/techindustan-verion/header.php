<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7 ltie8 ltie9" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8 ltie9" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />	
	<?php 
		$theevent_theme_option = get_option('theevent_admin_option', array());
		$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
		if( !empty($theevent_post_option) ){
			$theevent_post_option = json_decode( $theevent_post_option, true );					
		}
		wp_head(); ?>
</head>
<body <?php body_class( );?> id="home">
<div class="body-wrapper" data-home="<?php echo esc_url(home_url('/')); ?>">
	<?php theevent_get_header($theevent_theme_option); get_template_part( 'header', 'title' ); ?>
	<div class="content-wrapper">
	