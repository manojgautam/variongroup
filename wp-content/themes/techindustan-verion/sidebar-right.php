<?php
/**
 * A template for calling the right sidebar in everypage
 */

if( !function_exists('theevent_sidebar_right') ){
	function theevent_sidebar_right(){
	global $theevent_sidebar;?>
		<?php if( $theevent_sidebar['type'] == 'right-sidebar' || $theevent_sidebar['type'] == 'both-sidebar' ){ ?>
		<div class="kode-sidebar kode-right-sidebar columns">
			<?php dynamic_sidebar($theevent_sidebar['right-sidebar']); ?>
		</div>
		<?php }
	}
}	
theevent_sidebar_right();
 ?>