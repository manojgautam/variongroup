<?php get_header(); 
	$theevent_theme_option = get_option('theevent_admin_option', array());
	$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
	if( !empty($theevent_post_option) ){
		$theevent_post_option = json_decode( $theevent_post_option, true );					
	}
	$theevent_post_option['header-background'] = $theevent_theme_option['default-page-title'];
	if(isset($theevent_post_option['header-background'])){
		if( is_numeric($theevent_post_option['header-background']) ){
			$image_src = wp_get_attachment_image_src($theevent_post_option['header-background'], 'full');	
			$header_background = ' style="background-image: url(\'' . esc_url($image_src[0]) . '\');" ';		
		}else{
			if(esc_url($theevent_post_option['header-background']) <> ''){
				$header_background = ' style="background-image: url(\'' . esc_url($theevent_post_option['header-background']) . '\');" ';
			}else{
				$header_background = ' style="background-image: url(\'' . THEEVENT_PATH . '/images/subheader-bg.jpg\');" ';
			}		
		}
	}else{
		$header_background = '';
	}
	$theevent_theme_option['kode-header-style'] = (empty($theevent_theme_option['kode-header-style']))? 'header-style-1': $theevent_theme_option['kode-header-style'];
	$page_caption = '';
	$page_background = ''; $page_title = get_the_title(); 
	if(!empty($theevent_post_option['page-caption'])){ $page_caption = $theevent_post_option['page-caption'];} ?>
	<div <?php echo wp_kses($header_background,array('a'=>array()));?> class="sub-header"></div>
	<div  class=" kode_about_bg subheader-height subheader <?php echo esc_attr($theevent_theme_option['kode-header-style']);?>">
		<div class="container">
			<!--ROW START-->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2><?php echo esc_attr(theevent_text_filter($page_title)); ?></h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<?php if($theevent_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
						<?php theevent_get_breadcumbs();?>
					<?php }?>
				</div>
			</div>
			<!--ROW END-->
		</div>
	</div>
<div class="content kode-main-content-k">
	<div class="container">
		<div class="row">
		<?php 			
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			if( empty($theevent_post_option['sidebar']) || $theevent_post_option['sidebar'] == 'default-sidebar' ){
				$theevent_sidebar = array(
					'type'=>$theevent_theme_option['archive-sidebar-template'],
					'left-sidebar'=>$theevent_theme_option['archive-sidebar-left'], 
					'right-sidebar'=>$theevent_theme_option['archive-sidebar-right']
				); 
			}else{
				$theevent_sidebar = array(
					'type'=>$theevent_post_option['sidebar'],
					'left-sidebar'=>$theevent_post_option['left-sidebar'], 
					'right-sidebar'=>$theevent_post_option['right-sidebar']
				); 				
			}
			
			$theevent_sidebar = theevent_get_sidebar_class($theevent_sidebar);
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="<?php echo esc_attr($theevent_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="kode-main-content <?php echo esc_attr($theevent_sidebar['center'])?>">
				<?php		
					// set the excerpt length
					if( !empty($theevent_theme_option['archive-num-excerpt']) ){
						$theevent_excerpt_length = 55;
						add_filter('excerpt_length', 'theevent_set_excerpt_length');
					} 
					
					$theevent_lightbox_id++;
					$theevent_post_settings['title-num-fetch'] = 300;
					$theevent_post_settings['excerpt'] = 'full';
					$theevent_post_settings['thumbnail-size'] = 'full';			
					$theevent_post_settings['blog-style'] = $theevent_theme_option['archive-blog-style'];							
				
					echo '<div class="kode-blog-list kode-fullwidth-blog row">';
					if($theevent_post_settings['blog-style'] == 'blog-full'){
						
						$theevent_post_settings['blog-info'] = array('author', 'date', 'category', 'comment');
						echo theevent_get_blog_full($wp_query);
					}else{
						$theevent_post_settings['blog-info'] = array('date', 'comment');
						$theevent_post_settings['blog-info-widget'] = true;
						
						//$blog_size = str_replace('blog-1-', '', $theevent_theme_option['archive-blog-style']);
						$blog_size = 2;
						echo theevent_get_blog_grid($wp_query, $blog_size, 
							$theevent_theme_option['archive-thumbnail-size'], 'fitRows');
					}
					echo '<div class="clear"></div>';
					remove_filter('excerpt_length', 'theevent_set_excerpt_length');
					
					$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
					echo theevent_get_pagination($wp_query->max_num_pages, $paged);
					echo '</div>';
					
				?>
			</div>
			<?php
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'right-sidebar' && $theevent_sidebar['right'] != ''){ ?>
				<div class="<?php echo esc_attr($theevent_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>