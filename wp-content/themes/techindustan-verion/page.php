<?php get_header(); ?>
	<div class="content">
		<!-- Sidebar With Content Section-->
		<?php 
			if( !empty($theevent_content_raw) ){ 
				echo '<div class="vc-wrapper container">';
				while ( have_posts() ){ the_post();
					if( has_shortcode( get_the_content(), 'vc_row' ) ) {
						echo theevent_content_filter(get_the_content(), true); 
					}
				}
				echo '</div>';
				
				echo '<div class="pagebuilder-wrapper">';
				theevent_show_page_builder($theevent_content_raw);
				echo '</div>';
				
				
			}else{
				echo '<div class="container">';
					$default['show-title'] = 'enable';
					$default['show-content'] = 'enable'; 
					echo theevent_get_default_content_item($default);
				echo '</div>';
			}
		?>
	</div><!-- content -->
<?php get_footer(); ?>