<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 
			global $theevent_sidebar, $theevent_theme_option;
			
			// $theevent_theme_option['theevent-post-thumbnail-size'] = '';
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			if( empty($theevent_post_option['sidebar']) || $theevent_post_option['sidebar'] == 'default-sidebar' ){
				$theevent_sidebar = array(
					'type'=>$theevent_theme_option['post-sidebar-template'],
					'left-sidebar'=>$theevent_theme_option['post-sidebar-left'], 
					'right-sidebar'=>$theevent_theme_option['post-sidebar-right']
				); 
			}else{
				$theevent_sidebar = array(
					'type'=>$theevent_post_option['sidebar'],
					'left-sidebar'=>$theevent_post_option['left-sidebar'], 
					'right-sidebar'=>$theevent_post_option['right-sidebar']
				); 				
			}
			
			
			$theevent_theme_option['single-post-author'] = 'enable';
			
			$theevent_sidebar = theevent_get_sidebar_class($theevent_sidebar);			
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="<?php echo esc_attr($theevent_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="<?php echo esc_attr($theevent_sidebar['center'])?>">
				<div class="kode-item kode-team-full kode-single-detail">
				<?php while ( have_posts() ){ the_post();global $post; 
					
					$designation = $theevent_post_option['designation'];
					$phone = $theevent_post_option['phone'];
					$website = $theevent_post_option['website'];
					$email = $theevent_post_option['email'];
					$facebook = $theevent_post_option['facebook'];
					$twitter = $theevent_post_option['twitter'];
					$youtube = $theevent_post_option['youtube'];
					$pinterest = $theevent_post_option['pinterest'];
					?>
					<div class="kode_biblo_wrap_law">
						<div class="col-md-8">
							<div class="kode_biography_wrap_law">
								<figure>
									<?php echo get_the_post_thumbnail($post->ID, $theevent_theme_option['thumbnail-size']); ?>									
								</figure>
								<div class="biography_des_law">
									<h4><?php echo esc_attr(get_the_title());?></h4>
									<h5><?php echo esc_attr($designation);?></h5>
									<p><?php echo get_the_excerpt();?></p>
									<strong>Website: <span><?php echo esc_attr($website);?></span></strong>
									<strong>Email: <span><?php echo esc_attr($email);?></span></strong>
									<strong>Phone: <span><?php echo esc_attr($phone);?></span></strong>
									<ul class="social-icons-lawyer">
										<?php if($facebook <> ''){ ?><li><a href="<?php echo esc_url($facebook)?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
										<?php if($twitter <> ''){ ?><li><a href="<?php echo esc_url($twitter)?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
										<?php if($youtube <> ''){ ?><li><a href="<?php echo esc_url($youtube)?>"><i class="fa fa-youtube"></i></a></li><?php } ?>
										<?php if($pinterest <> ''){ ?><li><a href="<?php echo esc_url($pinterest)?>"><i class="fa fa-pinterest"></i></a></li><?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="kode_edu_wrap_law">
								<?php echo do_shortcode($theevent_post_option['team-right-side']);?>
							</div>
						</div>
					</div>
					<div class="kode_award_wrap_law">												
						<?php $content = get_the_content(); echo theevent_content_filter($content,false);?>						
					</div>
					<?php comments_template( '', true ); ?>
				<?php } ?>
				</div>
			</div>	
			<?php
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'right-sidebar' && $theevent_sidebar['right'] != ''){ ?>
				<div class="<?php echo esc_attr($theevent_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>