<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 
			$theevent_theme_option = get_option('theevent_admin_option', array());
			// $theevent_theme_option['theevent-post-thumbnail-size'] = '';
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			if( empty($theevent_post_option['sidebar']) || $theevent_post_option['sidebar'] == 'default-sidebar' ){
				$theevent_sidebar = array(
					'type'=>$theevent_theme_option['post-sidebar-template'],
					'left-sidebar'=>$theevent_theme_option['post-sidebar-left'], 
					'right-sidebar'=>$theevent_theme_option['post-sidebar-right']
				); 
			}else{
				$theevent_sidebar = array(
					'type'=>$theevent_post_option['sidebar'],
					'left-sidebar'=>$theevent_post_option['left-sidebar'], 
					'right-sidebar'=>$theevent_post_option['right-sidebar']
				); 				
			}
			
			
			$theevent_theme_option['single-post-author'] = 'enable';
			
			$theevent_sidebar = theevent_get_sidebar_class($theevent_sidebar);			
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="kode-left-sidebar-area <?php echo esc_attr($theevent_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="kode-content-area <?php echo esc_attr($theevent_sidebar['center'])?>">
				<div class="kode-item kode-team-full kode-single-detail">
				<?php while ( have_posts() ){ the_post();global $post; 
					$designation = $theevent_post_option['designation'];
					$phone = $theevent_post_option['phone'];
					$website = $theevent_post_option['website'];
					$email = $theevent_post_option['email'];
					$facebook = $theevent_post_option['facebook'];
					$twitter = $theevent_post_option['twitter'];
					$youtube = $theevent_post_option['youtube'];
					$pinterest = $theevent_post_option['pinterest']; ?>
					<div class="kf_who_am">
                        <figure>
                            <?php echo get_the_post_thumbnail($post->ID, $theevent_theme_option['thumbnail-size']); ?>
                            <figcaption class="kf_speaker_socil">
                                <ul>
									<?php if($facebook <> ''){ ?><li><a href="<?php echo esc_url($facebook)?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
									<?php if($twitter <> ''){ ?><li><a href="<?php echo esc_url($twitter)?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
									<?php if($youtube <> ''){ ?><li><a href="<?php echo esc_url($youtube)?>"><i class="fa fa-youtube"></i></a></li><?php } ?>
									<?php if($pinterest <> ''){ ?><li><a href="<?php echo esc_url($pinterest)?>"><i class="fa fa-pinterest"></i></a></li><?php } ?>
								</ul>
                            </figcaption>
                        </figure>
                        
                        <div class="kf_who_am_des">
                        	<!--<div class="kf_heading_1">
                            	<h5><?php echo esc_attr(get_the_title());?></h5>
                            	<i><?php echo $theevent_post_option['team-qoute']?></i>
                            </div>-->
                            <h4><?php echo esc_attr(get_the_title());?></h4>
							<?php if($designation <> ''){ ?><span><?php echo esc_attr($theevent_post_option['designation'])?></span><?php } ?>
                            <p><?php echo get_the_excerpt();?></p>
                            <ul>
                            	<?php if($phone <> ''){ ?>
								<li>
								<?php if(isset($theevent_theme_option['speaker-phone']) && $theevent_theme_option['speaker-phone'] <> ''){ ?>
                                	<strong><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['speaker-phone'])?></strong>
                                	<span><?php echo esc_attr($phone);?></span>
                                </li>
								<?php } ?>
								<?php } ?>		

								<?php if($email <> ''){ ?>
                                <li>
								<?php if(isset($theevent_theme_option['speaker-email']) && $theevent_theme_option['speaker-email'] <> ''){ ?>
                                	<strong><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['speaker-email'])?></strong>
                                	<a href="mailto:<?php echo esc_attr($email)?>"></a>
                                </li>
								<?php }?>
								<?php }?>
								
								<?php if($website <> ''){ ?>
                                <li>
								<?php if(isset($theevent_theme_option['speaker-website']) && $theevent_theme_option['speaker-website'] <> ''){ ?>
                                	<strong><?php echo sprintf(__('%s','kf_event'),$theevent_theme_option['speaker-website'])?></strong>
                                	<span><?php echo esc_url($website)?></span>
                                </li>
								<?php }?>
								<?php }?>
                            </ul>
                        </div>
                    </div>
					<div class="kode-team-bio">
						<div class="kode_bground_des">
							<h4><?php esc_attr_e('Background','the-event');?></h4>
							<?php the_content();?>
						</div>
					</div>
					<?php if(isset($theevent_theme_option['single-team-comments']) && $theevent_theme_option['single-team-comments'] == 'enable'){ ?>
						<?php comments_template( '', true ); ?>
					<?php } ?>
				<?php } ?>
				</div>
			</div>	
			<?php
			if($theevent_sidebar['type'] == 'both-sidebar' || $theevent_sidebar['type'] == 'right-sidebar' && $theevent_sidebar['right'] != ''){ ?>
				<div class="kode-right-sidebar-area <?php echo esc_attr($theevent_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>