<?php
	/*	
	*	Kodeforest Team Listing
	*	---------------------------------------------------------------------
	*	This file contains functions that help you create team item
	*	---------------------------------------------------------------------
	*/
	
	//Team Listing
	if( !function_exists('theevent_get_team_item') ){
		function theevent_get_team_item( $settings ){
			// $settings['category'];
			// $settings['tag'];
			// $settings['num-excerpt'];
			// $settings['num-fetch'];
			// $settings['team-style'];
			// $settings['order'];
			// $settings['orderby'];
			// $settings['order'];
			// $settings['margin-bottom'];
			// query posts section
			$args = array('post_type' => 'team', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : 1;

			if( !empty($settings['category']) || (!empty($settings['tag'])) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'team_category', 'field'=>'slug'));
				}
				if( !empty($settings['tag'])){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'team_tag', 'field'=>'slug'));
				}				
			}			
			$query = new WP_Query( $args );

			// create the team filter
			$settings['num-excerpt'] = empty($settings['num-excerpt'])? 0: $settings['num-excerpt'];
			$settings['team-column'] = empty($settings['team-column'])? 4: $settings['team-column'];
			$settings['feature-image-align'] = empty($settings['feature-image-align'])? '': $settings['feature-image-align'];
			
			
			$size = $settings['team-column'];
			
			$team  = '<div class="kode-team kode-team-classic col-md-12"><ul class="row">';
			if($settings['team-style'] == 'normal-view'){
				$size = $settings['team-column'];
				$team  = '<div class="kode-team kode-team-grid" ><div class="row">';
			}else if($settings['team-style'] == 'simple-view'){
				$size = $settings['team-column'];
				$team  = '<div class="kode-team kode-team-simple" ><div class="row">';
			}else if($settings['team-style'] == 'full-view'){
				$size = $settings['team-column'];
				$team  = '<div class="col-md-12 kode-team-list-clean kode-team-grid-clean" ><ul class="row">';
			}else if($settings['team-style'] == 'advance-view'){
				$size = 2;
				$team  = '<div class="col-md-12 kode-advance-list-clean kode-advance-grid-clean" ><div class="row">';
			}else if($settings['team-style'] == 'islamic-view'){
				$size = $settings['team-column'];
				$team  = '<div class="col-md-12 kode-team-list-clean kode-team-grid-clean" ><div class="row">';
			}else{
				$size = $settings['team-column'];
				$team  = '<div class="kode-team kode-team-classic col-md-12"><ul class="row">';
			}
			$settings['title-num-excerpt'] = (empty($settings['title-num-excerpt']))? '15': $settings['title-num-excerpt'];
			$current_size = 0;
			while($query->have_posts()){ $query->the_post();
				global $theevent_post_option,$post,$theevent_post_settings;
				$team_option = json_decode(theevent_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true)), true);
				$designation = $team_option['designation'];
				$phone = $team_option['phone'];
				$website = $team_option['website'];
				$email = $team_option['email'];
				$facebook = $team_option['facebook'];
				$twitter = $team_option['twitter'];
				$youtube = $team_option['youtube'];
				$pinterest = $team_option['pinterest'];
				if($settings['team-style'] == 'full-view'){
					if($settings['feature-image-align'] == 'left'){
						$team_class = 'our-team-second';
					}else{
						$team_class = 'our-team-first';
					}
				}
				
				if($settings['team-style'] == 'normal-view'){
					$team .= '<div class="col-sm-6 ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">
						<div class="kode-ux">
							<div class="kode-newspeaker">
								<div class="speaker-photo">
									<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
								</div>
								<div class="new-ourspeaker">
									<h3>'.esc_attr(get_the_title()).' / <span>'.$team_option['designation'].'</span></h3>
									<ul class="newpseaker-media">';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
									</ul>
								</div>
								<div class="new-ourspeaker over-state">
									<h3>'.esc_attr(get_the_title()).' / <span>'.$team_option['designation'].'</span></h3>
									<p>'.substr(get_the_content(),0,$settings['num-excerpt']).'</p>
									<ul class="newpseaker-media">';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
									</ul>
								</div>
								<!-- kode-new speaker over state End -->
							</div>
                        </div>
					</div>';
				}else if($settings['team-style'] == 'full-view'){
					$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
					$thumbnail = wp_get_attachment_image_src($thumbnail_id, 'full');
					$team .= '<div class="kode-team-full-view">
						<div class="'.esc_attr($team_class).'" style="background-image:url('.esc_url($thumbnail[0]).');">
							<!--Speaker Social Section Etart-->
							<div class="our-speaker-social-section">
								<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h3>
								<span>'.esc_attr($team_option['designation']).'</span>
								<p>'.substr(get_the_content(),0,$settings['num-excerpt']).'</p>
								<ul>';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
								</ul>
							</div>
							<!--Speaker Social Section End-->
						</div>
					</div>';
				}else if($settings['team-style'] == 'simple-view'){
					$team .= '
					<div class="kode-ux col-sm-6 ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">
						<div class="thumb">
							<figure>
								<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
							</figure>
							<div class="speaker-event-content">
								<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h3>
								<span>'.esc_attr($team_option['designation']).'</span>
								<strong class="bottom-border"></strong>
								<ul>';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
								</ul>
							</div>
						</div>
					</div>';
				}else if($settings['team-style'] == 'modern-view'){
					$team .= '
					<div class="kode-ux col-sm-6 ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">
						<div class="kode_style_var">
							<figure>
								<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
								<div class="style_var_des">
									<div class="team_var_de">
										<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h4>
										<h6>'.esc_attr($team_option['designation']).'</h6>
									</div>
									<p>'.substr(get_the_content(),0,20).'</p>
									<ul>';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
									</ul>
									<a class="modern-view-btn" href="'.esc_url(get_permalink()).'">Read more</a>
								</div>
							</figure>
							<div class="kode_team_2_des">
								<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h4>
								<h6>'.esc_attr($team_option['designation']).'</h6>
							</div>
						</div>
					</div>';
				}else if($settings['team-style'] == 'advance-view'){
					$team .= '
					<div class="col-sm-6 ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">
						<div class="kode_leader-detail">
							<figure>
								<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
								<div class="leader-social-icon">
									<ul>';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
									</ul>
								</div>
							</figure>
							<div class="leader-detail">
								<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h4>
								<h6>'.esc_attr($team_option['designation']).'</h6>
							</div>
						</div>
					</div>';
				}else if($settings['team-style'] == 'church-view'){
					$team .= '
					<div class="col-sm-6 kode-item ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">
						<div class="kode_chu_pastor kode-ux">
							<figure>
								'.get_the_post_thumbnail($post->ID, array(350,350)).'
							</figure>
							<div class="kode_chu_pastors_des">
								<h3>'.esc_attr(get_the_title()).'</h3>
								<h5>'.esc_attr($team_option['designation']).'</h5>
								<ul>';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
								</ul>
							</div>
						</div>
					</div>';
				}else{
				$team .= '<li class="col-sm-6 ' . esc_attr(theevent_get_column_class('1/' . $size)) . '">
					<div class="kode-ux">
						<div class="team-inner">
							<figure><a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, array(350,350)).'</a></figure>
							<div class="kode-teaminfo">
								<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h4>
								<span>'.esc_attr($team_option['designation']).'</span>
								<div class="team-network">
									<ul>';
									if(isset($facebook) && $facebook <> ''){
										$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
									}
									if(isset($twitter) && $twitter <> ''){
										$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
									}
									if(isset($youtube) && $youtube <> ''){
										$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
									}
									if(isset($pinterest) && $pinterest <> ''){
										$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
									}
								$team .= '
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>';
				}
				$current_size++;
			}
			if( $settings['pagination'] == 'enable' ){
				$team .= theevent_get_pagination($query->max_num_pages, $args['paged']);
			}
			if($settings['team-style'] == 'simple-view'){				
				$team .= '</div></div>';
			}else if($settings['team-style'] == 'church-view'){
				$team .= '</div></div>';
			}else if($settings['team-style'] == 'islamic-view'){
				$team .= '</div>';
			}else{
				$team .= '</div></div>';
				
			}
						
			return $team;
		}
	}	//Team Listing
	
	if( !function_exists('theevent_get_speaker_item') ){
		function theevent_get_speaker_item( $settings ){
			//$settings['candidate'];
			//$thumbnail-size
			//$title-num-excerpt
			//$settings['margin-bottom'];
			//query posts section
			$team_class = '';
			$teampost = get_post($settings['candidate']);
			$settings['num-excerpt'] = empty($settings['num-excerpt'])? 250: $settings['num-excerpt'];
			$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta($teampost->ID, 'post-option', true ));
			if( !empty($theevent_post_option) ){
				$theevent_post_option = json_decode( $theevent_post_option, true );					
			}
			if($settings['alignment'] == 'left'){
				$team_class = 'our-team-second';
			}else{
				$team_class = 'our-team-first';
			}
			$designation = $theevent_post_option['designation'];
			$phone = $theevent_post_option['phone'];
			$website = $theevent_post_option['website'];
			$email = $theevent_post_option['email'];
			$facebook = $theevent_post_option['facebook'];
			$twitter = $theevent_post_option['twitter'];
			$youtube = $theevent_post_option['youtube'];
			$pinterest = $theevent_post_option['pinterest'];
			$thumbnail_id = get_post_thumbnail_id( $teampost->ID );
			$thumbnail = wp_get_attachment_image_src($thumbnail_id, 'full');			
			$ret .= '<div class="kode-team-full-view">
				<div class="'.esc_attr($team_class).'" style="background-image:url('.esc_url($thumbnail[0]).');">
					<!--Speaker Social Section Etart-->
					<div class="our-speaker-social-section">
						<h3><a href="'.esc_url(get_permalink($teampost->ID)).'">'.esc_attr($teampost->post_title).'</a></h3>
						<span>'.esc_attr($theevent_post_option['designation']).'</span>
						<p>'.substr($teampost->post_content,0,$settings['num-excerpt']).'</p>
						<ul>';
							if(isset($facebook) && $facebook <> ''){
								$team .= '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';
							}
							if(isset($twitter) && $twitter <> ''){
								$team .= '<li><a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a></li>';
							}
							if(isset($youtube) && $youtube <> ''){
								$team .= '<li><a href="'.esc_url($youtube).'"><i class="fa fa-youtube"></i></a></li>';
							}
							if(isset($pinterest) && $pinterest <> ''){
								$team .= '<li><a href="'.esc_url($pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
							}
						$team .= '
						</ul>
					</div>
					<!--Speaker Social Section End-->
				</div>
			</div>';			
			return $ret;
		}
	}	
	
	
	
	
?>