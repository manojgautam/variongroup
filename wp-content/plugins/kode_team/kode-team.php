<?php
/*
Plugin Name: KodeForest Team
Plugin URI: 
Description: A Custom Post Type Plugin To Use With KodeForest Theme ( This plugin functionality might not working properly on another theme )
Version: 1.0.0
Author: KodeForest
Author URI: http://www.kodeforest.com
License: 
*/


include_once( 'kode-team-item.php');	
include_once( 'kode-team-option.php');	

// action to loaded the plugin translation file
add_action('plugins_loaded', 'theevent_team_init');
if( !function_exists('theevent_team_init') ){
	function theevent_team_init() {
		load_plugin_textdomain( 'the-event', false, dirname(plugin_basename( __FILE__ ))  . '/languages/' ); 
	}
}

// add action to create team post type
add_action( 'init', 'theevent_create_team' );
if( !function_exists('theevent_create_team') ){
	function theevent_create_team() {
		global $theme_option;
				
		$team_slug = 'team';
		$team_category_slug = 'team_category';
		$team_tag_slug = 'team_tag';		
		
		register_post_type( 'team',
			array(
				'labels' => array(
					'name'               => __('Speakers', 'the-event'),
					'singular_name'      => __('Speaker', 'the-event'),
					'add_new'            => __('Add New', 'the-event'),
					'add_new_item'       => __('Add New Speaker', 'the-event'),
					'edit_item'          => __('Edit Speaker', 'the-event'),
					'new_item'           => __('New Speaker', 'the-event'),
					'all_items'          => __('All Speakers', 'the-event'),
					'view_item'          => __('View Speaker', 'the-event'),
					'search_items'       => __('Search Speaker', 'the-event'),
					'not_found'          => __('No Speakers found', 'the-event'),
					'not_found_in_trash' => __('No Speakers found in Trash', 'the-event'),
					'parent_item_colon'  => '',
					'menu_name'          => __('Speaker', 'the-event')
				),
				'public'             => true,
				'publicly_queryable' => true,
				'rewrite' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,				
				'rewrite'            => array( 'slug' => $team_slug  ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => true,
				'menu_position'      => 25,
				'supports'           => array( 'title', 'editor','thumbnail', 'excerpt', 'comments', 'custom-fields' )
			)
		);
		
		// create team categories
		register_taxonomy(
			'team_category', array("team"), array(
				'hierarchical' => true,
				'show_admin_column' => true,
				'label' => __('Speaker Categories', 'the-event'), 
				'singular_label' => __('Speaker Category', 'the-event'), 
				'rewrite' => array( 'slug' => $team_category_slug  )));
		register_taxonomy_for_object_type('team_category', 'team');
		
		// create team tag
		register_taxonomy(
			'team_tag', array('team'), array(
				'hierarchical' => false, 
				'show_admin_column' => true,
				'label' => __('Speaker Tags', 'the-event'), 
				'singular_label' => __('Speaker Tag', 'the-event'),  
				'rewrite' => array( 'slug' => $team_tag_slug  )));
		register_taxonomy_for_object_type('team_tag', 'team');	

		// add filter to style single template
		add_filter('single_template', 'theevent_register_team_template');
		
	}
}

if( !function_exists('theevent_register_team_template') ){
	function theevent_register_team_template($single_template) {
		global $post,$theevent_theme_option;

			if ($post->post_type == 'team') {
				$single_template = dirname( __FILE__ ) . '/single-team.php';	
			}
		
		return $single_template;	
	}
}

// include_once( 'kode-team-element.php');	
?>
