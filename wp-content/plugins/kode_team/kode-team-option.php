<?php
	/*	
	*	KodeForest Team Option file
	*	---------------------------------------------------------------------
	*	This file creates all team options and attached to the theme
	*	---------------------------------------------------------------------
	*/
	
	// add a team option to team page
	if( is_admin() ){ add_action('after_setup_theme', 'theevent_create_team_options'); }
	if( !function_exists('theevent_create_team_options') ){
	
		function theevent_create_team_options(){
			global $theevent_theme_option;
			if(!isset($theevent_theme_option['sidebar-element'])){$theevent_theme_option['sidebar-element'] = array('blog','contact');}
			if( !class_exists('theevent_page_options') ) return;
			new theevent_page_options( 
				
				// page option settings
				array(
					'page-layout' => array(
						'title' => __('Page Layout', 'the-event'),
						'options' => array(
								'sidebar' => array(
									'type' => 'radioimage',
									'description' => 'Please select the side bar position from here.',
									'options' => array(
										'no-sidebar'=>		THEEVENT_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
										'both-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
										'right-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
										'left-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
									),
									'default' => 'no-sidebar'
								),	
								'left-sidebar' => array(
									'title' => __('Left Sidebar' , 'the-event'),
									'type' => 'combobox_sidebar',
									'options' => $theevent_theme_option['sidebar-element'],
									'wrapper-class' => 'sidebar-wrapper left-sidebar-wrapper both-sidebar-wrapper'
								),
								'right-sidebar' => array(
									'title' => __('Right Sidebar' , 'the-event'),
									'type' => 'combobox_sidebar',
									'options' => $theevent_theme_option['sidebar-element'],
									'wrapper-class' => 'sidebar-wrapper right-sidebar-wrapper both-sidebar-wrapper'
								),						
						)
					),
					
					'page-option' => array(
						'title' => __('Page Option', 'the-event'),
						'options' => array(
							'page-caption' => array(
								'title' => __('Page Caption' , 'the-event'),
								'type' => 'textarea',
								'description' => 'Please enter the Caption for the page here.',
							),							
							'header-background' => array(
								'title' => __('Header Background Image' , 'the-event'),
								'button' => __('Upload', 'the-event'),
								'type' => 'upload',
								'description' => 'Click here to Upload the Header Background Image.',
							),	
							'team-right-side' => array(
								'title' => __('Team Right Side Content' , 'the-event'),
								'type' => 'textarea',
								'wrapper-class' => '-wrapper single-team-layout-wrapper',
								'description' => 'Please add shortcode or content right to this team member like education or skill here activate only on lawyer layout which you can manage from xpress options > Team Single.',
							),
							'team-qoute' => array(
								'title' => __('Team Member Famous Qoute' , 'the-event'),
								'type' => 'textarea',
								'description' => 'Please enter the famous quote of the Team Member here.',
							),
							'designation' => array(
								'title' => __('Designation' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Designation of the Team Member here.',
							),
							'phone' => array(
								'title' => __('Phone Number' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Team Member Contact phone number here.',
							),
							'website' => array(
								'title' => __('Website' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Team Member Contact Website URL here.',
							),
							'email' => array(
								'title' => __('Email Id' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Team Member Contact Email Address here.',
							),	
							'facebook' => array(
								'title' => __('Facebook' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Facebook social profile URL of the Team Member here.',
							),
							'twitter' => array(
								'title' => __('Twitter' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Twitter social profile URL of the Team Member here.',
							),
							'youtube' => array(
								'title' => __('Youtube' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Youtube social profile URL of the Team Member here.',
							),
							'pinterest' => array(
								'title' => __('Pinterest' , 'the-event'),
								'type' => 'text',
								'description' => 'Please enter the Pinterest social profile URL of the Team Member here.',
							),	
						)
					),

				),
				// page option attribute
				array(
					'post_type' => array('team'),
					'meta_title' => __('Team Option', 'the-event'),
					'meta_slug' => 'team-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);
			
		}
	}	
	
	// add team in page builder area
	add_filter('theevent_page_builder_option', 'theevent_register_team_item');
	if( !function_exists('theevent_register_team_item') ){
		function theevent_register_team_item( $page_builder = array() ){
			global $theevent_spaces;
		
			$page_builder['content-item']['options']['team'] = array(
				'title'=> __('Team', 'the-event'), 
				'icon'=>'fa-user-plus',
				'type'=>'item',
				'options'=>array(					
					'title-num-excerpt'=> array(
						'title'=> __('Title Num Length (Word)' ,'KodeForest'),
						'type'=> 'text',	
						'default'=> '15',
						'description'=> __('This is a number of word (decided by spaces) that you want to show on the team title. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'KodeForest')
					),
					'category'=> array(
						'title'=> __('Category' ,'the-event'),
						'type'=> 'combobox',
						'options'=> theevent_get_term_list('team_category'),
						'description'=> __('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'the-event')
					),	
					'tag'=> array(
						'title'=> __('Tag' ,'the-event'),
						'type'=> 'combobox',
						'options'=> theevent_get_term_list('team_tag'),
						'description'=> __('Will be ignored when the team filter option is enabled.', 'the-event')
					),					
					'team-style'=> array(
						'title'=> __('Team Style' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(							
							'full-view' => __('Full View', 'the-event'),
							'simple-view' => __('Simple View', 'the-event'),
							'normal-view' => __('Normal View', 'the-event'),
						),
					),	
					'feature-image-align'=> array(
						'title'=> __('Feature Image Alignment' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'left' => __('Left Align', 'the-event'),
							'right' => __('Right Align', 'the-event'),
						),
						'wrapper-class' => 'team-style-wrapper full-view-wrapper',
					),
					'thumbnail-size' => array(
						'title' => esc_html__('Thumbnail Size', 'kf_democracy'),
						'type'=> 'combobox',
						'options'=> theevent_get_thumbnail_list(),
						'wrapper-class' => 'type-1-wrapper type-4-wrapper style-wrapper',
						'default'=> 'theevent-post-thumbnail-size'
					),
					'team-column'=> array(
						'title'=> __('Team Column Size' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'2' => __('2 Column', 'the-event'),
							'3' => __('3 Column', 'the-event'),
							'4' => __('4 Column', 'the-event')
						),
						'wrapper-class' => 'team-style-wrapper simple-view-wrapper normal-view-wrapper modern-view-wrapper',
					),						
					'num-fetch'=> array(
						'title'=> __('Num Fetch' ,'the-event'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> __('Specify the number of teams you want to pull out.', 'the-event')
					),	
					'num-excerpt'=> array(
						'title'=> __('Num Excerpt' ,'the-event'),
						'type'=> 'text',	
						'default'=> '20',						
					),
					'orderby'=> array(
						'title'=> __('Order By' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'date' => __('Publish Date', 'the-event'), 
							'title' => __('Title', 'the-event'), 
							'rand' => __('Random', 'the-event'), 
						)
					),
					'order'=> array(
						'title'=> __('Order' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>__('Descending Order', 'the-event'), 
							'asc'=> __('Ascending Order', 'the-event'), 
						)
					),			
					'pagination'=> array(
						'title'=> __('Enable Pagination' ,'the-event'),
						'type'=> 'checkbox'
					),					
					'margin-bottom' => array(
						'title' => __('Margin Bottom', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => __('Spaces after ending of this item', 'the-event')
					),				
				)
			);
			
			$page_builder['content-item']['options']['speaker'] = array(
				'title'=> __('Speaker', 'the-event'), 
				'icon'=>'fa-user-plus',
				'type'=>'item',
				'options'=>array(					
					'title-num-excerpt'=> array(
						'title'=> __('Title Num Length (Word)' ,'KodeForest'),
						'type'=> 'text',	
						'default'=> '15',
						'description'=> __('This is a number of word (decided by spaces) that you want to show on the team title. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'KodeForest')
					),
					'alignment'=> array(
						'title'=> __('Alignmnet' ,'the-event'),
						'type'=> 'combobox',
						'options'=> array(
							'left'=>__('Left Align', 'the-event'), 
							'right'=> __('Right Align', 'the-event'), 
						)
					),	
					'candidate'=> array(
						'title'=> __('Candidate' ,'the-event'),
						'type'=> 'combobox',
						'options'=> theevent_get_post_list_id('team'),
						'description'=> __('You can select candidate to show its biography.', 'the-event')
					),	
					'num-excerpt'=> array(
						'title'=> __('Num Excerpt' ,'the-event'),
						'type'=> 'text',	
						'default'=> '20',						
					),
					'thumbnail-size' => array(
						'title' => esc_html__('Thumbnail Size', 'kf_democracy'),
						'type'=> 'combobox',
						'options'=> theevent_get_thumbnail_list(),
						'wrapper-class' => 'type-1-wrapper type-4-wrapper style-wrapper',
						'default'=> 'theevent-post-thumbnail-size'
					),
					'margin-bottom' => array(
						'title' => __('Margin Bottom', 'the-event'),
						'type' => 'text',
						'default' => '',
						'description' => __('Spaces after ending of this item', 'the-event')
					),				
				)
			);
			return $page_builder;
		}
	}
	
?>