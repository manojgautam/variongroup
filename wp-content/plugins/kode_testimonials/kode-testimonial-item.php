<?php
	/*	
	*	Testimonial Listing
	*	---------------------------------------------------------------------
	*	This file contains functions that help you create testimonial item
	*	---------------------------------------------------------------------
	*/
	
	//Testimonial Listing
	if( !function_exists('theevent_get_testimonial_item') ){
		function theevent_get_testimonial_item( $settings ){
			// $settings['category'];
			// $settings['tag'];
			// $settings['num-excerpt'];
			// $settings['num-fetch'];
			// $settings['testimonial-style'];
			// $settings['scope'];
			// $settings['order'];
			// $settings['margin-bottom'];
			// query posts section
			$args = array('post_type' => 'testimonial', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			//$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : 1;

			if( !empty($settings['category']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'testimonial_category', 'field'=>'slug'));
				}
				// if( !empty($settings['tag'])){
					// array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'testimonial_tag', 'field'=>'slug'));
				// }				
			}			
			$query = new WP_Query( $args );

			// create the testimonial filter
			$settings['num-excerpt'] = empty($settings['num-excerpt'])? '196' : $settings['num-excerpt'];
			$settings['testi-size'] = empty($settings['testi-size'])? '2' : $settings['testi-size'];
			
			$size = 4;
			
			if($settings['testimonial-style'] == 'simple-view'){
				$testimonial  = '<div class="owl-carousel owl-theme kode_testimonial_wrap" data-slide="'.esc_attr($settings['testi-size']).'" >';
			}else if($settings['testimonial-style'] == 'modern-view'){
				$testimonial  = '<div class="owl-carousel owl-theme kf_testimonial_bg" data-slide="'.esc_attr($settings['testi-size']).'" >';
			}else if($settings['testimonial-style'] == 'normal-view'){
				$testimonial  = '<div class="kd-testimonial" > <ul class="bxslider">';
			}else{
				$testimonial  = '<div class="owl-carousel owl-theme kode_testimonial_wrap" data-slide="'.esc_attr($settings['testi-size']).'" >';
			}
			while($query->have_posts()){ $query->the_post();
			global $post;
			$testimonial_option = json_decode(theevent_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true)), true);
				// if( $current_size % $size == 0 ){
					// $testimonial .= '<div class="clear"></div>';
				// }	designation
				
				
				if($settings['testimonial-style'] == 'simple-view'){
				$testimonial .= '      
				<div class="client-holder">
					<div class="client-content">
						<blockquote>
							<q>'.substr(get_the_content(),0,$settings['num-excerpt']).'</q>
						</blockquote>
					</div>
					
					<div class="client-header">
						<div class="client-pic">
							<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, array(80,80)).'</a>
						</div>
						<h3>'.esc_attr(get_the_title()).'</h3>
						<strong>'.esc_attr($testimonial_option['designation']).'</strong>
					</div>
				</div>
                <!--ITEM END-->';
				}else if($settings['testimonial-style'] == 'modern-view'){
					$testimonial .= '					
					<div class="kf_event_testi_wrap">
						<figure>
							<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, array(80,80)).'</a>
						</figure>
						<p>'.substr(get_the_content(),0,$settings['num-excerpt']).'</p>
						<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).' </a></h4>
						<span>'.esc_attr($testimonial_option['designation']).'</span>
					</div>';
				}else if($settings['testimonial-style'] == 'normal-view'){
					$testimonial .= "
					 <li>
                      <i>''</i>
                     <p>".strip_tags(mb_substr(get_the_content(),0,$settings['num-excerpt']))."</p>
                      <span>".esc_attr($testimonial_option['designation'])."</span>
                    </li>
					";
				}else{
					$testimonial .= '      
				<div class="client-holder">
					<div class="client-content">
						<blockquote>
							<q>'.substr(get_the_content(),0,$settings['num-excerpt']).'</q>
						</blockquote>
					</div>
					
					<div class="client-header">
						<div class="client-pic">
							<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, array(80,80)).'</a>
						</div>
						<h3>'.esc_attr(get_the_title()).'</h3>
						<strong>'.esc_attr($testimonial_option['designation']).'</strong>
					</div>
				</div>
                <!--ITEM END-->';
				}
				//$current_size++;
			}
			if($settings['testimonial-style'] == 'simple-view'){
				$testimonial .= '</div>';
			}else if($settings['testimonial-style'] == 'modern-view'){
				$testimonial .= '</div>';
			}else{
				$testimonial .= '</div>';
			}			
			
			return $testimonial;
		}
	}	
	
?>