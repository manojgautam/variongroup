<?php
	/*	
	*	Testimonial Option file
	*	---------------------------------------------------------------------
	*	This file creates all testimonial options and attached to the theme
	*	---------------------------------------------------------------------
	*/
	
	// add a testimonial option to testimonial page
	if( is_admin() ){ add_action('after_setup_theme', 'theevent_create_testimonial_options'); }
	if( !function_exists('theevent_create_testimonial_options') ){
	
		function theevent_create_testimonial_options(){
			global $theevent_theme_option;
			if(!isset($theevent_theme_option['sidebar-element'])){$theevent_theme_option['sidebar-element'] = array('blog','contact');}
			if( !class_exists('theevent_page_options') ) return;
			new theevent_page_options( 
				
				
					  
				// page option settings
				array(
					'page-layout' => array(
						'title' => __('Page Layout', 'theevent_testimonial'),
						'options' => array(
							'sidebar' => array(
								'type' => 'radioimage',
								'options' => array(
									'no-sidebar'=>		THEEVENT_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
									'both-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
									'right-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
									'left-sidebar'=>	THEEVENT_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
								),
								'default' => 'no-sidebar'
							),	
							'left-sidebar' => array(
								'title' => __('Left Sidebar' , 'theevent_testimonial'),
								'type' => 'combobox',
								'options' => $theevent_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper left-sidebar-wrapper both-sidebar-wrapper'
							),
							'right-sidebar' => array(
								'title' => __('Right Sidebar' , 'theevent_testimonial'),
								'type' => 'combobox',
								'options' => $theevent_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper right-sidebar-wrapper both-sidebar-wrapper'
							),						
						)
					),
					
					'page-option' => array(
						'title' => __('Page Option', 'theevent_testimonial'),
						'options' => array(
							'page-caption' => array(
								'title' => __('Page Caption' , 'theevent_testimonial'),
								'type' => 'textarea'
							),							
							'header-background' => array(
								'title' => __('Header Background Image' , 'theevent_testimonial'),
								'button' => __('Upload', 'theevent_testimonial'),
								'type' => 'upload',
							),	
							'designation' => array(
								'title' => __('Designation' , 'theevent_testimonial'),
								'type' => 'text',
							),						
						)
					),

				),
				
				// page option attribute
				array(
					'post_type' => array('testimonial'),
					'meta_title' => __('Testimonial Option', 'theevent_testimonial'),
					'meta_slug' => 'testimonial-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);
			
		}
	}	
	
	// add testimonial in page builder area
	add_filter('theevent_page_builder_option', 'theevent_register_testimonial_item');
	if( !function_exists('theevent_register_testimonial_item') ){
		function theevent_register_testimonial_item( $page_builder = array() ){
			global $theevent_spaces;
		
			$page_builder['content-item']['options']['testimonial'] = array(
				'title'=> __('Testimonial', 'theevent_testimonial'), 
				'icon'=>'fa-quote-left',
				'type'=>'item',
				'options'=>array(					
					'category'=> array(
						'title'=> __('Category' ,'theevent_testimonial'),
						'type'=> 'multi-combobox',
						'options'=> theevent_get_term_list('testimonial_category'),
						'description'=> __('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'theevent_testimonial')
					),	
					// 'tag'=> array(
						// 'title'=> __('Tag' ,'theevent_testimonial'),
						// 'type'=> 'multi-combobox',
						// 'options'=> theevent_get_term_list('testimonial_tag'),
						// 'description'=> __('Will be ignored when the testimonial filter option is enabled.', 'theevent_testimonial')
					// ),					
					'testimonial-style'=> array(
						'title'=> __('Testimonial Style' ,'theevent_testimonial'),
						'type'=> 'combobox',
						'options'=> array(
							'simple-view' => __('Simple View', 'theevent_testimonial'),
							// 'normal-view' => __('Normal View', 'theevent_testimonial'),
							'modern-view' => __('Modern View', 'theevent_testimonial')
						),
					),		
					'testi-size'=> array(
						'title'=> esc_html__('Testimonial Size' ,'kf_democracy'),
						'type'=> 'combobox',
						'options'=> array(
							'1' => esc_html__('1 Column', 'kf_democracy'),
							'2' => esc_html__('2 Column', 'kf_democracy'),
							'3' => esc_html__('3 Column', 'kf_democracy'),
							'4' => esc_html__('4 Column', 'kf_democracy'),
							'5' => esc_html__('5 Column', 'kf_democracy'),
							'6' => esc_html__('6 Column', 'kf_democracy'),
						),
						'wrapper-class' => '',
						'default'=>'2'
					),					
					'num-fetch'=> array(
						'title'=> __('Num Fetch' ,'theevent_testimonial'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> __('Specify the number of testimonials you want to pull out.', 'theevent_testimonial')
					),	
					'num-excerpt'=> array(
						'title'=> __('Num Excerpt' ,'theevent_testimonial'),
						'type'=> 'text',	
						'default'=> '20'
					),
					'orderby'=> array(
						'title'=> __('Order By' ,'theevent_testimonial'),
						'type'=> 'combobox',
						'options'=> array(
							'date' => __('Publish Date', 'theevent_testimonial'), 
							'title' => __('Title', 'theevent_testimonial'), 
							'rand' => __('Random', 'theevent_testimonial'), 
						)
					),
					'order'=> array(
						'title'=> __('Order' ,'theevent_testimonial'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>__('Descending Order', 'theevent_testimonial'), 
							'asc'=> __('Ascending Order', 'theevent_testimonial'), 
						)
					),			
					// 'pagination'=> array(
						// 'title'=> __('Enable Pagination' ,'theevent_testimonial'),
						// 'type'=> 'checkbox'
					// ),					
					'margin-bottom' => array(
						'title' => __('Margin Bottom', 'theevent_testimonial'),
						'type' => 'text',
						'default' => '',
						'description' => __('Spaces after ending of this item', 'theevent_testimonial')
					),				
				)
			);
			return $page_builder;
		}
	}
	
?>