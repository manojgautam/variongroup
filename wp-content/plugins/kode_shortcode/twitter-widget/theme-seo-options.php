<?php
class theevent_seo_frontend {

	/**
	 * @var    object    Instance of this class
	 */
	public static $theevent_instance;

	public $options = array();

	private $ob_started = false;

	private $theevent_title = null;

	protected function __construct() {

		add_action( 'wp_head', array( $this, 'theevent_head' ), 1 );

		add_action( 'wpseo_head', array( $this, 'theevent_debug_marker' ), 2 );
		
		add_action( 'wpseo_head', array( $this, 'theevent_socialize' ), 2 );
		
		add_action( 'wpseo_head', array( $this, 'theevent_generate_description' ), 2 );		
		
		add_filter( 'pre_get_document_title', array( $this, 'theevent_title' ), 15 );
		
		add_filter( 'wp_title', array( $this, 'theevent_title' ), 15, 3 );

		add_filter( 'thematic_doctitle', array( $this, 'theevent_title' ), 15 );
		
		add_action( 'template_redirect', array( $this, 'theevent_force_rewrite_output_buffer' ), 99999 );
		
		add_action( 'wp_footer', array( $this, 'theevent_flush_cache' ), - 1 );
				
	}


	public static function theevent_get_instance() {
		if ( ! ( self::$theevent_instance instanceof self ) ) {
			self::$theevent_instance = new self();
		}

		return self::$theevent_instance;
	}

	public function theevent_is_home_posts_page() {
		return ( is_home() && 'posts' == get_option( 'show_on_front' ) );
	}

	public function theevent_is_home_static_page() {
		return ( is_front_page() && 'page' == get_option( 'show_on_front' ) && is_page( get_option( 'page_on_front' ) ) );
	}

	public function theevent_is_posts_page() {
		return ( is_home() && 'page' == get_option( 'show_on_front' ) );
	}

	public function theevent_title( $title, $separator = '', $separator_location = '' ) {
		if ( is_null( $this->theevent_title ) ) {
			$this->title = $this->theevent_generate_title( $title, $separator_location );
		}

		return $this->title;
	}

	private function theevent_generate_title( $title, $separator_location ) {
		global $theevent_theme_option;
		$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
		if( !empty($theevent_post_option) ){
			$theevent_post_option = json_decode( $theevent_post_option, true );					
		}
		
		if ( is_feed() ) {
			return $title;
		}


		if ( '' === trim( $separator_location ) ) {
			$separator_location = ( is_rtl() ) ? 'left' : 'right';
		}

		$original_title = $title;

		$modified_title = true;

		$title_part = '';

		if ( $this->theevent_is_home_static_page() ) {
			if(isset($theevent_theme_option['home-seo-meta-title']) || $theevent_theme_option['home-post-meta-title'] <> ''){
				$title = $theevent_theme_option['home-seo-meta-title'];
			}else{
				$title = $theevent_post_option['post-seo-meta-title'];	
			}			
		}
		elseif ( $this->theevent_is_home_posts_page() ) {
			if(!isset($theevent_theme_option['post-seo-meta-title']) || $theevent_theme_option['post-seo-meta-title'] == ''){
				$title = get_the_title();	
			}else{
				$title = $theevent_theme_option['post-seo-meta-title'];
			}			
			
		}
		elseif ( $this->theevent_is_posts_page() ) {
			$title = get_the_title(get_option( 'page_for_posts' ));				
		}
		elseif ( is_singular() ) {
			if(!isset($theevent_post_option['post-seo-meta-title']) || $theevent_post_option['post-seo-meta-title'] == ''){
				$title = get_the_title();	
			}else{
				$title = $theevent_post_option['post-seo-meta-title'];	
			}
			
			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = $original_title;
			}
			
		}
		elseif ( is_search() ) {
			if(!isset($theevent_theme_option['search-seo-meta-title']) || $theevent_theme_option['search-seo-meta-title'] == ''){
				$title = get_the_title();
			}else{
				$title = $theevent_theme_option['search-seo-meta-title'];
			}

			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = sprintf( __( 'Search for "%s"', 'kf_petcare' ), esc_html( get_search_query() ) );
			}
		}
		elseif ( is_category() || is_tag() || is_tax() ) {
			if(!isset($theevent_theme_option['archive-seo-meta-title']) || $theevent_theme_option['archive-seo-meta-title'] == ''){
				$title = get_the_title();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-title'];
			}	

			if ( ! is_string( $title ) || '' === $title ) {
				if ( is_category() ) {
					$title_part = single_cat_title( '', false );
				}
				elseif ( is_tag() ) {
					$title_part = single_tag_title( '', false );
				}
				else {
					$title_part = single_term_title( '', false );
					if ( $title_part === '' ) {
						$term       = $GLOBALS['wp_query']->get_queried_object();
						$title_part = $term->name;
					}
				}
			}
		}
		elseif ( is_author() ) {
			$title = get_the_title();

			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = get_the_author_meta( 'display_name', get_query_var( 'author' ) );
			}
		}
		elseif ( is_post_type_archive() ) {
			$post_type = get_query_var( 'post_type' );

			if ( is_array( $post_type ) ) {
				$post_type = reset( $post_type );
			}

			if(!isset($theevent_theme_option['archive-seo-meta-title']) || $theevent_theme_option['archive-seo-meta-title'] == ''){
				$title = get_the_title();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-title'];
			}	

			if ( ! is_string( $title ) || '' === $title ) {
				$post_type_obj = get_post_type_object( $post_type );
				if ( isset( $post_type_obj->labels->menu_name ) ) {
					$title_part = $post_type_obj->labels->menu_name;
				}
				elseif ( isset( $post_type_obj->name ) ) {
					$title_part = $post_type_obj->name;
				}
				else {
					$title_part = ''; // To be determined what this should be.
				}
			}
		}
		elseif ( is_archive() ) {
			if(!isset($theevent_theme_option['archive-seo-meta-title']) || $theevent_theme_option['archive-seo-meta-title'] == ''){
				$title = get_the_title();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-title'];
			}	

			if ( empty( $title ) ) {
				if ( is_month() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), single_month_title( ' ', false ) );
				}
				elseif ( is_year() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_query_var( 'year' ) );
				}
				elseif ( is_day() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_the_date() );
				}
				else {
					$title_part = __( 'Archives', 'kf_petcare' );
				}
			}
		}
		elseif ( is_404() ) {

			if ( 0 !== get_query_var( 'year' ) || ( 0 !== get_query_var( 'monthnum' ) || 0 !== get_query_var( 'day' ) ) ) {
				// @todo [JRF => Yoast] Should these not use the archive default if no title found ?
				if ( 0 !== get_query_var( 'day' ) ) {
					$date       = sprintf( '%04d-%02d-%02d 00:00:00', get_query_var( 'year' ), get_query_var( 'monthnum' ), get_query_var( 'day' ) );
					$date       = mysql2date( get_option( 'date_format' ), $date, true );
					$date       = apply_filters( 'get_the_date', $date, '' );
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), $date );
				}
				elseif ( 0 !== get_query_var( 'monthnum' ) ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), single_month_title( ' ', false ) );
				}
				elseif ( 0 !== get_query_var( 'year' ) ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_query_var( 'year' ) );
				}
				else {
					$title_part = __( 'Archives', 'kf_petcare' );
				}
			}
			else {
				if(!isset($theevent_theme_option['error-seo-meta-title']) || $theevent_theme_option['error-seo-meta-title'] == ''){
					$title = get_the_title();
				}else{
					$title = $theevent_theme_option['error-seo-meta-title'];
				}

				if ( empty( $title ) ) {
					$title_part = __( 'Page not found', 'kf_petcare' );
				}
			}
		}
		else {
			// In case the page type is unknown, leave the title alone.
			$modified_title = false;

		}

		if ( ( $modified_title && empty( $title ) ) || ! empty( $title_part ) ) {
			$title = $title = get_the_title();
		}

		if ( defined( 'ICL_LANGUAGE_CODE' ) && false !== strpos( $title, ICL_LANGUAGE_CODE ) ) {
			$title = str_replace( ' @' . ICL_LANGUAGE_CODE, '', $title );
		}

		return esc_html( strip_tags( stripslashes( $title ) ) );
	}
	
	
	public function theevent_generate_keyword( ) {
		global $theevent_theme_option;
		$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
		if( !empty($theevent_post_option) ){
			$theevent_post_option = json_decode( $theevent_post_option, true );					
		}
		$original_title = '';
		if ( is_feed() ) {
			return $title;
		}

		$separator_location = '';
		if ( '' === trim( $separator_location ) ) {
			$separator_location = ( is_rtl() ) ? 'left' : 'right';
		}

		$title = '';;

		$modified_title = true;

		$title_part = '';

		if ( $this->theevent_is_home_static_page() ) {
			if(isset($theevent_theme_option['home-seo-meta-keyword']) || $theevent_theme_option['home-post-meta-keyword'] <> ''){
				$title = $theevent_theme_option['home-seo-meta-keyword'];
			}else{
				$title = $theevent_post_option['post-seo-meta-keyword'];	
			}	
		}
		elseif ( $this->theevent_is_home_posts_page() ) {
			if(!isset($theevent_theme_option['post-seo-meta-keyword']) || $theevent_theme_option['post-seo-meta-keyword'] == ''){
				$title = get_the_content();	
			}else{
				$title = $theevent_theme_option['post-seo-meta-keyword'];
			}			
			
		}
		elseif ( $this->theevent_is_posts_page() ) {
			$title = get_the_content(get_option( 'page_for_posts' ));				
		}
		elseif ( is_singular() ) {
			if(!isset($theevent_post_option['post-seo-meta-keyword']) || $theevent_post_option['post-seo-meta-keyword'] == ''){
				$title = get_the_content();	
			}else{
				$title = $theevent_post_option['post-seo-meta-keyword'];	
			}
			
			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = $original_title;
			}
			
		}
		elseif ( is_search() ) {
			if(!isset($theevent_theme_option['search-seo-meta-keyword']) || $theevent_theme_option['search-seo-meta-keyword'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['search-seo-meta-keyword'];
			}

			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = sprintf( __( 'Search for "%s"', 'kf_petcare' ), esc_html( get_search_query() ) );
			}
		}
		elseif ( is_category() || is_tag() || is_tax() ) {
			if(!isset($theevent_theme_option['archive-seo-meta-keyword']) || $theevent_theme_option['archive-seo-meta-keyword'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-keyword'];
			}	

			if ( ! is_string( $title ) || '' === $title ) {
				if ( is_category() ) {
					$title_part = single_cat_title( '', false );
				}
				elseif ( is_tag() ) {
					$title_part = single_tag_title( '', false );
				}
				else {
					$title_part = single_term_title( '', false );
					if ( $title_part === '' ) {
						$term       = $GLOBALS['wp_query']->get_queried_object();
						$title_part = $term->name;
					}
				}
			}
		}
		elseif ( is_author() ) {
			$title = get_the_content();

			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = get_the_author_meta( 'display_name', get_query_var( 'author' ) );
			}
		}
		elseif ( is_post_type_archive() ) {
			$post_type = get_query_var( 'post_type' );

			if ( is_array( $post_type ) ) {
				$post_type = reset( $post_type );
			}

			if(!isset($theevent_theme_option['archive-seo-meta-keyword']) || $theevent_theme_option['archive-seo-meta-keyword'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-keyword'];
			}	

			if ( ! is_string( $title ) || '' === $title ) {
				$post_type_obj = get_post_type_object( $post_type );
				if ( isset( $post_type_obj->labels->menu_name ) ) {
					$title_part = $post_type_obj->labels->menu_name;
				}
				elseif ( isset( $post_type_obj->name ) ) {
					$title_part = $post_type_obj->name;
				}
				else {
					$title_part = ''; // To be determined what this should be.
				}
			}
		}
		elseif ( is_archive() ) {
			if(!isset($theevent_theme_option['archive-seo-meta-keyword']) || $theevent_theme_option['archive-seo-meta-keyword'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-keyword'];
			}	

			if ( empty( $title ) ) {
				if ( is_month() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), single_month_title( ' ', false ) );
				}
				elseif ( is_year() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_query_var( 'year' ) );
				}
				elseif ( is_day() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_the_date() );
				}
				else {
					$title_part = __( 'Archives', 'kf_petcare' );
				}
			}
		}
		elseif ( is_404() ) {

			if ( 0 !== get_query_var( 'year' ) || ( 0 !== get_query_var( 'monthnum' ) || 0 !== get_query_var( 'day' ) ) ) {
				// @todo [JRF => Yoast] Should these not use the archive default if no title found ?
				if ( 0 !== get_query_var( 'day' ) ) {
					$date       = sprintf( '%04d-%02d-%02d 00:00:00', get_query_var( 'year' ), get_query_var( 'monthnum' ), get_query_var( 'day' ) );
					$date       = mysql2date( get_option( 'date_format' ), $date, true );
					$date       = apply_filters( 'get_the_date', $date, '' );
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), $date );
				}
				elseif ( 0 !== get_query_var( 'monthnum' ) ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), single_month_title( ' ', false ) );
				}
				elseif ( 0 !== get_query_var( 'year' ) ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_query_var( 'year' ) );
				}
				else {
					$title_part = __( 'Archives', 'kf_petcare' );
				}
			}
			else {
				if(!isset($theevent_theme_option['error-seo-meta-keyword']) || $theevent_theme_option['error-seo-meta-keyword'] == ''){
					$title = get_the_content();
				}else{
					$title = $theevent_theme_option['error-seo-meta-keyword'];
				}

				if ( empty( $title ) ) {
					$title_part = __( 'Page not found', 'kf_petcare' );
				}
			}
		}
		else {
			// In case the page type is unknown, leave the title alone.
			$modified_title = false;

		}

		if ( ( $modified_title && empty( $title ) ) || ! empty( $title_part ) ) {
			$title = $title = get_the_content();
		}

		return esc_html( strip_tags( stripslashes( $title ) ) );
	}
	
	
	public function theevent_generate_description( ) {
		global $theevent_theme_option;
		$theevent_post_option = theevent_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
		if( !empty($theevent_post_option) ){
			$theevent_post_option = json_decode( $theevent_post_option, true );					
		}
		
		if ( is_feed() ) {
			return $title;
		}

		$separator_location = '';
		if ( '' === trim( $separator_location ) ) {
			$separator_location = ( is_rtl() ) ? 'left' : 'right';
		}
		
		$original_title = '';
		$title = '';

		$modified_title = true;

		$title_part = '';

		if ( $this->theevent_is_home_static_page() ) {
			if(isset($theevent_theme_option['home-seo-meta-description']) || $theevent_theme_option['home-post-meta-description'] <> ''){
				$title = $theevent_theme_option['home-seo-meta-description'];
			}else{
				$title = $theevent_post_option['post-seo-meta-description'];	
			}			
		}
		elseif ( $this->theevent_is_home_posts_page() ) {
			if(!isset($theevent_theme_option['post-seo-meta-description']) || $theevent_theme_option['post-seo-meta-description'] == ''){
				$title = get_the_content();	
			}else{
				$title = $theevent_theme_option['post-seo-meta-description'];
			}			
			
		}
		elseif ( $this->theevent_is_posts_page() ) {
			$title = get_the_content(get_option( 'page_for_posts' ));				
		}
		elseif ( is_singular() ) {
			if(!isset($theevent_post_option['post-seo-meta-description']) || $theevent_post_option['post-seo-meta-description'] == ''){
				$title = get_the_content();	
			}else{
				$title = $theevent_post_option['post-seo-meta-description'];	
			}
			
			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = $original_title;
			}
			
		}
		elseif ( is_search() ) {
			if(!isset($theevent_theme_option['search-seo-meta-description']) || $theevent_theme_option['search-seo-meta-description'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['search-seo-meta-description'];
			}

			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = sprintf( __( 'Search for "%s"', 'kf_petcare' ), esc_html( get_search_query() ) );
			}
		}
		elseif ( is_category() || is_tag() || is_tax() ) {
			if(!isset($theevent_theme_option['archive-seo-meta-description']) || $theevent_theme_option['archive-seo-meta-description'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-description'];
			}	

			if ( ! is_string( $title ) || '' === $title ) {
				if ( is_category() ) {
					$title_part = single_cat_title( '', false );
				}
				elseif ( is_tag() ) {
					$title_part = single_tag_title( '', false );
				}
				else {
					$title_part = single_term_title( '', false );
					if ( $title_part === '' ) {
						$term       = $GLOBALS['wp_query']->get_queried_object();
						$title_part = $term->name;
					}
				}
			}
		}
		elseif ( is_author() ) {
			$title = get_the_content();

			if ( ! is_string( $title ) || '' === $title ) {
				$title_part = get_the_author_meta( 'display_name', get_query_var( 'author' ) );
			}
		}
		elseif ( is_post_type_archive() ) {
			$post_type = get_query_var( 'post_type' );

			if ( is_array( $post_type ) ) {
				$post_type = reset( $post_type );
			}

			if(!isset($theevent_theme_option['archive-seo-meta-description']) || $theevent_theme_option['archive-seo-meta-description'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-description'];
			}	

			if ( ! is_string( $title ) || '' === $title ) {
				$post_type_obj = get_post_type_object( $post_type );
				if ( isset( $post_type_obj->labels->menu_name ) ) {
					$title_part = $post_type_obj->labels->menu_name;
				}
				elseif ( isset( $post_type_obj->name ) ) {
					$title_part = $post_type_obj->name;
				}
				else {
					$title_part = ''; // To be determined what this should be.
				}
			}
		}
		elseif ( is_archive() ) {
			if(!isset($theevent_theme_option['archive-seo-meta-description']) || $theevent_theme_option['archive-seo-meta-description'] == ''){
				$title = get_the_content();
			}else{
				$title = $theevent_theme_option['archive-seo-meta-description'];
			}	

			if ( empty( $title ) ) {
				if ( is_month() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), single_month_title( ' ', false ) );
				}
				elseif ( is_year() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_query_var( 'year' ) );
				}
				elseif ( is_day() ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_the_date() );
				}
				else {
					$title_part = __( 'Archives', 'kf_petcare' );
				}
			}
		}
		elseif ( is_404() ) {

			if ( 0 !== get_query_var( 'year' ) || ( 0 !== get_query_var( 'monthnum' ) || 0 !== get_query_var( 'day' ) ) ) {
				// @todo [JRF => Yoast] Should these not use the archive default if no title found ?
				if ( 0 !== get_query_var( 'day' ) ) {
					$date       = sprintf( '%04d-%02d-%02d 00:00:00', get_query_var( 'year' ), get_query_var( 'monthnum' ), get_query_var( 'day' ) );
					$date       = mysql2date( get_option( 'date_format' ), $date, true );
					$date       = apply_filters( 'get_the_date', $date, '' );
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), $date );
				}
				elseif ( 0 !== get_query_var( 'monthnum' ) ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), single_month_title( ' ', false ) );
				}
				elseif ( 0 !== get_query_var( 'year' ) ) {
					$title_part = sprintf( __( '%s Archives', 'kf_petcare' ), get_query_var( 'year' ) );
				}
				else {
					$title_part = __( 'Archives', 'kf_petcare' );
				}
			}
			else {
				if(!isset($theevent_theme_option['error-seo-meta-description']) || $theevent_theme_option['error-seo-meta-description'] == ''){
					$title = get_the_content();
				}else{
					$title = $theevent_theme_option['error-seo-meta-description'];
				}

				if ( empty( $title ) ) {
					$title_part = __( 'Page not found', 'kf_petcare' );
				}
			}
		}
		else {
			// In case the page type is unknown, leave the title alone.
			$modified_title = false;

		}

		if ( ( $modified_title && empty( $title ) ) || ! empty( $title_part ) ) {
			$title = $title = get_the_content();
		}

		return esc_html( strip_tags( stripslashes( $title ) ) );
	}

	public function theevent_add_to_title( $sep, $seplocation, $title, $title_part ) {
		if ( 'right' === $seplocation ) {
			return $title . $sep . $title_part;
		}

		return $title_part . $sep . $title;
	}

	function theevent_force_wp_title() {
		global $wp_query;
		$old_wp_query = null;

		if ( ! $wp_query->is_main_query() ) {
			$old_wp_query = $wp_query;
			wp_reset_query();
		}

		$title = $this->theevent_title( ' ' );

		if ( ! empty( $old_wp_query ) ) {
			$GLOBALS['wp_query'] = $old_wp_query;
			unset( $old_wp_query );
		}

		return $title;
	}

	public function theevent_debug_marker( $echo = true ) {
		$marker = sprintf('<!--KodeForest SEO Plugin-->',( 'v 4.1'));

		if ( $echo === false ) {
			return $marker;
		}
		else {
			echo "\n${marker}\n";
		}
	}
	
	public function theevent_socialize(){
		global $theevent_theme_option;
		// Alexa code
		if($theevent_theme_option['seo-meta-alexa'] <> ''){
			echo '<meta name="alexaVerifyID" content="' . esc_attr( $theevent_theme_option['seo-meta-alexa'] ) . "\" />\n";
		}
		// Bing code
		if($theevent_theme_option['seo-meta-bing'] <> ''){
			echo '<meta name="msvalidate.01" content="' . esc_attr( $theevent_theme_option['seo-meta-bing'] ) . "\" />\n";
		}
		// Google code
		if($theevent_theme_option['seo-meta-google'] <> ''){
			echo '<meta name="google-site-verification" content="' . esc_attr( $theevent_theme_option['seo-meta-google'] ) . "\" />\n";
		}		
		// Yandex code
		if($theevent_theme_option['seo-meta-yandex'] <> ''){
			echo '<meta name="yandex-verification" content="' . esc_attr( $theevent_theme_option['seo-meta-yandex'] ) . "\" />\n";
		}
		
		if($this->theevent_generate_keyword() <> ''){
			echo '<meta name="keywords" content="', esc_attr( strip_tags( stripslashes( $this->theevent_generate_keyword() ) ) ), '"/>', "\n";
		}
		if($this->theevent_generate_description() <> ''){
			echo '<meta name="description" content="', esc_attr( strip_tags( stripslashes( $this->theevent_generate_description() ) ) ), '"/>', "\n";
		}
		
		if($theevent_theme_option['seo-meta-bots'] == 'disable'){
			echo '<meta name="robots" content="noindex" />', "\n";
		}
	}

	
	public function theevent_head() {
		global $wp_query;

		$old_wp_query = null;

		if ( ! $wp_query->is_main_query() ) {
			$old_wp_query = $wp_query;
			wp_reset_query();
		}

		do_action( 'wpseo_head' );

		

		if ( ! empty( $old_wp_query ) ) {
			$GLOBALS['wp_query'] = $old_wp_query;
			unset( $old_wp_query );
		}

		return;
	}

	function theevent_flush_cache() {

		global $wp_query,$theevent_theme_option;
		
		if ( $this->ob_started !== true ) {
			return false;
		}

		$content = ob_get_clean();

		$old_wp_query = $wp_query;

		wp_reset_query();

		$title = $this->theevent_title( '' );
		
		if(isset($theevent_theme_option['seo-title-transform']) && $theevent_theme_option['seo-title-transform'] <> ''){
			if($theevent_theme_option['seo-title-transform'] == 'uppercase'){
				$title = strtoupper($title);	
			}else if($theevent_theme_option['seo-title-transform'] == 'lowercase'){
				$title = strtolower($title);
			}else if($theevent_theme_option['seo-title-transform'] == 'capitalize'){
				$title = ucfirst($title);
			}else{
				$title = $title;
			}
		}
		// Find all titles, strip them out and add the new one in within the debug marker, so it's easily identified whether a site uses force rewrite.
		$content = preg_replace( '/<title.*?\/title>/i', '', $content );
		$content = str_replace( $this->theevent_debug_marker( false ), $this->theevent_debug_marker( false ) . "\n" . '<title>' . $title . ' '.$theevent_theme_option['seo-title-sep'].' '.$theevent_theme_option['seo-website-slug'] .'</title>', $content );

		$GLOBALS['wp_query'] = $old_wp_query;

		echo $content;

		return true;
	}

	function theevent_force_rewrite_output_buffer() {
		$this->ob_started = true;
		ob_start();
	}
	
} /* End of class */


add_action( 'plugins_loaded', 'theevent_fire_seo_plugin', 15 );
function theevent_fire_seo_plugin(){
	
	add_action( 'init', 'theevent_initialize_wpseo_front', 15 );	
	
}
if ( ! function_exists( 'theevent_initialize_wpseo_front' ) ) {	
	function theevent_initialize_wpseo_front() {
		theevent_seo_frontend::theevent_get_instance();
	}
}