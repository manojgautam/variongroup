<?php
/**
 * Plugin Name: KodeForest Twitter Widget
 * Plugin URI: http://KodeForest.com/
 * Description: A widget that show feeds from twitter.
 * Version: 1.0
 * Author: KodeForest
 * Author URI: http://www.KodeForest.com
 *
 */
include_once('twitteroauth.php');
add_action( 'widgets_init', 'theevent_twitter_widget' );
if( !function_exists('theevent_twitter_widget') ){
	function theevent_twitter_widget() {
		register_widget( 'Kodeforest_Twitter_Widget' );
	}
}

if( !class_exists('Kodeforest_Twitter_Widget') ){
	class Kodeforest_Twitter_Widget extends WP_Widget {

		// Initialize the widget
		function __construct(){
			parent::__construct('theevent_twitter_widget', 
				__('Kodeforest Twitter','theevent_forest'), 
				array('description' => __('A widget that show twitter feeds.', 'theevent_forest')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			$twitter_username = $instance['twitter_username'];
			$show_num = $instance['show_num'];
			$consumer_key = $instance['consumer_key'];
			$consumer_secret = $instance['consumer_secret'];
			$access_token = $instance['access_token'];
			$access_token_secret = $instance['access_token_secret'];		
			$cache_time = $instance['cache_time'];		
			$layout = $instance['layout'];		
			
			
			// Opening of widget
			echo $args['before_widget'];
			if($layout == 'simple'){
				
				echo '<strong class="title">Twitter</strong>';
				// Open of title tag
				if( !empty($title) ){ 
					echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
				}
				
				$theevent_twitter = get_option('theevent_twitter', array());
				if( !is_array($theevent_twitter) && !empty($theevent_twitter) ){ 
					$theevent_twitter = unserialize($theevent_twitter);
				}
				if( !is_array($theevent_twitter) ){	
					$theevent_twitter = array(); 
				}
				
				if( empty($theevent_twitter[$twitter_username][$show_num]['data']) ||
					empty($theevent_twitter[$twitter_username][$show_num]['cache_time']) || 
					time() - intval($theevent_twitter[$twitter_username][$show_num]['cache_time']) >= ($cache_time * 3600)){
				
					$tweets_data_simple = theevent_get_tweets($consumer_key, $consumer_secret, 
						$access_token, $access_token_secret, $twitter_username, $show_num);
					
					if( !empty($tweets_data_simple) ){
						$theevent_twitter[$twitter_username][$show_num]['data'] = $tweets_data_simple;
						$theevent_twitter[$twitter_username][$show_num]['cache_time'] = time();
						
						update_option('theevent_twitter', $theevent_twitter);	
					}
				}else{
					$tweets_data_simple = $theevent_twitter[$twitter_username][$show_num]['data'];
				}
				
				echo '<ul class="twitterfeed-nav">';
				foreach( $tweets_data_simple as $tweet_data ){
					echo '<li>' . $tweet_data . '</li>';
				}
				echo '</ul>';
				
			}else{
				echo '<div class="kf_aside_twitter">';
				// Open of title tag
				if( !empty($title) ){ 
					echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
				}
				
				$theevent_twitter = get_option('theevent_twitter', array());
				if( !is_array($theevent_twitter) && !empty($theevent_twitter) ){ 
					$theevent_twitter = unserialize($theevent_twitter);
				}
				if( !is_array($theevent_twitter) ){	
					$theevent_twitter = array(); 
				}
				
				if( empty($theevent_twitter[$twitter_username][$show_num]['data']) ||
					empty($theevent_twitter[$twitter_username][$show_num]['cache_time']) || 
					time() - intval($theevent_twitter[$twitter_username][$show_num]['cache_time']) >= ($cache_time * 3600)){
				
					$tweets_data = theevent_get_tweets($consumer_key, $consumer_secret, 
						$access_token, $access_token_secret, $twitter_username, $show_num);
					
					if( !empty($tweets_data) ){
						$theevent_twitter[$twitter_username][$show_num]['data'] = $tweets_data;
						$theevent_twitter[$twitter_username][$show_num]['cache_time'] = time();
						
						update_option('theevent_twitter', $theevent_twitter);	
					}
				}else{
					$tweets_data = $theevent_twitter[$twitter_username][$show_num]['data'];
				}
				
				echo '<ul class="aside_tweets bxslider" data-slide="1" data-mode="fade" data-min="1" data-max="1">';
				foreach( $tweets_data as $tweet_data ){
					echo '<li>' . $tweet_data . '</li>';
				}
				echo '</ul>';
				echo '</div>';
				
				
				
				
			}
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$twitter_username = isset($instance['twitter_username'])? $instance['twitter_username']: '';
			$show_num = isset($instance['show_num'])? $instance['show_num']: '5';
			$consumer_key = isset($instance['consumer_key'] )? $instance['consumer_key']: '';
			$consumer_secret = isset($instance['consumer_secret'])? $instance['consumer_secret']: '';
			$access_token = isset($instance['access_token'])? $instance['access_token']: '';
			$access_token_secret = isset($instance['access_token_secret'])? $instance['access_token_secret']: '';
			$cache_time = isset($instance['cache_time'])? $instance['cache_time']: '1';		
			$layout = isset($instance['layout'])? $instance['layout']: 'slider';		
			
			?>
			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e( 'Title :', 'the-event' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>

			<!-- Twitter Username -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('twitter_username')); ?>"><?php esc_html_e( 'Twitter username :', 'the-event' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('twitter_username')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter_username')); ?>" type="text" value="<?php echo esc_attr($twitter_username); ?>" />
			</p>		
			
			<!-- Show Num --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'show_num' )); ?>"><?php esc_html_e('Show Count :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'show_num' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_num' )); ?>" type="text" value="<?php echo esc_attr($show_num); ?>" />
			</p>
			
			<!-- Consumer Key --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'consumer_key' )); ?>"><?php esc_html_e('Consumer Key :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'consumer_key' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'consumer_key' )); ?>" type="text" value="<?php echo esc_attr($consumer_key); ?>" />
			</p>

			<!-- Consumer Secret --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'consumer_secret' )); ?>"><?php esc_html_e('Consumer Secret :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'consumer_secret' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'consumer_secret' )); ?>" type="text" value="<?php echo esc_attr($consumer_secret); ?>" />
			</p>

			<!-- Access Token --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'access_token' )); ?>"><?php esc_html_e('Access Token :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'access_token' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'access_token' )); ?>" type="text" value="<?php echo esc_attr($access_token); ?>" />
			</p>

			<!-- Access Token Secret --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'access_token_secret' )); ?>"><?php esc_html_e('Access Token Secret :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'access_token_secret' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'access_token_secret' )); ?>" type="text" value="<?php echo esc_attr($access_token_secret); ?>" />
			</p>		

			<!-- Cache Time --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'cache_time' )); ?>"><?php esc_html_e('Cache Time (hour) :', 'the-event'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'cache_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'cache_time' )); ?>" type="text" value="<?php echo esc_attr($cache_time); ?>" />
			</p>	
			<!-- Post layout -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('layout')); ?>"><?php esc_html_e('Layout :', 'kf_democracy'); ?></label>		
				<select class="widefat" name="<?php echo esc_attr($this->get_field_name('layout')); ?>" id="<?php echo esc_attr($this->get_field_id('layout')); ?>">
				<option value="" <?php if(empty($layout)) echo ' selected '; ?>><?php esc_html_e('All', 'kf_democracy') ?></option>
					<option value="<?php esc_html_e('slider', 'kf_democracy') ?>" <?php if ($layout == 'slider') echo ' selected '; ?>><?php esc_html_e('slider', 'kf_democracy') ?></option>				
					<option value="<?php esc_html_e('simple', 'kf_democracy') ?>" <?php if ($layout == 'simple') echo ' selected '; ?>><?php esc_html_e('simple', 'kf_democracy') ?></option>				
				</select> 
			</p>
			
			<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['twitter_username'] = (empty($new_instance['twitter_username']))? '': strip_tags($new_instance['twitter_username']);
			$instance['show_num'] = (empty($new_instance['show_num']))? '': strip_tags($new_instance['show_num']);
			$instance['consumer_key'] = (empty($new_instance['consumer_key']))? '': strip_tags($new_instance['consumer_key']);
			$instance['consumer_secret'] = (empty($new_instance['consumer_secret']))? '': strip_tags($new_instance['consumer_secret']);
			$instance['access_token'] = (empty($new_instance['access_token']))? '': strip_tags($new_instance['access_token']);
			$instance['access_token_secret'] = (empty($new_instance['access_token_secret']))? '': strip_tags($new_instance['access_token_secret']);
			$instance['cache_time'] = (empty($new_instance['cache_time']))? '': strip_tags($new_instance['cache_time']);
			$instance['layout'] = (empty($new_instance['layout']))? '': strip_tags($new_instance['layout']);
			
			return $instance;
		}	
	}
}
?>