<?php
	/*	
	*	Kodeforest Woo Option file
	*	---------------------------------------------------------------------
	*	This file creates all post options to the post page
	*	---------------------------------------------------------------------
	*/

if( !function_exists('thevent_paypal_form') ){
	function thevent_paypal_form($settings){
		global $theevent_theme_option;
		
		$user = empty($theevent_theme_option['paypal-recipient-email'])? '': $theevent_theme_option['paypal-recipient-email'];
		$action = 'https://www.' . ((!empty($theevent_theme_option['paypal-action']) && $theevent_theme_option['paypal-action'] == 'sandbox')? 'sandbox.': '') . 'paypal.com/cgi-bin/webscr';
		$currency_code = empty($theevent_theme_option['paypal-currency-code'])? 'USD': $theevent_theme_option['paypal-currency-code'];

		
		ob_start();
?>
<!--BOOK_EVENT wrap START-->
<div class="book_event_wrap">
	<div class="book_event_des">
		<div class="book_event_heading">
			<h3><?php echo esc_attr($settings['booking-title']);?></h3>
		</div>
		<!--BOOK_EVENT FORM wrap START-->
		<div class="book_event_form_wrap">
			<form class="kode-paypal-form" action="<?php echo esc_attr($action); ?>" method="post" data-ajax="<?php echo esc_url(THEEVENT_AJAX_URL); ?>" >
				<div class="col-md-12">
					<input type="text" name="name" placeholder="Name" />
					<input type="text" name="last-name" placeholder="Last Name" />
					<input type="email" name="email" placeholder="Email" />
					<input type="text" name="phone" placeholder="Phone" />
					<textarea placeholder="Address" name="address"></textarea>
					<textarea placeholder="Additional Note" name="additional-note"></textarea>					
					
					<!--<input type="button" class="kode-button-mail kode-button with-border" value="<?php esc_attr_e('Book By E-Mail And We Will Contact You Back', 'the-event'); ?>" />-->
					<?php if(isset($theevent_theme_option['enable-paypal']) && $theevent_theme_option['enable-paypal'] == 'enable') { ?>
						<input type="button" class="kode-button-paypal kode-button with-border" value="<?php esc_attr_e('Check Out Via PayPal', 'the-event'); ?>" />
					<?php }?>
					<input type="hidden" name="cmd" value="_xclick" />
					<input type="hidden" name="business" value="<?php echo esc_attr($user); ?>" />
					<input type="hidden" name="price_item_name" value="<?php echo get_the_title(); ?>" />
					<input type="hidden" name="price_item_id" value="<?php echo get_the_ID(); ?>" />
					<input type="hidden" name="price_detail" value="" />
					<input type="hidden" name="amount" value="<?php echo esc_attr($settings['booking-price'])?>" />  
					<input type="hidden" name="return" value="<?php echo esc_url(get_permalink()); ?>" />
					<input type="hidden" name="no_shipping" value="0" />
					<input type="hidden" name="no_note" value="1" />
					<input type="hidden" name="currency_code" value="<?php echo esc_attr($currency_code); ?>" />
					<input type="hidden" name="lc" value="AU" />
					<input type="hidden" name="bn" value="PP-BuyNowBF" />
					<input type="hidden" class="kode-paypal-action" name="action" value="" />
					<input type="hidden" name="security" value="<?php echo wp_create_nonce('kode-paypal-create-nonce'); ?>" />
					<div class="kode-notice email-invalid" ><?php echo sprintf(__('Invalid Email Address ', 'the-event'),$settings['booking-by-email-btn']); ?></div>
					<div class="kode-notice require-field" ><?php echo sprintf(__('Please fill all required fields', 'the-event'),$settings['booking-by-paypal']); ?></div>
					<div class="kode-notice alert-message" ></div>
					<div class="kode-paypal-loader" ></div>
				</div>
			</form>
		</div>
		<!--BOOK_EVENT FORM wrap END-->
	</div>
</div>
<!--BOOK_EVENT wrap End-->
<?php	
		$ret = ob_get_contents();
		ob_end_clean();
		
		return $ret;
	}	
}

// ajax to send contact form
add_action( 'wp_ajax_send_contact_form_mail', 'thevent_send_contact_form_mail' );
add_action( 'wp_ajax_nopriv_send_contact_form_mail', 'thevent_send_contact_form_mail' );
if( !function_exists('thevent_send_contact_form_mail') ){
	function thevent_send_contact_form_mail(){
		global $theevent_theme_option;
	
		$ret = array();
		if( false && !check_ajax_referer('kode-paypal-create-nonce', 'security', false) ){
			$ret['status'] = 'failed'; 
			$ret['message'] = __('The page has been expired. Please refresh the page to try this again.', 'the-event');
		}else{
			$recipient = empty($theevent_theme_option['paypal-recipient-name'])? '': $theevent_theme_option['paypal-recipient-name'];			
			$ticket_price = 59;
			

			$headers  = 'From: ' . $recipient . ' <' . $_POST['business'] . '>' . "\r\n";
			$message  = __('Ticket Title :', 'the-event') . ' ' . $_POST['price_item_name'] . "\r\n";
			$message .= __('Name :', 'the-event') . ' ' . $_POST['name'] . ' ' . $_POST['last-name'] . "\r\n";
			$message .= __('Email :', 'the-event') . ' ' . $_POST['email'] . "\r\n";
			$message .= __('Phone :', 'the-event') . ' ' . $_POST['phone'] . "\r\n";
			$message .= __('Price :', 'the-event') . ' ' . $ticket_price . "\r\n";
			$message .= __('Address :', 'the-event') . ' ' . $_POST['address'] . "\r\n";
			$message .= __('Additional Message :', 'the-event') . ' ' . $_POST['additional-note'] . "\r\n";
	
			if( wp_mail($_POST['business'], __('You receive new ticket booking message', 'the-event'), $message, $headers ) ){
				$ret['status'] = 'success'; 
				$ret['message'] = __('Your message was sent successfully.', 'the-event');
			}else{
				$ret['status'] = 'failed'; 
				$ret['message'] = __('Failed to send your message. Please try later or contact the administrator by another method.', 'the-event');
				$ret['log'] = $message;
			}
		}
		die(json_encode($ret));		
	}
}

// ajax to save form data
add_action( 'wp_ajax_save_paypal_form', 'thevent_save_paypal_form' );
add_action( 'wp_ajax_nopriv_save_paypal_form', 'thevent_save_paypal_form' );
if( !function_exists('thevent_save_paypal_form') ){
	function thevent_save_paypal_form(){
		$ret = array();
		if( false && !check_ajax_referer('kode-paypal-create-nonce', 'security', false) ){
			$ret['status'] = 'failed'; 
			$ret['message'] = __('The page has been expired. Please refresh the page to try this again.', 'the-event');
		}else{
			$record = get_option('thevent_paypal',array());
			$item_id = sizeof($record); 


			$record[$item_id]['name'] = $_POST['name'];
			$record[$item_id]['last-name'] = $_POST['last-name'];
			$record[$item_id]['email'] = $_POST['email'];
			$record[$item_id]['phone'] = $_POST['phone'];
			$record[$item_id]['address'] = $_POST['address'];
			$record[$item_id]['addition'] = $_POST['additional-note'];
			$record[$item_id]['post-id'] = $_POST['price_item_id'];
			$record[$item_id]['amount'] = $_POST['amount'];
			
			$ret['status'] = 'success'; 
			$ret['message'] = __('Redirecting to paypal', 'the-event');
			$ret['amount'] = $_POST['amount'];
			$ret['price_detail'] = $item_id;
			
			update_option('thevent_paypal',$record);
		}
		die(json_encode($ret));
	}
}

if( isset($_GET['paypal']) ){
	global $thevent_theme_option;
	$paypal_action = 'https://www.' . ((!empty($thevent_theme_option['paypal-action']) && $thevent_theme_option['paypal-action'] == 'sandbox')? 'sandbox.': '') . 'paypal.com/cgi-bin/webscr';
	
	// STEP 1: read POST data
	 
	// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
	// Instead, read raw POST data from the input stream. 
	$raw_post_data = file_get_contents('php://input');
	$raw_post_array = explode('&', $raw_post_data);
	$myPost = array();
	foreach ($raw_post_array as $keyval) {
	  $keyval = explode ('=', $keyval);
	  if (count($keyval) == 2)
		 $myPost[$keyval[0]] = urldecode($keyval[1]);
	}
	// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
	$req = 'cmd=_notify-validate';
	if(function_exists('get_magic_quotes_gpc')) {
	   $get_magic_quotes_exists = true;
	} 
	foreach ($myPost as $key => $value) {        
	   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
			$value = urlencode(stripslashes($value)); 
	   } else {
			$value = urlencode($value);
	   }
	   $req .= "&$key=$value";
	}
	 
	 
	// Step 2: POST IPN data back to PayPal to validate
	$ch = curl_init($paypal_action);
	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
	 
	if( !($res = curl_exec($ch)) ) {
		curl_close($ch);
		exit;
	}
	// update_option('thevent_paypal', '1:' . $ret . ':2:' . curl_error($ch));
	// update_option('thevent_paypal', $_POST);
	curl_close($ch);
	
	// inspect IPN validation result and act accordingly
	if( strcmp ($res, "VERIFIED") == 0 ) {
		$recipient = empty($thevent_theme_option['paypal-recipient-name'])? '': $thevent_theme_option['paypal-recipient-name'];
	
		$record = get_option('thevent_paypal', array());
		$num = $_POST['price_detail'];
		$record[$num]['status'] = $_POST['payment_status'];
		$record[$num]['txn_id'] = $_POST['txn_id'];
		$record[$num]['pay_amount'] = $_POST['mc_gross'] . ' ' . $_POST['mc_currency'];
		
		$price_item_name = $_POST['price_item_name'];

		if( $_POST['payment_status'] == 'Completed' ){
		
			// send the mail
			$headers  = 'From: ' . $recipient . ' <' . $_POST['receiver_email'] . '>' . "\r\n";
			$message  = __('Thank you very much for your purchasing for', 'the-event') . ' ' . $_POST['price_item_name'] . "\r\n";
			$message .= __('Below is details of your purchasing.', 'the-event') . "\r\n";
			$message .= __('Name of Recipient :', 'the-event') . ' ' . $_POST['receiver_email'] . "\r\n";
			$message .= __('Name :', 'the-event') . ' ' . $record[$num]['name'] . ' ' . $record[$num]['last-name'] . "\r\n";
			$message .= __('Date :', 'the-event') . ' ' . $_POST['payment_date'] . "\r\n";
			$message .= __('Amount :', 'the-event') . ' ' . $record[$num]['amount'] . "\r\n";
			$message .= __('Pay Amount :', 'the-event') . ' ' . $record[$num]['pay_amount'] . "\r\n";
			$message .= __('Transaction ID :', 'the-event') . ' ' . $record[$num]['txn_id'] . "\r\n";
			$message .= __('Regards,', 'the-event') . ' ' . $recipient;
	
			if( wp_mail($record[$num]['email'], __('Thank you for your purchasing', 'the-event'), $message, $headers ) ){
				$record[$num]['mail_status'] = 'complete';
			}else{
				$record[$num]['mail_status'] = 'failed';
			}
			
			$headers  = 'From: ' . $recipient . "\r\n";
			$message  = __('Ticket Name :', 'the-event') . ' ' . $_POST['price_item_name'] . "\r\n";
			$message .= __('Name :', 'the-event') . ' ' . $record[$num]['name'] . ' ' . $record[$num]['last-name'] . "\r\n";
			$message .= __('Email :', 'the-event') . ' ' . $record[$num]['email'] . "\r\n";
			$message .= __('Phone :', 'the-event') . ' ' . $record[$num]['phone'] . "\r\n";
			$message .= __('Address :', 'the-event') . ' ' . $record[$num]['address'] . "\r\n";
			$message .= __('Additional Message :', 'the-event') . ' ' . $record[$num]['addition'] . "\r\n";
			$message .= __('Date :', 'the-event') . ' ' . $_POST['payment_date'] . "\r\n";
			$message .= __('Amount :', 'the-event') . ' ' . $record[$num]['amount'] . "\r\n";
			$message .= __('Pay Amount :', 'the-event') . ' ' . $record[$num]['pay_amount'] . "\r\n";
			$message .= __('Transaction ID :', 'the-event') . ' ' . $record[$num]['txn_id'];

			if( wp_mail($_POST['receiver_email'], __('You received new payment', 'the-event'), $message, $headers ) ){
				$record[$num]['notify_status'] = 'complete';
			}else{
				$record[$num]['notify_status'] = 'failed';
			}			
		}
		update_option('thevent_paypal', $record);
	}else if( strcmp ($res, "INVALID") == 0 ){
		echo "The response from IPN was: " . $res;
	}
}else if( isset($_GET['paypal_print']) && is_user_logged_in() ){
	print_r(get_option('thevent_paypal', array()));
	die();
}else if( isset($_GET['paypal_clear']) && is_user_logged_in() ){
	delete_option('thevent_paypal');
	echo 'Option Deleted';
	die();
}
?>