<?php
/*-----------------------------------------------------------------------------------*/
/*	Default Options
/*-----------------------------------------------------------------------------------*/

// Number of posts array
function kodeforest_shortcodes_range ( $range, $all = true, $default = false, $range_start = 1 ) {
	if($all) {
		$number_of_posts['-1'] = 'All';
	}

	if($default) {
		$number_of_posts[''] = 'Default';
	}

	foreach(range($range_start, $range) as $number) {
		$number_of_posts[$number] = $number;
	}

	return $number_of_posts;
}

// Taxonomies
function kodeforest_shortcodes_categories ( $taxonomy, $empty_choice = false ) {
	if($empty_choice == true) {
		$post_categories[''] = 'Default';
	}

	$get_categories = get_categories('hide_empty=0&taxonomy=' . $taxonomy);

	if( ! array_key_exists('errors', $get_categories) ) {
		if( $get_categories && is_array($get_categories) ) {
			$post_categories['All'] = 'All';
			foreach ( $get_categories as $cat ) {
				if(isset($cat->slug)){
					$post_categories[$cat->slug] = $cat->name;
				}	
			}
		}

		if(isset($post_categories)) {
			return $post_categories;
		}
	}
}
// return the slug list of each post
function get_post_list_sc( $post_type ){
	
	$posts = get_posts(array('post_type' => $post_type, 'numberposts'=>100));
	
	if( ! array_key_exists('errors', $posts) ) {
		if( $posts && is_array($posts) ) {
			$posts_title = array();
			foreach ($posts as $post) {
				$posts_title[$post->ID] = $post->post_title;
			}
		}	
	}
	
	if(isset($posts_title)) {
		return $posts_title;
	}
	

}


$album_category = kodeforest_shortcodes_categories('album-categories');
$post_category = kodeforest_shortcodes_categories('category');
$event_category =  kodeforest_shortcodes_categories('event-categories');
$team_category =  kodeforest_shortcodes_categories('team-categories');
$testi_category =  kodeforest_shortcodes_categories('testimonial_category');
$choices = array('yes' => 'Yes', 'no' => 'No');
$reverse_choices = array('no' => 'No', 'yes' => 'Yes');
$dec_numbers = array('0.1' => '0.1', '0.2' => '0.2', '0.3' => '0.3', '0.4' => '0.4', '0.5' => '0.5', '0.6' => '0.6', '0.7' => '0.7', '0.8' => '0.8', '0.9' => '0.9', '1' => '1' );

// Fontawesome icons list
$pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
$fontawesome_path = KODEFOREST_TINYMCE_DIR . '/css/font-awesome.css';
if( file_exists( $fontawesome_path ) ) {
	@$subject = file_get_contents($fontawesome_path);
}

preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

$icons = array();

foreach($matches as $match){
	$icons[$match[1]] = $match[2];
}

$checklist_icons = array ( 'icon-check' => '\f00c', 'icon-star' => '\f006', 'icon-angle-right' => '\f105', 'icon-asterisk' => '\f069', 'icon-remove' => '\f00d', 'icon-plus' => '\f067' );

/*-----------------------------------------------------------------------------------*/
/*	Shortcode Selection Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['shortcode-generator'] = array(
	'no_preview' => true,
	'params' => array(),
	'shortcode' => '',
	'popup_title' => ''
);

/*-----------------------------------------------------------------------------------*/
/*	Alert Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['alert'] = array(
	'no_preview' => true,
	'params' => array(

		'type' => array(
			'type' => 'select',
			'label' => __( 'Alert Type', 'the-event' ),
			'desc' => __( 'Select the type of alert message', 'the-event' ),
			'options' => array(
				'general' => 'General',
				'error' => 'Error',
				'success' => 'Success',
				'notice' => 'Notice',
			)
		),
		'icon' => array(
			'type' => 'iconpicker',
			'label' => __('Select Icon', 'the-event'),
			'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),
		'content' => array(
			'std' => 'Your Content Goes Here',
			'type' => 'textarea',
			'label' => __( 'Alert Content', 'the-event' ),
			'desc' => __( 'Insert the alert\'s content', 'the-event' ),
		),
		// 'animation_type' => array(
			// 'type' => 'select',
			// 'label' => __( 'Animation Type', 'the-event' ),
			// 'desc' => __( 'Select the type on animation to use on the shortcode', 'the-event' ),
			// 'options' => array(
				// '0' => 'None',
				// 'bounce' => 'Bounce',
				// 'fade' => 'Fade',
				// 'flash' => 'Flash',
				// 'shake' => 'Shake',
				// 'slide' => 'Slide',
			// )
		// ),
		// 'animation_direction' => array(
			// 'type' => 'select',
			// 'label' => __( 'Direction of Animation', 'the-event' ),
			// 'desc' => __( 'Select the incoming direction for the animation', 'the-event' ),
			// 'options' => array(
				// 'down' => 'Down',
				// 'left' => 'Left',
				// 'right' => 'Right',
				// 'up' => 'Up',
			// )
		// ),
		// 'animation_speed' => array(
			// 'type' => 'select',
			// 'std' => '',
			// 'label' => __( 'Speed of Animation', 'the-event' ),
			// 'desc' => __( 'Type in speed of animation in seconds (0.1 - 1)', 'the-event' ),
			// 'options' => $dec_numbers,
		// )
	),
	'shortcode' => '[alert icon="{{icon}}" type="{{type}}" ]{{content}}[/alert]',
	'popup_title' => __( 'Alert Shortcode', 'the-event' )
);


/*-----------------------------------------------------------------------------------*/
/*	education Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['education'] = array(
	'no_preview' => true,
	'params' => array(
		'heading' => array(
			'type' => 'text',
			'label' => __( 'Heading Title', 'the-event' ),
			'desc' => __( 'Add Heading or Title for this element.', 'the-event' )
		),
		
	),
	'shortcode' => '[education heading="{{heading}}" ]{{child_shortcode}}[/education]',
	'popup_title' => __( 'Education Degree / Diploma Shortcode', 'the-event' ),	
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'year' => array(
				'type' => 'text',
				'label' => __( 'Year', 'the-event' ),
				'desc' => __( 'Add Year of the degree / Diploma here', 'the-event' )
			),
			'degree' => array(
				'type' => 'text',
				'label' => __( 'Degree', 'the-event' ),
				'desc' => __( 'Add title of the degree here', 'the-event' )
			),
			'align' => array(
				'type' => 'select',
				'label' => __( 'Image Align', 'the-event' ),
				'desc' => __( 'Select the Image Alignment', 'the-event' ),
				'options' => array(
					'left' => 'Left',
					'right' => 'Right',
				)
			),
			'image' => array(
				'type' => 'uploader',
				'label' => __('Education Image', 'the-event'),
				'desc' => 'Clicking this image will show with text here'
			),
			'title' => array(
				'type' => 'text',
				'label' => __( 'Brief Title of Degree', 'the-event' ),
				'desc' => __( 'Add Breif Title of the degree here', 'the-event' )
			),
			'description' => array(
				'type' => 'textarea',
				'label' => __( 'Description', 'the-event' ),
				'desc' => __( 'Add description of the eduction list here', 'the-event' )
			),
			'class' => array(
				'type' => 'text',
				'label' => __( 'Class', 'the-event' ),
				'desc' => __( 'Add class of the icon', 'the-event' )			
			)
		),
		'shortcode' => '[education_item year="{{year}}" degree="{{degree}}" align="{{align}}" image="{{image}}" title="{{title}}" description="{{description}}" class="{{class}}" ][/education_item]',
		'clone_button' => __('Educational Degree List', 'the-event')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	education Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['experience'] = array(
	'no_preview' => true,
	'params' => array(
		'heading' => array(
			'type' => 'text',
			'label' => __( 'Heading Title', 'the-event' ),
			'desc' => __( 'Add Heading or Title for this element.', 'the-event' )
		),
		'sub_heading' => array(
			'type' => 'text',
			'label' => __( 'Sub Heading Title', 'the-event' ),
			'desc' => __( 'Add Sub Heading or Sub Title for this element.', 'the-event' )
		),
		'icon' => array(
			'type' => 'iconpicker',
			'label' => __('Select Icon', 'the-event'),
			'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),
	),
	'shortcode' => '[experience heading="{{heading}}" sub_heading="{{sub_heading}}" icon="{{icon}}"]{{child_shortcode}}[/experience]',
	'popup_title' => __( 'Experience Shortcode', 'the-event' ),	
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'year' => array(
				'type' => 'text',
				'label' => __( 'Year', 'the-event' ),
				'desc' => __( 'Add Year of the degree / Diploma here', 'the-event' )
			),
			'degree' => array(
				'type' => 'text',
				'label' => __( 'Degree', 'the-event' ),
				'desc' => __( 'Add title of the degree here', 'the-event' )
			),			
			'description' => array(
				'type' => 'textarea',
				'label' => __( 'Description', 'the-event' ),
				'desc' => __( 'Add description of the eduction list here', 'the-event' )
			),
			'class' => array(
				'type' => 'text',
				'label' => __( 'Class', 'the-event' ),
				'desc' => __( 'Add class of the icon', 'the-event' )			
			)
		),
		'shortcode' => '[experience_item year="{{year}}" degree="{{degree}}" description="{{description}}" class="{{class}}" ][/experience_item]',
		'clone_button' => __('Experience List', 'the-event')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	education Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['performance_chart'] = array(
	'no_preview' => true,
	'params' => array(
		'heading' => array(
			'type' => 'text',
			'label' => __( 'Heading Title', 'the-event' ),
			'desc' => __( 'Add Heading or Title for this element.', 'the-event' )
		),
		'caption' => array(
			'type' => 'text',
			'label' => __( 'Caption', 'the-event' ),
			'desc' => __( 'Add Caption or Title for this element.', 'the-event' )
		),
		'title_of_chart' => array(
			'type' => 'text',
			'label' => __( 'Title Of Chart', 'the-event' ),
			'desc' => __( 'Add Heading or Title for this element.', 'the-event' )
		),
		'value_of_chart' => array(
			'type' => 'text',
			'label' => __( 'Add Value Text of chart', 'the-event' ),
			'desc' => __( 'Add value text of chart which you would like to show under chart.', 'the-event' )
		),
		
	),
	'shortcode' => '[performance_chart heading="{{heading}}" caption="{{caption}}" title_of_chart="{{title_of_chart}}" value_of_chart="{{value_of_chart}}" ]{{child_shortcode}}[/performance_chart]',
	'popup_title' => __( 'Performance chart Shortcode', 'the-event' ),	
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'year' => array(
				'type' => 'text',
				'label' => __( 'Year', 'the-event' ),
				'desc' => __( 'Add Year of the proformance here', 'the-event' )
			),
			'value' => array(
				'type' => 'text',
				'label' => __( 'Value', 'the-event' ),
				'desc' => __( 'Add Value of the year proformance here', 'the-event' )
			),
			'class' => array(
				'type' => 'text',
				'label' => __( 'Class', 'the-event' ),
				'desc' => __( 'Add class of the icon', 'the-event' )			
			)
		),
		'shortcode' => '[performance_item year="{{year}}" value="{{value}}" class="{{class}}" ][/performance_item]',
		'clone_button' => __('Performance List', 'the-event')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	TimeLine Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['timeline'] = array(
	'no_preview' => true,
	'params' => array(

		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of social icons', 'the-event' ),
			'options' => array(
				'small' => 'Small',
				'medium' => 'Medium',
				'large' => 'Large',
			)
		)
	),
	'shortcode' => '[timeline size="{{size}}" ]{{child_shortcode}}[/timeline]',
	'popup_title' => __( 'TimeLine Shortcode', 'the-event' ),	
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(

			'year' => array(
				'type' => 'text',
				'label' => __( 'Year', 'the-event' ),
				'desc' => __( 'Add Year of the timeline here', 'the-event' )
			),
			'title' => array(
				'type' => 'text',
				'label' => __( 'Title', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),
			'desc' => array(
				'type' => 'textarea',
				'label' => __( 'Description', 'the-event' ),
				'desc' => __( 'Add description of the timeline here', 'the-event' )
			),
			'icon' => array(
				'type' => 'iconpicker',
				'label' => __('Select Icon', 'the-event'),
				'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
				'options' => $icons
			),
			'class' => array(
				'type' => 'text',
				'label' => __( 'Class', 'the-event' ),
				'desc' => __( 'Add class of the icon', 'the-event' )			
			)
		),
		'shortcode' => '[timeline_item year="{{year}}" title="{{title}}" desc="{{desc}}" icon="{{icon}}" class="{{class}}" ][/timeline_item]',
		'clone_button' => __('Social Icon Item', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	TimeLine Box Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['timeline_box'] = array(
	'no_preview' => true,
	'params' => array(
		'title' => array(
			'type' => 'text',
			'label' => __( 'Timeline Title', 'the-event' ),
			'desc' => __( 'Add title to the timeline box.', 'the-event' )
		),
		'description' => array(
			'type' => 'text',
			'label' => __( 'Timeline Description', 'the-event' ),
			'desc' => __( 'Add description of the timeline here', 'the-event' )
		),
		'timeline_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Text Color', 'the-event'),
			'desc' => __('Set the Text color of the timeline box.', 'the-event')
		),
		'timeline_bg' => array(
			'type' => 'colorpicker',
			'label' => __('Select Background Color', 'the-event'),
			'desc' => __('Set the background color of the timeline box.', 'the-event')
		),
		'class' => array(
			'type' => 'text',
			'label' => __( 'Class', 'the-event' ),
			'desc' => __( 'Add class of the icon', 'the-event' )			
		)
		
	),
	'shortcode' => '[timeline_box title="{{title}}" description="{{description}}" timeline_color="{{timeline_color}}" timeline_bg="{{timeline_bg}}" class="{{class}}" ][/timeline_box]',
	'popup_title' => __( 'Timeline Box Shortcode', 'the-event' ),	
	'no_preview' => true,
);

/*-----------------------------------------------------------------------------------*/
/*	TimeLine Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['about_section'] = array(
	'no_preview' => true,
	'params' => array(

		// 'size' => array(
			// 'type' => 'select',
			// 'label' => __( 'Size', 'the-event' ),
			// 'desc' => __( 'Select the size of social icons', 'the-event' ),
			// 'options' => array(
				// 'small' => 'Small',
				// 'medium' => 'Medium',
				// 'large' => 'Large',
			// )
		// )
	),
	'shortcode' => '[about_section]{{child_shortcode}}[/about_section]',
	'popup_title' => __( 'About Democracy Shortcode', 'the-event' ),	
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(

			'heading' => array(
				'type' => 'text',
				'label' => __( 'Tab Title', 'the-event' ),
				'desc' => __( 'Add tab title here this name should be unique should not be same like other tabs title.', 'the-event' )
			),
			'sub_title' => array(
				'type' => 'text',
				'label' => __( 'Content Area Sub Title', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),
			'title' => array(
				'type' => 'text',
				'label' => __( 'Content Area Title', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),
			'image' => array(
				'type' => 'uploader',
				'label' => __('Upload Image', 'the-event'),
				'desc' => 'Clicking this image will show beside box'
			),
			'content' => array(
				'type' => 'textarea',
				'label' => __( 'Content', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),
			'read_more_text' => array(
				'type' => 'text',
				'label' => __( 'Content Area Read More Text', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),
			'read_more_link' => array(
				'type' => 'text',
				'label' => __( 'Content Area Read More Link', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),
			'class' => array(
				'type' => 'text',
				'label' => __( 'Class', 'the-event' ),
				'desc' => __( 'Add class of the icon', 'the-event' )			
			)
		),
		'shortcode' => '[about_item heading="{{heading}}" sub_title="{{sub_title}}" title="{{title}}" image="{{image}}" read_more_text="{{read_more_text}}" read_more_link="{{read_more_link}}" ]{{content}}[/about_item]',
		'clone_button' => __('About Item', 'the-event')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	TimeLine Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['contact_detail'] = array(
	'no_preview' => true,
	'params' => array(

		// 'size' => array(
			// 'type' => 'select',
			// 'label' => __( 'Size', 'the-event' ),
			// 'desc' => __( 'Select the size of social icons', 'the-event' ),
			// 'options' => array(
				// 'small' => 'Small',
				// 'medium' => 'Medium',
				// 'large' => 'Large',
			// )
		// )
	),
	'shortcode' => '[contact_detail]{{child_shortcode}}[/contact_detail]',
	'popup_title' => __( 'Contact Detail Shortcode', 'the-event' ),	
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(

			'heading' => array(
				'type' => 'text',
				'label' => __( 'Tab Title', 'the-event' ),
				'desc' => __( 'Add tab title here this name should be unique should not be same like other tabs title.', 'the-event' )
			),
			'icon_1' => array(
				'type' => 'iconpicker',
				'label' => __('Select Icon', 'the-event'),
				'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
				'options' => $icons
			),
			'title_1' => array(
				'type' => 'text',
				'label' => __( 'Title', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),			
			'content_1' => array(
				'type' => 'textarea',
				'label' => __( 'Content', 'the-event' ),
				'desc' => __( 'Add title of the contact detail here', 'the-event' )
			),
			'icon_2' => array(
				'type' => 'iconpicker',
				'label' => __('Select Icon', 'the-event'),
				'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
				'options' => $icons
			),
			'title_2' => array(
				'type' => 'text',
				'label' => __( 'Title', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),			
			'content_2' => array(
				'type' => 'textarea',
				'label' => __( 'Content', 'the-event' ),
				'desc' => __( 'Add title of the contact detail here', 'the-event' )
			),
			'icon_3' => array(
				'type' => 'iconpicker',
				'label' => __('Select Icon', 'the-event'),
				'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
				'options' => $icons
			),
			'title_3' => array(
				'type' => 'text',
				'label' => __( 'Title', 'the-event' ),
				'desc' => __( 'Add title of the timeline here', 'the-event' )
			),			
			'content_3' => array(
				'type' => 'textarea',
				'label' => __( 'Content', 'the-event' ),
				'desc' => __( 'Add title of the contact detail here', 'the-event' )
			),
			'class' => array(
				'type' => 'text',
				'label' => __( 'Class', 'the-event' ),
				'desc' => __( 'Add class of the icon', 'the-event' )			
			)
		),
		'shortcode' => '[office heading="{{heading}}" icon_1="{{icon_1}}" title_1="{{title_1}}" content_1="{{content_1}}" icon_2="{{icon_2}}" title_2="{{title_2}}" content_2="{{content_2}}" icon_3="{{icon_3}}" title_3="{{title_3}}" content_3="{{content_3}}" ][/office]',
		'clone_button' => __('Office Item', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	Social Icon Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['social_icons'] = array(
	'no_preview' => true,
	'params' => array(

		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of social icons', 'the-event' ),
			'options' => array(
				'small' => 'Small',
				'medium' => 'Medium',
				'large' => 'Large',
			)
		)
	),
	'shortcode' => '[social_icons size="{{size}}" ]{{child_shortcode}}[/social_icons]',
	'popup_title' => __( 'Social Icons Shortcode', 'the-event' ),	
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(

			'link' => array(
				'type' => 'text',
				'label' => __( 'Link', 'the-event' ),
				'desc' => __( 'Add link or url of the destination', 'the-event' )			
			),
			'icon' => array(
				'type' => 'iconpicker',
				'label' => __('Select Icon', 'the-event'),
				'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
				'options' => $icons
			),
			'class' => array(
				'type' => 'text',
				'label' => __( 'Class', 'the-event' ),
				'desc' => __( 'Add class of the icon', 'the-event' )			
			),
			'target' => array(
				'type' => 'select',
				'label' => __( 'Select Target', 'the-event' ),
				'desc' => __( 'Select the type on animation to use on the shortcode', 'the-event' ),
				'options' => array(
					'_self' => '_Self',
					'_blank' => '_Blank',
					'_new' => '_New',
					'_top' => '_Top',				
				)
			),		
		),
		'shortcode' => '[social_icon_item link="{{link}}" icon="{{icon}}" class="{{class}}" target="{{target}}" ][/social_icon_item]',
		'clone_button' => __('Social Icon Item', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	progress_circles Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['progress_circles'] = array(
	'no_preview' => true,
	'params' => array(
		'circle_text' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Circle Text', 'the-event'),
			'desc' => __('Add circle text here', 'the-event')
		),
		'circle_value' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Circle Value', 'the-event'),
			'desc' => __('Add circle value here', 'the-event')
		),
		'circle_width' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Circle Width', 'the-event'),
			'desc' => __('Add circle width in px here', 'the-event')
		),
		'circle_height' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Circle height', 'the-event'),
			'desc' => __('Add circle height in px here', 'the-event')
		),
		
		
		
	),
	'shortcode' => '[progress_circles circle_text="{{circle_text}}" circle_value="{{circle_value}}" circle_width="{{circle_width}}" circle_height="{{circle_height}}" ][/progress_circles]',
	'popup_title' => __( 'Progress Circles Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Testimonial
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['testimonial'] = array(
	'no_preview' => true,
	'params' => array(
		'heading' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Heading', 'the-event'),
			'desc' => __('Add heading text here', 'the-event')
		),
		'sub_heading' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Sub Heading', 'the-event'),
			'desc' => __('Add Sub Heading here', 'the-event')
		),
		'category' => array(
			'type' => 'select',
			'label' => __( 'Select Testimonial Category', 'the-event' ),
			'desc' => __( 'Select the testimonial category', 'the-event' ),
			'options' => kodeforest_shortcodes_categories('testimonial_category'),
		),		
	),
	'shortcode' => '[testimonial heading="{{heading}}" sub_heading="{{sub_heading}}" category="{{category}}" ][/testimonial]',
	'popup_title' => __( 'Testimonial Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Testimonial
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['next_event'] = array(
	'no_preview' => true,
	'params' => array(
		'heading' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Heading', 'the-event'),
			'desc' => __('Add heading text here', 'the-event')
		),
		'sub_heading' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Sub Heading', 'the-event'),
			'desc' => __('Add Sub Heading here', 'the-event')
		),
		'event' => array(
			'type' => 'select',
			'label' => __( 'Select Event', 'the-event' ),
			'desc' => __( 'Select the Event', 'the-event' ),
			'options' => get_post_list_sc('event'),
		),		
	),
	'shortcode' => '[next_event heading="{{heading}}" sub_heading="{{sub_heading}}" event="{{event}}" ][/next_event]',
	'popup_title' => __( 'Next Event Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Speakers Tickets Shortcodes
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['tickets'] = array(
	'no_preview' => true,
	'params' => array(
		'heading' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Heading', 'the-event'),
			'desc' => __('Add Heading here here', 'the-event'),
		),
		'ticket_title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Ticket Title', 'the-event'),
			'desc' => __('Add Ticket type here here', 'the-event'),
		),
		'ticket_price' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Ticket Price', 'the-event'),
			'desc' => __('Add Ticket Price value here', 'the-event'),
		),
		'ticket_qty' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Tickets Quantity', 'the-event'),
			'desc' => __('Add Tickets Quantity here', 'the-event'),
		),
		'ticket_book' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Ticket Button Now Text', 'the-event'),
			'desc' => __('Add Tickets Book Text here.', 'the-event'),
		),		
	),
	'shortcode' => '[tickets heading="{{heading}}" ticket_title="{{ticket_title}}" ticket_price="{{ticket_price}}" ticket_qty="{{ticket_qty}}" ticket_book="{{ticket_book}}" ] {{child_shortcode}} [/tickets]',
	'popup_title' => __( 'Events Tickets Shortcode', 'the-event' ),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'class' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Class', 'the-event'),
				'desc' => __('Add class here here', 'the-event'),
			),
			'ticket_type' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Ticket Type', 'the-event'),
				'desc' => __('Add Ticket type here here', 'the-event'),
			),
			'ticket_price' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Ticket Price', 'the-event'),
				'desc' => __('Add Ticket Price value here', 'the-event'),
			),
			'ticket_qty' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Tickets Quantity', 'the-event'),
				'desc' => __('Add Tickets Quantity here', 'the-event'),
			),
			'ticket_book' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Ticket Button Now', 'the-event'),
				'desc' => __('Add Tickets Button text here.', 'the-event'),
			),
			'ticket_book_url' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Ticket Book Now URL', 'the-event'),
				'desc' => __('Add Tickets Button URL text here.', 'the-event'),
			),
		),
		'shortcode' => '[tickets_item class="{{class}}" ticket_type="{{ticket_type}}" ticket_price="{{ticket_price}}" ticket_qty="{{ticket_qty}}" ticket_book="{{ticket_book}}" ticket_book_url="{{ticket_book_url}}" ][/tickets_item]',
		'clone_button' => __('Add More Event Tickets', 'the-event'),
	)
);
/*-----------------------------------------------------------------------------------*/
/*	Albums Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['albums'] = array(
	'no_preview' => true,
	'params' => array(
		
		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of shortcode element', 'the-event' ),
			'options' => array(
				'element1-1' => 'Full Width',
				// 'element1-2' => 'Half Width',
				// 'element1-3' => 'One Third Width',
				// 'element1-4' => 'One Forth',
			)
		),
		'header' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Header Title', 'the-event'),
			'desc' => __('Add header title here', 'the-event')
		),
		
		'category' => array(
			'type' => 'select',
			'label' => __( 'Select Category', 'the-event' ),
			'desc' => __( 'Select category to fetch its items', 'the-event' ),
			'options' => $album_category
		),
		'num_fetch' => array(
			'std' => 4,
			'type' => 'select',
			'label' => __( 'Fetch number of items/posts', 'the-event' ),
			'desc' => __( 'Select number of items/posts to show on page', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 20, false )
		),
		
		'num_excerpt' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Number of Characters', 'the-event'),
			'desc' => __('Add Number of Characters to show under each post/items', 'the-event')
		),
		
		'pagination' => array(
			'type' => 'select',
			'label' => __( 'Pagination', 'the-event' ),
			'desc' => __( 'Do you want to turn on pagination.', 'the-event' ),
			'options' => array(
				'Yes' => 'Yes',
				'No' => 'No',
			)
		),
		'orderby' => array(
			'type' => 'select',
			'label' => __( 'Orderby', 'the-event' ),
			'desc' => __( 'Show post order by.', 'the-event' ),
			'options' => array(
				'date' => 'date',
				'title' => 'title',
				'rand' => 'rand',
				'comment_count' => 'comment_count',
			)
		),
		
		'order' => array(
			'type' => 'select',
			'label' => __( 'Order', 'the-event' ),
			'desc' => __( 'Select your posts/item order.', 'the-event' ),
			'options' => array(
				'asc' => 'ASC',
				'desc' => 'DESC',
			)
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[albums size="{{size}}" header="{{header}}" category="{{category}}" num_fetch="{{num_fetch}}" num_excerpt="{{num_excerpt}}" pagination="{{pagination}}" orderby="{{orderby}}" order="{{order}}" item_margin="{{item_margin}}"][/albums]',
	'popup_title' => __( 'Albums Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Progress Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['progressbar'] = array(
	'no_preview' => true,
	'params' => array(
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title', 'the-event'),
			'desc' => __('Add title here', 'the-event')
		),
		'text_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Text Color', 'the-event'),
			'desc' => __('Set the text color of the progress bar.', 'the-event')
		),
		'filled_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Filled Color', 'the-event'),
			'desc' => __('Set the filled color of the progress bar.', 'the-event')
		),
		'unfilled_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Unfilled Color', 'the-event'),
			'desc' => __('Set the unfilled color of the progress bar.', 'the-event')
		),
		'height' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Height', 'the-event'),
			'desc' => __('Add Height in px here for example: 30px', 'the-event')
		),
		'filled' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Filled Amount', 'the-event'),
			'desc' => __('Add Filled value here in percent for example: 80%', 'the-event')
		)
	),
	'shortcode' => '[progressbar height="{{height}}" title="{{title}}" text_color="{{text_color}}" filled_color="{{filled_color}}" unfilled_color="{{unfilled_color}}" filled="{{filled}}" ][/progressbar]',
	'popup_title' => __( 'Progress bar Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Blog Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['heading'] = array(
	'no_preview' => true,
	'params' => array(
		
		'align' => array(
			'type' => 'select',
			'label' => __( 'Align', 'the-event' ),
			'desc' => __( 'Select heading alignment', 'the-event' ),
			'options' => array(
				'align-left' => 'Align Left',
				'align-right' => 'Align Right',
				'align-center' => 'Align Center'
			)
		),		
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title', 'the-event'),
			'desc' => __('Add title here', 'the-event')
		),		
		'title_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select title Color', 'the-event'),
			'desc' => __('Set the title color of the heading title.', 'the-event')
		),
		'caption' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Caption', 'the-event'),
			'desc' => __('Add caption text here', 'the-event')
		),
		'caption_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select caption Color', 'the-event'),
			'desc' => __('Set the caption color of the heading caption.', 'the-event')
		),
		'line_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Line Color', 'the-event'),
			'desc' => __('Set the line color of the heading caption.', 'the-event')
		),
		'content' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Content', 'the-event'),
			'desc' => __('Add Content text here', 'the-event')
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[heading align="{{align}}" title="{{title}}" title_color="{{title_color}}" caption="{{caption}}" caption_color="{{caption_color}}" line_color="{{line_color}}" item_margin="{{item_margin}}"  ]{{content}}[/heading]',
	'popup_title' => __( 'Heading Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Blog Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['blog'] = array(
	'no_preview' => true,
	'params' => array(
		
		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of shortcode element', 'the-event' ),
			'options' => array(
				'1/1 Full Thumbnail' => 'Full Width',
				'1/2 Blog Grid' => 'Half Width',
				'1/3 Blog Grid' => 'One Third Width',
				'1/4 Blog Widget' => 'One Forth',
			)
		),		
		'header' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Header Title', 'the-event'),
			'desc' => __('Add header title here', 'the-event')
		),
		
		'category' => array(
			'type' => 'select',
			'label' => __( 'Select Category', 'the-event' ),
			'desc' => __( 'Select category to fetch its items', 'the-event' ),
			'options' => $post_category
		),
		'num_fetch' => array(
			'std' => 4,
			'type' => 'select',
			'label' => __( 'Fetch number of items/posts', 'the-event' ),
			'desc' => __( 'Select number of items/posts to show on page', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 20, false )
		),
		
		'num_excerpt' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Number of Characters', 'the-event'),
			'desc' => __('Add Number of Characters to show under each post/items', 'the-event')
		),
		
		'pagination' => array(
			'type' => 'select',
			'label' => __( 'Pagination', 'the-event' ),
			'desc' => __( 'Do you want to turn on pagination.', 'the-event' ),
			'options' => array(
				'Yes' => 'Yes',
				'No' => 'No',
			)
		),
		'orderby' => array(
			'type' => 'select',
			'label' => __( 'Orderby', 'the-event' ),
			'desc' => __( 'Show post order by.', 'the-event' ),
			'options' => array(
				'date' => 'date',
				'title' => 'title',
				'rand' => 'rand',
				'comment_count' => 'comment_count',
			)
		),
		
		'order' => array(
			'type' => 'select',
			'label' => __( 'Order', 'the-event' ),
			'desc' => __( 'Select your posts/item order.', 'the-event' ),
			'options' => array(
				'asc' => 'ASC',
				'desc' => 'DESC',
			)
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[blog size="{{size}}" header="{{header}}" category="{{category}}" num_fetch="{{num_fetch}}" num_excerpt="{{num_excerpt}}" pagination="{{pagination}}" orderby="{{orderby}}" order="{{order}}" item_margin="{{item_margin}}"][/blog]',
	'popup_title' => __( 'Blog Shortcode', 'the-event' )
);
	// <Recent-Posts>
		// <size>element2-3</size>
		// <show-caption>&lt;span class=&quot;color&quot;&gt;From Our&lt;/span&gt; Blog</show-caption>
		// <feature-post>98</feature-post>
		// <num-excerpt>140</num-excerpt>
		// <category>6</category>
		// <num-fetch>2</num-fetch>
		// <item-margin>30</item-margin>
	// </Recent-Posts>
/*-----------------------------------------------------------------------------------*/
/*	Recent Post Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['recent_post'] = array(
	'no_preview' => true,
	'params' => array(
		
		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of shortcode element', 'the-event' ),
			'options' => array(
				//'element1-1' => 'Full Width',
				//'element1-2' => 'Half Width',
				'element2-3' => 'Two Third',
				//'element1-4' => 'One Forth',
			)
		),
		'header' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Header Title', 'the-event'),
			'desc' => __('Add header title here', 'the-event')
		),
		'feature_post' => array(
			'type' => 'select',
			'label' => __( 'Select Feature Post', 'the-event' ),
			'desc' => __( 'Select feature post/item', 'the-event' ),
			'options' => get_post_list_sc('post')
		),
		'post_num_excerpt' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Number of Characters', 'the-event'),
			'desc' => __('Add Number of Characters to show under feature post/items', 'the-event')
		),
		
		'category' => array(
			'type' => 'select',
			'label' => __( 'Select Category', 'the-event' ),
			'desc' => __( 'Select category to fetch its items', 'the-event' ),
			'options' => $post_category
		),
		'num_fetch' => array(
			'std' => 4,
			'type' => 'select',
			'label' => __( 'Fetch number of items/posts', 'the-event' ),
			'desc' => __( 'Select number of items/posts to show on page', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 20, false )
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[recent_post size="{{size}}" header="{{header}}" feature_post="{{feature_post}}" post_num_excerpt="{{post_num_excerpt}}" category="{{category}}" num_fetch="{{num_fetch}}" item_margin="{{item_margin}}"][/recent_post]',
	'popup_title' => __( 'Recent Posts Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Counter Circle Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['counter_circle'] = array(
	'no_preview' => true,
	'params' => array(
		
		'header' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Header Title', 'the-event'),
			'desc' => __('Add header title here', 'the-event')
		),
		'event_id' => array(
			'type' => 'select',
			'label' => __( 'Select Event', 'the-event' ),
			'desc' => __( 'Select Event to show its counter', 'the-event' ),
			'options' => get_post_list_sc('event')
		),
		'width' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Add Width of Counter', 'the-event'),
			'desc' => __('Add width in pixels of counter circles', 'the-event')
		),
		'height' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Add Height of Counter', 'the-event'),
			'desc' => __('Add height in pixels of counter circles', 'the-event')
		),
		'color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Text Color', 'the-event'),
			'desc' => __('Set the color of the circle text.', 'the-event')
		),
		'unfilled_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Unfilled Color', 'the-event'),
			'desc' => __('Set the unfilled color of the circle.', 'the-event')
		),
		'filled_color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Filled Color', 'the-event'),
			'desc' => __('Set the filled color of the circle.', 'the-event')
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[counter_circle header="{{header}}" event_id="{{event_id}}" width="{{width}}" height="{{height}}" color="{{color}}" unfilled_color="{{unfilled_color}}" filled_color="{{filled_color}}" item_margin="{{item_margin}}"][/counter_circle]',
	'popup_title' => __( 'Event Counter Circle Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Skill Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['skill'] = array(
	'no_preview' => true,
	'params' => array(
		
		'filled_color' => array(
			'type' => 'colorpicker',
			'label' => esc_attr__('Select Filled Color', 'the-event'),
			'desc' => esc_attr__('Set the filled color of the progress bar.', 'the-event')
		),
		'unfilled_color' => array(
			'type' => 'colorpicker',
			'label' => esc_attr__('Select Unfilled Color', 'the-event'),
			'desc' => esc_attr__('Set the unfilled color of the progress bar.', 'the-event')
		),
		'value' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_attr__('Value', 'the-event'),
			'desc' => esc_attr__('Add Value here', 'the-event')
		),
		'unit' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_attr__('Unit', 'the-event'),
			'desc' => esc_attr__('Add Filled unit here', 'the-event')
		)
	),
	'shortcode' => '[skill filled_color="{{filled_color}}" unfilled_color="{{unfilled_color}}" value="{{value}}" unit="{{unit}}" ][/skill]',
	'popup_title' => esc_attr__( 'Skill Circle Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	News Slider Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['news_slider'] = array(
	'no_preview' => true,
	'params' => array(
		
		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of shortcode element', 'the-event' ),
			'options' => array(
				//'element1-1' => 'Full Width',
				//'element1-2' => 'Half Width',
				'element1-3' => 'One Third',
				//'element1-4' => 'One Forth',
			)
		),
		'header' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Header Title', 'the-event'),
			'desc' => __('Add header title here', 'the-event')
		),
		'category' => array(
			'type' => 'select',
			'label' => __( 'Select Category', 'the-event' ),
			'desc' => __( 'Select category to fetch its items', 'the-event' ),
			'options' => $post_category
		),
		'num_fetch' => array(
			'std' => 4,
			'type' => 'select',
			'label' => __( 'Fetch number of items/posts', 'the-event' ),
			'desc' => __( 'Select number of items/posts to show on page', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 20, false )
		),
		'num_excerpt' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Number of Characters', 'the-event'),
			'desc' => __('Add Number of Characters to show under each post/items', 'the-event')
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[news_slider size="{{size}}" header="{{header}}" category="{{category}}" num_fetch="{{num_fetch}}" num_excerpt="{{num_excerpt}}" item_margin="{{item_margin}}"][/news_slider]',
	'popup_title' => __( 'News/Blog Slider Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Event Config
/*-----------------------------------------------------------------------------------*/
// <Event>
		// <size>element1-1</size>
		// <header>&lt;span class=&quot;color&quot;&gt;Upcoming &lt;/span&gt; Events</header>
		// <event-type>Event Listing</event-type>
		// <num-excerpt>300</num-excerpt>
		// <item-scope>Future</item-scope>
		// <category>14</category>
		// <num-fetch>2</num-fetch>
		// <pagination>Yes</pagination>
		// <order>desc</order>
		// <item-margin>0</item-margin>
	// </Event>
$kodeforest_shortcodes['events'] = array(
	'no_preview' => true,
	'params' => array(
		
		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of shortcode element', 'the-event' ),
			'options' => array(
				'element1-1' => 'Full Width',
				'element1-2' => 'Half Width',
				'element1-3' => 'One Third Width',
				'element1-4' => 'One Forth',
			)
		),
		'header' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Header Title', 'the-event'),
			'desc' => __('Add header title here', 'the-event')
		),
		'category' => array(
			'type' => 'select',
			'label' => __( 'Select Category', 'the-event' ),
			'desc' => __( 'Select category to fetch its items', 'the-event' ),
			'options' => $event_category
		),
		'item_scope' => array(
			'type' => 'select',
			'label' => __( 'Item Scope', 'the-event' ),
			'desc' => __( 'Select the item scope of shortcode element', 'the-event' ),
			'options' => array(
				'Future' => 'Future',
				'Past' => 'Past',
				'All' => 'All',
			)
		),
		'num_fetch' => array(
			'std' => 4,
			'type' => 'select',
			'label' => __( 'Fetch number of items/posts', 'the-event' ),
			'desc' => __( 'Select number of items/posts to show on page', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 20, false )
		),
		
		'num_excerpt' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Number of Characters', 'the-event'),
			'desc' => __('Add Number of Characters to show under each post/items', 'the-event')
		),
		
		// 'pagination' => array(
			// 'type' => 'select',
			// 'label' => __( 'Pagination', 'the-event' ),
			// 'desc' => __( 'Do you want to turn on pagination.', 'the-event' ),
			// 'options' => array(
				// 'Yes' => 'Yes',
				// 'No' => 'No',
			// )
		// ),
		// 'orderby' => array(
			// 'type' => 'select',
			// 'label' => __( 'Orderby', 'the-event' ),
			// 'desc' => __( 'Show post order by.', 'the-event' ),
			// 'options' => array(
				// 'date' => 'date',
				// 'title' => 'title',
				// 'rand' => 'rand',
				// 'comment_count' => 'comment_count',
			// )
		// ),
		
		'order' => array(
			'type' => 'select',
			'label' => __( 'Order', 'the-event' ),
			'desc' => __( 'Select your posts/item order.', 'the-event' ),
			'options' => array(
				'asc' => 'ASC',
				'desc' => 'DESC',
			)
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[events size="{{size}}" header="{{header}}" category="{{category}}" item_scope="{{item_scope}}" num_fetch="{{num_fetch}}" num_excerpt="{{num_excerpt}}" order="{{order}}" item_margin="{{item_margin}}"][/events]',
	'popup_title' => __( 'Event Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Event Config
/*-----------------------------------------------------------------------------------*/
// <Team>
	// <size>element1-1</size>
	// <header>&lt;span class=&quot;color&quot;&gt;Our &lt;/span&gt;Team</header>
	// <num-fetch>4</num-fetch>
	// <category>event-managers</category>
	// <pagination>No</pagination>
	// <item-margin>0</item-margin>
// </Team>
$kodeforest_shortcodes['teams'] = array(
	'no_preview' => true,
	'params' => array(
		
		'size' => array(
			'type' => 'select',
			'label' => __( 'Size', 'the-event' ),
			'desc' => __( 'Select the size of shortcode element', 'the-event' ),
			'options' => array(
				'element1-1' => 'Full Width',
			)
		),
		'header' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Header Title', 'the-event'),
			'desc' => __('Add header title here', 'the-event')
		),
		'category' => array(
			'type' => 'select',
			'label' => __( 'Select Category', 'the-event' ),
			'desc' => __( 'Select category to fetch its items', 'the-event' ),
			'options' => $team_category
		),
		'num_fetch' => array(
			'std' => 4,
			'type' => 'select',
			'label' => __( 'Fetch number of items/posts', 'the-event' ),
			'desc' => __( 'Select number of items/posts to show on page', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 20, false )
		),
		'pagination' => array(
			'type' => 'select',
			'label' => __( 'Pagination', 'the-event' ),
			'desc' => __( 'Do you want to turn on pagination.', 'the-event' ),
			'options' => array(
				'Yes' => 'Yes',
				'No' => 'No',
			)
		),
		'item_margin' => array(
			'std' => 30,
			'type' => 'select',
			'label' => __( 'Item Margin', 'the-event' ),
			'desc' => __( 'Select margin from bottom', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		
	),
	'shortcode' => '[teams size="{{size}}" header="{{header}}" category="{{category}}" num_fetch="{{num_fetch}}" pagination="{{pagination}}" item_margin="{{item_margin}}"][/teams]',
	'popup_title' => __( 'Teams Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Button Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['button'] = array(
	'no_preview' => true,
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Class', 'the-event'),
			'desc' => __('Add the button\'s class ex: kode_link_1, kode_link_2, kode_link_3', 'the-event')
		),
		'call_back' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Call Back', 'the-event'),
			'desc' => __('It will trigger the lightbox if # been provided for example: #kodeforest', 'the-event')
		),
		'style' => array(
			'type' => 'select',
			'label' => __('Button Style', 'the-event'),
			'desc' => __('Select the button\'s style', 'the-event'),
			'options' => array(
				'type-1' => 'Style 1',
				'type-2' => 'Style 2',
				'type-3' => 'Style 3',
			)
		),
		'icon' => array(
			'type' => 'iconpicker',
			'label' => __('Select Icon', 'the-event'),
			'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),
		'iconcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Icon Color', 'the-event'),
			'desc' => __('Leave blank for default', 'the-event')
		),
		'iconbgcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Select Background Color', 'the-event'),
			'desc' => __('Set the color of the Button background.', 'the-event')
		),
		'url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button URL', 'the-event'),
			'desc' => __('Add the button\'s url ex: http://example.com', 'the-event')
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Button Target', 'the-event'),
			'desc' => __('_self = open in same window <br />_blank = open in new window', 'the-event'),
			'options' => array(
				'_self' => '_self',
				'_blank' => '_blank'
			)
		),
		'color' => array(
			'type' => 'colorpicker',
			'label' => __('Select Text Color', 'the-event'),
			'desc' => __('Set the color of the Button text.', 'the-event')
		),
		'bgcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Select Background Color', 'the-event'),
			'desc' => __('Set the color of the Button background.', 'the-event')
		),
		'size' => array(
			'type' => 'select',
			'label' => __('Button Size', 'the-event'),
			'desc' => __('Select the button\'s size', 'the-event'),
			'options' => array(
				'small' => 'Small',
				'medium' => 'Medium',
				'large' => 'Large'
			)
		),
		'content' => array(
			'std' => 'Button Text',
			'type' => 'text',
			'label' => __('Button\'s Text', 'the-event'),
			'desc' => __('Add the text that will display in the button', 'the-event'),
		),
	),
	'shortcode' => '[button class="{{class}}" call_back="{{call_back}}" style="{{style}}" icon="{{icon}}" iconcolor="{{iconcolor}}" iconbgcolor="{{iconbgcolor}}" url="{{url}}" target="{{target}}" color="{{color}}" bgcolor="{{bgcolor}}" size="{{size}}" ]{{content}}[/button]',
	'popup_title' => __('Button Shortcode', 'the-event')
);
/*-----------------------------------------------------------------------------------*/
/*	imagetoaction Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['imagetoaction'] = array(
	'no_preview' => true,
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Class', 'the-event'),
			'desc' => __('Add the image to action class.', 'the-event')
		),
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title of image', 'the-event'),
			'desc' => __('Add the title of the image', 'the-event')
		),
		'style' => array(
			'type' => 'select',
			'label' => __('Image Style', 'the-event'),
			'desc' => __('Select the image to action \'s style', 'the-event'),
			'options' => array(
				'type-1' => 'Style 1',
				'type-2' => 'Style 2',
				'type-3' => 'Style 3',
			)
		),
		'overlaycolor' => array(
			'type' => 'colorpicker',
			'label' => __('Select Image Overlay Color', 'the-event'),
			'desc' => __('Set the color of the image overlay color on hover.', 'the-event')
		),
		'opactiy' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Overlay Opacity', 'the-event'),
			'desc' => __('Add image overlay opacity', 'the-event')
		),
		'image' => array(
			'type' => 'uploader',
			'label' => __('Image', 'the-event'),
			'desc' => 'add the image to show'
		),
		'icon' => array(
			'type' => 'iconpicker',
			'label' => __('Select Button Icon', 'the-event'),
			'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),
		'iconcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Button Icon Color', 'the-event'),
			'desc' => __('Leave blank for default', 'the-event')
		),
		'url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button URL', 'the-event'),
			'desc' => __('Add the button\'s url ex: http://example.com', 'the-event')
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Button Target', 'the-event'),
			'desc' => __('_self = open in same window <br />_blank = open in new window', 'the-event'),
			'options' => array(
				'_self' => '_self',
				'_blank' => '_blank'
			)
		),
		
	),
	'shortcode' => '[imagetoaction class="{{class}}" style="{{style}}" title="{{title}}" image="{{image}}" icon="{{icon}}" iconcolor="{{iconcolor}}" url="{{url}}" target="{{target}}" overlaycolor="{{overlaycolor}}" opactiy="{{opactiy}}" ][/imagetoaction]',
	'popup_title' => __('Image to Action Shortcode', 'the-event')
);


/*-----------------------------------------------------------------------------------*/
/*	Checklist Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['checklist'] = array(
	'params' => array(

		'icon' => array(
			'type' => 'iconpicker',
			'label' => __('Select Icon', 'the-event'),
			'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),
		'iconcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Icon Color', 'the-event'),
			'desc' => __('Leave blank for default', 'the-event')
		),
		'circle' => array(
			'type' => 'select',
			'label' => __('Icon in Circle', 'the-event'),
			'desc' => __('Choose to display the icon in a circle', 'the-event'),
			'options' => $choices
		),
	),

	'shortcode' => '[checklist icon="{{icon}}" iconcolor="{{iconcolor}}" circle="{{circle}}"]&lt;ul&gt;{{child_shortcode}}&lt;/ul&gt;[/checklist]',
	'popup_title' => __('Checklist Shortcode', 'the-event'),
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'content' => array(
				'std' => 'Your Content Goes Here',
				'type' => 'textarea',
				'label' => __( 'List Item Content', 'the-event' ),
				'desc' => __( 'Add list item content', 'the-event' ),
			),
		),
		'shortcode' => '&lt;li&gt;{{content}}&lt;/li&gt;',
		'clone_button' => __('Add New List Item', 'the-event')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	Dropcap Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['dropcap'] = array(
	'no_preview' => true,
	'params' => array(
		'color' => array(
			'type' => 'colorpicker',
			'std' => '',
			'label' => __('Dropcap color', 'the-event'),
			'desc' => 'Leave blank for default'
		),
		'content' => array(
			'std' => 'A',
			'type' => 'textarea',
			'label' => __( 'Dropcap Letter', 'the-event' ),
			'desc' => __( 'Add the letter to be used as dropcap', 'the-event' ),
		)

	),
	'shortcode' => '[dropcap color="{{color}}"]{{content}}[/dropcap]',
	'popup_title' => __( 'Dropcap Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Call To Action Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['calltoaction'] = array(
	'no_preview' => true,
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Add Action Class', 'the-event'),
			'desc' => __('Add class of the action.', 'the-event')
		),
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title Text of action', 'the-event'),
			'desc' => __('Add title of the action.', 'the-event')
		),
		'color' => array(
			'type' => 'colorpicker',
			'std' => '',
			'label' => __('Choose color', 'the-event'),
			'desc' => 'Leave blank for default'
		),
		'style' => array(
			'type' => 'select',
			'label' => __('Select Style', 'the-event'),
			'desc' => 'Select alignment of call to action text.',
			'options' => array(
				'style-1' => 'Style 1',
				'style-2' => 'Style 2',
				// 'style-3' => 'style 3'
			)
		),
		'align' => array(
			'type' => 'select',
			'label' => __('Alignment', 'the-event'),
			'desc' => 'Select alignment of call to action text.',
			'options' => array(
				'left' => 'Left',
				'right' => 'Right',
				'center' => 'Center'
			)
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __( 'Description', 'the-event' ),
			'desc' => __( 'Add the description of call to action here', 'the-event' ),
		),
		'btn_text' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button Text', 'the-event'),
			'desc' => __('Add button text of the action.', 'the-event')
		),
		'btn_url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button URL', 'the-event'),
			'desc' => __('Add button URL of the action.', 'the-event')
		),
	),
	'shortcode' => '[calltoaction class="{{class}}" style="{{style}}" title="{{title}}" color="{{color}}" align="{{align}}" btn_text="{{btn_text}}" btn_url="{{btn_url}}" ]{{content}}[/calltoaction]',
	'popup_title' => __( 'Call to Action Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Theme Button Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['theme_button'] = array(
	'no_preview' => true,
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button Class', 'the-event'),
			'desc' => __('Add button class here.', 'the-event')
		),
		'btn_text' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button Text', 'the-event'),
			'desc' => __('Add Button Text Here.', 'the-event')
		),
		'color' => array(
			'type' => 'colorpicker',
			'std' => '',
			'label' => __('Button Text color', 'the-event'),
			'desc' => 'Leave blank for default'
		),
		'align' => array(
			'type' => 'select',
			'label' => __('Alignment', 'the-event'),
			'desc' => 'Select alignment of call to action text.',
			'options' => array(
				'left' => 'Left',
				'right' => 'Right',
				'center' => 'Center'
			)
		),
		'btn_background' => array(
			'type' => 'colorpicker',
			'std' => '',
			'label' => __('Button Background color', 'the-event'),
			'desc' => 'Leave blank for default'
		),
		'btn_url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button URL', 'the-event'),
			'desc' => __('Add button URL of the action.', 'the-event')
		)
	),
	'shortcode' => '[theme_button class="{{class}}" btn_text="{{btn_text}}" color="{{color}}" align="{{align}}" btn_background="{{btn_background}}" btn_url="{{btn_url}}" ][/theme_button]',
	'popup_title' => __( 'Call to Action Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Social Network Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['social_network'] = array(
	'no_preview' => true,
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Social Network Class', 'the-event'),
			'desc' => __('Add social network class here.', 'the-event')
		),
		
	),
	'shortcode' => '[social_network class="{{class}}" ][/social_network]',
	'popup_title' => __( 'Social Network Shortcode', 'the-event' )
);
$kodeforest_shortcodes['our_clients'] = array(
	'params' => array(
		'style' => array(
			'type' => 'select',
			'label' => __('Style', 'the-event'),
			'desc' => 'Select style.',
			'options' => array(
				'style-1' => 'Style 1',
				'style-2' => 'Style 2',				
			)
		),
		
	),
	'no_preview' => true,
	'shortcode' => '[our_clients style="{{style}}"]{{child_shortcode}}[/our_clients]',
	'popup_title' => __('Insert Tab Shortcode', 'the-event'),

	'child_shortcode' => array(
		'params' => array(
		
			'image' => array(
				'type' => 'uploader',
				'label' => __('client Image', 'the-event'),
				'desc' => 'Clicking this image will show lightbox'
			),
			'link' => array(
				'std' => 'Link',
				'type' => 'text',
				'label' => __('Add link for client', 'the-event'),
				'desc' => __('link for client', 'the-event'),
			)
		),
		'shortcode' => '[client link="{{link}}" image="{{image}}" ][/client]',
		'clone_button' => __('Add Client', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	Social Network Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['list_items'] = array(
	'no_preview' => true,
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Social Network Class', 'the-event'),
			'desc' => __('Add social network class here.', 'the-event')
		),
		'align' => array(
			'type' => 'select',
			'label' => __('Alignment', 'the-event'),
			'desc' => 'Select alignment of call to action text.',
			'options' => array(
				'left' => 'Left',
				'right' => 'Right',
				'center' => 'Center'
			)
		),
	),
	'shortcode' => '[list_items class="{{class}}" align={{align}}]{{child_shortcode}}[/list_items]',
	'popup_title' => __( 'List Items Shortcode', 'the-event' ),
	'child_shortcode' => array(
		'params' => array(
			'icon' => array(
				'type' => 'iconpicker',
				'label' => __('Select Icon', 'the-event'),
				'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
				'options' => $icons
			),
			'title' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Title ', 'the-event'),
				'desc' => __('Add title here.', 'the-event')
			),
			'caption' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Caption ', 'the-event'),
				'desc' => __('Add title here.', 'the-event')
			),
			'link' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Link ', 'the-event'),
				'desc' => __('Add external Link here.', 'the-event')
			),
			'color' => array(
				'type' => 'colorpicker',
				'std' => '',
				'label' => __('Button Text color', 'the-event'),
				'desc' => 'Leave blank for default'
			),
		),
		'shortcode' => '[list_item icon="{{icon}}" title="{{title}}" link="{{link}}" caption="{{caption}}" color="{{color}}" ][/list_item]',
		'clone_button' => __('Add Item', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	Social Network Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['social_profile'] = array(
	'no_preview' => true,
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Social Network Class', 'the-event'),
			'desc' => __('Add social network class here.', 'the-event')
		),		
	),
	'shortcode' => '[social_profile class="{{class}}"]{{child_shortcode}}[/social_profile]',
	'popup_title' => __( 'Social Profile Shortcode', 'the-event' ),
	'child_shortcode' => array(
		'params' => array(
			'class' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Class ', 'the-event'),
				'desc' => __('Add class here.', 'the-event')
			),
			'icon' => array(
				'type' => 'iconpicker',
				'label' => __('Select Icon', 'the-event'),
				'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
				'options' => $icons
			),
			'amount' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Number of Followers or Fans ', 'the-event'),
				'desc' => __('Number of Followers or Fans.', 'the-event')
			),
			'amount_text' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Followers Text ', 'the-event'),
				'desc' => __('Add follower text which will be shown beneath number of followers.', 'the-event')
			),
			'link' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Link ', 'the-event'),
				'desc' => __('Add external Link here.', 'the-event')
			),
			'link_text' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Link Text ', 'the-event'),
				'desc' => __('Add external Link text here.', 'the-event')
			),
		),
		'shortcode' => '[social_profile_item class="{{class}}" icon="{{icon}}" amount="{{amount}}" amount_text="{{amount_text}}" link="{{link}}" link_text="{{link_text}}" ][/social_profile_item]',
		'clone_button' => __('Add Profile Item', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	FontAwesome Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['fontawesome'] = array(
	'no_preview' => true,
	'params' => array(

		'icon' => array(
			'type' => 'iconpicker',
			'label' => __('Select Icon', 'the-event'),
			'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),
		'circle' => array(
			'type' => 'select',
			'label' => __('Icon in Circle', 'the-event'),
			'desc' => 'Choose to display the icon in a circle',
			'options' => $choices
		),
		'size' => array(
			'type' => 'select',
			'label' => __('Size of Icon', 'the-event'),
			'desc' => 'Select the size of the icon',
			'options' => array(
				'large' => 'Large',
				'medium' => 'Medium',
				'small' => 'Small'
			)
		),
		'iconcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Icon Color', 'the-event'),
			'desc' => __('Leave blank for default', 'the-event')
		),
		'circlecolor' => array(
			'type' => 'colorpicker',
			'label' => __('Icon Circle Background Color', 'the-event'),
			'desc' => __('Leave blank for default', 'the-event')
		),
		'circlebordercolor' => array(
			'type' => 'colorpicker',
			'label' => __('Icon Circle Border Color', 'the-event'),
			'desc' => __('Leave blank for default', 'the-event')
		),
	),
	'shortcode' => '[fontawesome icon="{{icon}}" circle="{{circle}}" size="{{size}}" iconcolor="{{iconcolor}}" circlecolor="{{circlecolor}}" circlebordercolor="{{circlebordercolor}}"]',
	'popup_title' => __( 'FontAwesome Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Fullwidth Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['fullwidth'] = array(
	'no_preview' => true,
	'params' => array(
		'backgroundcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Background Color', 'the-event'),
			'desc' => __('Leave blank for default', 'the-event')
		),
		'backgroundimage' => array(
			'type' => 'uploader',
			'label' => __('Backgrond Image', 'the-event'),
			'desc' => 'Upload an image to display in the background'
		),
		'backgroundrepeat' => array(
			'type' => 'select',
			'label' => __('Background Repeat', 'the-event'),
			'desc' => 'Choose how the background image repeats.',
			'options' => array(
				'no-repeat' => 'No Repeat',
				'repeat' => 'Repeat Vertically and Horizontally',
				'repeat-x' => 'Repeat Horizontally',
				'repeat-y' => 'Repeat Vertically'
			)
		),
		'backgroundposition' => array(
			'type' => 'select',
			'label' => __('Background Position', 'the-event'),
			'desc' => 'Choose the postion of the background image',
			'options' => array(
				'left top' => 'Left Top',
				'left center' => 'Left Center',
				'left bottom' => 'Left Bottom',
				'right top' => 'Right Top',
				'right center' => 'Right Center',
				'right bottom' => 'Right Bottom',
				'center top' => 'Center Top',
				'center center' => 'Center Center',
				'center bottom' => 'Center Bottom'
			)
		),
		'backgroundattachment' => array(
			'type' => 'select',
			'label' => __('Background Scroll', 'the-event'),
			'desc' => 'Choose how the background image scrolls',
			'options' => array(
				'scroll' => 'Scroll: background scrolls along with the element',
				'fixed' => 'Fixed: background is fixed giving a parallax effect',
				'local' => 'Local: background scrolls along with the element\'s contents'
			)
		),
		'paddingtop' => array(
			'std' => 20,
			'type' => 'select',
			'label' => __( 'Padding Top', 'the-event' ),
			'desc' => __( 'In pixels', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		'paddingbottom' => array(
			'std' => 20,
			'type' => 'select',
			'label' => __( 'Padding Bottom', 'the-event' ),
			'desc' => __( 'In pixels', 'the-event' ),
			'options' => kodeforest_shortcodes_range( 100, false )
		),
		'content' => array(
			'std' => 'Your Content Goes Here',
			'type' => 'textarea',
			'label' => __( 'Content', 'the-event' ),
			'desc' => __( 'Add content', 'the-event' ),
		),
	),
	'shortcode' => '[fullwidth backgroundcolor="{{backgroundcolor}}" backgroundimage="{{backgroundimage}}" backgroundrepeat="{{backgroundrepeat}}" backgroundposition="{{backgroundposition}}" backgroundattachment="{{backgroundattachment}}" paddingTop="{{paddingtop}}px" paddingBottom="{{paddingbottom}}px"]{{content}}[/fullwidth]',
	'popup_title' => __( 'Fullwidth Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Google Map Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['googlemap'] = array(
	'no_preview' => true,
	'params' => array(

		'type' => array(
			'type' => 'select',
			'label' => __('Map Type', 'the-event'),
			'desc' => __('Select the type of google map to display', 'the-event'),
			'options' => array(
				'roadmap' => 'Roadmap',
				'satellite' => 'Satellite',
				'hybrid' => 'Hybrid',
				'terrain' => 'Terrain'
			)
		),
		'width' => array(
			'std' => '100%',
			'type' => 'text',
			'label' => __('Map Width', 'the-event'),
			'desc' => __('Map Width in Percentage or Pixels', 'the-event')
		),
		'height' => array(
			'std' => '300px',
			'type' => 'text',
			'label' => __('Map Height', 'the-event'),
			'desc' => __('Map Height in Percentage or Pixels', 'the-event')
		),
		'zoom' => array(
			'std' => 14,
			'type' => 'select',
			'label' => __('Zoom Level', 'the-event'),
			'desc' => 'Higher number will be more zoomed in.',
			'options' => kodeforest_shortcodes_range( 25, false )
		),
		'scrollwheel' => array(
			'type' => 'select',
			'label' => __('Scrollwheel on Map', 'the-event'),
			'desc' => 'Enable zooming using a mouse\'s scroll wheel',
			'options' => $choices
		),
		'scale' => array(
			'type' => 'select',
			'label' => __('Show Scale Control on Map', 'the-event'),
			'desc' => 'Display the map scale',
			'options' => $choices
		),
		'zoom_pancontrol' => array(
			'type' => 'select',
			'label' => __('Show Pan Control on Map', 'the-event'),
			'desc' => 'Displays pan control button',
			'options' => $choices
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __( 'Address', 'the-event' ),
			'desc' => __( 'Add address to the location which will show up on map. For multiple addresses, separate addresses by using the | symbol. <br />ex: Address 1|Address 2|Address 3', 'the-event' ),
		)
	),
	'shortcode' => '[map address="{{content}}" type="{{type}}" width="{{width}}" height="{{height}}" zoom="{{zoom}}" scrollwheel="{{scrollwheel}}" scale="{{scale}}" zoom_pancontrol="{{zoom_pancontrol}}"][/map]',
	'popup_title' => __( 'Google Map Shortcode', 'the-event' ),
);

/*-----------------------------------------------------------------------------------*/
/*	Donation Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['kode_donation'] = array(
	'no_preview' => true,
	'params' => array(

		'email' => array(
			'std' => 'info@example.com',
			'type' => 'text',
			'label' => __('Email', 'the-event'),
			'desc' => __('Add paypal email where you want to receive donation.', 'the-event')
		),
		'notify' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Process Page URL', 'the-event'),
			'desc' => __('Process page URL where you want to store any value to database.', 'the-event')
		),
		'success' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Thank you page', 'the-event'),
			'desc' => 'Thank you page or sucess page.',			
		)
	),
	'shortcode' => '[kode_donation email="{{email}}" notify="{{notify}}" success="{{success}}" ][/kode_donation]',
	'popup_title' => __( 'Donation Shortcode', 'the-event' ),
);
/*-----------------------------------------------------------------------------------*/
/*	Highlight Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['highlight'] = array(
	'no_preview' => true,
	'params' => array(

		'color' => array(
			'type' => 'colorpicker',
			'label' => __('Highlight Text Color', 'the-event'),
			'desc' => __('Pick a highlight color', 'the-event')
		),
		'bg_color' => array(
			'type' => 'colorpicker',
			'label' => __('Highlight Background Color', 'the-event'),
			'desc' => __('Pick a highlight color', 'the-event')
		),
		
		'content' => array(
			'std' => 'Your Content Goes Here',
			'type' => 'textarea',
			'label' => __( 'Content to Higlight', 'the-event' ),
			'desc' => __( 'Add your content to be highlighted', 'the-event' ),
		)

	),
	'shortcode' => '[highlight color="{{color}}" bg_color="{{bg_color}}"]{{content}}[/highlight]',
	'popup_title' => __( 'Highlight Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Lightbox Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['lightbox'] = array(
	'no_preview' => true,
	'params' => array(

		'full_image' => array(
			'type' => 'uploader',
			'label' => __('Full Image', 'the-event'),
			'desc' => 'Upload an image that will show up in the lightbox'
		),
		'thumb_image' => array(
			'type' => 'uploader',
			'label' => __('Thumbnail Image', 'the-event'),
			'desc' => 'Clicking this image will show lightbox'
		),
		'alt' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Alt Text', 'the-event'),
			'desc' => 'The alt attribute provides alternative information if an image cannot be viewed'
		),
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Lightbox Description', 'the-event'),
			'desc' => 'This will show up in the lightbox as a description below the image'
		),
	),
	'shortcode' => '&lt;a title="{{title}}" href="{{full_image}}" rel="prettyPhoto"&gt;&lt;img alt="{{alt}}" src="{{thumb_image}}" /&gt;&lt;/a&gt;',
	'popup_title' => __( 'Lightbox Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Team/Persons Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['member'] = array(
	'no_preview' => true,
	'params' => array(

		'picture' => array(
			'type' => 'uploader',
			'label' => __('Picture', 'the-event'),
			'desc' => 'Upload an image to display'
		),
		'pic_link' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Picture Link URL', 'the-event'),
			'desc' => 'Add the URL the picture will link to, ex: http://example.com'
		),
		'name' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Name', 'the-event'),
			'desc' => 'Insert the name of the person'
		),
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title', 'the-event'),
			'desc' => 'Insert the title of the person'
		),
		'email' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Email Address', 'the-event'),
			'desc' => 'Insert an email address to display the email icon'
		),
		'facebook' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Facebook Profile Link', 'the-event'),
			'desc' => 'Insert a url to display the facebook icon'
		),
		'twitter' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Twitter Profile Link', 'the-event'),
			'desc' => 'Insert a url to display the twitter icon'
		),
		'linkedin' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('LinkedIn Profile Link', 'the-event'),
			'desc' => 'Insert a url to display the linkedin icon'
		),
		'dribbble' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Dribbble Profile Link', 'the-event'),
			'desc' => 'Insert a url to display the dribbble icon'
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Link Target', 'the-event'),
			'desc' => __('_self = open in same window <br /> _blank = open in new window', 'the-event'),
			'options' => array(
				'_self' => '_self',
				'_blank' => '_blank'
			)
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Profile Description', 'the-event'),
			'desc' => 'Enter the content to be displayed'
		),
	),
	'shortcode' => '[member name="{{name}}" picture="{{picture}}" pic_link="{{pic_link}}" title="{{title}}" email="{{email}}" facebook="{{facebook}}" twitter="{{twitter}}" linkedin="{{linkedin}}" dribbble="{{dribbble}}" linktarget="{{target}}"]{{content}}[/member]',
	'popup_title' => __( 'Member Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Separator Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['separator'] = array(
	'no_preview' => true,
	'params' => array(

		'style' => array(
			'type' => 'select',
			'label' => __( 'Style', 'the-event' ),
			'desc' => 'Choose the separator line style',
			'options' => array(
				'none' => 'No Style',
				'single' => 'Single Border',
				'double' => 'Double Border',
				'dashed' => 'Dashed Border',
				'dotted' => 'Dotted Border',
				'shadow' => 'Shadow'
			)
		),
		'top' => array(
			'std' => 40,
			'type' => 'select',
			'label' => __('Margin Top', 'the-event'),
			'desc' => 'Spacing above the separator',
			'options' => kodeforest_shortcodes_range( 100, false, false, 0 )
		),
		'bottom' => array(
			'std' => 40,
			'type' => 'select',
			'label' => __('Margin Bottom', 'the-event'),
			'desc' => 'Spacing below the separator',
			'options' => kodeforest_shortcodes_range( 100, false, false, 0 )
		)
	),
	'shortcode' => '[separator top="{{top}}" bottom="{{bottom}}" style="{{style}}"]',
	'popup_title' => __( 'Separator Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Sharing Box Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['sharingbox'] = array(
	'no_preview' => true,
	'params' => array(

		'tagline' => array(
			'std' => 'Share This Story, Choose Your Platform!',
			'type' => 'text',
			'label' => __('Tagline', 'the-event'),
			'desc' => 'The title tagline that will display'
		),
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title', 'the-event'),
			'desc' => 'The post title that will be shared'
		),
		'link' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Link', 'the-event'),
			'desc' => 'The link that will be shared'
		),
		'description' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Description', 'the-event'),
			'desc' => 'The description that will be shared'
		),
		'link' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Link to Share', 'the-event'),
			'desc' => ''
		),
		'pinterest_image' => array(
			'std' => '',
			'type' => 'uploader',
			'label' => __('Choose Image to Share on Pinterest', 'the-event'),
			'desc' => ''
		),
		'backgroundcolor' => array(
			'type' => 'colorpicker',
			'label' => __('Background Color', 'the-event'),
			'desc' => __('Leave blank for default color', 'the-event')
		),
	),
	'shortcode' => '[sharing tagline="{{tagline}}" title="{{title}}" link="{{link}}" description="{{description}}" pinterest_image="{{pinterest_image}}" backgroundcolor="{{backgroundcolor}}"][/sharing]',
	'popup_title' => __( 'Sharing Box Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	SoundCloud Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['soundcloud'] = array(
	'no_preview' => true,
	'params' => array(

		'url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('SoundCloud Url', 'the-event'),
			'desc' => 'The SoundCloud url, ex: http://api.soundcloud.com/tracks/110813479'
		),
		'comments' => array(
			'type' => 'select',
			'label' => __('Show Comments', 'the-event'),
			'desc' => 'Choose to display comments',
			'options' => $choices
		),
		'auto_play' => array(
			'type' => 'select',
			'label' => __('Autoplay', 'the-event'),
			'desc' => 'Choose to autoplay the track',
			'options' => $reverse_choices
		),
		'color' => array(
			'type' => 'colorpicker',
			'std' => '#ff7700',
			'label' => __('Color', 'the-event'),
			'desc' => 'Select the color of the shortcode'
		),
		'width' => array(
			'std' => '100%',
			'type' => 'text',
			'label' => __('Width', 'the-event'),
			'desc' => 'In pixels (px) or percentage (%)'
		),
		'height' => array(
			'std' => '81px',
			'type' => 'text',
			'label' => __('Height', 'the-event'),
			'desc' => 'In pixels (px)'
		),
	),
	'shortcode' => '[soundcloud url="{{url}}" comments="{{comments}}" auto_play="{{auto_play}}" color="{{color}}" width="{{width}}" height="{{height}}"]',
	'popup_title' => __( 'Sharing Box Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Social Links Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['sociallinks'] = array(
	'no_preview' => true,
	'params' => array(

		'colorscheme' => array(
			'type' => 'select',
			'label' => __('Color Scheme', 'the-event'),
			'desc' => 'Choose the color scheme for the social links',
			'options' => array(
				'' => 'Default',
				'light' => 'Light',
				'dark' => 'Dark'
			)
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Link Target', 'the-event'),
			'desc' => __('_self = open in same window <br />_blank = open in new window', 'the-event'),
			'options' => array(
				'_self' => '_self',
				'_blank' => '_blank'
			)
		),
		'rss' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('RSS Link', 'the-event'),
			'desc' => 'Insert your custom RSS link'
		),
		'facebook' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Facebook Link', 'the-event'),
			'desc' => 'Insert your custom Facebook link'
		),
		'twitter' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Twitter Link', 'the-event'),
			'desc' => 'Insert your custom Twitter link'
		),
		'dribbble' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Dribbble Link', 'the-event'),
			'desc' => 'Insert your custom Dribbble link'
		),
		'google' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Google+ Link', 'the-event'),
			'desc' => 'Insert your custom Google+ link'
		),
		'linkedin' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('LinkedIn Link', 'the-event'),
			'desc' => 'Insert your custom LinkedIn link'
		),
		'blogger' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Blogger Link', 'the-event'),
			'desc' => 'Insert your custom Blogger link'
		),
		'tumblr' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Tumblr Link', 'the-event'),
			'desc' => 'Insert your custom Tumblr link'
		),
		'reddit' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Reddit Link', 'the-event'),
			'desc' => 'Insert your custom Reddit link'
		),
		'yahoo' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Yahoo Link', 'the-event'),
			'desc' => 'Insert your custom Yahoo link'
		),
		'deviantart' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Deviantart Link', 'the-event'),
			'desc' => 'Insert your custom Deviantart link'
		),
		'vimeo' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Vimeo Link', 'the-event'),
			'desc' => 'Insert your custom Vimeo link'
		),
		'youtube' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Youtube Link', 'the-event'),
			'desc' => 'Insert your custom Youtube link'
		),
		'pinterest' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Pinterst Link', 'the-event'),
			'desc' => 'Insert your custom Pinterest link'
		),
		'digg' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Digg Link', 'the-event'),
			'desc' => 'Insert your custom Digg link'
		),
		'flickr' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Flickr Link', 'the-event'),
			'desc' => 'Insert your custom Flickr link'
		),
		'forrst' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Forrst Link', 'the-event'),
			'desc' => 'Insert your custom Forrst link'
		),
		'myspace' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Myspace Link', 'the-event'),
			'desc' => 'Insert your custom Myspace link'
		),
		'skype' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Skype Link', 'the-event'),
			'desc' => 'Insert your custom Skype link'
		),
		'show_custom' => array(
			'type' => 'select',
			'label' => __('Show Custom Social Icon', 'the-event'),
			'desc' => __('Show the custom social icon specified in Theme Options', 'the-event'),
			'options' => $reverse_choices
		),
	),
	'shortcode' => '[social_links colorscheme="{{colorscheme}}" linktarget="{{target}}" rss="{{rss}}" facebook="{{facebook}}" twitter="{{twitter}}" dribbble="{{dribbble}}" google="{{google}}" linkedin="{{linkedin}}" blogger="{{blogger}}" tumblr="{{tumblr}}" reddit="{{reddit}}" yahoo="{{yahoo}}" deviantart="{{deviantart}}" vimeo="{{vimeo}}" youtube="{{youtube}}" pinterest="{{pinterest}}" digg="{{digg}}" flickr="{{flickr}}" forrst="{{forrst}}" myspace="{{myspace}}" skype="{{skype}}" show_custom="{{show_custom}}"]',
	'popup_title' => __( 'Social Links Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Tabs Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['our_clients'] = array(
	'params' => array(

		
	),
	'no_preview' => true,
	'shortcode' => '[our_clients]{{child_shortcode}}[/our_clients]',
	'popup_title' => __('Insert Tab Shortcode', 'the-event'),

	'child_shortcode' => array(
		'params' => array(
		
			'image' => array(
				'type' => 'uploader',
				'label' => __('client Image', 'the-event'),
				'desc' => 'Clicking this image will show lightbox'
			),
			'link' => array(
				'std' => 'Link',
				'type' => 'text',
				'label' => __('Add link for client', 'the-event'),
				'desc' => __('link for client', 'the-event'),
			)
		),
		'shortcode' => '[client link="{{link}}" image="{{image}}"][/client]',
		'clone_button' => __('Add Client', 'the-event')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	Tabs Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['kode_image_gallery'] = array(
	'params' => array(
		'gallery_col' => array(
			'type' => 'select',
			'label' => __('Gallery Column', 'the-event'),
			'desc' => __('Select gallery column to show it in grid view.', 'the-event'),
			'options' => array(
				'2' => '2 Column',
				'3' => '3 Column',
				'4' => '4 Column'
			)
		),
	),
	'no_preview' => true,
	'shortcode' => '[kode_image_gallery]{{child_shortcode}}[/kode_image_gallery]',
	'popup_title' => __('Insert Tab Shortcode', 'the-event'),

	'child_shortcode' => array(
		'params' => array(
		
			'image' => array(
				'type' => 'uploader',
				'label' => __('client Image', 'the-event'),
				'desc' => 'Clicking this image will show lightbox'
			),
			'link' => array(
				'std' => 'Link',
				'type' => 'text',
				'label' => __('Add link for client', 'the-event'),
				'desc' => __('link for client', 'the-event'),
			)
		),
		'shortcode' => '[gallery_item link="{{link}}" image="{{image}}"][/gallery_item]',
		'clone_button' => __('Add Gallery', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	Tabs Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['tabs'] = array(
	'params' => array(

		'layout' => array(
			'type' => 'select',
			'label' => __('Layout', 'the-event'),
			'desc' => 'Choose the layout of the shortcode',
			'options' => array(
				'horizontal' => 'Horizontal',
				'vertical' => 'Vertical'
			)
		),
		'backgroundcolor' => array(
			'type' => 'colorpicker',
			'std' => '',
			'label' => __('Background Color', 'the-event'),
			'desc' => 'Leave blank for default'
		),
		'inactivecolor' => array(
			'type' => 'colorpicker',
			'std' => '',
			'label' => __('Inactive Color', 'the-event'),
			'desc' => 'Leave blank for default'
		),
	),
	'no_preview' => true,
	'shortcode' => '[tabs layout="{{layout}}" backgroundcolor="{{backgroundcolor}}" inactivecolor="{{inactivecolor}}"]{{child_shortcode}}[/tabs]',
	'popup_title' => __('Insert Tab Shortcode', 'the-event'),

	'child_shortcode' => array(
		'params' => array(
			'title' => array(
				'std' => 'Title',
				'type' => 'text',
				'label' => __('Tab Title', 'the-event'),
				'desc' => __('Title of the tab', 'the-event'),
			),
			'content' => array(
				'std' => 'Tab Content',
				'type' => 'textarea',
				'label' => __('Tab Content', 'the-event'),
				'desc' => __('Add the tabs content', 'the-event')
			)
		),
		'shortcode' => '[tab title="{{title}}"]{{content}}[/tab]',
		'clone_button' => __('Add Tab', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	Title Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['title'] = array(
	'no_preview' => true,
	'params' => array(

		'size' => array(
			'type' => 'select',
			'label' => __('Title Size', 'the-event'),
			'desc' => 'Choose the title size, H1-H6',
			'options' => kodeforest_shortcodes_range( 6, false )
		),
		'align' => array(
			'type' => 'select',
			'label' => __('Select Alignment', 'the-event'),
			'desc' => 'Choose the alinment of the text shortcode',
			'options' => array(
				'left' => 'left',
				'right' => 'right',
				'center' => 'center',
				'none' => 'none'
			)
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Title', 'the-event'),
			'desc' => 'Insert the title text'
		),
	),
	'shortcode' => '[title align="{{align}}" size="{{size}}"]{{content}}[/title]',
	'popup_title' => __( 'Sharing Box Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Toggles Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['accordion'] = array(
	'params' => array(

	),
	'no_preview' => true,
	'shortcode' => '[accordian]{{child_shortcode}}[/accordian]',
	'popup_title' => __('Insert Toggles Shortcode', 'the-event'),

	'child_shortcode' => array(
		'params' => array(
			'title' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Title', 'the-event'),
				'desc' => 'Insert the accordion title',
			),
			'open' => array(
				'type' => 'select',
				'label' => __('Open by Default', 'the-event'),
				'desc' => 'Choose to have the accordion open when page loads',
				'options' => $reverse_choices
			),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => __('Accordion Content', 'the-event'),
				'desc' => 'Insert the accordion content'
			)
		),
		'shortcode' => '[toggle title="{{title}}" open="{{open}}"]{{content}}[/toggle]',
		'clone_button' => __('Add Accordion', 'the-event')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	Tooltip Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['tooltip'] = array(
	'no_preview' => true,
	'params' => array(

		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Tooltip Text', 'the-event'),
			'desc' => 'Insert the text that displays in the tooltip'
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Content', 'the-event'),
			'desc' => 'Insert the text that will activate the tooltip hover'
		),
	),
	'shortcode' => '[tooltip title="{{title}}"]{{content}}[/tooltip]',
	'popup_title' => __( 'Tooltip Shortcode', 'the-event' )
);

/*-----------------------------------------------------------------------------------*/
/*	Vimeo Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['vimeo'] = array(
	'no_preview' => true,
	'params' => array(

		'id' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Video ID', 'the-event'),
			'desc' => 'For example the Video ID for <br />https://vimeo.com/75230326 is 75230326'
		),
		'width' => array(
			'std' => '600',
			'type' => 'text',
			'label' => __('Width', 'the-event'),
			'desc' => 'In pixels but only enter a number, ex: 600'
		),
		'height' => array(
			'std' => '350',
			'type' => 'text',
			'label' => __('Height', 'the-event'),
			'desc' => 'In pixels but enter a number, ex: 350'
		),
		'autoplay' => array(
			'type' => 'select',
			'label' => __( 'Autoplay Video', 'the-event' ),
			'desc' =>  __( 'Set to yes to make video autoplaying', 'the-event' ),
			'options' => $reverse_choices
		),
		'apiparams' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('AdditionalAPI Parameter', 'the-event'),
			'desc' => 'Use additional API parameter, for example &title=0 to disable title on video. VimeoPlus account may be required.'
		),
	),
	'shortcode' => '[vimeo id="{{id}}" width="{{width}}" height="{{height}}" autoplay="{{autoplay}}" api_params="{{apiparams}}"]',
	'popup_title' => __( 'Vimeo Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Youtube Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['youtube'] = array(
	'no_preview' => true,
	'params' => array(

		'id' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Video ID', 'the-event'),
			'desc' => 'For example the Video ID for <br />http://www.youtube.com/LOfeCR7KqUs is LOfeCR7KqUs'
		),
		'width' => array(
			'std' => '600',
			'type' => 'text',
			'label' => __('Width', 'the-event'),
			'desc' => 'In pixels but only enter a number, ex: 600'
		),
		'height' => array(
			'std' => '350',
			'type' => 'text',
			'label' => __('Height', 'the-event'),
			'desc' => 'In pixels but only enter a number, ex: 350'
		),
		'autoplay' => array(
			'type' => 'select',
			'label' => __( 'Autoplay Video', 'the-event' ),
			'desc' =>  __( 'Set to yes to make video autoplaying', 'the-event' ),
			'options' => $reverse_choices
		),
		'apiparams' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('AdditionalAPI Parameter', 'the-event'),
			'desc' => 'Use additional API parameter, for example &rel=0 to disable related videos'
		),
	),
	'shortcode' => '[youtube id="{{id}}" width="{{width}}" height="{{height}}" autoplay="{{autoplay}}" api_params="{{apiparams}}"]',
	'popup_title' => __( 'Vimeo Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	newsletter Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['newsletter'] = array(
	'no_preview' => true,
	'params' => array(
		
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title', 'the-event'),
			'desc' => 'Add title here.'
		),
		'caption' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('caption', 'the-event'),
			'desc' => 'Add caption here.'
		),
		'email' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Email ID', 'the-event'),
			'desc' => 'Add Email ID in order to receive subscribers.'
		),
		'layout' => array(
			'type' => 'select',
			'label' => __('Layout', 'the-event'),
			'desc' => __('Select the newsletter layout.', 'the-event'),
			'options' => array(
				'style-1' => 'Style 1',
				'style-2' => 'Style 2',
			)
		),
	),
	'shortcode' => '[newsletter title="{{title}}" caption="{{caption}}" email="{{email}}" layout={{layout}}]',
	'popup_title' => __( 'Newsletter Shortcode', 'the-event' )
);


/*-----------------------------------------------------------------------------------*/
/*	Columns Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['columns'] = array(
	'shortcode' => ' {{child_shortcode}} ', // as there is no wrapper shortcode
	'popup_title' => __('Insert Columns Shortcode', 'the-event'),
	'no_preview' => true,
	'params' => array(),

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'column' => array(
				'type' => 'select',
				'label' => __('Column Type', 'the-event'),
				'desc' => __('Select the width of the column', 'the-event'),
				'options' => array(
					'one_third' => 'One Third',
					'two_third' => 'Two Thirds',
					'one_half' => 'One Half',
					'one_fourth' => 'One Fourth',
					'three_fourth' => 'Three Fourth',
				)
			),
			'last' => array(
				'type' => 'select',
				'label' => __('Last Column', 'the-event'),
				'desc' => 'Choose if the column is last in a set. This has to be set to "Yes" for the last column in a set',
				'options' => $reverse_choices
			),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => __('Column Content', 'the-event'),
				'desc' => __('Insert the column content', 'the-event'),
			)
		),
		'shortcode' => '[{{column}} last="{{last}}"]{{content}}[/{{column}}] ',
		'clone_button' => __('Add Column', 'the-event')
	)
);
/*-----------------------------------------------------------------------------------*/
/*	Project Facts Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['facts'] = array(
	'no_preview' => true,
	'params' => array(
		'type' => array(
			'type' => 'select',
			'label' => __( 'Fact Style', 'the-event' ),
			'desc' => __( 'Select the type of facts', 'the-event' ),
			'options' => array(
				'style-1' => 'Style 1',
				'style-2' => 'Style 2',
				'style-3' => 'Style 3',
			)
		),
		'icon' => array(
			'type' => 'iconpicker',
			'label' => esc_attr__('Select Icon', 'the-event'),
			'desc' => esc_attr__('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),	
		'color' => array(
			'type' => 'colorpicker',
			'label' => esc_attr__('Select Color', 'the-event'),
			'desc' => esc_attr__('Set the fact color.', 'the-event')
		),
		'value' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_attr__('Value', 'the-event'),
			'desc' => esc_attr__('Add Value here', 'the-event')
		),
		'sub_text' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_attr__('Sub Text', 'the-event'),
			'desc' => esc_attr__('Add Sub Text here', 'the-event')
		)
	),
	'shortcode' => '[facts type="{{type}}" icon="{{icon}}" color="{{color}}" value="{{value}}" sub_text="{{sub_text}}" ][/facts]',
	'popup_title' => esc_attr__( 'Project Facts Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Services Config
/*-----------------------------------------------------------------------------------*/
$kodeforest_shortcodes['services'] = array(
	'no_preview' => true,
	'params' => array(
		'type' => array(
			'type' => 'select',
			'label' => __( 'Services Style', 'the-event' ),
			'desc' => __( 'Select the type of services element', 'the-event' ),
			'options' => array(
				'style-1' => 'Style 1',
				'style-2' => 'Style 2',				
				'style-3' => 'Style 3',	
				'style-4' => 'Style 4',	
				'style-5' => 'Style 5',	
			)
		),
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title', 'the-event'),
			'desc' => __('Add title here', 'the-event')
		),
		'image' => array(
			'type' => 'uploader',
			'label' => __('Upload Image', 'the-event'),
			'desc' => 'Clicking this image will show beside box'
		),
		'icon' => array(
			'type' => 'iconpicker',
			'label' => __('Select Icon', 'the-event'),
			'desc' => __('Click an icon to select, click again to deselect', 'the-event'),
			'options' => $icons
		),
		'content' => array(
			'std' => 'Your Content Goes Here',
			'type' => 'textarea',
			'label' => __( 'Alert Content', 'the-event' ),
			'desc' => __( 'Insert the alert\'s content', 'the-event' ),
		),
		'button_text' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button Text', 'the-event'),
			'desc' => __('Add Button Text here', 'the-event')
		),
		'button_link' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button Link', 'the-event'),
			'desc' => __('Add Button Link here', 'the-event')
		),
		
	),
	'shortcode' => '[services type="{{type}}" title="{{title}}" image="{{image}}" icon="{{icon}}" button_text="{{button_text}}" button_link="{{button_link}}" ]{{content}}[/services]',
	'popup_title' => __( 'Services Shortcode', 'the-event' )
);


/*-----------------------------------------------------------------------------------*/
/*	Table Config
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['table'] = array(
	'no_preview' => true,
	'params' => array(

		'type' => array(
			'type' => 'select',
			'label' => __('Type', 'the-event'),
			'desc' => __('Select the table style', 'the-event'),
			'options' => array(
				'1' => 'Style 1',
				'2' => 'Style 2',
			)
		),
		'columns' => array(
			'type' => 'select',
			'label' => __('Number of Columns', 'the-event'),
			'desc' => 'Select how many columns to display',
			'options' => array(
				'1' => '1 Column',
				'2' => '2 Columns',
				'3' => '3 Columns',
				'4' => '4 Columns'
			)
		)
	),
	'shortcode' => '',
	'popup_title' => __( 'Table Shortcode', 'the-event' )
);
/*-----------------------------------------------------------------------------------*/
/*	Price Table
/*-----------------------------------------------------------------------------------*/

$kodeforest_shortcodes['price_table'] = array(
	'params' => array(
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Title', 'kf_petcare'),
			'desc' => 'Insert the price table title',
		),
		'sub_title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Sub Title', 'kf_petcare'),
			'desc' => 'Insert the price table sub title',
		),
		'price' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Price', 'kf_petcare'),
			'desc' => 'Insert the price table price',
		),
		'duration' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Duration', 'kf_petcare'),
			'desc' => 'Insert the price table duration',
		),
		'button' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button Text', 'kf_petcare'),
			'desc' => 'Insert the price table button text',
		),
		'button_url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button URL', 'kf_petcare'),
			'desc' => 'Insert the price table button url',
		),
		
	),
	'no_preview' => true,
	'shortcode' => '[price_table title="{{title}}" sub_title="{{sub_title}}" price="{{price}}" duration="{{duration}}" button="{{button}}" button_url="{{button_url}}" ]{{child_shortcode}}[/price_table]',
	'popup_title' => __('Insert Price Table Shortcode', 'kf_petcare'),

	'child_shortcode' => array(
		'params' => array(
			'status' => array(
				'type' => 'select',
				'label' => __('List Status', 'kf_petcare'),
				'desc' => __('Select the List Status.', 'kf_petcare'),
				'options' => array(
					'enable' => 'Available',
					'disable' => 'Not Available',
				)
			),
			'link' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Add List Link', 'kf_petcare'),
				'desc' => 'Insert the price table items',
			),
			'title' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Add List Title', 'kf_petcare'),
				'desc' => 'Insert the price table items',
			),
		),
		'shortcode' => '[price_item link="{{link}}" title="{{title}}" status="{{status}}"][/price_item]',
		'clone_button' => __('Add Price Item', 'kf_petcare')
	)
);