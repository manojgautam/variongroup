<?php
function theevent_newsletter_mailchimp(){
		global $theevent_theme_option;
		$email = $_POST['email'];				
		require_once( KODEFOREST_TINYMCE_DIR . '/mailchimp/mailchimp.php' );		
		$Mailchimp = new Mailchimp( $theevent_theme_option['mail-chimp-api'] );
		if(isset($MailChimp->errorCode) && $MailChimp->errorCode == 214) {
			echo json_encode(array('success'=>false, 'message'=>esc_html__('You have already subscribed to the list.','kf_democracy')));
		}else{ 
			$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
			$subscriber = $Mailchimp_Lists->subscribe( $theevent_theme_option['mail-chimp-listid'], array( 'email' => htmlentities($email) ) );
			if ( ! empty( $subscriber['leid'] ) ) {
			   echo json_encode(array('success'=>false, 'message'=>esc_html__('Thank You For Your Subscription.','kf_democracy')));
			}else{
				echo json_encode(array('fail'=>false, 'message'=>esc_html__('Please try again back later.','kf_democracy')));
			}
		}

		die();
	}	
	
	function theevent_ajax_newsletter_mailchimp(){

		wp_register_script('theevent-news-ltr', THEEVENT_PATH.'/js/newsletter.js', array('jquery') ); 
		wp_enqueue_script('theevent-news-ltr');

		wp_localize_script( 'theevent-news-ltr', 'ajax_login_object', array( 'loadingmessage' => esc_html__('Sending user info, please wait...','kf_democracy')));

		// Enable the user with no privileges to run newsletter_mailchimp() in AJAX
		add_action( 'wp_ajax_nopriv_newsletter_mailchimp', 'theevent_newsletter_mailchimp' );
		add_action( 'wp_ajax_newsletter_mailchimp', 'theevent_newsletter_mailchimp' );
	}	
	
	// Execute the action only if the user isn't logged in
	add_action('init', 'theevent_ajax_newsletter_mailchimp');	